#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Amplitude calibration tasks (tsys, gain curve, accor, bandpass). """
import shutil
try:
    import astropy.io.fits as pyfits
except ModuleNotFoundError:
    import pyfits
import numpy as np
import pipe_modules.auxiliary                as auxiliary
import pipe_modules.calibration              as calibration
import pipe_modules.opacity_correction       as opacity_correction
import pipe_modules.JIVE_scripts.gc          as gc
import pipe_modules.JIVE_scripts.append_wx   as append_wx
import pipe_modules.JIVE_scripts.append_tsys as append_tsys
import pipe_modules.JIVE_scripts.importtsys  as importtsys
from pipe_modules.default_casa_imports import *


def remove_invalid_tsys(_inp_params):
    mytb = casac.table()
    invalid_tsys_identifiers = _inp_params.invalid_tsys_identifiers
    if not isinstance(invalid_tsys_identifiers, list):
        invalid_tsys_identifiers = [invalid_tsys_identifiers]
    syscal_tab = _inp_params.ms_name + '/SYSCAL'
    if not auxiliary.isdir(syscal_tab):
        return
    mytb.open(syscal_tab, nomodify=False)
    _nrows   = mytb.nrows()
    all_rows = range(_nrows)
    del_rows = []
    for _row in all_rows:
        these_tsys = mytb.getcell('TSYS', _row)
        if any(np.asarray(these_tsys)<0):
            del_rows.append(_row)
        for tsys_tobe_removed in invalid_tsys_identifiers:
            if all(np.asarray(these_tsys)==tsys_tobe_removed):
                del_rows.append(_row)
            else:
                pass
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def fill_tsys_scangaps(_inp_params, _ms_metadata):
    ms_tsys = _inp_params.ms_name + '/SYSCAL'
    if not auxiliary.isdir(ms_tsys):
        return
    mytb = casac.table()
    scan_bounds = {}
    scan_ants   = {}
    all_scans   = _ms_metadata.all_scans
    extrap      = {'TIME': [], 'ANTENNA_ID': [], 'SPECTRAL_WINDOW_ID': [], 'TSYS': []}
    mytb.open(ms_tsys)
    feedid = mytb.getcell('FEED_ID', 0)
    interv = mytb.getcell('INTERVAL', 0)
    spwids = np.unique(mytb.getcol('SPECTRAL_WINDOW_ID'))
    antids = np.unique(mytb.getcol('ANTENNA_ID'))
    mytb.done()
    for s in all_scans:
        scan_bounds[s] = _ms_metadata.yield_scantime(s, 'both')
        scan_ants[s]   = [_ms_metadata.yield_antID(a) for a in _ms_metadata.yield_antennas(s)]
    for spw in spwids:
        for ant in antids:
            qcrit = f'ANTENNA_ID=={ant} && SPECTRAL_WINDOW_ID=={spw}'
            ttime, ttsys = auxiliary.read_CASA_table(_inp_params, 'TIME,TSYS', qcrit, _tablename=ms_tsys)
            if not np.array(ttime).any():
                aname = _ms_metadata.yield_antname(ant)
                print(f'    No data available to extrapolate {aname} Tsys in spw {spw}')
                continue
            try:
                _ = ttsys[0][0]
                ttsys = np.swapaxes(ttsys, 0, 1)
            except IndexError:
                pass
            etimes     = []
            alltimes   = []
            multiplier = {}
            for s in scan_ants:
                if ant not in scan_ants[s]:
                    # No data for this scan.
                    continue
                if not np.any((ttime > scan_bounds[s][0]-1) & (ttime < scan_bounds[s][1]+1)):
                    if _inp_params.verbose:
                        aname = _ms_metadata.yield_antname(ant)
                        print(f'    Extrapolating missing {aname} Tsys for scan {s} in spw {spw}')
                    etime = np.mean(scan_bounds[s])
                    etimes.append(etime)
                    sstart = min(scan_bounds[s])
                    send   = max(scan_bounds[s])
                    diff   = send - sstart
                    if diff > 60:
                        incr = 30
                    else:
                        incr = max(int(diff/6), _ms_metadata.acp)
                    nextt   = sstart
                    counter = 0
                    while nextt < send:
                        alltimes.append(nextt)
                        nextt  += incr
                        counter+= 1
                    multiplier[etime] = counter
            if not etimes:
                continue
            prev_m = np.searchsorted(ttime,etimes,side='left')
            etsys  = []
            for i,pm in enumerate(prev_m):
                if pm == 0:
                    # Constant extrapolation from first measurement.
                    for _ in range(multiplier[etimes[i]]):
                        etsys.append(ttsys[pm])
                    continue
                try:
                    tsys1 = ttsys[pm]
                    time1 = ttime[pm]
                    tsys0 = ttsys[pm-1]
                    time0 = ttime[pm-1]
                    tintp = []
                    try:
                        for t0,t1 in zip(tsys0, tsys1):
                            tintp.append(np.interp(etimes[i], [time0,time1], [t0,t1]))
                    except TypeError:
                        tintp.append(np.interp(etimes[i], [time0,time1], [tsys0,tsys1]))
                    # Fill each time stamp in the scan with the same value
                    etsys.extend([np.array(tintp)]*multiplier[etimes[i]])
                except IndexError:
                    # Constant extrapolation from last measurement.
                    for _ in range(multiplier[etimes[i]]):
                        etsys.append(ttsys[pm-1])
            Next = len(alltimes)
            extrap['TIME'].extend(alltimes)
            extrap['ANTENNA_ID'].extend([ant]*Next)
            extrap['SPECTRAL_WINDOW_ID'].extend([spw]*Next)
            extrap['TSYS'].extend(etsys)
            x = 0
            for key in multiplier:
                x+= multiplier[key]
    N_addr = len(extrap['TIME'])
    if not N_addr:
        return
    try:
        _ = extrap['TSYS'][0][0]
        extrap['TSYS'] = np.swapaxes(extrap['TSYS'], 0, 1)
    except IndexError:
        pass
    mytb.open(ms_tsys, nomodify=False)
    nrows_orig = mytb.nrows()
    mytb.addrows(N_addr)
    mytb.putcol('FEED_ID', [feedid]*N_addr, startrow=nrows_orig)
    mytb.putcol('INTERVAL', [interv]*N_addr, startrow=nrows_orig)
    for colname in extrap:
        mytb.putcol(colname, extrap[colname], startrow=nrows_orig)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def attach_tsys_to_idi(_inp_params, antabtable, idifiles):
    """
    Attaches a SYSTEM_TEMPERATURE table to first fits-idi file from antabtable.
    Does noting if a SYSTEM_TEMPERATURE table is already attached.
    Needs all idi files loaded for time indices.
    Does nothing for the EHT and GMVA arrays where EHT_add_tsys_to_MS() is used instead.
    """
    if _inp_params.array_type not in ('EHT', 'EHTsb', 'GMVA', 'meqsil'):
        if auxiliary.isdir(_inp_params.ms_name) and not idifiles:
            print('\nI did not see any fits-idi files but a MS is already present.')
            print('Will assume that this MS already has a SYSTEM_TEMPERATURE column\n')
            return
        checkfile = pyfits.open(idifiles[0], ignore_missing_end=True)
        try:
            check = checkfile['SYSTEM_TEMPERATURE']
            print('\nA Tsys table is already attached to the (first) fits-idi file.\nNot updating with ANTAB Tsys this time.\n')
            checkfile.close()
        except KeyError:
            if antabtable:
                print('\nAttaching Tsys values from\n  '+antabtable+'\nto the (first) fits-idi file:\n  '+idifiles[0]+' ...')
                checkfile.close()
                append_tsys.append_tsys(antabtable, idifiles)
                print('Done\n')
            else:
                print('\nWarning: No valid ANTAB table found.\nAny amplitude calibration attempt will fail.\n')


def EHT_add_tsys_to_MS(_inp_params, antabtable):
    """
    EHT can be correlated with scattered fits-idi files.
    As not all antennas are present in each single file, the Tsys values will instead be appended to the MS directly.
    GMVA ANTAB tables may have errors, so these are also attached to the MS directly.
    """
    if _inp_params.array_type in ('EHT', 'EHTsb', 'GMVA', 'meqsil'):
        if auxiliary.isdir(_inp_params.ms_name+'/SYSCAL'):
            print('\nA system temperature table is already attached to the EHT MS.')
            print('Not updating with ANTAB Tsys this time.\n')
        elif antabtable and auxiliary.check_for_filecontent(antabtable):
            print('\nAttaching Tsys values from\n  '+antabtable+'\nto the MS...')
            importtsys.importtsys(antabtable, _inp_params.ms_name)
            print('Done\n')
        else:
            print('\nWarning: No valid ANTAB table found.\nAny amplitude calibration attempt will fail.\n')


def attach_wx_to_idi(_inp_params, wxfile, idifiles):
    """
    Attaches a WEATHER table to first fits-idi file from wxfile.
    Does noting if a WEATHER table is already attached.
    Needs all idi files loaded for time indices.
    """
    if auxiliary.isdir(_inp_params.ms_name) and not idifiles:
        print('\nI did not see any fits-idi files but a MS is already present.')
        print('Will assume that this MS already has a WEATHER column\n')
        return
    checkfile = pyfits.open(idifiles[0], ignore_missing_end=True)
    try:
        check = checkfile['WEATHER']
        print('\nA Weather table is already attached to the (first) fits-idi file.\nNot updating with WX data this time.\n')
        checkfile.close()
    except KeyError:
        if wxfile:
            print('\nAttaching Weather values from\n  ' + wxfile +'\nto the (first) fits-idi file:\n  ' + idifiles[0] + ' ...')
            checkfile.close()
            append_wx.append_wx(wxfile, idifiles)
            print('Done\n')
        else:
            print('\nWarning: No valid WX table found.\nAny opacity attenuation calibration will fail.\n')


def task_tsys(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
              _spwpartition, _mpi_client):
    """
    Uses the Tsys values attached to the idi files (as SYSTEM_TEMPERATURE) loaded into the MS
      to generate a Tsys calibration table. Ignores on-the-fly calibration.
    """
    if _inp_params.verbose:
        print('  Note: On-the-fly calibration is actually ignored for the tsys task.')
    remove_invalid_tsys(_inp_params)
    fill_tsys_scangaps(_inp_params, _ms_metadata)
    calibration.task_gencal_general(_inp_params, caltable, 'tsys', '', spw=_spwpartition)
    return True


def task_tsys_add_exptau(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                         _spwpartition, _mpi_client):
    """
    Similar to task_tsys but first generates a .no_tauc table and then creates the real caltable,
      which has the correction Tsys -> Tsys * exp(tau) applied.
    """
    if _inp_params.verbose:
        print('  Note: On-the-fly calibration is actually ignored for the tsys task.')
    remove_invalid_tsys(_inp_params)
    fill_tsys_scangaps(_inp_params, _ms_metadata)
    _init_table = caltable + '.before_tauc'
    calibration.task_gencal_general(_inp_params, _init_table, 'tsys', '', spw=_spwpartition)
    if _inp_params.no_opac_corr_stations == 'all':
        print('  Skipping the opacity correction as specified with the no_opac_corr_stations input parameter.')
        shutil.move(_init_table, caltable)
    else:
        opacity_correction.opacity_correction(_inp_params, _ms_metadata, _init_table, caltable)
    return True


def do_gc_conversion(_inp_params, _ms_metadata, antabtable):
    """
    Reads polynomial gain curves from ANTAB tables and formats into VLA-type (*DPFU, shift and take sqrt).
    """
    if antabtable:
        print('\nPreparing DPFU and gain curve conversion file based on the ANTAB table\n  ' + antabtable + ' ...')
        if not auxiliary.check_for_filecontent(antabtable):
            raise IOError('The ANTAB table appears to be empty. Cannot proceed.')
        gc.gc(antabtable, _inp_params.gc_conversion_file)
        calib_ants = list(auxiliary.read_CASA_table(_inp_params, _query_select='ANTENNA',
                                                    _tablename=_inp_params.gc_conversion_file))
        for ant in _ms_metadata.antennanames:
            if ant not in calib_ants:
                print('    Warning: No a-priori gain calibration found for: ' + ant)
        print('Done\n')
    else:
        print('\nWarning: No ANTAB table found for gain curve conversion.\nGain curve calibration will fail.\n')


def task_gaincurve(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                   _spwpartition, _mpi_client):
    """
    Uses the generated VLA-type gain curve file (conversion from ANTAB polynomial gaincurve + DPFU)
      to generate a gc calibration table. Ignores on-the-fly calibration.
    """
    if _inp_params.verbose:
        print('  Note: On-the-fly calibration is actually ignored for the gaincurve task.')
    calibration.task_gencal_general(_inp_params, caltable, 'gc', _inp_params.gc_conversion_file, spw=_spwpartition)
    return True


def task_accor(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
               _spwpartition, _mpi_client):
    """
    Executes CASA's accor task to remove the digital sampler bias.
    Uses auto-correlation spectra to determine corrections.
    Required for data from the DiFX correlator.
    Should not apply any calibration on the fly.
      The fly parameters are passed for code consistency but they are not used.
    """
    if _inp_params.verbose:
        print('  Note: On-the-fly calibration is actually ignored for the accor task.')
    if _mpi_client:
        scratchdir = _inp_params.scratchdir + '/'
        auxiliary.makedir(scratchdir)
        mpi_jobIDs_import = []
        for server in list(_inp_params.MPI_processIDs.keys()):
            mpi_jobIDs_import.append(_mpi_client.push_command_request(
                                    'from pipe_modules.amplitude_calibration import task_accor_general',
                                     target_server=server            )[0])
        _mpi_client.get_command_response(mpi_jobIDs_import, True)
        mpi_jobIDs = []
        ctb_list   = []
        for scan in _ms_metadata.all_scans:
            t_ctb = auxiliary.unique_filename(scratchdir + 'scan' + str(scan) + '.sampler-corr.t')
            cmd = ("""task_accor_general('{0}',""".format(_inp_params.ms_name)
                  +"""'{0}',""".format(_inp_params.accor_solint)
                  +"""'{0}',""".format(str(t_ctb))
                  +"""_scan='{0}'),""".format(str(scan))
                  )
            mpi_jobIDs.append(_mpi_client.push_command_request(cmd)[0])
            ctb_list.append(t_ctb)
        _mpi_client.get_command_response(mpi_jobIDs, True)
        auxiliary.concat_caltables(ctb_list, caltable)
    else:
        task_accor_general(_inp_params.ms_name, _inp_params.accor_solint, caltable)
    return True


def task_acscl(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
               _spwpartition, _mpi_client):
    """
    Ensure correct correlation coefficient scaling (autocorrelations to unity) using only
    the inner 40% channels after accor + scalar bandpass.
    Not needed anymore as accor + subsequent scalar bandpass seem to do the normalization already perfectly.
    NOTE: Assumes on-the-fly calibration tables to be solved for all sources (gainfield=[]).
    """
    numchan   = min(_ms_metadata.channels)
    startchan = int(0.3*numchan)
    endchan   = int(0.7*numchan)
    spwselect = '*:{0}~{1}'.format(str(startchan), str(endchan))
    if _mpi_client:
        scratchdir = _inp_params.scratchdir + '/'
        auxiliary.makedir(scratchdir)
        mpi_jobIDs_import = []
        for server in list(_inp_params.MPI_processIDs.keys()):
            mpi_jobIDs_import.append(_mpi_client.push_command_request(
                                    'from pipe_modules.amplitude_calibration import task_accor_general',
                                     target_server=server            )[0])
        _mpi_client.get_command_response(mpi_jobIDs_import, True)
        mpi_jobIDs = []
        ctb_list   = []
        for scan in _ms_metadata.all_scans:
            t_ctb = auxiliary.unique_filename(scratchdir + 'scan' + str(scan) + '.sampler-corr.t')
            cmd = ("""task_accor_general('{0}',""".format(_inp_params.ms_name)
                  +"""'{0}',""".format(_inp_params.accor_solint)
                  +"""'{0}',""".format(str(t_ctb))
                  +"""{0},""".format(str(_fly_calib_tables))
                  +"""{0},""".format(str(_fly_calib_interp))
                  + """_spw='{0}',""".format(str(spwselect))
                  +"""_scan='{0}'),""".format(str(scan))
                  )
            mpi_jobIDs.append(_mpi_client.push_command_request(cmd)[0])
            ctb_list.append(t_ctb)
        _mpi_client.get_command_response(mpi_jobIDs, True)
        auxiliary.concat_caltables(ctb_list, caltable)
    else:
        task_accor_general(_inp_params.ms_name, _inp_params.accor_solint, caltable, _fly_calib_tables, _fly_calib_interp,
                           spwselect
                          )
    return True


def task_accor_general(_ms_name, _solint, caltable, _fly_calib_tables=[], _fly_calib_interp=[], _spw='', _scan=''):
    """
    Generic accor task - used for normal accor over all channels before bandpass and over central channels after bandpass.
    """
    tasks.accor(vis                =  _ms_name,
                caltable           =  caltable,
                field              =  "",
                spw                =  _spw,
                intent             =  "",
                selectdata         =  True,
                timerange          =  "",
                antenna            =  "",
                scan               =  _scan,
                observation        =  "",
                msselect           =  "",
                solint             =  str(_solint),
                combine            =  "",
                append             =  False,
                docallib           =  False,
                callib             =  "",
                gaintable          =  _fly_calib_tables,
                gainfield          =  [],
                interp             =  _fly_calib_interp,
                corrdepflags       =  True,
                spwmap             =  []
               )


def task_scalar_bandpass(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                         _spwpartition, _mpi_client):
    """
    Generate a scalar (amplitude only) bandpass calibration table based on the autocorrelations.
    Uses my own simple scalar bandpass function until there is an official CASA scalar bandpass task.
    """
    if not auxiliary.is_set(_inp_params, 'solvemode_scalar_bandpass'):
        my_scalar_bandpass_per_scan(_inp_params, _ms_metadata, caltable, _mpi_client)
    elif _inp_params.solvemode_scalar_bandpass == 'perscan':
        my_scalar_bandpass_per_scan(_inp_params, _ms_metadata, caltable, _mpi_client)
    elif _inp_params.solvemode_scalar_bandpass == 'const':
        my_scalar_bandpass_constant(_inp_params, _ms_metadata, caltable, _mpi_client)
    elif _inp_params.solvemode_scalar_bandpass == 'skip':
        print('    Skipping scalar_bandpass calibration as specified by the solvemode_scalar_bandpass parameter.')
        return False
    else:
        raise ValueError('{0} is no valid solvemode_scalar_bandpass option.'.format(str(_inp_params.solvemode_scalar_bandpass)))
    return True


def my_scalar_bandpass_constant(_inp_params, _ms_metadata, caltable, _mpi_client=False):
    """
    As my_scalar_bandpass_per_scan task but for a constant bandpass based on the median data across scans.
    """
    my_scalar_bandpass_per_scan(_inp_params, _ms_metadata, caltable, _mpi_client)
    calibration.median_all_scans(_inp_params, _ms_metadata, caltable, 'CPARAM', 1+0j)


def my_scalar_bandpass_per_scan(_inp_params, _ms_metadata, caltable, _mpi_client=False):
    """
    Using basic CASA table and calibration tools to write my own simple (but efficient) scalar bandpass calibration.
    The accor tasks must be run as well to do the proper normalization.
    """
    auxiliary.rm_dir_if_present(caltable)
    mytb = casac.table()
    if _inp_params.verbose:
        print('  Note: On-the-fly calibration is actually ignored for this scalar bandpass task.')
    scratchdir = _inp_params.scratchdir + '/'
    auxiliary.makedir(scratchdir)
    if len(_ms_metadata.correlations) == 1:
        #only need data[0]
        multi_pol = False
    else:
        #need data[0] and data[-1] (RR,LL autocorr have no complex part but RL and LR do)
        multi_pol = True
    s_counter = 0
    ctb_list  = []
    if _mpi_client:
        mpi_jobIDs = []
        mpi_jobIDs_import = []
        for server in list(_inp_params.MPI_processIDs.keys()):
            mpi_jobIDs_import.append(_mpi_client.push_command_request(
                                    'from pipe_modules.amplitude_calibration import scalar_bandpass_per_scan_compute',
                                     target_server=server            )[0])
        _mpi_client.get_command_response(mpi_jobIDs_import, True)
    if auxiliary.is_set(_inp_params, 'spectral_line'):
        these_scans = auxiliary.flatten_list(_ms_metadata.yield_scans(_ms_metadata.yield_calibrators(_inp_params)), True)
    else:
        these_scans = _ms_metadata.all_scans
    for scan in these_scans:
        fieldname = _ms_metadata.yield_sourcename_from_scan(scan)
        fieldID   = _ms_metadata.yield_sourceID(fieldname)
        s_counter += 1
        try:
            ants = list(_ms_metadata.yield_antennas(scan))
        except TypeError:
            print('    Warning: Skipping scan {0}, which does not appear to have any data'.format(str(scan)))
            continue
        spwds = [list(s) for s in _ms_metadata.yield_spwds(scan)]
        t_ctb = auxiliary.unique_filename(scratchdir + 'scan' + str(scan) + '.scalar-bandpass.t')
        auxiliary.create_caltable(_inp_params, t_ctb, 'Complex', 'B Jones', False)
        if _mpi_client:
            cmd = ("""scalar_bandpass_per_scan_compute('{0}',""".format(t_ctb)
                  +"""'{0}',""".format(_inp_params.ms_name)
                  +"""{0},""".format(str(multi_pol))
                  +"""{0},""".format(str(ants))
                  +"""{0},""".format(str(spwds))
                  +"""{0},""".format(str(scan))
                  +"""{0},""".format(str(fieldID))
                  +"""{0},""".format(str(_ms_metadata.yield_antID(_inp_params.refant.split(',')[0])))
                  +"""{0})""".format(str(_inp_params.verbose))
                  )
            mpi_jobIDs.append(_mpi_client.push_command_request(cmd)[0])
        else:
            scalar_bandpass_per_scan_compute(t_ctb, _inp_params.ms_name, multi_pol, ants, spwds, scan, fieldID,
                                             _ms_metadata.yield_antID(_inp_params.refant.split(',')[0]), _inp_params.verbose
                                            )
        ctb_list.append(t_ctb)
    if _mpi_client:
        _mpi_client.get_command_response(mpi_jobIDs, True)
    auxiliary.concat_caltables(ctb_list, caltable)
    mytb.open(caltable, nomodify=False)
    #Remove 0+0j solutions, which can mess up switching pol stations.
    try:
        cvals = mytb.getcol('CPARAM')
        cvals[cvals==0+0j]=1+0j
        mytb.putcol('CPARAM', cvals)
    except RuntimeError:
        #Different spectral windows have different numbers of channels.
        _nrows   = mytb.nrows()
        all_rows = range(_nrows)
        for _row in all_rows:
            thiscval = mytb.getcell('CPARAM', _row)
            thiscval[thiscval==0+0j]=1+0j
            mytb.putcell('CPARAM', _row, thiscval)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def scalar_bandpass_per_scan_compute(scb_s_tab, ms_name, _multi_pol, _ants, _spwds, _scan, _fieldID, _scan_refant, _verbose=False):
    """
    Single function that can run in MPI mode to create per-scan scalar bandpass calibration tables (scb_s_tab) for
    my_scalar_bandpass_per_scan().
    """
    mytb = casac.table()
    mytb.open(scb_s_tab, nomodify=False)
    if _verbose:
        print('    Processing scan ' + str(_scan))
    r_counter = 0
    for ant, spws in zip(_ants, _spwds):
        bandpass  = {}
        flag_sols = {}
        for spw in spws:
            qselc            = 'FLAG, TIME, DATA'
            qcrit            = 'SCAN_NUMBER==' + str(_scan) + ' && ANTENNA1==' + str(ant) + ' && ANTENNA2==' + str(ant) + \
                               ' && DATA_DESC_ID==' + str(spw)
            qsort            = 'TIME'
            flag, time, data = auxiliary.read_CASA_table(None, qselc, qcrit, qsort, _tablename=ms_name)
            try:
                if not any(data):
                    continue
                pass
            except ValueError:
                if not data.any():
                    continue
                pass
            if _multi_pol:
                flag = np.take(flag, indices = [0,-1], axis = 0)
                data = np.take(data, indices = [0,-1], axis = 0)
            data    = data.real
            data    = np.ma.masked_array(data, flag)
            meddata = np.ma.mean(data, -1)
            caltime = np.ma.unique(time)[int(len(time)/2)]
            if meddata.any():
                meddata[0] /= np.ma.mean(meddata[0])
                try:
                    meddata[1] /= np.ma.mean(meddata[1])
                except IndexError:
                    #single pol
                    meddata = [meddata[0], meddata[0]]
                    flag    = [flag[0], flag[0]]
                bandpass[spw]  = meddata
                #We have a valid solution if a channel is not flagged for at least one integration.
                #I.e. at least one entry of the last axis of the flag array must be <False>
                these_flags  = np.mean(np.invert(flag), -1).astype(bool)
                these_flags  = np.invert(these_flags)
                #No flagging with scalar bandpass to not mess up switching pol stations
                these_flags.fill(False)
                flag_sols[spw] = these_flags
        for spw in bandpass.keys():
            this_meddata = bandpass[spw]
            this_shape   = np.shape(this_meddata)
            mytb.addrows(1)
            mytb.putcell('TIME', r_counter, caltime)
            mytb.putcell('FIELD_ID', r_counter, _fieldID)
            mytb.putcell('SPECTRAL_WINDOW_ID', r_counter, spw)
            mytb.putcell('ANTENNA1', r_counter, ant)
            mytb.putcell('ANTENNA2', r_counter, _scan_refant)
            mytb.putcell('SCAN_NUMBER', r_counter, int(_scan))
            mytb.putcell('CPARAM', r_counter, np.sqrt(np.abs(this_meddata)))
            mytb.putcell('PARAMERR', r_counter, np.ones(this_shape))
            mytb.putcell('FLAG', r_counter, flag_sols[spw])
            mytb.putcell('SNR', r_counter, np.full(this_shape, 999))
            r_counter += 1
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def task_complex_bandpass(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                          _spwpartition, _mpi_client):
    """
    Executes CASA's bandpass task to generate a complex (phase+ampltidude) bandpass calibration table.
    Wrapper around task_complex_bandpass_general, executing it for several sources and averaging the results.
    """
    if auxiliary.is_set(_inp_params, 'calibrators_bandpass'):
        scalar_bandpass_table = getattr(_inp_params, 'calib_scalar_bandpass')[_inp_params.C_NAM]
        if auxiliary.is_set(_inp_params, 'phaseonly_cmplx_bandpass'):
            do_amp_corr = False
            if _inp_params.verbose:
                print('    phaseonly_cmplx_bandpass input param set. Will not solve for amplitudes with the complex bandpass.')
        elif auxiliary.isdir(scalar_bandpass_table):
            do_amp_corr = False
            if _inp_params.verbose:
                print('    Found a scalar bandpass table. Will not correct amplitudes with the complex bandpass table.')
        else:
            do_amp_corr = True
            if _inp_params.verbose:
                print('    Did not find scalar bandpass table. Will correct amplitudes with the complex bandpass table.')
        bp_calib = _inp_params.calibrators_bandpass.split(',')
        if not do_amp_corr:
            #the amplitude calibration is better done with the scalar bandpass:
            _degamp = 0
        else:
            _degamp = _inp_params.degamp_cmplx_bandpass
        if _inp_params.bandtype_cmplx_bandpass == 'B':
            tmp_tables = []
            for source in bp_calib:
                this_table = auxiliary.unique_filename(caltable + '.' + source)
                task_complex_bandpass_general(_inp_params, _ms_metadata, this_table, field=source,
                                              minblperant=_inp_params.minblperant_cmplx_bandpass,
                                              minsnr=_inp_params.minsnr_cmplx_bandpass,
                                              solnorm=_inp_params.solnorm_cmplx_bandpass, bandtype='B',
                                              fillgaps=_inp_params.fillgaps_cmplx_bandpass,
                                              degamp=_degamp,degphase=_inp_params.degphase_cmplx_bandpass,
                                              visnorm=_inp_params.visnorm_cmplx_bandpass,
                                              gaintable=_fly_calib_tables, interp=_fly_calib_interp,
                                              gainfield=_fly_calib_gainfd
                                             )
                tmp_tables.append(this_table)
            calibration.avg_caltables(_inp_params, tmp_tables, caltable)
            if not do_amp_corr:
                calibration.remove_amp_corr(_inp_params, caltable)
            elif _inp_params.solnorm_cmplx_bandpass:
                calibration.normalize_amp_corr(caltable)
            calibration.skip_failed_solutions(_inp_params, _ms_metadata, caltable)
        elif _inp_params.bandtype_cmplx_bandpass == 'BPOLY':
            if len(bp_calib) != 1:
                raise ValueError('Got bandtype_cmplx_bandpass=BPOLY input with more than one calibrator specified: ' \
                                 + str(bp_calib)
                                )
            task_complex_bandpass_general(_inp_params, _ms_metadata, caltable, field=bp_calib[0], combine='scan',
                                          minblperant=_inp_params.minblperant_cmplx_bandpass,
                                          minsnr=_inp_params.minsnr_cmplx_bandpass,
                                          solnorm=_inp_params.solnorm_cmplx_bandpass, bandtype='BPOLY',
                                          fillgaps=_inp_params.fillgaps_cmplx_bandpass,
                                          degamp=_degamp,degphase=_inp_params.degphase_cmplx_bandpass,
                                          visnorm=_inp_params.visnorm_cmplx_bandpass,
                                          gaintable=_fly_calib_tables, interp=_fly_calib_interp, gainfield=_fly_calib_gainfd
                                         )
        else:
            raise ValueError('bandtype_cmplx_bandpass inp must be <B> or <BPOLY>. Got '+str(_inp_params.bandtype_cmplx_bandpass))
        return True
    else:
        print('    Skipping complex bandpass calibration as no bandpass calibrators were specified.')
        return False


def task_complex_bandpass_general(_inp_params, _ms_metadata, caltable, field='', antenna='', scan='', solint='inf',
                                  combine='scan', minblperant=4, minsnr=3.0, solnorm=True, bandtype='B', fillgaps=0,
                                  degamp=3, degphase=3, visnorm=True, gaintable=[], gainfield=[], interp=[], spwmap=[],
                                  overwrite_refant=False):
    """
    Executes CASA's bandpass task to generate a complex (phase+ampltidude) bandpass calibration table.
    """
    if field:
        gaintable, interp, gainfield, _, _ = calibration.get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve', field)
    else:
        gainfield = calibration.get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve')
    if not spwmap:
        spwmap = auxiliary.get_spwmap(_inp_params, _ms_metadata, gaintable)
    if isinstance(overwrite_refant, bool):
        refant= str(_inp_params.refant)
    else:
        refant = overwrite_refant
    tasks.bandpass(vis          = _inp_params.ms_name,
                   caltable     = caltable,
                   field        = field,
                   spw          = '',
                   intent       = '',
                   selectdata   = True,
                   timerange    = '',
                   uvrange      = '',
                   antenna      = antenna,
                   scan         = scan,
                   observation  = '',
                   msselect     = '',
                   solint       = solint,
                   combine      = combine,
                   refant       = refant,
                   minblperant  = minblperant,
                   minsnr       = minsnr,
                   solnorm      = solnorm,
                   bandtype     = bandtype,
                   smodel       = [],
                   append       = False,
                   fillgaps     = fillgaps,
                   degamp       = degamp,
                   degphase     = degphase,
                   visnorm      = visnorm,
                   maskcenter   = 0,
                   maskedge     = 0,
                   docallib     = False,
                   callib       = '',
                   gaintable    = gaintable,
                   gainfield    = gainfield,
                   interp       = interp,
                   spwmap       = spwmap,
                   corrdepflags = True,
                   parang       = _inp_params.parang
                  )


def task_phase_jumps_NOEMA(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                           _spwpartition, _mpi_client):
    """
    Code from Sebastiano von Fellenberg.
    Generates a bandpass calibration table with a-priori corrections for intra-spw phase jumps of phased NOEMA in the EHT data.
    """
    if not 'NN' in _ms_metadata.antennanames:
        return False
    task_complex_bandpass_general(_inp_params, _ms_metadata, caltable, scan='1', overwrite_refant='0', minblperant=1, minsnr=0.01)

    mytb = casac.table()
    mytb.open(caltable, nomodify=False)
    ## set uniform SNR, set 0 degree CPARAM
    SNR_shape = mytb.getcol("SNR").shape
    cparam_shape = mytb.getcol("CPARAM").shape
    cflag_shape = mytb.getcol("FLAG").shape
    ant2_shape = mytb.getcol("ANTENNA2").shape
    mytb.putcol("SNR", np.ones(SNR_shape)*10.) # artifically set SNR to 10.
    mytb.putcol("CPARAM", np.exp(1j*np.zeros(cparam_shape))) ## set CPARAM to 1*exp(0degree)
    mytb.putcol("FLAG", np.zeros(cflag_shape, dtype=bool)) ## unflag all
    mytb.putcol("ANTENNA2", -np.ones(ant2_shape, dtype=int))
    mytb.putcol("PARAMERR", np.ones(cparam_shape, dtype=float))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    put_NNoffsets(_inp_params, _ms_metadata.channels_nu, caltable, _ms_metadata.yield_antID('NN'))
    return True

def put_NNoffsets(_inp_params, _data_freqs, caltable, nn_ant_idx, track='e21e18'):
    """
    Code from Sebastiano von Fellenberg.
    tested for e21e18, band4, band3, band2 and (band1)
    known to not work for band2 e21a14
    band : str band for which caltable is created, allowed values [band1, band2, band3, band4]
    track : str track for which caltable is created, allowed values [e21e18]
    """
    if track not in ['e21e18']:
        raise ValueError("provided track is not in ['e21e18'], bit is: ", track)

    mytb = casac.table()

    data_freqs = np.array(_data_freqs).flatten()

    offset_rr    = []
    indexe_rr    = []
    offset_ll    = []
    indexe_ll    = []
    cumoffset_rr = []
    cumoffset_ll = []
    method       = []

    ## offset definitions in degree per band

    ## b1
    if min(np.abs(data_freqs - 212191.7891 * 1.e6)) < 1.e9:
                    #0,     1,   2,   3,   4,   5,   6,   7,   8,    9,  10,  11,  12,  13,  14,  15
        offset_rrb = [180, 180,  90, 270, 180, 180,  90, 270, 180,  180,   0, 270, 180, 180, 180, 270,
                  # 16,  17,  18,  19,  20,  21,  22,  23,
                    90, 180, 180, 180, 180, 180,   0, 180, 270,    0, 180, 180,   0,   0,   0,   0]

        indexe_rrb = [ 98,  87,  75,  65,  54,  43,  33,  22,  11,    0,   1, 107,  96,  85,  74,  63,
                    52,  42,  31,  20,   9,   0,   0, 105,  94,   83,  72,  61,   0,    0,   0,   0]

                    #0,     1,   2,   3,   4,   5,   6,   7,   8,    9,  10,  11,  12,  13,  14,  15
        offset_llb = [180, 180,   0, 270, 180, 180, 180, 180, 180,   180, 90, 270, 180, 180, 180, 180,
                   180, 180, 180, 270,  90, 180,   0, 180, 180,  180, 180, 180,   0,   0,   0,   0]

        indexe_llb = [ 98,  87,  75,  65, 54,  43,  33,  22,  11,    0,   0, 107, 96,  85,  74,  63,
                    52,   42, 31,  20, 9,   0,   0, 105,  94,   83,  72,  61,  0,    0,   0,   0]
        cumoffset_rrb = np.cumsum(offset_rrb)
        cumoffset_llb = np.cumsum(offset_llb)

        offset_rr.extend(offset_rrb)
        offset_ll.extend(offset_llb)
        indexe_rr.extend(indexe_rrb)
        indexe_ll.extend(indexe_llb)
        cumoffset_rr.extend(cumoffset_rrb)
        cumoffset_ll.extend(cumoffset_llb)
        method.extend(['lowerSB']*len(offset_rrb))

    ## b2
    if min(np.abs(data_freqs - 216008.1953 * 1.e6)) < 1.e9:
        offset_rrb = [  0,   0,   0,   0,   0,   0,   0,   0,    0,   0,  0 ,  180, 180, 180,   0,  0,
                   # 16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,   27,
                   180,  180,  90,   0, 180, 180, 180, 180, 180, 180, 180,   90,   0,   0,   0,  0]

        indexe_rrb = [  0,    0,   0,   0,   0,   0,   0,   0,   0,    0,   0,  75,  64,  53,  42,  31,
                    20,   10,   0,   0, 105,  94,  83,  73,  62,   51,  40,  29,   0,   0,   0,  0]

                      #0,     1,   2,   3,   4,   5,   6,   7,   8,    9,  10,  11,  12,  13,  14,  15
        offset_llb = [180,  180, 180, 180, 180, 270,  90,   0, 180,  180, 270,   0, 180, 180, 270,  90,
                   # 16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,   27,
                   180,  180,  90,   0, 180, 180, 180,  90, 270,  180, 180,  90,  0,   0,   0,   0]

        indexe_llb = [  65,   55,  43,  33,  22,  12,   0,   0, 107,   96,  85,   0,  64,  53,  42,  31,
                    20,   10,   0,   0, 105,  94,  83,  73,  62,   51,  40,  29,   0,   0,   0,   0]
        cumoffset_rrb = np.cumsum(offset_rrb)
        cumoffset_llb = np.cumsum(offset_llb)

        offset_rr.extend(offset_rrb)
        offset_ll.extend(offset_llb)
        indexe_rr.extend(indexe_rrb)
        indexe_ll.extend(indexe_llb)
        cumoffset_rr.extend(cumoffset_rrb)
        cumoffset_ll.extend(cumoffset_llb)
        method.extend(['lowerSB']*len(offset_rrb))

    ## b3
    if min(np.abs(data_freqs - 226191.7891 * 1.e6)) < 1.e9:
                    #0,     1,   2,   3,   4,   5,   6,   7,   8,    9,  10,  11,  12,  13,  14,  15
        offset_rrb = [180,   0,  90, 180, 180, 270, 180, 270, 180,  180, 270,  90, 180, 180, 270,  0,
                  # 16,  17,  18,  19,  20,  21,  22,  23,
                   180, 180,   0,   0,  90, 180, 180, 270,   0,  180, 180,   0,   0,   0,   0,  0]

        indexe_rrb = [51,   62,  72,  82,  93, 104, 114,   0,   8,   20,  30,  41,  53,  63,  73, 85,
                   95,  105,   0,   0,  10,  21,  32,  43,  53,   65,  75,  85,   0,   0,   0,  0]

                    #0,     1,   2,   3,   4,   5,   6,   7,   8,    9,  10,  11,  12,  13,  14,  15
        offset_llb = [180, 270,  0, 180, 180,  90,   0,   0, 180,  180, 180,  90, 180, 180, 270, 180,
                  # 16,  17,  18,  19,  20,  21,  22,  23,  24,   25,  26,  27
                   180, 180,   0, 180, 180, 180, 180, 270,  90,  180, 180,   0,   0,   0,   0,   0]

        indexe_llb = [51,   62,  72,  82,  93, 104, 114,   0,   8,   20,  30,  41,  53,  63,  73,  85,
                   95,  105,   0,   0,  10,  21,  32,  43,  53,   65,  75,  85,   0,   0,   0,   0]
        cumoffset_rrb = np.cumsum(offset_rrb)
        cumoffset_llb = np.cumsum(offset_llb)

        offset_rr.extend(offset_rrb)
        offset_ll.extend(offset_llb)
        indexe_rr.extend(indexe_rrb)
        indexe_ll.extend(indexe_llb)
        cumoffset_rr.extend(cumoffset_rrb)
        cumoffset_ll.extend(cumoffset_llb)
        method.extend(['upperSB']*len(offset_rrb))

    ## b4
    if min(np.abs(data_freqs - 230008.1953 * 1.e6)) < 1.e9:
                    #0,     1,   2,   3,   4,   5,   6,   7,   8,     9,  10,   11,  12,  13,  14,  15
        offset_rrb = [180, 180, 270, 180, 180, 180, 270,  90, 180,  180,  0,   90, 180, 180, 180, 180,
                   180, 180, 180,  90, 270,   0, 180, 180,  90,  270, 180, 180,   0,   0,   0,   0]

        indexe_rrb = [ 20,  29,   40,   50, 61,  72,  84, 93,  104,  115, 30,   8,  20,  30,  41,  52,
                    63,  74,   85,  95,  105,  4,  0,  11,  22,   32,  43,  54,  0,    0,   0,   0]

                    #0,     1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15
        offset_llb = [180, 180, 270, 180, 180, 180,   0,   0, 180, 180,   0, 180,  90, 180, 180, 270,
                   180, 180, 180,  90,   0,   0,  180,180,   0, 270, 180, 180,   0,   0,   0,   0]

        indexe_llb = [ 20,  29,   40, 50,  61,  72,  84,  93,  104,115,  30,   8,  20,  30,  41,  52,
                    63,  74,   85, 95, 105,   4,   0,  11,   22, 32,  43,  54,   0,   0,   0,   0]

        cumoffset_rrb = np.cumsum(offset_rrb)
        cumoffset_llb = np.cumsum(offset_llb)

        offset_rr.extend(offset_rrb)
        offset_ll.extend(offset_llb)
        indexe_rr.extend(indexe_rrb)
        indexe_ll.extend(indexe_llb)
        cumoffset_rr.extend(cumoffset_rrb)
        cumoffset_ll.extend(cumoffset_llb)
        method.extend(['upperSB']*len(offset_rrb))

    cumoffset_rr = np.array(cumoffset_rr)
    cumoffset_ll = np.array(cumoffset_ll)
    print('  Loaded EHT data with phased NOEMA in it. Will correct phases jumps.')
    print('  A quick validation check.. These RCP jumps will be applied:')
    print(offset_rr)
    ## complxify
    offset_rr = np.exp(1j*np.radians(offset_rr))
    offset_ll = np.exp(1j*np.radians(offset_ll))
    cumoffset_rr = np.exp(1j*np.radians(cumoffset_rr))
    cumoffset_ll = np.exp(1j*np.radians(cumoffset_ll))

    mytb.open(caltable, nomodify=False)
    _nrows = mytb.nrows()
    _ant1_row = mytb.getcol('ANTENNA1')
    _spw_row = mytb.getcol('SPECTRAL_WINDOW_ID')
    # Add phase jump entires in tables and flags at +/-1 the jump location.
    for _row in range(_nrows):
        if int(_ant1_row[_row]) == int(nn_ant_idx):
            ## found NN as ant1
            thevalue = mytb.getcell('FLAG', _row)
            spw0 = _spw_row[_row]
            ir = indexe_rr[spw0]
            il = indexe_ll[spw0]
            ## flag +/- 1 around index of spw0 on RR
            if 0 < ir < 116:
                thevalue[0, ir] = True
            if 0 < ir +1 < 116:
                thevalue[0, ir +1] = True
            if 0 < ir -1 < 116:
                thevalue[0, ir -1] = True
            ## flag +/- 1 around index of spw0 on LL
            if 0 < il < 116:
                thevalue[1, il] = True
            if 0 < il +1 < 116:
                thevalue[1, il +1] = True
            if 0 < il -1 < 116:
                thevalue[1, il -1] = True
            # put value
            mytb.putcell('FLAG', _row, thevalue)

            thevalue = mytb.getcell('CPARAM', _row)
            spw0 = _spw_row[_row]
            # add cummulated offset up to spw0 to the value
            if spw0 != 0:
                thevalue[0,:] = thevalue[0,:]*cumoffset_rr[spw0-1]
                thevalue[1,:] = thevalue[1,:]*cumoffset_ll[spw0-1]
                ## add offset of spw0 to thevalue
            if method[spw0] == 'upperSB':
                thevalue[0,indexe_rr[spw0]:] = thevalue[0,indexe_rr[spw0]:]*offset_rr[spw0]
                thevalue[1,indexe_ll[spw0]:] = thevalue[1,indexe_ll[spw0]:]*offset_ll[spw0]
            elif method[spw0] == 'lowerSB':
                ## add offset of spw0 to thevalue
                thevalue[0,:indexe_rr[spw0]] = thevalue[0,:indexe_rr[spw0]]*offset_rr[spw0]
                thevalue[1,:indexe_ll[spw0]] = thevalue[1,:indexe_ll[spw0]]*offset_ll[spw0]
            mytb.putcell('CPARAM',_row, thevalue)
        else:
            continue
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    #Seems like FLAGs from bandpass tables are not applied? So we have to do it the hard way.
    spwf = ''
    for s,i in enumerate(indexe_rr):
        if i:
            spwf+='{0}:{1}~{2},'.format(str(s), str(i-1), str(i+1))
    tasks.flagdata(vis=_inp_params.ms_name, mode='manual', antenna='NN', spw=spwf[:-1], datacolumn='DATA', correlation='RR,RL,LR',
                   flagbackup=True
                  )
    spwf = ''
    for s,i in enumerate(indexe_ll):
        if i:
            spwf+='{0}:{1}~{2},'.format(str(s), str(i-1), str(i+1))
    tasks.flagdata(vis=_inp_params.ms_name, mode='manual', antenna='NN', spw=spwf[:-1], datacolumn='DATA', correlation='RR,RL,LR',
                   flagbackup=True
                  )
    auxiliary.only_keep_latest_flagver(_inp_params, 'flagdata')
