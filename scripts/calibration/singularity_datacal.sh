#!/bin/bash
#Set the WORKDIR, RAWDATA, INPUTDIR, and DOCKERTAG variables below and calibrate data with an rPicard singularity image by doing:
#chmod +x singularity_datacal.sh && ./singularity_datacal.sh
set -e

#The working directory where the data will be processed and output will be created.
WORKDIR=/data/VLBI/VLBA/BW0106/rPicard_processed

#The path to the input data: FITS-IDI files, ANTAB tables, flag tables, ... (see rPicard documentation).
#If your raw visibility data is already in a MeasurementSet (MS) format, then copy it to $WORKDIR beforehand.
RAWDATA=/data/VLBI/VLBA/BW0106/rawdata

#Input files for the processing, see picard/input_template for an example.
#If a specific MS in $WORKDIR/<MSname> is to be processed, set ms_name=<MSname> in observation.inp.
#And make sure to set workdir = $pwd in observation.inp.
INPUTDIR=/data/VLBI/VLBA/BW0106/rPicard_input

#The version of the docker image for the processing of the data (will be converted to a singularity image).
#See https://hub.docker.com/r/mjanssen2308/casavlbi.
DOCKERTAG=0e3d86832470f95de2fdc8a430169726baa4e458
#DOCKERTAG=latest


#Specify the number of CPU cores rPicard can use here. The default is to use all available cores.
N_cores=$(nproc --all)
let N_cores=N_cores+1


mkdir -p $WORKDIR
cd $WORKDIR
ln -fs $RAWDATA .
rm -rf input
cp -r $INPUTDIR input
rm -rf casavlbi.pipe
singularity build casavlbi.pipe docker://mjanssen2308/casavlbi:$DOCKERTAG
singularity exec --cleanenv -B $WORKDIR:/data -B $RAWDATA:/rawdata ./casavlbi.pipe picard --input /data/input -n $N_cores
