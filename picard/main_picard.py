#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Main file of the pipeline. Use the picard script to launch.
"""
import os
import sys
#be able to launch the pipeline from its own folder:
sys.path.append(os.getcwd())
import pipe_modules.auxiliary                as auxiliary
import pipe_modules.calibration              as calibration
import pipe_modules.diagnostics              as diagnostics
import pipe_modules.tune_parameters          as tune_parameters
import pipe_modules.phase_calibration        as phase_calibration
import pipe_modules.flagging_algorithm       as flagging_algorithm
import pipe_modules.manual_calibration       as manual_calibration
import pipe_modules.amplitude_calibration    as amplitude_calibration
import pipe_modules.ionospheric_calibration  as ionospheric_calibration
import pipe_modules.polarization_calibration as polarization_calibration
from pipe_modules.default_casa_imports import *


_help = '\n\n*** Usage:\n'
_help += '*** $ picard [-p, --input <path>] [-q (<list>)] [-f] [-m] [-l (e)] [-d] [-r (a)] [-s] [-n <#cores>] [-i] [-h]\n'
_help += '*** Where:\n'
_help += '*** --input:new input location (or use -p to use a input/ folder in the pwd),\n'
_help += '*** -q:quickmode,\n'
_help += '*** -f:re-optimize fringe-fit,\n'
_help += '*** -m:re-determine the metadata,\n'
_help += '*** -l:run listobs again,\n'
_help += '*** -d:use previous diagnostics folder,\n'
_help += '*** -r:restore flags even in quickmode,\n'
_help += '*** -s:scrap old cal tables,\n'
_help += '*** -n:overwrite default number of cores used,\n'
_help += '*** -i: interactive mode,\n'
_help += '*** -h:print help and exit.\n\n'

graceful_exit_message = '- FINISHED -\n'
graceful_exit_message += 'Now: Tea. Earl Grey. Hot.\nWaiting for the MPI environment wrap-up...'

mpi_client = auxiliary.start_mpi()

#Unfortunately, python's nice built-in argparse functionality does not easily work together with CASA
#and I must pass the absolute path to the default input location because __file__ is not defined for casa -c:
picard_dir, casalogf, errlogf, new_input_location, quicklist, no_fringe_params_load, no_ms_metadata_load, \
force_restore_flags, make_new_listobsf, use_previous_diagdir, scrap_old_caltb, interactive_mode, help_and_exit \
= auxiliary.handle_cmd_args()

auxiliary.fancy_msg(\
'Thank you for using rPICARD '+auxiliary.get_git_version(picard_dir)+'\n' + \
'\n\nThis program will automatically calibrate your VLBI data set\n' \
' - Make it so! - ')

if help_and_exit:
    print ('\nGot <help> option:\nWill print help and exit.\n')
    print (_help)
    auxiliary.finalize([casalogf, errlogf], graceful_exit_message)
    sys.exit()

if new_input_location:
    print ('\nGot --input option:\nReading input from\n'+new_input_location+'\n')
    input_location = new_input_location
else:
    input_location = picard_dir + '/input/'
input_location += '*.inp'

inp_params, input_files = auxiliary.read_inputs(input_location, casalogf)
auxiliary.check_input(inp_params)
diagnostics.set_plot_sizes(inp_params)
original_dir = auxiliary.changedir(inp_params.workdir)

if use_previous_diagdir:
    new_diag_dir = diagnostics.get_last_diagdir(inp_params)
    if new_diag_dir:
        inp_params = inp_params._replace(diagdir=new_diag_dir)
        print ('\nGot -d option:\nWill not create a new diagnostics folder.')
        print ('Instead I will continue writing to the one that was last modified.')
        print ('Note that old diagnostic files and plots may be overwritten.\n')
    else:
        print ('\nGot -d option but I did not find any existing diagnostics folder.\nIgnoring -d for this run then.\n')
diagnostics.init_diagnostics(inp_params, input_files)

if interactive_mode:
    print ('\nGot -i option:\nWill operate in interacive mode where the user needs to press Enter to advance steps.\n')
if no_ms_metadata_load:
    inp_params = inp_params._replace(ms_metadata_load=False)
    print ('\nGot -m option:\nWill enforce the re-determination of the metadata collection from scratch.\n')
if force_restore_flags:
    print ('\nGot -r option:\nWill enfore to (re)store an initial flag version of the data.\n')
if no_fringe_params_load:
    inp_params = inp_params._replace(fringe_params_load=False)
    print ('\nGot -f option:\nWill enforce the re-calculation of the optimal fringe parameters.\n')

loaded_antab   = auxiliary.get_antabtable(inp_params)
loaded_weather = auxiliary.get_wxfiles(inp_params)
loaded_fitsidi = auxiliary.get_fitsidifiles(inp_params)
loaded_fitsidi = auxiliary.sort_fitsidi_files(loaded_fitsidi)
auxiliary.check_data(inp_params, loaded_fitsidi)
amplitude_calibration.attach_tsys_to_idi(inp_params, loaded_antab, loaded_fitsidi)
amplitude_calibration.attach_wx_to_idi(inp_params, loaded_weather, loaded_fitsidi)
auxiliary.load_the_data(inp_params, loaded_fitsidi)
if inp_params.antenna_mount_corrections_file and auxiliary.isfile(inp_params.antenna_mount_corrections_file):
    auxiliary.fix_antenna_mounts(inp_params, inp_params.antenna_mount_corrections_file)
if not quicklist or force_restore_flags:
    auxiliary.restore_init_flags(inp_params, force_restore_flags)
diagnostics.task_listobs(inp_params, make_new_listobsf)
if make_new_listobsf == 'exit':
    print ('\nGot -l e option:\nExiting now that I have written a listobs file.\n')
    auxiliary.finalize([casalogf, errlogf], graceful_exit_message, inp_params.diagdir, original_dir)
    sys.exit()
if inp_params.ms_metadata_load:
    if diagnostics.check_for_metadata_reload(inp_params):
        inp_params = inp_params._replace(ms_metadata_load=False)
        inp_params = inp_params._replace(fringe_params_load=False)
        print('\nDetected a change of input parameters or CASA version. Will therefore determine metadata again.\n')
    else:
        pass
if inp_params.ms_metadata_load and auxiliary.isfile(inp_params.store_ms_metadata):
    if inp_params.verbose:
        print('\n -- Reading metadata collection from storage --')
    ms_metadata = auxiliary.store_object(inp_params.store_ms_metadata)
else:
    if inp_params.verbose:
        print('\n -- Determining metadata collection from scratch --')
    corrs, snames, sIDs , anames, aIDs, all_spws, spw_channels, channel_freqs, scans, antennas, spwds, scan_lengths, scan_nvis, \
    scan_startimes, scan_endtimes, int_time \
    = auxiliary.get_info_from_listobs(inp_params)
#vasco call would go here --> update inp_params:
#first call: pass info from list obs and get parameters needed for optimization
#second call (again to a vasco function): pass optimization parameters, call fringefit inside vasco, return parameters to update inp_params before initializing metadata below (e.g., science target, calibrator names, refant list, if phaseref,), pass inp_params already for scratchdir etc.
#parameters determined by vasco are saved on disk similar to ms_metadata, then read back every time unless file was deleted or command line argument set to re-determine. Then, all inp_params where <compute-by-vasco> is set are updated based on the params file read
#check that check_for_metadata_reload gives no trouble and have auxiliary.check_input skip <compute-by-vasco> string
    ms_metadata = diagnostics.ms_metadata(inp_params, corrs, snames, sIDs, anames, aIDs, all_spws, spw_channels, channel_freqs,
                                          scans, antennas, spwds, scan_lengths, scan_nvis, scan_startimes, scan_endtimes,
                                          int_time
                                         )
    auxiliary.store_object(inp_params.store_ms_metadata, ms_metadata, 'write')
if inp_params.verbose:
    print ('  Using the following scans of the following sources for\n  diagnostic plots, flagging, ' \
           'and maybe for trial and error fringe-fitting:\n'
           + auxiliary.wrap_list(ms_metadata.selected_scans, indent='    ', two_dim=True)
          )

amplitude_calibration.EHT_add_tsys_to_MS(inp_params, loaded_antab)
loaded_antab = auxiliary.search_gcdpfu_in_idi(inp_params, ms_metadata, loaded_antab, loaded_fitsidi)
amplitude_calibration.do_gc_conversion(inp_params, ms_metadata, loaded_antab)

inp_params = tune_parameters.update_fringe_solint_inp_params(inp_params, ms_metadata)
if inp_params.refant == 'pick':
    inp_params = inp_params._replace(refant=ms_metadata.most_common_ants)
    print ('\n\nWarning: No refants specified. Picked\n  ' + inp_params.refant + '\nas the list of most common stations.\n')
inp_params = auxiliary.check_refants(inp_params, ms_metadata)
if auxiliary.is_set(inp_params, 'calibrators_phaseref'):
    print('\nThe parameter calibrators_phaseref has been set.\nOperating in phase-referencing mode now.\n')

try:
    mem_allowed = os.environ['RPICARD_MAXMEM_GB']
    if inp_params.verbose:
        print(f'\nLimiting the amount of memory for fringe-fitting to {mem_allowed}GB as specified by RPICARD_MAXMEM_GB.\n')
except KeyError:
    pass

#################################################################################################################################
## Below the calibration strategies are given for each array.
##

array_specific_steps = {}

#For a generic VLBI array, incl. accor and opacity correction (same as VLBIhi)
array_specific_steps['generic'] = [amplitude_calibration.task_accor,
                                   amplitude_calibration.task_scalar_bandpass,
                                   amplitude_calibration.task_tsys_add_exptau,
                                   amplitude_calibration.task_gaincurve,
                                   ionospheric_calibration.task_ionospheric_tec_map,
                                   phase_calibration.task_fringefit_single,
                                   phase_calibration.task_fringefit_solint_cal,
                                   phase_calibration.task_fringefit_multi_cal_short,
                                   amplitude_calibration.task_complex_bandpass,
                                   polarization_calibration.task_rldelay,
                                   polarization_calibration.task_rlphase,
                                   polarization_calibration.task_dterms,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short,
                                   phase_calibration.task_atmo_selfcal_startmod
                                  ]

array_specific_steps['EHT']     = [polarization_calibration.task_rldelay,
                                   polarization_calibration.task_rlphase,
                                   polarization_calibration.task_dterms,
                                   amplitude_calibration.task_accor,
                                   amplitude_calibration.task_scalar_bandpass,
                                   amplitude_calibration.task_tsys,
                                   amplitude_calibration.task_gaincurve,
                                   manual_calibration.task_NNphasecal,
                                   phase_calibration.task_phase_offsets_ALMA,
                                   phase_calibration.task_alignbands_ffospws,
                                   phase_calibration.task_fringefit_solint_cal,
                                   phase_calibration.task_fringefit_multi_cal_coher,
                                   phase_calibration.task_fringefit_single,
                                   phase_calibration.task_coarse_phbpass_ffospws,
                                   phase_calibration.task_fringefit_multi_cal_short,
                                   amplitude_calibration.task_complex_bandpass,
                                   phase_calibration.task_fringefit_multi_sci_long_spwant,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_multi_sci_long2,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short,
                                   phase_calibration.task_atmo_selfcal_startmod
                                  ]

#quicker EHT single-band calibration in actual order of steps
array_specific_steps['EHTsb']     = [phase_calibration.task_phase_offsets_ALMA,
                                     phase_calibration.task_fringefit_single,
                                     phase_calibration.task_fringefit_multi_sci_long,
                                     phase_calibration.task_fringefit_solint_sci,
                                     phase_calibration.task_fringefit_multi_sci_short,
                                     amplitude_calibration.task_accor,
                                     amplitude_calibration.task_scalar_bandpass,
                                     amplitude_calibration.task_tsys,
                                     amplitude_calibration.task_gaincurve,
                                     phase_calibration.task_atmo_selfcal_startmod
                                    ]

array_specific_steps['GMVA']    = [amplitude_calibration.task_accor,
                                   amplitude_calibration.task_scalar_bandpass,
                                   amplitude_calibration.task_tsys_add_exptau,
                                   amplitude_calibration.task_gaincurve,
                                   phase_calibration.task_fringefit_solint_cal,
                                   phase_calibration.task_fringefit_multi_cal_coher,
                                   phase_calibration.task_fringefit_single,
                                   phase_calibration.task_fringefit_multi_cal_short,
                                   amplitude_calibration.task_complex_bandpass,
                                   polarization_calibration.task_rldelay,
                                   polarization_calibration.task_rlphase,
                                   polarization_calibration.task_dterms,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short,
                                   phase_calibration.task_atmo_selfcal_startmod
                                  ]

#High frequency observations - opacity correction for Tsys(->Tsys*) needed, no tec correction needed.
array_specific_steps['VLBAhi']  = [amplitude_calibration.task_accor,
                                   amplitude_calibration.task_scalar_bandpass,
                                   amplitude_calibration.task_tsys_add_exptau,
                                   amplitude_calibration.task_gaincurve,
                                   phase_calibration.task_fringefit_single,
                                   phase_calibration.task_fringefit_solint_cal,
                                   phase_calibration.task_fringefit_multi_cal_short,
                                   amplitude_calibration.task_complex_bandpass,
                                   polarization_calibration.task_rldelay,
                                   polarization_calibration.task_rlphase,
                                   polarization_calibration.task_dterms,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short,
                                   phase_calibration.task_atmo_selfcal_startmod
                                  ]

#Low frequency observations - no opacity correction for Tsys needed, tec correction needed.
array_specific_steps['VLBAlo']  = [amplitude_calibration.task_accor,
                                   amplitude_calibration.task_scalar_bandpass,
                                   amplitude_calibration.task_tsys,
                                   amplitude_calibration.task_gaincurve,
                                   ionospheric_calibration.task_ionospheric_tec_map,
                                   phase_calibration.task_fringefit_single,
                                   phase_calibration.task_fringefit_solint_cal,
                                   phase_calibration.task_fringefit_multi_cal_short,
                                   amplitude_calibration.task_complex_bandpass,
                                   polarization_calibration.task_rldelay,
                                   polarization_calibration.task_rlphase,
                                   polarization_calibration.task_dterms,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short
                                  ]
array_specific_steps['VLBA'] = array_specific_steps['VLBAlo']

#For the EVN the ACCOR task is not needed as it is already done by the correlator itself.
array_specific_steps['EVN']     = [amplitude_calibration.task_scalar_bandpass,
                                   amplitude_calibration.task_tsys,
                                   amplitude_calibration.task_gaincurve,
                                   ionospheric_calibration.task_ionospheric_tec_map,
                                   phase_calibration.task_fringefit_single,
                                   phase_calibration.task_fringefit_solint_cal,
                                   phase_calibration.task_fringefit_multi_cal_short,
                                   amplitude_calibration.task_complex_bandpass,
                                   polarization_calibration.task_rldelay,
                                   polarization_calibration.task_rlphase,
                                   polarization_calibration.task_dterms,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short
                                  ]

#For synthetic data from the MeqSilhouette package.
array_specific_steps['meqsil']  = [amplitude_calibration.task_tsys,
                                   phase_calibration.task_fringefit_multi_sci_long,
                                   phase_calibration.task_fringefit_solint_sci,
                                   phase_calibration.task_fringefit_multi_sci_short,
                                   phase_calibration.task_atmo_selfcal_startmod
                                  ]

if inp_params.array_type in array_specific_steps.keys():
    all_calibration_steps = array_specific_steps[inp_params.array_type]
else:
    raise ValueError('No valid array_type specified in input/array.inp. Must be one of these:\n' \
                     + auxiliary.wrap_list(array_specific_steps.keys(), items_per_line=5, indent='  '))

                                                                                                                               ##
#################################################################################################################################
#################################################################################################################################

if scrap_old_caltb:
    print ('\nGot -s option:\nWill delete all relevant calibration tables from previous runs.\n')
    calibration.scrap_old_caltables(inp_params, all_calibration_steps)

#Stepslist will be a {[foo,bar]} array with foo=(ID, step description)
#and bar=(corresponding function for that step, input arguments for this function).
stepslist = []

#Steps to be executed before the calibration tasks:
stepslist.append([('a','load models of observed sources (if present)'),
                  (calibration.load_models, inp_params,ms_metadata)])
stepslist.append([('b','use online flags from idi files (if present)'),
                  (flagging_algorithm.go_flag, inp_params,ms_metadata,'from_idifile',loaded_fitsidi)])
stepslist.append([('c','use flags from metadata (if present)'),
                  (flagging_algorithm.go_flag, inp_params,ms_metadata,'metadata',loaded_fitsidi)])
stepslist.append([('d','flag based on outlier detection from auto-correlations vs time'),
                  (flagging_algorithm.go_flag, inp_params,ms_metadata,'autocorr_vs_time')])
stepslist.append([('e','flag based on outlier detection from auto-correlations vs frequency'),
                  (flagging_algorithm.go_flag, inp_params,ms_metadata,'autocorr_vs_freq')])
stepslist.append([('f','flag edge channels'),
                  (flagging_algorithm.go_flag, inp_params,ms_metadata,'edge_channels')])
stepslist.append([('g','flag start and end segments of scans (quacking)'),
                  (flagging_algorithm.go_flag, inp_params,ms_metadata,'quack')])
#Calibration tasks:
for i,step in enumerate(all_calibration_steps):
    stepslist.append([(str(i),str(step.__name__)),
                      (calibration.go_calibrate, inp_params,ms_metadata,step,
                       'solved_functions',all_calibration_steps,mpi_client)])
#Steps to be executed after the calibration tasks:
stepslist.append([('h','clear the calibrated data column of the MS from previous applycal runs'),
                  (calibration.task_clearcal, inp_params)])
stepslist.append([('i','apply all existing tables from all_calibration_steps'),
                  (calibration.task_applycal, inp_params,ms_metadata,'solved_functions',all_calibration_steps)])
stepslist.append([('j','print overview of flagged data (can be slow)'),
                  (diagnostics.print_flags, inp_params,ms_metadata)])
stepslist.append([('k','make diagnostic plots of calibrated visibilities and create a calibration summary file'),
                  (diagnostics.plot_calibrated_visibilities, inp_params,ms_metadata,casalogf)])
stepslist.append([('l','average and export the calibrated data'),
                  (auxiliary.export_the_data, inp_params,ms_metadata, mpi_client)])

spwpartition_indices = {}
print ('\nThe pipeline will execute the following steps for the\n' + inp_params.array_type +' array in the given order:')
for item in stepslist:
    print('%4s ' %item[0][0] + ': ' + item[0][1])
print ('Can use quickmode [-q] to execute only a subset of these steps.\n')
if quicklist == 'Abort':
    print('Got -q argument without a list specified:\nExiting now that I have shown the list of steps.')
    auxiliary.finalize([casalogf, errlogf], graceful_exit_message, inp_params.diagdir, original_dir)
    sys.exit()
if quicklist:
    #Captain's log: I have implemented a hidden option to execute parts of spwpartition steps as step.spwpartition_index.
    for i, step in enumerate(quicklist):
        if '.' in step:
            parts                          = step.split('.')
            quicklist[i]                   = parts[0]
            spwpartition_indices[parts[0]] = parts[1]
    possible_steps = [step[0][0] for step in stepslist]
    quicklist      = sorted(list(set(quicklist).intersection(possible_steps)))
    print('Using quickmode - only the following steps are executed:\n' \
          +auxiliary.wrap_list(quicklist, items_per_line=5, indent='  '))
print ('')

#Execute all steps in the order defined above while checking for match if quicklist is used:
search_line = False
if auxiliary.is_set(inp_params, 'spectral_line') and inp_params.spectral_line == 'search':
    search_line = True
for step in stepslist:
    if not quicklist or step[0][0] in quicklist:
        if interactive_mode:
            wait_for_input = auxiliary.input23('\nPress Enter to continue with step ' + str(step[0][0]) + \
                                               '. Write anything and press Enter to exit here: \n >')
            if wait_for_input:
                print('Exiting.')
                auxiliary.finalize([casalogf, errlogf], graceful_exit_message, inp_params.diagdir, original_dir)
                sys.exit()
        print ('          -- Executing step ' + str(step[0][0]) + ' --')
        if search_line and 'fringefit' in step[0][1] and 'sci' in step[0][1]:
            # Search line before first science target phase calibration.
            # I.e., after flagging and bandpass calibration.
            sline_loc = manual_calibration.find_line_peak_autocorrs(inp_params, ms_metadata)
            search_line = False
        if step[0][0] in spwpartition_indices:
            step[1][0](*step[1][1:], spwpartition_part=spwpartition_indices[step[0][0]])
        else:
            step[1][0](*step[1][1:])

auxiliary.finalize([casalogf, errlogf], graceful_exit_message, inp_params.diagdir, original_dir)
