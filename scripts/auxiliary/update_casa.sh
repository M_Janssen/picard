#!/bin/bash
#Keep a tarball of the CASA version required by rPicard up-to-date (e.g., for leap seconds).
set -e

PicardInstallLocation=/usr/local/src/picard
CasaStorageLocation=/vol/ftp/pub/astro/mjanssen
scratchdir=/home/mjanssen/


while true
do
	cd $PicardInstallLocation
	git pull
	CASADL="$(cat README.md | grep wget | cut -d' ' -f3)"
	OLDIFS=$IFS
	IFS=$'/' CASAVER=($CASADL)
	IFS=$OLDIFS
	CASAVER=${CASAVER[-1]}
	CASADIR="${CASAVER//.tar.xz}"
	echo Extracting $CASAVER ...
	cd $CasaStorageLocation
	tar xJf $CASAVER -C $scratchdir
	cd ${scratchdir}/${CASADIR}/data
	echo Running rsync ...
	rsync -az rsync://casa-rsync.nrao.edu/casa-data .
	cd ${scratchdir}/${CASADIR}
	cd `find lib -name __data__`
	rsync -az rsync://casa-rsync.nrao.edu/casa-data .
	cd $CasaStorageLocation
	echo Creating ${CASADIR}.updated.tar.xz ...
	tar -C ${scratchdir} -cJf ${CASADIR}.updated.tar.xz ${CASADIR}
        cd $CasaStorageLocation
	mv $CASAVER ${CASAVER}.old
	mv ${CASADIR}.updated.tar.xz $CASAVER
	rm -fr ${scratchdir}/${CASADIR}
	rm -fr ${CASAVER}.old
	echo Updated $CASAVER on $(date -u)
	sleep 86400
done
