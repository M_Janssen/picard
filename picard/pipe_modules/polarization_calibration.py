#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Polarization calibration tasks (RL phase, RL delay, D-terms). """
import numpy as np
import pipe_modules.auxiliary   as auxiliary
import pipe_modules.calibration as calibration
from pipe_modules.default_casa_imports import *


def task_rldelay(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                 _spwpartition, _mpi_client):
    """
    Executes CASA's gaincal task with gaintype='KCROSS' for polarization experiments to take out the RL delay.
    """
    if auxiliary.is_set(_inp_params, 'rldly_calibsteps'):
        if 'delay' not in _inp_params.rldly_calibsteps:
            print('    Skipping rldelay calibration as specified by the rldly_calibsteps input parameter.')
            return False
        else:
            pass
    if len(_ms_metadata.correlations) < 3:
        print('    Skipping rldelay calibration as no cross-hand correlations are present in the data.')
        return False
    #if auxiliary.is_set(_inp_params, 'rldelay_per_station'):
    #    print('    Computing RLdelays separately for each station, which are later ad-hoc inserted by applycal.')
    #    if not auxiliary.is_set(_inp_params, 'calibrators_rldly'):
    #        print('    Never mind. No RL delay calibration is performed because calibrators_rldly is not set.')
    #    refant   = _inp_params.refant.split(',')[0]
    #    all_ants = _ms_metadata.antenna_set
    #    for ant in all_ants:
    #        if ant==refant:
    #            continue
    #        this_caltable = caltable + '.__{0}__'.format(ant)
    #        tmp_tables    = []
    #        _basel        = '{0}&{1}'.format(refant, ant)
    #        for source in _inp_params.calibrators_rldly.split(','):
    #            this_table = auxiliary.unique_filename(caltable + '.' + source)
    #            calibration.task_gaincal_general(_inp_params, _ms_metadata, this_table, gaintype='KCROSS', antenna=_basel,
    #                                             field=source, spw=_spwpartition, combine=_inp_params.rldelay_axis_combine,
    #                                             minblperant=0, minsnr=_inp_params.rldly_minsnr, smodel=[1,1,0,0], calmode='p',
    #                                             gaintable=_fly_calib_tables, interp=_fly_calib_interp,
    #                                             gainfield=_fly_calib_gainfd
    #                                            )
    #            tmp_tables.append(this_table)
    #        calibration.avg_caltables(_inp_params, tmp_tables, this_caltable, pass_missing=True)
    #    return True
    #if auxiliary.is_set(_inp_params, 'Use_ALMA_rldly') and 'AA' in _ms_metadata.antennanames:
    #    print('    Skipping this polarization calibration step, since ALMA will be used to do this later.')
    #    return False
    if auxiliary.is_set(_inp_params, 'calibrators_rldly'):
        avg_spwdelay = False
        tmp_tables   = []
        if auxiliary.is_set(_inp_params, 'rldly_stations'):
            these_ants = _inp_params.rldly_stations
        else:
            these_ants = ''
        if auxiliary.is_set(_inp_params, 'rldelay_axis_combine'):
            this_combine = _inp_params.rldelay_axis_combine
            if '!' in this_combine:
                this_combine  = this_combine.replace('!', '')
                this_combine2 = this_combine.replace('spw', '')
                this_combine2 = this_combine2.replace(' ', '')
                if this_combine2[0] == ',':
                    this_combine2 = this_combine2[1:]
                if this_combine2[-1] == ',':
                    this_combine2 = this_combine2[:-1]
                avg_spwdelay  = True
                tmp_tables2   = []
            else:
                pass
        else:
            this_combine = 'scan'
        for source in _inp_params.calibrators_rldly.split(','):
            this_table = auxiliary.unique_filename(caltable + '.' + source)
            calibration.task_gaincal_general(_inp_params, _ms_metadata, this_table, gaintype='KCROSS',
                                             field=source, spw=_spwpartition, antenna=these_ants,
                                             combine=this_combine, minblperant=0,
                                             minsnr=_inp_params.rldly_minsnr, smodel=_inp_params.rldly_smodels[source],
                                             calmode='p',
                                             gaintable=_fly_calib_tables, interp=_fly_calib_interp, gainfield=_fly_calib_gainfd
                                            )
            tmp_tables.append(this_table)
            if avg_spwdelay:
                this_table2 = auxiliary.unique_filename(caltable + '.2.' + source)
                calibration.task_gaincal_general(_inp_params, _ms_metadata, this_table2, gaintype='KCROSS',
                                                 field=source, spw=_spwpartition, antenna=these_ants,
                                                 combine=this_combine2, minblperant=0,
                                                 minsnr=_inp_params.rldly_minsnr, smodel=_inp_params.rldly_smodels[source],
                                                 calmode='p',
                                                 gaintable=_fly_calib_tables, interp=_fly_calib_interp,
                                                 gainfield=_fly_calib_gainfd
                                                )
                tmp_tables2.append(this_table2)
        calibration.avg_caltables(_inp_params, tmp_tables, caltable)
        if avg_spwdelay:
            calibration.avg_caltables(_inp_params, tmp_tables2, caltable+'.2')
            calibration.overwrite_solutions_by_spwavg(caltable, caltable+'.2')
        return True
    else:
        print('    Skipping rldelay calibration as no rldly calibrators were specified.')
        return False


def task_rlphase(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                 _spwpartition, _mpi_client):
    """
    Executes CASA's polcal task for polarization experiments to take out the RL phase,
      either per channel (high SNR) or per spw.
    """
    if auxiliary.is_set(_inp_params, 'rldly_calibsteps'):
        if 'phase' not in _inp_params.rldly_calibsteps:
            print('    Skipping rlphase calibration as specified by the rldly_calibsteps input parameter.')
            return False
        else:
            pass
    if len(_ms_metadata.correlations) < 3:
        print('    Skipping rlphase calibration as no cross-hand correlations are present in the data.')
        return False
    if auxiliary.is_set(_inp_params, 'calibrators_rldly'):
        if _inp_params.rldly_instrphase_solve == 'channel':
            _solint   = 'inf'
        elif isinstance(_inp_params.rldly_instrphase_solve, float) or isinstance(_inp_params.rldly_instrphase_solve, int):
            _solint   = 'inf,{0}'.format(str(int(_inp_params.rldly_instrphase_solve)))
        elif _inp_params.rldly_instrphase_solve == 'spw':
            _numchans = str(_ms_metadata.channels[0])
            _solint   = 'inf,'+_numchans
        else:
            raise ValueError('No valid rldly_instrphase_solve provided. Must be either channel or spw.')
        tmp_tables = []
        if auxiliary.is_set(_inp_params, 'rldly_stations'):
            these_ants = _inp_params.rldly_stations
        else:
            these_ants = ''
        for source in _inp_params.calibrators_rldly.split(','):
            this_table = auxiliary.unique_filename(caltable + '.' + source)
            task_polcal_general(_inp_params, _ms_metadata, this_table, field=source, spw=_spwpartition,
                                antenna=these_ants, solint=_solint, minblperant=0,
                                minsnr=_inp_params.rldly_minsnr, poltype='Xf', smodel=_inp_params.rldly_smodels[source],
                                gaintable=_fly_calib_tables, interp=_fly_calib_interp, gainfield=_fly_calib_gainfd
                               )
            tmp_tables.append(this_table)
        calibration.avg_caltables(_inp_params, tmp_tables, caltable)
        calibration.remove_amp_corr(_inp_params, caltable)
        return True
    else:
        print('    Skipping rlphase calibration as no rldly calibrators were specified.')
        return False


def task_dterms(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                _spwpartition, _mpi_client):
    """
    Executes CASA's polcal task for polarization experiments to calibrate for the polarization leakage (D-terms),
      either per channel (extremely high SNR, dream on...), per spw (good SNR) or for all spw combined.
    """
    if len(_ms_metadata.correlations) < 3:
        print('    Skipping D-terms calibration as no cross-hand correlations are present in the data.')
        return False
    elif auxiliary.is_set(_inp_params, 'calibrators_dterms'):
        if _inp_params.dterms_solve == 'channel':
            _poltype    = 'Df+QU'
            _solint     = 'inf'
            _combine    = 'scan'
            _extend_spw = False
        elif isinstance(_inp_params.dterms_solve, float) or isinstance(_inp_params.dterms_solve, int):
            _poltype    = 'Df+QU'
            _solint     = 'inf,{0}'.format(str(int(_inp_params.dterms_solve)))
            _combine    = 'scan'
            _extend_spw = False
        elif _inp_params.dterms_solve == 'spw':
            _poltype    = 'D+QU'
            _solint     = 'inf'
            _combine    = 'scan'
            _extend_spw = False
        elif _inp_params.dterms_solve == 'combined':
            _poltype    = 'D+QU'
            _solint     = 'inf'
            _combine    = 'scan,spw'
            _extend_spw = True
        else:
            raise ValueError('No valid dterms_solve provided. Must be either channel, spw, or combined.')
        tmp_tables = []
        for source in _inp_params.calibrators_dterms.split(','):
            this_table = auxiliary.unique_filename(caltable + '.' + source)
            task_polcal_general(_inp_params, _ms_metadata, this_table, field=source, spw=_spwpartition, solint=_solint,
                                combine=_combine, minblperant=3, minsnr=_inp_params.dterms_minsnr, poltype=_poltype,
                                gaintable=_fly_calib_tables, interp=_fly_calib_interp, gainfield=_fly_calib_gainfd
                               )
            tmp_tables.append(this_table)
        calibration.avg_caltables(_inp_params, tmp_tables, caltable)
        #if _extend_spw:
        #    auxiliary.extend_solutions_to_all_spw(_ms_metadata, caltable)
        return True
    else:
        print('    Skipping D-terms calibration as no dterms calibrators were specified.')
        return False


def task_polcal_general(_inp_params, _ms_metadata, caltable, field='', spw='', antenna='', scan='', solint='inf',
                        combine='scan', preavg=0.0, minblperant=4, minsnr=3.0, poltype='D+QU', smodel=[], append=False,
                        gaintable=[], gainfield=[], interp=[], spwmap=[]):
    """
    Generic polcal task. Used to take out instrumental RL phase offsets and to determine D-terms.
    """
    if field:
        gaintable, interp, gainfield, _, _ = calibration.get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve', field)
    else:
        gainfield = calibration.get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve')
    if not spwmap:
        spwmap = auxiliary.get_spwmap(_inp_params, _ms_metadata, gaintable)
    tasks.polcal(vis                =  _inp_params.ms_name,
                 caltable           =  caltable,
                 field              =  field,
                 spw                =  spw,
                 intent             =  "",
                 selectdata         =  True,
                 timerange          =  "",
                 uvrange            =  "",
                 antenna            =  antenna,
                 scan               =  scan,
                 observation        =  "",
                 msselect           =  "",
                 solint             =  solint,
                 combine            =  combine,
                 preavg             =  preavg,
                 refant             =  str(_inp_params.refant),
                 minblperant        =  minblperant,
                 minsnr             =  minsnr,
                 poltype            =  poltype,
                 smodel             =  smodel,
                 append             =  append,
                 docallib           =  False,
                 callib             =  "",
                 gaintable          =  gaintable,
                 gainfield          =  gainfield,
                 interp             =  interp,
                 spwmap             =  spwmap
                )
