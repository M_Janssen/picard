#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Ad-hoc calibration methods outside of the proper CASA framework. """
import os
import copy
import json
from collections import defaultdict
from ast import literal_eval
import numpy as np
from scipy.interpolate import interp1d
import pipe_modules.auxiliary as auxiliary
from pipe_modules.default_casa_imports import *


def my_rerefant(_inp_params, _ms_metadata, caltb):
    """
    The CASA rerefant task is likely to flag data when no direct re-referencing route is found to neighboring scans,
    which is not practical for many VLBI experiments.
    This custom re-referencing function offers a robust alternative to re-reference all fringe solutions in a Fringe Jones table
    to a single reference station as follows:
        - All flagged fringe solutions are discarded.
        - The first available station from the _inp_params.refant list available in caltb will be chosen as the
          primary refant (PR).
          It is the users responsibility to ensure that all fringe calibration tables share the same PR and that no
          single pol stations are used as refant, unless all stations are single pol. Similarly, the refant stations must have
          data for all spectral windows.
          A warning will be printed when PR!=_inp_params.refant[0].
        - All scans where ANTENNA2>0 and ANTENNA2!=PR are identified. The scans are called RS and the scan refant RS_A2.
        - Unless, all fringes to RS_A2 are single pol in the RS scan, dual-pol fringe solutions will be required for
          re-referencing.
        - The closest fringe solution between PR and RS_A2 from the central time of RS will be chosen for the re-referencing.
          Note #1: This means that no higher order re-referencing (i.e., s1->s2->s3->...) will be done.
          Note #2: This also means that fringe solutions at the start and end of scans will typically be used for the
                   re-referencing. It is the users responsibility to ensure that these are not 'bad' fringe solutions, by
                   'quacking' bad data if needed.
        - The ANTENNA2 column will remain unchanged to keep track of the original refant used.
        - Lists are printed for the scans that could and could not be re-referenced if _inp_params.verbose is set.
        - For the re-referencing, the fringe solutions of alternative refants relative to the PR are added to the fringe
          solutions of all antennas. Thereby, only the R-L differences are effectively re-referenced for mb fringefit solutions,
          while global phase stability is maintained for sb solutions.
    """
    if not auxiliary.isdir(caltb):
        return
    PRs = [_ms_metadata.yield_antID(r) for r in _inp_params.refant.split(',')]
    if len(PRs) == 1:
        return
    mytb         = casac.table()
    scans, flags = auxiliary.read_CASA_table(_inp_params, 'SCAN_NUMBER, FLAG', _tablename=caltb)
    flags        = np.logical_not(   np.mean(np.mean(  np.logical_not(flags)  , 0), 0)   ).astype(int)
    scans        = np.unique(auxiliary.remove_flagged_values(scans, flags))
    RS           = {}
    PR           = -1
    PRindex      = 1337
    for scan in scans:
        # Only one refant per scan.
        qcrit = 'SCAN_NUMBER=='+str(scan)
        ant2  = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA2', qcrit, _tablename=caltb)[0]
        if ant2 < 0:
            continue
        RS[scan]   = ant2
        this_index = PRs.index(ant2)
        if ant2 in PRs and this_index < PRindex:
            PR      = ant2
            PRindex = this_index
    if PR < 0:
        print('Warning: Cannot re-reference {0}'.format(caltb))
        return
    if PR != PRs[0]:
        an1 = str(_ms_metadata.yield_antname(PR))
        an2 = str(_ms_metadata.yield_antname(PRs[0]))
        print('Warning: {0} is chosen as primary refant instead of the preferred {1} antenna.'.format(an1, an2))
    official_refant_list = auxiliary.store_object(caltb+'.refants', _operation='read', _ignore_missing=True)
    if official_refant_list:
        RS = official_refant_list
    else:
        print('Warning: Could not find a refant list for {0}. Will use the old method to determine refants.'.format(caltb))
    for scan in list(RS.keys()):
        if RS[scan] == PR:
            del RS[scan]
        else:
            pass
    if not RS:
        # All scans already referenced to the primary refant.
        return
    reref_dict = defaultdict(dict)
    reref_yeah = []
    reref_nope = []
    for scan in RS:
        RS_A2     = RS[scan]
        scan_time = _ms_metadata.yield_scantime(str(scan))
        # Re-referencing per spw.
        qcrit0      = 'SCAN_NUMBER=='+str(scan)
        spws        = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', qcrit0, _tablename=caltb)
        could_reref = False
        for spw in spws:
            # Check if RS_A2 is a single-pol station.
            qcrit = qcrit0 + ' && SPECTRAL_WINDOW_ID=='+str(spw)
            flags = auxiliary.read_CASA_table(_inp_params, 'FLAG', qcrit, _tablename=caltb)
            spol  = 0
            if all(np.unique(flags[_inp_params.F_0PHAS]) == [True]):
                spol+= 1
            if all(np.unique(flags[_inp_params.F_1PHAS]) == [True]):
                spol+= 10
            if spol == 11:
                # No valid data for this spw.
                continue
            # Get the re-referencing data.
            qcrit                = 'SPECTRAL_WINDOW_ID=={0} && ANTENNA1=={1} && ANTENNA2=={2}'.format(str(spw), str(RS_A2),
                                                                                                      str(PR)
                                                                                                     )
            rtime, rparam, rflag = auxiliary.read_CASA_table(_inp_params, 'TIME, FPARAM, FLAG', qcrit, _tablename=caltb)
            if not any(rtime):
                continue
            if spol:
                indx  = {1: _inp_params.F_1PHAS, 10:_inp_params.F_0PHAS}[spol]
                rflag = np.mean( flags[indx] ).astype(bool)
            else:
                # Data from both pols must be unflagged.
                rflag = np.mean(  np.mean(rflag, 0)  , 0).astype(bool)
            goodtimes = auxiliary.remove_flagged_values(rtime, rflag)
            if not any(goodtimes):
                continue
            closest_match = np.abs(goodtimes - scan_time).argmin()
            # Find the index in the original time-series of data (before flagging).
            findx                           = np.where(rtime == goodtimes[closest_match])[0][0]
            reref_dict[int(scan)][int(spw)] = np.take(rparam, findx, -1)
            could_reref                     = True
        if could_reref:
            reref_yeah.append(int(scan))
        else:
            reref_nope.append(int(scan))
    if any(reref_yeah):
        # Go through every row in the table and re-reference by adding up the fringe solutions.
        mytb.open(caltb, nomodify=False)
        row_iter = range(mytb.nrows())
        for row in row_iter:
            scan = int(mytb.getcell('SCAN_NUMBER', row))
            if scan in reref_yeah:
                spw = int(mytb.getcell('SPECTRAL_WINDOW_ID', row))
                if spw in reref_dict[scan]:
                    fparam = mytb.getcell('FPARAM', row) + reref_dict[scan][spw]
                    mytb.putcell('FPARAM', row, fparam)
                else:
                    pass
        mytb.flush()
        mytb.done()
        mytb.clearlocks()
    if _inp_params.verbose:
        print ('  Re-referenced fringe solutions across scans in {0}'.format(caltb))
        print ('  These scans were re-referenced to the common {0} refant:'.format(_ms_metadata.yield_antname(PR)))
        print (auxiliary.wrap_list(reref_yeah, items_per_line=7, indent='    '))
        print ('  And these could not be re-referenced:')
        print (auxiliary.wrap_list(reref_nope, items_per_line=7, indent='    '))


def multi_band_reref_across_scans(_inp_params, _ms_metadata, caltb, closest_scan=False):
    """
    #*Unused: Skipped to be compatible with Difmap startmod (phases steered to zero on most sensitive baselines).*
    The my_rerefant task above ensures R-L phase stability for *instrumental solutions*.
    For subsequent multi-band fringefit solutions, we want to ensure stability of baseline phases across scans when different
    reference antennas are selected. We do this as a final step after all incremental fringefit tables have been obtained to
    prevent intermediate fringefit steps from undoing our phase corrections by steering the phases to the original values again
    (the ones of the commonly used pointsource model).
    Thus, this step should be done immediately after fringefit_multi_cal_coher and multi_cal_short. For fringefit_multi_sci_long
    and fringefit_multi_sci_long_spwant, it should be run before atmo_sc or applycal.
    Per source, antenna, spw, pol, we offset *only the antenna phases* for all scans where a secondary refant is used based on
    the phase difference between the fringe solutions of the closest two scans where the secondary refant and the primary
    refant were used, respectively, if closest_scan is set to True. Else, offset phases by difference in median phase solutions
    per refant.
    """
    if not auxiliary.isdir(caltb):
        return
    PRs = [_ms_metadata.yield_antID(r) for r in _inp_params.refant.split(',')]
    if len(PRs) == 1:
        return
    mytb         = casac.table()
    scans, flags = auxiliary.read_CASA_table(_inp_params, 'SCAN_NUMBER, FLAG', _tablename=caltb)
    flags        = np.logical_not(   np.mean(np.mean(  np.logical_not(flags)  , 0), 0)   ).astype(int)
    scans        = np.unique(auxiliary.remove_flagged_values(scans, flags))
    RS           = {}
    PR           = -1
    PRindex      = 1337
    for scan in scans:
        # Only one refant per scan.
        qcrit = 'SCAN_NUMBER=='+str(scan)
        ant2  = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA2', qcrit, _tablename=caltb)[0]
        if ant2 < 0:
            continue
        RS[scan]   = ant2
        this_index = PRs.index(ant2)
        if ant2 in PRs and this_index < PRindex:
            PR      = ant2
            PRindex = this_index
    if PR < 0:
        print('Warning: Cannot re-reference {0}'.format(caltb))
        return
    if PR != PRs[0]:
        an1 = str(_ms_metadata.yield_antname(PR))
        an2 = str(_ms_metadata.yield_antname(PRs[0]))
        print('Warning: {0} is chosen as primary refant instead of the preferred {1} antenna.'.format(an1, an2))
    official_refant_list = auxiliary.store_object(caltb+'.refants', _operation='read', _ignore_missing=True)
    if official_refant_list:
        RS = copy.deepcopy(official_refant_list)
    else:
        raise ValueError('Could not find a refant list for {0}.'.format(caltb))
    for scan in list(RS.keys()):
        if RS[scan] == PR:
            del RS[scan]
        else:
            pass
    if not RS:
        # All scans already referenced to the primary refant.
        return
    #per source: [src][spw][refant][scan][ant1] = [r phase sol, l phase sol]
    reref_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(dict))))
    for scan in scans:
        this_scan   = str(scan)
        this_src    = _ms_metadata.yield_sourcename_from_scan(this_scan)
        this_refant = official_refant_list[this_scan]
        qcrit0      = 'SCAN_NUMBER=='+this_scan
        these_spws  = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', qcrit0, _tablename=caltb)
        these_ant1s = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', qcrit0, _tablename=caltb)
        for this_spw in these_spws:
            for this_ant1 in these_ant1s:
                qcrit         = qcrit0 + ' && SPECTRAL_WINDOW_ID=='+str(this_spw) + ' && ANTENNA1=='+str(this_ant1)
                flags, fparam = auxiliary.read_CASA_table(_inp_params, 'FLAG, FPARAM', qcrit, _tablename=caltb)
                this_fparam   = np.ma.masked_array(fparam, flags)
                med_fparam    = np.ma.median(this_fparam, axis=-1)
                if med_fparam.mask.all():
                    continue
                polphases = np.array([med_fparam[_inp_params.F_0PHAS][0], med_fparam[_inp_params.F_1PHAS][0]])
                reref_dict[this_src][int(this_spw)][this_refant][int(this_ant1)][int(this_scan)] = polphases
    something_to_reref = False
    for src in reref_dict:
        #[spw][scan][ant1] = [r phase diff, l phase diff]
        reref_sols = defaultdict(lambda: defaultdict(dict))
        for spw in reref_dict[src]:
            refants = np.unique(list(reref_dict[src][spw].keys()))
            if len(refants) == 1:
                continue
            if PR not in refants:
                continue
            for ant2 in refants:
                if str(ant2) == str(PR):
                    continue
                for ant1 in list(reref_dict[src][spw][ant2].keys()):
                    primary_scans = list(reref_dict[src][spw][PR][ant1].keys())
                    if not primary_scans:
                        continue
                    primary_scans = np.array(sorted([int(s) for s in primary_scans]))
                    ref_scans     = list(reref_dict[src][spw][ant2][ant1].keys())
                    if not ref_scans:
                        continue
                    ref_scans              = np.array(sorted([int(s) for s in ref_scans]))
                    if closest_scan:
                        matches                = [(np.abs(primary_scans - rs)).argmin() for rs in ref_scans]
                        rpoints                = np.unique(matches)
                        sol_diffs              = {}
                        last_invrt             = min(min(np.where(matches==rpoints[-1]))) #TODO: maybe this should be max(max()) here. E.g., for primary_scans=[11 12 13 14 15 16 17] and ref_scans=[ 1  2  3  4  5  6  7  8  9 10].
                        sol_diffs[rpoints[-1]] = reref_dict[src][spw][PR][ant1][primary_scans[rpoints[-1]]] - \
                                                 reref_dict[src][spw][ant2][ant1][ref_scans[last_invrt]]
                        for rp in rpoints[:-1]:
                            sol_diffs[rp] = reref_dict[src][spw][PR][ant1][primary_scans[rp]] - \
                                            reref_dict[src][spw][ant2][ant1][ref_scans[max(max(np.where(matches==rp)))]]
                        for ref_scan, match in zip(ref_scans, matches):
                            reref_sols[spw][ref_scan][ant1] = sol_diffs[match]
                            something_to_reref              = True
                    else:
                        this_ref    = np.median(list(reref_dict[src][spw][ant2][ant1].values()), axis=0)
                        primary_ref = np.median(list(reref_dict[src][spw][PR][ant1].values()), axis=0)
                        for ref_scan in ref_scans:
                            reref_sols[spw][ref_scan][ant1] = primary_ref - this_ref
                            something_to_reref              = True
    if something_to_reref:
        mytb.open(caltb, nomodify=False)
        row_iter = range(mytb.nrows())
        for row in row_iter:
            scan = int(mytb.getcell('SCAN_NUMBER', row))
            spw  = int(mytb.getcell('SPECTRAL_WINDOW_ID', row))
            ant1 = int(mytb.getcell('ANTENNA1', row))
            try:
                fparam = mytb.getcell('FPARAM', row)
                fparam[_inp_params.F_0PHAS]+= np.array([reref_sols[spw][scan][ant1][0]])
                fparam[_inp_params.F_1PHAS]+= np.array([reref_sols[spw][scan][ant1][1]])
                mytb.putcell('FPARAM', row, fparam)
            except KeyError:
                pass
        mytb.flush()
        mytb.done()
        mytb.clearlocks()


def find_line_peak_autocorrs(_inp_params, _ms_metadata):
    """
    Averages RR and LL of all autocorrelation spectra together to find the channel corresponding to the highest peak
    of a spectral line.
    See spectral_line parameter in input/observation.inp.
    """
    if not auxiliary.is_set(_inp_params, 'spectral_line'):
        return ''
    if _inp_params.spectral_line != 'search':
        return ''
    if not _inp_params.science_target:
        raise ValueError('Cannot determine spectral line location without a specified science target.')
    if len(_inp_params.science_target.split(',')) > 1:
        raise ValueError('Cannot determine spectral line location for multiple science targets. Please set it by hand.')
    if len(_ms_metadata.correlations) == 1:
        #only need data[0]
        multi_pol = False
    else:
        #need data[0] and data[-1] (RR,LL autocorr have no complex part but RL and LR do)
        multi_pol = True
    print('\nSearching for the line locations. This will take a while.\n')
    cals = {}
    for a in _ms_metadata.antennaIDs:
        cals[a] = 1.
    bandpass_calib = [_inp_params.calib_scalar_bandpass[0], _inp_params.calib_complex_bandpass[0]]
    for bc in bandpass_calib:
        if auxiliary.isdir(bc):
            if _ms_metadata.line_spw>-1:
                qcrit='SPECTRAL_WINDOW_ID=='+str(_ms_metadata.line_spw)
            else:
                qcrit=''
        calants = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _query_criteria=qcrit, _tablename=bc)
        for a in calants:
            this_where = qcrit
            if this_where:
                this_where+= ' && ANTENNA1=='+str(a)
            else:
                this_where = 'ANTENNA1=='+str(a)
            cparam = auxiliary.read_CASA_table(_inp_params, 'CPARAM', _query_criteria=this_where,_tablename=bc)
            cals[a]*= np.abs(np.median(cparam, axis=-1))
    sci_scans = auxiliary.flatten_list(_ms_metadata.yield_scans(_ms_metadata.yield_science_targets(_inp_params)), True)
    sci_scans = [int(s) for s in sci_scans]
    qcrit     = 'ANTENNA1==ANTENNA2'
    if _ms_metadata.line_spw>-1:
        qcrit+=' && DATA_DESC_ID=='+str(_ms_metadata.line_spw)
    line_locs  = {}
    line_sum   = []
    for s in sci_scans:
        flag, data, wgt, ant = auxiliary.read_CASA_table(_inp_params, 'FLAG, DATA, WEIGHT, ANTENNA1',
                                                         _query_criteria=qcrit + ' && SCAN_NUMBER=='+str(s)
                                                        )
        if multi_pol:
            data = np.take(data, indices = [0,-1], axis = 0)
            flag = np.take(flag, indices = [0,-1], axis = 0)
            wgt  = np.take(wgt,  indices = [0,-1], axis = 0)
        gains = np.array([cals[a] for a in ant])
        gains = np.moveaxis(gains, 0, -1)
        data /= gains
        wgt  = np.repeat(wgt[:,np.newaxis,:], np.shape(data)[1], 1)
        data = np.ma.masked_array(data, flag)
        data = np.ma.average(data, axis=(0,-1), weights=wgt)
        if np.ma.count(data) == 0:
            line_locs[s] = -1
        else:
            lloc = np.ma.argmax(data)
            line_sum.append(lloc)
            line_locs[s] = lloc
    line_sum = int(np.median(line_sum))
    good_scs = []
    for s in sci_scans:
        val = line_locs[s]
        if abs(val - line_sum)>_inp_params.line_var:
            line_locs[s] = -1
        if line_locs[s] >-1:
            good_scs.append(s)
    for s in sci_scans:
        val = line_locs[s]
        if val < 0:
            if good_scs:
                val = line_locs[min(good_scs, key=lambda x:abs(x-s))]
            else:
                val = line_sum
            line_locs[s] = val
    for s in sci_scans:
        val = line_locs[s]
        if _ms_metadata.line_spw>-1:
            val = str(_ms_metadata.line_spw)+':'+str(val)
        else:
            val = '0:'+str(val)
        line_locs[s] = val
    if _ms_metadata.line_spw>-1:
        line_sum = str(_ms_metadata.line_spw)+':'+str(line_sum)
    else:
        line_sum = '0:'+str(line_sum)
    with open('spectral_line_peak_channel.txt', 'w', encoding='utf-8') as f:
        for s in sci_scans:
            f.write('{0} {1}\n'.format(s, line_locs[s]))
        f.write('avg {0}'.format(line_sum))
    return lloc


def rldelay_per_station_constant(_inp_params, _ms_metadata, station_delays):
    """
    *Unused*: This could be used as a diagnostic but it should not really be necessary if R and L are fringe-fitted properly.
    Puts in delays for RL and LR correlations while leaving RR and LL unaffected.
    The delays (which can be different for different station IDs) are to be given in the station_delays dicts. They are assumed
    to be constant over experiments.
    """
    if not station_delays:
        return
    mytb = casac.table()
    if _inp_params.verbose:
        print('Pre-computing RL phases from delay values that are to be applied...')
    ph_rot = defaultdict(lambda: defaultdict(dict))
    ant1s  = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1')
    ant2s  = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA2')
    spws   = auxiliary.read_CASA_table(_inp_params, 'unique DATA_DESC_ID')
    for ant1 in ant1s:
        for ant2 in ant2s:
            if ant1==ant2:
                continue
            try:
                delay1 = station_delays[ant1]
            except KeyError:
                delay1 = 0
            try:
                delay2 = station_delays[ant2]
            except KeyError:
                delay2 = 0
            this_delay = delay1 + delay2
            if not this_delay:
                continue
            for spw in spws:
                this_freq               = np.asarray(_ms_metadata.yield_freq_of_channels_in_spw(spw))
                ph_rot[ant1][ant2][spw] = auxiliary.delay2phase(this_delay, this_freq*1.e-9,
                                                                _ms_metadata.reference_frequency*1.e-9
                                                               )
    if _inp_params.verbose:
        print('Applying computed phases to the data...')
    mytb.open(_inp_params.ms_name, nomodify=False)
    _nrows   = mytb.nrows()
    all_rows = range(_nrows)
    _prt0    = 0
    _prt1    = 1
    for row in all_rows:
        _prt0, _prt1 = auxiliary.progress_print(row, _nrows, _prt0, _prt1)
        antenna1     = mytb.getcell('ANTENNA1', row)
        antenna2     = mytb.getcell('ANTENNA2', row)
        this_spw     = mytb.getcell('DATA_DESC_ID', row)
        try:
            this_phase = ph_rot[antenna1][antenna2][this_spw]
        except KeyError:
            continue
        this_data    = mytb.getcell('CORRECTED_DATA', row)
        this_data[1] = auxiliary.phase_rotate(this_data[1], this_phase)
        this_data[2] = auxiliary.phase_rotate(this_data[2], -this_phase)
        mytb.putcell('CORRECTED_DATA', row, this_data)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def rldelay_per_station_per_scan(_inp_params, _ms_metadata, station_delays_sci, station_delays_cal):
    """
    *Unused*: This could be used as a diagnostic but it should not really be necessary if R and L are fringe-fitted properly.
    Puts in delays for RL and LR correlations while leaving RR and LL unaffected.
    The delays (which can be different for different station IDs) are to be given in the station_delays dicts for sci and cal
    targets for each fringe-fit calibration table that is to applied to the data per scan.
    """
    if _inp_params.verbose:
        print('Pre-computing RL phases from delay values that are to be applied...')
    ph_rot    = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    all_scans = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER')
    for scan in all_scans:
        this_src = _ms_metadata.yield_sourcename_from_scan(scan)
        if this_src in _ms_metadata.yield_science_targets(_inp_params):
            station_delays = station_delays_sci
        elif this_src in _ms_metadata.yield_calibrators(_inp_params):
            station_delays = station_delays_cal
        else:
            continue
        qselc = 'SCAN_NUMBER=={0}'.format(str(scan))
        ant1s = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', qselc)
        ant2s = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA2', qselc)
        spws  = auxiliary.read_CASA_table(_inp_params, 'unique DATA_DESC_ID', qselc)
        for ant1 in ant1s:
            for ant2 in ant2s:
                if ant1==ant2:
                    continue
                delay1_sum = 0
                delay2_sum = 0
                for caltb in station_delays:
                    if ant1 in station_delays[caltb]:
                        available_scans = list(station_delays[caltb][ant1].keys())
                        closest_scan    = auxiliary.closest_match(available_scans, scan)
                        delay1_sum      += station_delays[caltb][ant1][closest_scan]
                    if ant2 in station_delays[caltb]:
                        available_scans = list(station_delays[caltb][ant2].keys())
                        closest_scan    = auxiliary.closest_match(available_scans, scan)
                        delay2_sum      += station_delays[caltb][ant2][closest_scan]
                    else:
                        pass
                this_delay = delay1_sum - delay2_sum
                if not this_delay:
                    continue
                for spw in spws:
                    this_freq                     = np.asarray(_ms_metadata.yield_freq_of_channels_in_spw(spw))
                    ph_rot[scan][ant1][ant2][spw] = auxiliary.delay2phase(this_delay, this_freq*1.e-9,
                                                                          _ms_metadata.reference_frequency*1.e-9
                                                                         )
    if _inp_params.verbose:
        print('Applying computed phases to the data...')
    mytb = casac.table()
    mytb.open(_inp_params.ms_name, nomodify=False)
    _nrows   = mytb.nrows()
    all_rows = range(_nrows)
    _prt0 = 0
    _prt1 = 1
    for row in all_rows:
        _prt0, _prt1 = auxiliary.progress_print(row, _nrows, _prt0, _prt1)
        antenna1     = mytb.getcell('ANTENNA1', row)
        antenna2     = mytb.getcell('ANTENNA2', row)
        this_spw     = mytb.getcell('DATA_DESC_ID', row)
        this_scan    = mytb.getcell('SCAN_NUMBER', row)
        try:
            this_phase = ph_rot[this_scan][antenna1][antenna2][this_spw]
        except KeyError:
            continue
        this_data    = mytb.getcell('CORRECTED_DATA', row)
        this_data[1] = auxiliary.phase_rotate(this_data[1], this_phase)
        this_data[2] = auxiliary.phase_rotate(this_data[2], -this_phase)
        mytb.putcell('CORRECTED_DATA', row, this_data)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()



############################
#                          #
# start NN phase jump code #
#                          #
############################



#nn_band1 = np.array([2.12364e+11, 2.12428e+11, 2.12492e+11, 2.12556e+11, 2.12620e+11,
#       2.12684e+11, 2.12748e+11, 2.12812e+11, 2.12876e+11, 2.12940e+11,
#       2.13004e+11, 2.13068e+11, 2.13132e+11, 2.13196e+11, 2.13260e+11,
#       2.13324e+11, 2.13388e+11, 2.13452e+11, 2.13516e+11, 2.13580e+11,
#       2.13644e+11, 2.13708e+11, 2.13772e+11, 2.13836e+11, 2.13900e+11,
#       2.13964e+11, 2.14028e+11, 2.14092e+11, 2.14156e+11, 2.14220e+11,
#       2.14284e+11, 2.14348e+11]) ## WARNING on sky-frequencies as derived from NOEMA .xlsx by Roberto Garcia

nn_band1 = np.array([2.12364e+11, 2.12428e+11, 2.12492e+11, 2.12556e+11, 2.12620e+11,
       2.12684e+11, 2.12748e+11, 2.12812e+11, 2.12876e+11, 2.12940e+11,
       2.13004e+11, 2.13068e+11, 2.13132e+11, 2.13196e+11, 2.13260e+11,
       2.13324e+11, 2.13388e+11, 2.13452e+11, 2.13516e+11, 2.13580e+11,
       2.13644e+11, 2.13708e+11, 2.13772e+11, 2.13836e+11, 2.13900e+11,
       2.13964e+11, 2.14028e+11, 2.14092e+11])


nn_band2 = np.array([2.14156e+11, 2.14220e+11, 2.14284e+11, 2.14348e+11, 2.14412e+11,
       2.14476e+11, 2.14540e+11, 2.14604e+11, 2.14668e+11, 2.14732e+11,
       2.14796e+11, 2.14860e+11, 2.14924e+11, 2.14988e+11, 2.15052e+11,
       2.15116e+11, 2.15180e+11, 2.15244e+11, 2.15308e+11, 2.15372e+11,
       2.15436e+11, 2.15500e+11, 2.15564e+11, 2.15628e+11, 2.15692e+11,
       2.15756e+11, 2.15820e+11, 2.15884e+11, 2.15948e+11, 2.16012e+11,
       2.16076e+11])
#nn_band3 = np.array([2.25868e+11, 2.25932e+11, 2.25996e+11, 2.26060e+11, 2.26124e+11,
#       2.26188e+11, 2.26252e+11, 2.26316e+11, 2.26380e+11, 2.26444e+11,
#       2.26508e+11, 2.26572e+11, 2.26636e+11, 2.26700e+11, 2.26764e+11,
#       2.26828e+11, 2.26892e+11, 2.26956e+11, 2.27020e+11, 2.27084e+11,
#       2.27148e+11, 2.27212e+11, 2.27276e+11, 2.27340e+11, 2.27404e+11,
#       2.27468e+11, 2.27532e+11, 2.27596e+11, 2.27660e+11, 2.27724e+11,
#       2.27788e+11, 2.27852e+11, 2.27916e+11, 2.27980e+11, 2.28044e+11,
#       2.28108e+11]) ## WARNING not validated ## apdated to account for non-eht sky frequencies

nn_band3 = np.array([2.26124e+11, 2.26188e+11, 2.26252e+11, 2.26316e+11, 2.26380e+11,
       2.26444e+11, 2.26508e+11, 2.26572e+11, 2.26636e+11, 2.26700e+11,
       2.26764e+11, 2.26828e+11, 2.26892e+11, 2.26956e+11, 2.27020e+11,
       2.27084e+11, 2.27148e+11, 2.27212e+11, 2.27276e+11, 2.27340e+11,
       2.27404e+11, 2.27468e+11, 2.27532e+11, 2.27596e+11, 2.27660e+11,
       2.27724e+11, 2.27788e+11, 2.27852e+11, 2.27916e+11, 2.27980e+11])


nn_band4 = np.sort([2.28108e+11,
       2.28172e+11, 2.28236e+11, 2.28300e+11, 2.28364e+11, 2.28428e+11,
       2.28492e+11, 2.28556e+11, 2.28620e+11, 2.28684e+11, 2.28748e+11,
       2.28812e+11, 2.28876e+11, 2.28940e+11, 2.29004e+11, 2.29068e+11,
       2.29132e+11, 2.29196e+11, 2.29260e+11, 2.29324e+11, 2.29388e+11,
       2.29452e+11, 2.29516e+11, 2.29580e+11, 2.29644e+11, 2.29708e+11,
       2.29772e+11, 2.29836e+11]) ## WARNING not validated # deleted first 4 nnspws 2.27852e+11, 2.27916e+11, 2.27980e+11, 2.28044e+11

class EHTspwNotFound(Exception):
    """
    Exception raised when the EHT spectral window corresponding to a given Noema spectral window is not found.

    Parameters:
    - message (str): Custom error message (default is "The EHT spw corresponding to this noema spw was not found").
    """
    def __init__(self, message="The EHT spw corresponding to this noema spw was not found"):
        self.message = message
        super().__init__(self.message)

class NNfringefitError(Exception):
    """
    Exception raised when the NN (Nearest Neighbor) band corresponding to a Noema spectral window could not be fringefitted.

    Parameters:
    - message (str): Custom error message (default is "The NN band corresponding to this noema spw could not be fringefitted").
    """
    def __init__(self, message="The NN band corresponding to this noema spw could not be fringefitted"):
        self.message = message
        super().__init__(self.message)

class NoExtrapolatableTableError(Exception):
    """
    Exception raised when there is no extrapolatable table for a given EHT spectral window.

    Parameters:
    - message (str): Custom error message (default is "There exists not extrapolatable table for this ehtspw").
    """
    def __init__(self, message="There exists not extrapolatable table for this ehtspw"):
        self.message = message
        super().__init__(self.message)

class NoPhasejumpinEHTSPW(Exception):
    """
    Exception raised when there is no phase jump in a given EHT spectral window.

    Parameters:
    - message (str): Custom error message (default is "There is no phase jump in this ehtspw").
    """
    def __init__(self, message="There is no phase jump in this ehtspw"):
        self.message = message
        super().__init__(self.message)

def check_idx(idx):
    """
    Check and adjust an index to ensure it falls within a valid range [0, 115].

    Parameters:
    - idx (int): The input index to be checked.

    Returns:
    - int: The adjusted index within the valid range [0, 115].
    """
    # Ensure the index is not below the lower bound
    if idx < 0:
        idx = 0

    # Ensure the index is not above the upper bound
    if idx > 115:
        idx = 115

    return idx

def find_nearest_idx(array, value):
    """
    Find the index of the nearest value in the given array to the specified value.

    Parameters:
    - array (array-like): The input array.
    - value: The value to find the nearest index for.

    Returns:
    - int: The index of the nearest value in the array.

    Example:
    >>> find_nearest_idx([1, 3, 5, 7], 4)
    2
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def get_visphi(visname, band, scan, ant1, ant2, start_ehtspw, end_ehtspw, singleband):
    """
    helper function which returns an array of shape 4,32,116 containing phases in radians
    visname : string - name to .ms
    scan : string - scan to obtain phases format
    ant1 : str ant index for first antenna (i.e., NN)
    ant2 : str ant index for second antenna (i.e., PV)
    """
    if visname is None:
        raise ValueError("No .ms name provided, please provide ms name")
    tb_tool = casac.table()
    tb_tool.open(visname)
    cal = ('(SCAN_NUMBER == {}'.format(scan) + ') && (ANTENNA1=={}').format(ant1) + ') && (ANTENNA2=={}'.format(ant2) + ')'
    scan_rows = tb_tool.query(cal)
    dd = scan_rows.getcol("DATA")
    if len(dd) == 0:
        return []
    ss = scan_rows.getcol("DATA_DESC_ID")
    tb_tool.close()
    if singleband:
        spw_off = 0
    else:
        spw_off = int((int(band)-1)*32)
    visphi = np.zeros((4, 32, 116))
    visphi_error = np.zeros((4, 32, 116))
    for n in range(start_ehtspw, end_ehtspw):
        v = dd[:,:,ss==n]
        visphi[:,n-spw_off,:] = np.angle(np.nanmean(v, axis=-1))

        # Calculate circular deviation
        deviations = np.angle(np.exp(1j * (np.angle(v) - visphi[:,n-spw_off,:][:,:,None])))

        # Calculate circular standard deviation
        circular_std = np.sqrt(-2 * np.log(np.abs(np.mean(np.exp(1j * deviations), axis=-1))))

        visphi_error[:,n-spw_off,:] =  circular_std / np.sqrt(450) ## compute circular standard error

    return visphi, visphi_error

def get_ehtspw_nn_upperband(_ms_metadata, nnspw, nn_bands, start_ehtspw=0, end_ehtspw=32):
    """
    nnspw : int nn-spw to get the corresponding ehtspw
    nn_bands : np.array array containing the nn_spw frequencies (Hz)

    startspw : int [0] do not change
    endspw : int [32] do not change
    """
    eht_freq = []
    eht_spws = []

    for ehtspw in range(start_ehtspw, end_ehtspw):
        eht_freq = np.append(eht_freq, _ms_metadata.yield_freq_of_channels_in_spw(ehtspw))
        eht_spws = np.append(eht_spws, np.ones(116)*ehtspw)
    eht_spws = eht_spws.astype(int)
    nu_low = nn_bands[nnspw]
    nu_max = nn_bands[nnspw+1]
    mask = (nu_low < eht_freq) & (eht_freq < nu_max)
    eht_spws = np.sort(np.unique(eht_spws[mask]))
    if len(eht_spws) == 3:
        print("warning, found 3 eht_spws. Assuming that the first one can be discarded.")
        eht_spws = eht_spws[1:3]

    if len(eht_spws) == 0:

        raise EHTspwNotFound()
    elif len(eht_spws) == 1: # case where there is only one band covered
        eht_freq = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])
        mask = (nu_low < eht_freq) & (eht_freq < nu_max)
        i_lower = np.argmax(mask != 0)
        i_upper = len(mask) - 1 - np.argmax(mask[::-1] != 0)
        i_lower += 3 # add three spw margin
        i_upper -= 3 # subtract three margin

        i_lower = check_idx(i_lower)
        i_upper = check_idx(i_upper)

        tabname = str(int(eht_spws[0])) + ":"+str(i_lower)+"~"+str(i_upper)
    elif len(eht_spws) == 2:
        eht_freq = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])
        mask = (nu_low < eht_freq) & (eht_freq < nu_max)
        i_lower = np.argmax(mask != 0)
        i_lower += 3 # add three spw margin

        eht_freq = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[1])
        mask = (nu_low < eht_freq) & (eht_freq < nu_max)
        i_upper = len(mask) - 1 - np.argmax(mask[::-1] != 0)
        i_upper -= 3 # subtract three margin

        i_lower = check_idx(i_lower)
        i_upper = check_idx(i_upper)

        tabname = str(int(eht_spws[0])) + ":"+str(i_lower)+"~115,"
        tabname = tabname + str(int(eht_spws[1])) + ":0~"+str(i_upper)
    else:
        raise ValueError("len of spw is longer than 2, not implemented, len(eht_spw): ", len(eht_spws))
    return eht_spws, i_lower, i_upper, tabname

def get_ehtspw_nn_lowerband(_ms_metadata, nnspw, nn_bands, start_ehtspw=0, end_ehtspw=32):
    """
    nnspw : int nn-spw to get the corresponding ehtspw
    nn_bands : np.array array containing the nn_spw frequencies (Hz)

    startspw : int [0] do not change
    endspw : int [32] do not change
    """
    eht_freq = []
    eht_spws = []
    for ehtspw in range(start_ehtspw, end_ehtspw):
        eht_freq = np.append(eht_freq, _ms_metadata.yield_freq_of_channels_in_spw(ehtspw))
        eht_spws = np.append(eht_spws, np.ones(116)*ehtspw)
    eht_spws = eht_spws.astype(int)
    nu_low = nn_bands[nnspw]
    nu_max = nn_bands[nnspw+1]
    mask = (nu_low < eht_freq) & (eht_freq < nu_max)
    eht_spws = np.unique(eht_spws[mask])
    if len(eht_spws) == 3:
        print("warning, found 3 eht_spws. Assuming that the first one can be discarded.")
        eht_spws = eht_spws[1:3]

    if len(eht_spws) == 0:
        raise EHTspwNotFound("The EHT spw corresponding to this noema spw was not found, the nnspw is: " + str(nnspw))
    elif len(eht_spws) == 1: # case where there is only one band covered
        eht_freq = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])
        mask = ~((nu_low < eht_freq) & (eht_freq < nu_max)) # invert logic for band 1+2
        i_lower = np.argmax(mask != 0)
        i_upper = len(mask) - 1 - np.argmax(mask[::-1] != 0)
        # i_upper += 3 # subtract three margin
        i_lower = check_idx(i_lower)
        i_upper = check_idx(i_upper)

        tabname = str(int(eht_spws[0])) + ":"+str(i_upper)+"~115"
    elif len(eht_spws) == 2:

        eht_freq = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])
        mask = ~((nu_low < eht_freq) & (eht_freq < nu_max)) # invert logic for band 1+2
        i_lower = np.argmax(mask != 0)
        i_lower -= 3 # add three spw margin

        eht_freq = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[1])
        mask = ~((nu_low < eht_freq) & (eht_freq < nu_max))
        i_upper = len(mask) - 1 - np.argmax(mask[::-1] != 0)
        i_upper += 3 # subtract three margin

        i_lower = check_idx(i_lower)
        i_upper = check_idx(i_upper)

        if i_upper ==115:
            i_upper = 0
        if i_lower == 0:
            i_lower = 115
        tabname = str(int(eht_spws[0])) + ":0~"+str(i_lower) + ","
        tabname = tabname + str(int(eht_spws[1])) + ":"+str(i_upper) + "~115"
    else:
        raise ValueError("len of spw is longer than 2, not implemented, len(eht_spw): ", len(eht_spws))
    return eht_spws, i_lower, i_upper, tabname

def fringefit_nn(_ms_metadata, nn_bands, visname, path, scan, band, combine="", singleband=True): # WARNING here there is a single band to band-combine keyword, untested
    """
    nn_bands : np.array - nn spw frequencies
    visname : str - .ms file
    scan : str - scan to obtain phases format
    path : str - absolute path for temporary fringefit table storage
    combine : str [""] combine keyword to fringefit
    singleband : bool [True] set to False to have band-combine mode UNTESTED!!
    """
    _ms_metadata = auxiliary.store_object(_ms_metadata, _operation='read')
    band     = int(band)
    nn_bands = literal_eval(nn_bands)
    list_tabnames = []
    for nnspw in range(len(nn_bands)-1):
        try:
            if band in [1,2]:
                if singleband:
                    _,_,_, tabname = get_ehtspw_nn_lowerband(_ms_metadata, nnspw, nn_bands, start_ehtspw=0, end_ehtspw=32)
                else:
                    _,_,_, tabname = get_ehtspw_nn_lowerband(_ms_metadata, nnspw, nn_bands, start_ehtspw=32*(band-1),
                                                             end_ehtspw=32*band
                                                            )

            elif band in [3,4]:
                if singleband:
                    _,_,_, tabname = get_ehtspw_nn_upperband(_ms_metadata, nnspw, nn_bands, start_ehtspw=0, end_ehtspw=32)
                else:
                    _,_,_, tabname = get_ehtspw_nn_upperband(_ms_metadata, nnspw, nn_bands, start_ehtspw=32*(band-1),
                                                             end_ehtspw=32*band
                                                            )

            else:
                raise ValueError("band should be in 1,2,3,4 [int], but is: ", band, type(band) )
            print("fitting: " + tabname)# WARNING switch to logging?
            # logging.info("fitting: " + tabname)
            print("saving to: " + str(path + "scan_" + scan + "_spw_" + tabname+".tab"))
            list_tabnames.append(path + "scan_" + scan + "_spw_" + tabname+".tab")
            tasks.fringefit(vis=visname, caltable=path + "scan_" + scan + "_spw_" + tabname+".tab", scan=scan, spw=tabname,
                            globalsolve=True, antenna='NN&PV', refant='NN', combine=combine, corrdepflags=True
                           )
        except Exception as e:
            print(e)
            continue
            #raise NNfringefitError()
    return list_tabnames

def get_spwhelper_nn_upperband(_ms_metadata, nnspw, nn_bands, band, visphi, cor, singleband, start_ehtspw=0, end_ehtspw=32):
    eht_spws, i_lower, i_upper, tabname = get_ehtspw_nn_upperband(_ms_metadata, nnspw, nn_bands, start_ehtspw=start_ehtspw,
                                                                  end_ehtspw=end_ehtspw
                                                                 )
    if singleband:
        spw_off = 0
    else:
        spw_off = int((int(band)-1)*32)
    if len(eht_spws) == 0:
        raise EHTspwNotFound()
    elif len(eht_spws) == 1: # case where there is only one band covered
        freq_lower1 = np.array([])
        freq_upper1 = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])[  i_lower:i_upper]

        phi_lower1 = np.array([])
        phi_upper1 = visphi[cor, eht_spws[0]-spw_off,  i_lower:i_upper]
    elif len(eht_spws) == 2:
        freq_lower1 = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])[  i_lower:115]
        freq_upper1 = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[1])[0:i_upper]

        phi_lower1 = visphi[cor, eht_spws[0]-spw_off,i_lower:115]
        phi_upper1 = visphi[cor, eht_spws[1]-spw_off,      0:i_upper]
    else:
        raise ValueError("len of spw is longer than 2, not implemented, len(eht_spw): ", len(eht_spws))
    return freq_lower1, freq_upper1, phi_lower1, phi_upper1, eht_spws, i_lower, i_upper, tabname

def get_spwhelper_nn_lowerband(_ms_metadata, nnspw, nn_bands, band, visphi, cor, singleband, start_ehtspw=0, end_ehtspw=32):
    # logging.warning("This assumes the inverted .ms ordering of the spw lower side bands as in 2021, will fail for differnt ordering") # SET LOGGING.WARN
    eht_spws, i_lower, i_upper, tabname = get_ehtspw_nn_lowerband(_ms_metadata, nnspw, nn_bands, start_ehtspw=start_ehtspw,
                                                                  end_ehtspw=end_ehtspw)
    if singleband:
        spw_off = 0
    else:
        spw_off = int((int(band)-1)*32)
    if len(eht_spws) == 0:
        raise EHTspwNotFound()
    elif len(eht_spws) == 1: # case where there is only one band covered
        freq_lower1 = np.array([])
        freq_upper1 = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])[  i_lower:i_upper]

        phi_lower1 = np.array([])
        phi_upper1 = visphi[cor, eht_spws[0]-spw_off,  i_lower:i_upper]
    elif len(eht_spws) == 2:
        freq_lower1 = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[0])[0:i_lower]
        freq_upper1 = _ms_metadata.yield_freq_of_channels_in_spw(eht_spws[1])[  i_upper:115]

        phi_lower1 = visphi[cor, eht_spws[0]-spw_off,  0:i_lower]
        phi_upper1 = visphi[cor, eht_spws[1]-spw_off,    i_upper:115]
    else:
        raise ValueError("len of spw is longer than 2, not implemented, len(eht_spw): ", len(eht_spws))
    return freq_lower1, freq_upper1, phi_lower1, phi_upper1, eht_spws, i_lower, i_upper, tabname


def get_phasejump_idx_nn_upperband(_ms_metadata, eht_spw1, freq_upper1):
    if len(eht_spw1) == 0:
        raise EHTspwNotFound()
    elif len(eht_spw1) == 1:
        phasejump_spw = eht_spw1[0] ## if eht_spw1 is 1 long, the jump happens in eht_spw1
    elif len(eht_spw1) == 2:
        phasejump_spw = eht_spw1[1] ## if eht_spw1 is 2 long, the jump happens in spw eht_spw1[1]
    else:
        raise ValueError("len(eht_spw) longer than 2, can't be", len(eht_spw1))
    freqs = _ms_metadata.yield_freq_of_channels_in_spw(phasejump_spw)

    phase_jump_idx = np.where(freqs == np.max(freq_upper1))[0][0] # get the jump is where the maximum frequency of freq_upper1 is. Add 3 to account for fitting margin!
    phase_jump_idx += 3

    return phasejump_spw, phase_jump_idx

def get_phasejump_idx_nn_lowerband(_ms_metadata, eht_spw1, freq_upper1):
    if len(eht_spw1) == 0:
        raise EHTspwNotFound()
    elif len(eht_spw1) == 1:
        phasejump_spw = eht_spw1[0] ## if eht_spw1 is 1 long, the jump happens in eht_spw1
    elif len(eht_spw1) == 2:
        phasejump_spw = eht_spw1[1] ## if eht_spw1 is 2 long, the jump happens in spw eht_spw1[1]
    else:
        raise ValueError("len(eht_spw) longer than 2, can't be", len(eht_spw1))
    freqs = _ms_metadata.yield_freq_of_channels_in_spw(phasejump_spw)

    phase_jump_idx = np.where(freqs == np.min(freq_upper1))[0][0] # get the jump is where the maximum frequency of freq_upper1 is. Add 3 to account for fitting margin!
    phase_jump_idx -= 3

    return phasejump_spw, phase_jump_idx

def get_SNR(tabname, pv_ant, path, scan):
    """
    gets SNR of tabname
    """
    mytb = casac.table()
    mytb.open(path + "scan_" + scan + "_spw_" + tabname + ".tab")
    snr = mytb.getcol("SNR")[:,:,mytb.getcol("ANTENNA1") == pv_ant]
    mytb.close()
    return snr

def get_model(_ms_metadata, freq, phi, delay):
    """
    computes the best fit model given freq, phi and delay
    freq : np.array - frequencies to evalute delay
    phi : np.array - observed phases
    delay : float - delay computed by fringefit
    """
    model     = auxiliary.delay2phase(delay, freq*1e-9, _ms_metadata.reference_frequency*1e-9)
    intercept = np.angle(np.nanmean(np.exp(1j*phi) / np.exp(1j*model)))
    return np.angle(np.exp(1j*(model+intercept)))

def get_delay(_ms_metadata, tabname1, freq_lower1, freq_upper1, pv_ant, path, scan, cor, skip_edge_channels=20):
    """
    retrieves the delays computed by fringefit
    tabname1 : string - absolute path to left hand side fringe fit
    """
    if cor == 0:
        cor_ = 1
    elif cor == 3:
        cor_ = 5 ## set cor_index to 5 for LL corr

    # snr1 = get_SNR(tabname1, pv_ant, path, scan)
    ## not using SNRs at the moment, weird values?
    tb = casac.table()
    tb.open(path + "scan_" + scan + "_spw_" + tabname1 + ".tab")
    fparam1 = tb.getcol("FPARAM")[:,:,tb.getcol("ANTENNA1") == pv_ant]
    tb.close()

    if fparam1.shape[-1] == 1:
        delay1 = -fparam1[cor_,0,0]
    else:
        if len(freq_lower1) < skip_edge_channels:
            delay1 = -fparam1[cor_, 0, 1] # too little data in lower channel, using upper channel
        elif len(freq_upper1) < skip_edge_channels:
            delay1 = -fparam1[cor_, 0, 0] # too little data in upper channel, using lower channel
        else:
            if (fparam1[cor_,0,:] == 0.).sum == 1:
                fparam1[fparam1 == 0.] = np.nan
            fparam1 = np.nanmean(fparam1, axis=-1).flatten()
            delay1 = -fparam1[cor_]

    return delay1

def get_phase_nn(_ms_metadata, visphi, scan, band, cor, path, nn_bands, nnspw, pv_ant, test=False, singleband=True):
    """
    visphi : np.array - phases to obtain the best fit delay
    scan : string - scan to obtain phases format
    idx : list - the appropriate list of idices of the NN phase jumps
    path : string - absolute path for temporary fringefit table storage
    nn_bands : list - channel edges of nn-spws
    nnspw : int - nn spw to be evaluted
    cor : int - correlation to be evaluted

    test : bool - will display test plots [False]
    singleband : bool [True] set to False to have band-combine mode UNTESTED!!
    """
    if band in [1,2]:
        help_f  = get_spwhelper_nn_lowerband
        help_f2 = get_phasejump_idx_nn_lowerband
    elif band in [3,4]:
        help_f  = get_spwhelper_nn_upperband
        help_f2 = get_phasejump_idx_nn_upperband
    else:
        raise ValueError("band not in [1,2,3,4] (int), but is: ", band, type(band))
    if singleband:
        freq_lower1, freq_upper1, phi_lower1, phi_upper1, eht_spws1, i_lower1, i_upper1, tabname1 = \
        help_f(_ms_metadata, nnspw , nn_bands, band, visphi, cor, singleband, start_ehtspw=0, end_ehtspw=32)
    else:
        freq_lower1, freq_upper1, phi_lower1, phi_upper1, eht_spws1, i_lower1, i_upper1, tabname1 = \
        help_f(_ms_metadata, nnspw , nn_bands, band, visphi, cor, singleband, start_ehtspw=32*(band-1), end_ehtspw=32*band) # WARNING untested band-combine mode

    if not os.path.exists(path + "scan_" + scan + "_spw_" + tabname1 + ".tab"):
        raise NNfringefitError

    phasejump_spw, phasejump_idx = help_f2(_ms_metadata, eht_spws1, freq_upper1)

    freq1 = np.append(freq_lower1, freq_upper1)

    phi1 = np.append(phi_lower1, phi_upper1)

    delay1 = get_delay(_ms_metadata, tabname1, freq_lower1, freq_upper1, pv_ant, path, scan, cor)

    model1 = get_model(_ms_metadata, freq1, phi1, delay1)

    if not test:
        auxiliary.rm_dir_if_present(path + "scan_" + scan + "_spw_" + tabname1 + ".tab")    #TODO / WARNING: uncomment this line!
    return freq1, model1, phasejump_spw, phasejump_idx

def get_distance(_ms_metadata, model1, model2, ehtspw, phasejump_idx):
    """
    Calculate the angular distance between two models at a specific phase jump.

    Parameters:
    - model1 (callable):   A function representing the first model. Should take frequency as input.
    - model2 (callable):   A function representing the second model. Should take frequency as input.
    - phasejump_spw (str): String representation of the spectral window for the phase jump.
    - phasejump_idx (int): Index for the phase jump in the spectral window.

    Returns:
    - float: The angular distance between the two models at the specified phase jump, in degrees.
    """

    # Define offset set for different phases
    offset_set = np.exp(1j * np.radians([0, 90, 180, 270]))

    # Calculate phases for model1 and model2 at the specified phase jump
    freq = _ms_metadata.yield_freq_of_channels_in_spw(ehtspw)
    phase1 = np.exp(1j * model1(freq[phasejump_idx]))
    phase2 = np.exp(1j * model2(freq[phasejump_idx]))

    # Apply offset set to phase2
    phase2 = phase2 * offset_set

    # Calculate angular distance
    distance = np.abs(np.angle(phase1 / phase2))

    # Return the angle corresponding to the minimum distance in degrees
    return np.angle(offset_set[np.argmin(distance)], deg=True)

def get_jumpindx(_ms_metadata, ehtspw, nn_bands, band):
    """
    Get the index of the frequency jump for a given EHT spectral window and nearest neighbor bands.

    Parameters:
    - _ms_emtadata: ms_metadata object
    - ehtspw (int): The EHT spectral window index.
    - nn_bands (array-like): Array of nearest neighbor bands.

    Returns:
    - int: The index of the frequency jump in the EHT spectral window.
    """
    freq = _ms_metadata.yield_freq_of_channels_in_spw(ehtspw)
    nu_min, nu_max = np.min(freq), np.max(freq)
    nn_bands = np.array(nn_bands)
    # Filter nn_bands within the frequency range of the EHT spectral window
    mask = (nu_min <= nn_bands) & (nn_bands <= nu_max)
    nnspw = nn_bands[mask]
    if len(nnspw) == 0:
        if band in [1,2]:
            freq_lo = _ms_metadata.yield_freq_of_channels_in_spw(ehtspw-1)
            nu_min_lo, nu_max_lo = np.min(freq_lo), np.max(freq_lo)
            mask_lo = (nu_min_lo <= nn_bands) & (nn_bands <= nu_max_lo)
            nnspw_lo = nn_bands[mask_lo]
            if len(nnspw_lo) == 0:
                ## also no index in spw -1, implies that the index must be at 0.
                return 0

            # get index of last nnspw:
            idx_lo = find_nearest_idx(freq_lo, nnspw_lo)

            # get frequency of last nnspw
            freq_lo = freq_lo[idx_lo]

            if abs(freq_lo - freq[0]) < 64e6: # if difference is smaller than 64MHz return 115, else return 0
                raise NoPhasejumpinEHTSPW
            else:
                return 0

        else:
            freq_lo = _ms_metadata.yield_freq_of_channels_in_spw(ehtspw-1)
            nu_min_lo, nu_max_lo = np.min(freq_lo), np.max(freq_lo)
            mask_lo = (nu_min_lo <= nn_bands) & (nn_bands <= nu_max_lo)
            nnspw_lo = nn_bands[mask_lo]
            if len(nnspw_lo) == 0:
                ## also no index in spw -1, implies that the index must be at 0.
                return 0

            # get index of last nnspw:
            idx_lo = find_nearest_idx(freq_lo, nnspw_lo)

            # get frequency of last nnspw
            freq_lo = freq_lo[idx_lo]
            if abs(freq_lo - freq[0]) < 64e6: # if difference is smaller than 64MHz return 115, else return 0
                raise NoPhasejumpinEHTSPW
            else:
                return 0
        return 0
    else:
        # Find the index of the nearest frequency in the EHT spectral window
        idx = find_nearest_idx(freq, nnspw)
        return idx

def get_chi2red(_ms_metadata, visphi, visphi_err, band, offset, ehtspw, jumpidx, model):
    """
    Calculate the reduced chi-squared value for a given correlation, EHT spectral window, and model.

    Parameters:
    - _ms_metadata: ms_metadata object
    - visphi (np.array): Array of measured phases for the specified correlation and EHT spectral window.
    - visphi_err (np.array): Array of measured phase errors for the specified correlation and EHT spectral window.
    - cor (int): Correlation index for which to calculate the chi-squared value.
    - ehtspw (int): EHT spectral window index for which to calculate the chi-squared value.
    - jumpidx (int): The jump idx corresponding to this EHT spw
    - model (callable): A function representing the model. Should take frequency as input.

    Returns:
    - float: The reduced chi-squared value.
    """
    # Get the frequencies corresponding to the EHT spectral window
    freq = _ms_metadata.yield_freq_of_channels_in_spw(ehtspw)

    # Calculate the model phases for the given frequencies
    phi    = model(freq)
    visphi = np.exp(1j*visphi)
    phi    = np.exp(1j*phi)

    if band in [1,2]:
        visphi[:jumpidx] = visphi[:jumpidx] * np.exp(1j*np.radians(offset))
    elif band in [3,4]:
        visphi[jumpidx:] = visphi[jumpidx:] * np.exp(1j*np.radians(offset))

    # Calculate the reduced chi-squared value
    chi2_red = np.nansum( np.angle(visphi / phi)**2 / visphi_err**2) / len(visphi)

    return chi2_red

def get_matchingspws(ehtspw, model_dict):
    """
    Get matching spectral window indices for a given EHT spectral window index.

    Parameters:
    - ehtspw (int): The EHT spectral window index.
    - model_dict (dict): A dictionary containing model information, where keys are string representations of EHT spectral window indices.

    Returns:
    - tuple: A tuple containing two EHT spectral window indices (idx1 and idx2) that match the given EHT spectral window index.

    Raises:
    - ValueError: If no matching spectral windows are found.

    Example:
    >>> model_dict = {"0": "model0", "1": "model1", "2": "model2", "3": "model3"}
    >>> get_matchingspws(1, model_dict)
    (1, 2)
    """
    avail_spws =  np.sort(np.array(list(model_dict.keys()), dtype=int))
    if ehtspw in avail_spws:
        # EHT idx1 was found
        idx1 = ehtspw
    else:
        # Find the largest EHT spectral window index among lower ones
        mask_ = avail_spws < ehtspw
        if mask_.sum() == 0:
            raise NoExtrapolatableTableError("No lower matching spectral windows found. ehtspw is: " + str(ehtspw) + " tables are: "+str(avail_spws))
        lowerehtspws = avail_spws[mask_]
        idx1 = np.max(lowerehtspws)

    if ehtspw + 1 in avail_spws:
        # EHT idx2 was found
        idx2 = ehtspw + 1
    else:
        # Find the smallest EHT spectral window index among larger ones
        mask_ = avail_spws > ehtspw
        if mask_.sum() == 0:
            raise NoExtrapolatableTableError("No upper matching spectral windows found. ehtspw is: " + str(ehtspw) + " tables are: "+str(avail_spws))
        upperehtspws = avail_spws[mask_]
        idx2 = np.min(upperehtspws)

    return idx1, idx2
import matplotlib.pyplot as plt
def offsets_nn(_ms_metadata, visname, nn_bands, scan, band, path, nn_ant, pv_ant, start_nnspw=0, end_nnspw=32, start_ehtspw=0,
               end_ehtspw=32, max_chi2=10., singleband=True, test=False):
    """
    Calculate phase offsets between NN and PV  antennas for RR and LL correlations.

    Parameters:
    - visname (str): Name of the visibility data.
    - nn_bands (list): List of spectral bands to consider for NN fringefit.
    - scan (str): Scan to obtain phases format.
    - band (str): Band of the visibility data.
    - path (str): Absolute path for temporary fringefit table storage.
    - nn_ant (str): Antenna index for the NN antenna.
    - pv_ant (str): Antenna index for the PV antenna.
    - startspw (int): Starting spectral window index (default is 0).
    - endspw (int): Ending spectral window index (default is 32).
    - min_snr (float): chi^2 cut to apply for FF (Fringe Fitting) solutions (default is 10.).
    returns True
    """

    pv_ant, nn_ant, band = int(pv_ant), int(nn_ant), int(band)
    if singleband:
        visphi, visphi_error = get_visphi(visname, band, scan, nn_ant, pv_ant, start_ehtspw=0, end_ehtspw=32,
                                          singleband=singleband
                                         )
    else:
        visphi, visphi_error = get_visphi(visname, band, scan, nn_ant, pv_ant, start_ehtspw=int((int(band)-1)*32),
                                          end_ehtspw=int(int(band)*32), singleband=singleband
                                         )


    model_dict_rr, model_dict_ll = {}, {}

    for cor in [0,3]:  # 0: RR, 3: LL
        for nnspw in range(start_nnspw, end_nnspw):
            try:
                freq1_raw, model1_raw, phasejump_spw, phasejump_idx = get_phase_nn(_ms_metadata, visphi, scan, band, cor, path,
                                                                                   nn_bands, nnspw, pv_ant, test=test,
                                                                                   singleband=singleband
                                                                                  )
                if cor == 0:
                    model_dict_rr[str(phasejump_spw)] = [freq1_raw, model1_raw, phasejump_spw, phasejump_idx]
                else:
                    model_dict_ll[str(phasejump_spw)] = [freq1_raw, model1_raw, phasejump_spw, phasejump_idx]
            except NNfringefitError as e:
                print(f"Couldn't fringefit band, scan, nnspw: {band}, {scan},{nnspw}")
    model_dict = [model_dict_rr, model_dict_ll]
    offset_rr, offset_ll = {}, {}
    cor = [0,3]
    if singleband:
        spw_off = 0
    else:
        spw_off = int((int(band)-1)*32)

    for cor_idx in [0, 1]:
        for ehtspw in range(start_ehtspw, end_ehtspw):
            try:
                idx1, idx2 = get_matchingspws(ehtspw, model_dict[cor_idx])

                freq1_raw, model1_raw, phasejump_spw1, phasejump_idx1 = model_dict[cor_idx][str(idx1)]
                freq2_raw, model2_raw, phasejump_spw2, phasejump_idx2 = model_dict[cor_idx][str(idx2)]

                model1 = interp1d(freq1_raw, model1_raw, bounds_error=False, fill_value="extrapolate")
                model2 = interp1d(freq2_raw, model2_raw, bounds_error=False, fill_value="extrapolate")

                try:
                    ## get jump idx in ehtspw
                    jumpidx = get_jumpindx(_ms_metadata, ehtspw, nn_bands, band)

                    ## get_offset
                    offset = get_distance(_ms_metadata, model1, model2, ehtspw, jumpidx)

                    chi = get_chi2red(_ms_metadata, visphi[cor[cor_idx], ehtspw-spw_off].copy(), visphi_error[cor[cor_idx],
                                      ehtspw-spw_off].copy(), band, offset, ehtspw, jumpidx, model1
                                     )

                    if chi > max_chi2:
                        offset_dict = offset_rr if cor_idx == 0 else offset_ll
                        offset_dict[str(ehtspw)] = ["FLAG", int(jumpidx), chi]
                    else:
                        offset_dict = offset_rr if cor_idx == 0 else offset_ll
                        offset_dict[str(ehtspw)] = [offset, int(jumpidx), chi]

                except NoPhasejumpinEHTSPW:
                    offset = get_distance(_ms_metadata, model1, model2, ehtspw, 0)
                    offset_dict = offset_rr if cor_idx == 0 else offset_ll
                    if band in [3,4]:
                        offset_dict[str(ehtspw)] = [0, 0, 1]
                    else:
                        offset_dict[str(ehtspw)] = [offset, 0, 1] # chi2_red artificially set to 1

            except NoExtrapolatableTableError:

                print("No table found for band, scan, ehtspw: " + str(band) + "," + str(scan) + "," + str(ehtspw))
                try:
                    jumpidx = get_jumpindx(_ms_metadata, ehtspw, nn_bands, band)
                    offset_dict = offset_rr if cor_idx == 0 else offset_ll
                    offset_dict[str(ehtspw)] = ["FLAG", int(jumpidx), 1e99] # chi2_red artificially set to inf
                except NoPhasejumpinEHTSPW:
                    offset_dict = offset_rr if cor_idx == 0 else offset_ll
                    offset_dict[str(ehtspw)] = ["FLAG", 0, 1e99] # chi2_red artificially set to inf
                except ValueError:
                    offset_dict = offset_rr if cor_idx == 0 else offset_ll
                    offset_dict[str(ehtspw)] = ["FLAG", 0, 1e99] # chi2_red artificially set to inf

    json_file_path = path + "offset_rr_band_" + str(band) + "_scan_" +str(scan)+".json"
    with open(json_file_path, 'w') as json_file:
        json.dump(offset_rr, json_file, indent=4)

    json_file_path = path + "offset_ll_band_" + str(band) + "_scan_" +str(scan)+".json"
    with open(json_file_path, 'w') as json_file:
        json.dump(offset_ll, json_file, indent=4)
    return True

def get_truechanges(a_, nnscans, change_len=5):
    a = np.copy(a_)
    if len(nnscans) - 2 <= change_len:
        return a_
    for num in range(1, len(a)-change_len):
        if a[num] - a[num-1] == 0:
            continue
        elif all([a[num] == a[num+n] for n in range(change_len)]):
            # true change
            continue
        else:
            # false change replace value
            a[num] = a[num-1]

    for num in np.arange(change_len, len(a)-1)[::-1]:
        if a[num] - a[num+1] == 0:
            continue
        elif all([a[num] == a[num-n] for n in range(change_len)]):
            continue
        else:
            # false change replace value
            a[num] = a[num+1]

     ## if the first and last value is different, than change it to the boundary
    if a[0] != a[1]:
        a[0] = a[1]
    if a[-1] != a[-2]:
        a[-1] = a[-2]

    return a

def get_offsetModel(nnscans, ms_metadata, path, band, start_ehtspw, end_ehtspw, change_len=5):
    """
    Get interpolated offset models for RR and LL correlations from JSON files.

    Parameters:
    - nnscans (list): List of scan numbers.
    - path (str): Path where JSON files are stored.

    Returns:
    - tuple: A tuple containing list and two dicts representing the indexs per eht spw, and interpolated offset models
             for RR and LL correlations, respectively.
    """
    # get all scans, using now all scans as nnscans only has scans with NNPV, but sometimes NN is present, but PV isn't and we need to extrapolate those as well.
    max_scan = max(np.array(ms_metadata.all_scans, dtype=int))

    # Initialize arrays to store interpolated offset models
    offset_model_rr = np.zeros((max_scan+1, 32), dtype=object)*np.nan
    offset_model_ll = np.zeros((max_scan+1, 32), dtype=object)*np.nan
    idx_model = np.zeros(32, dtype=object)

    # Loop through each scans
    for num, scan in enumerate(nnscans):
        # File paths for RR and LL JSON files
        json_file_path_rr = path + "offset_rr_band_" + str(band) + "_scan_" + str(scan) + ".json"
        json_file_path_ll = path + "offset_ll_band_" + str(band) + "_scan_" + str(scan) + ".json"
            # Load RR JSON file
        with open(json_file_path_rr, 'r') as json_file:
            rr_mod = json.load(json_file)

        with open(json_file_path_ll, 'r') as json_file:
            ll_mod = json.load(json_file)
        offset_model_rr[int(scan)] = [rr_mod[str(n)][0] for n in range(start_ehtspw, end_ehtspw)]
        offset_model_ll[int(scan)] = [ll_mod[str(n)][0] for n in range(start_ehtspw, end_ehtspw)]
        auxiliary.rm_file_if_present(json_file_path_rr)
        auxiliary.rm_file_if_present(json_file_path_ll)

    # get index
    idx_model =  [rr_mod[str(n)][1] for n in range(start_ehtspw, end_ehtspw)]

    # Replace "FLAG" with NaN
    offset_model_rr[offset_model_rr == "FLAG"] = np.nan
    offset_model_ll[offset_model_ll == "FLAG"] = np.nan

    # Create copies of the original arrays for interpolation
    offset_model_rr_int = offset_model_rr.copy().astype(float)
    offset_model_ll_int = offset_model_ll.copy().astype(float)

    # Indices for interpolation
    indices = np.arange(max_scan+1) # note that there is a "zero scan" which doesn't exist. I liked it more than than iterating over scan - 1

    # Create NaN masks for RR and LL
    nan_mask_rr = np.isnan(offset_model_rr_int)
    nan_mask_ll = np.isnan(offset_model_ll_int)

    # Interpolate only the NaN values along the first axis using nearest-neighbor interpolation
    for i in range(32):
        if np.sum(~nan_mask_rr[:,i]) > 2:
            offset_model_rr_int[:,i] = interp1d(indices[~nan_mask_rr[:,i]], offset_model_rr_int[:,i][~nan_mask_rr[:,i]],
                                                fill_value="extrapolate", bounds_error=False, kind="nearest")(indices)
        if np.sum(~nan_mask_ll[:,i]) > 2:
            offset_model_ll_int[:,i] = interp1d(indices[~nan_mask_ll[:,i]], offset_model_ll_int[:,i][~nan_mask_ll[:,i]],
                                                fill_value="extrapolate", bounds_error=False, kind="nearest")(indices)

    # corrected interpolated solutions if at least change_len values are constantly different.
    offset_model_rr_cor = np.zeros_like(offset_model_rr_int)
    offset_model_ll_cor = np.zeros_like(offset_model_ll_int)

    for i in range(32):
        offset_model_rr_cor[:,i] = get_truechanges(offset_model_rr_int[:,i], nnscans, change_len=change_len)
        offset_model_ll_cor[:,i] = get_truechanges(offset_model_ll_int[:,i], nnscans, change_len=change_len)

    # save derived offsets for prosperity
    np.save(path + 'offset_model_rr_band'+str(band)+'_raw.npy', offset_model_rr_int)
    np.save(path + 'offset_model_ll_band'+str(band)+'_raw.npy', offset_model_ll_int)

    np.save(path + 'offset_model_rr_band'+str(band)+'.npy', offset_model_rr_cor)
    np.save(path + 'offset_model_ll_band'+str(band)+'.npy', offset_model_ll_cor)

    offset_model_rr_cor[np.isnan(offset_model_rr_cor)] = 0. # remove reminder nans to avoild literal eval_error
    offset_model_ll_cor[np.isnan(offset_model_ll_cor)] = 0.
    offset_model_rr_cor = [list(l) for l in offset_model_rr_cor] #
    offset_model_ll_cor = [list(l) for l in offset_model_ll_cor] # index 0 is nonsense, but will not be called as we call per scan (which are real scan indices, i.e. index 1 == scan 1)
    return list(idx_model), offset_model_rr_cor, offset_model_ll_cor

def get_helpercaltab(spw0, _scan, idx_model_dict, offset_model_rr_dict, offset_model_ll_dict, singleband):
    """
    helper function which writes hands the singleband/multiband distinctions
    """
    _scan = int(_scan)
    if singleband: # here we make an explicit distinction for clarity
        band = None
        start_ehtspw = 0
        index_model = idx_model_dict["single"]

        ## get offsets and cumoffsets
        offset_rr = np.array([offset_model_rr_dict["single"][_scan][n] for n in range(32)])
        offset_ll = np.array([offset_model_ll_dict["single"][_scan][n] for n in range(32)])
    else:
        if spw0 in np.arange(0,32):
            band = 1
            start_ehtspw = 0
            index_model  = idx_model_dict["1"]
            ## get offsets and cumoffsets
            offset_rr = np.array([offset_model_rr_dict["1"][_scan][n] for n in range(32)])
            offset_ll = np.array([offset_model_ll_dict["1"][_scan][n] for n in range(32)])
        elif spw0 in np.arange(32, 64):
            band = 2
            start_ehtspw = 32
            index_model = idx_model_dict["2"]
            ## get offsets and cumoffsets
            offset_rr = np.array([offset_model_rr_dict["2"][_scan][n] for n in range(32)])
            offset_ll = np.array([offset_model_ll_dict["2"][_scan][n] for n in range(32)])
        elif spw0  in np.arange(64, 96):
            band = 3
            start_ehtspw = 64
            index_model = idx_model_dict["3"]
            ## get offsets and cumoffsets
            offset_rr = np.array([offset_model_rr_dict["3"][_scan][n] for n in range(32)])
            offset_ll = np.array([offset_model_ll_dict["3"][_scan][n] for n in range(32)])
        elif spw0 in np.arange(96, 128):
            band = 4
            start_ehtspw = 96
            index_model = idx_model_dict["4"]
            ## get offsets and cumoffsets
            offset_rr = np.array([offset_model_rr_dict["4"][_scan][n] for n in range(32)])
            offset_ll = np.array([offset_model_ll_dict["4"][_scan][n] for n in range(32)])
        else:
            raise ValueError("spw0 not in range(0,32) or range(32,64) or range(64,96) or range(96,128), but is: "+str(spw0))

    cumoffset_rr = np.cumsum(offset_rr)
    cumoffset_ll = np.cumsum(offset_ll)

    ## get phasors
    offset_rr = np.exp(1j*np.radians(offset_rr))
    offset_ll = np.exp(1j*np.radians(offset_ll))
    cumoffset_rr = np.exp(1j*np.radians(cumoffset_rr))
    cumoffset_ll = np.exp(1j*np.radians(cumoffset_ll))
    return band, start_ehtspw, index_model, offset_rr, offset_ll, cumoffset_rr,  cumoffset_ll

def get_caltab(_ms_metadata, _inp_params, nn_scans, nn_tabname, idx_model_dict, offset_model_rr_dict, offset_model_ll_dict, singleband, bands, pv_ant, nn_ant):
    """
    writes the casa caltabs
    """
    _scan_refant = pv_ant # WARNING unclear if this should be NN ant?! will result in a -1 of phases error if wrong
    auxiliary.create_caltable(_inp_params, nn_tabname, 'Complex', 'B Jones', False)
    mytb = casac.table()
    mytb.open(nn_tabname, nomodify=False)

    r_counter = 0
    for _scan in _ms_metadata.yield_scans(): # loop over all scans, might fail if for some reason an ant is found outside of nn_ants?
        _scan = str(_scan)
        _fieldname = _ms_metadata.yield_sourcename_from_scan(_scan)
        _fieldID   = _ms_metadata.yield_sourceID(_fieldname)
        try:
            _ants = list(_ms_metadata.yield_antennas(_scan))
        except TypeError:
            print('    Warning: Skipping scan {0}, which does not appear to have any data'.format(str(_scan)))
            continue
        _spwds = [list(s) for s in _ms_metadata.yield_spwds(_scan)]
        for ant, spws in zip(_ants, _spwds):
            bandpass = {}
            for spw0 in spws:
                thevalue = np.exp(1j*np.zeros(( 2, 116))) # This creates a bandpass per spw assuming its 116 long, will fail if wrong
                if ant == nn_ant:
                    band, start_ehtspw, idx_model, offset_rr, offset_ll, cumoffset_rr, cumoffset_ll = get_helpercaltab(spw0, _scan, idx_model_dict, offset_model_rr_dict, offset_model_ll_dict, singleband)
                    if singleband:
                        band = bands[0]
                    if spw0-start_ehtspw != 0:
                        thevalue[0,:] = thevalue[0,:]/cumoffset_rr[spw0-start_ehtspw-1]
                        thevalue[1,:] = thevalue[1,:]/cumoffset_ll[spw0-start_ehtspw-1]
                    if band in [1,2]:
                        thevalue[0,:idx_model[spw0-start_ehtspw]] = thevalue[0,:idx_model[spw0-start_ehtspw]]/offset_rr[spw0-start_ehtspw] # here we need to subtract the start ehtspw.
                        thevalue[1,:idx_model[spw0-start_ehtspw]] = thevalue[1,:idx_model[spw0-start_ehtspw]]/offset_ll[spw0-start_ehtspw] # i.e. for a single band the caltab will be 32, 116 otherwise 128,116
                    elif band in [3,4]:
                        thevalue[0,idx_model[spw0-start_ehtspw]:] = thevalue[0,idx_model[spw0-start_ehtspw]:]/offset_rr[spw0-start_ehtspw] # we would select the corresponding 32,116 chunk for each band.
                        thevalue[1,idx_model[spw0-start_ehtspw]:] = thevalue[1,idx_model[spw0-start_ehtspw]:]/offset_ll[spw0-start_ehtspw]
                bandpass[spw0] = thevalue

            qselc = 'TIME'
            qcrit = 'SCAN_NUMBER==' + str(_scan) + ' && ANTENNA1==' + str(ant) + ' && ANTENNA2==' + str(ant) + ' && DATA_DESC_ID==' + str(0)
            qsort = 'TIME'
            time  = auxiliary.read_CASA_table(None, qselc, qcrit, qsort, _tablename=_inp_params.ms_name)
            caltime = np.ma.unique(time)[int(len(time)/2)]

            for spw in bandpass.keys():
                this_data = bandpass[spw]
                this_shape = np.shape(this_data)
                # add new row to caltab and populate
                mytb.addrows(1)
                mytb.putcell('TIME', r_counter, caltime)
                mytb.putcell('FIELD_ID', r_counter, _fieldID)
                mytb.putcell('SPECTRAL_WINDOW_ID', r_counter, spw)
                mytb.putcell('ANTENNA1', r_counter, int(ant))
                mytb.putcell('ANTENNA2', r_counter, -1)
                mytb.putcell('SCAN_NUMBER', r_counter, int(_scan))
                mytb.putcell('CPARAM', r_counter, this_data)
                mytb.putcell('PARAMERR', r_counter, np.ones(this_shape))
                mytb.putcell('FLAG', r_counter, np.full(this_shape, False))
                mytb.putcell('SNR', r_counter, np.full(this_shape, 999))
                r_counter += 1

    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    mytb.open(nn_tabname, nomodify=False)
    #Remove 0+0j solutions, which can mess up switching pol stations.
    try:
        cvals = mytb.getcol('CPARAM')
        cvals[cvals==0+0j]=1+0j
        mytb.putcol('CPARAM', cvals)
    except RuntimeError:
        #Different spectral windows have different numbers of channels.
        _nrows   = mytb.nrows()
        all_rows = range(_nrows)
        for _row in all_rows:
            thiscval = mytb.getcell('CPARAM', _row)
            thiscval[thiscval==0+0j]=1+0j
            mytb.putcell('CPARAM', _row, thiscval)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


### singleband/multiband and other helper functions
def get_band(_ms_metadata): #
    data_freqs = np.array(_ms_metadata.channels_nu).flatten()
    if min(np.abs(data_freqs - 212191.7891 * 1.e6)) < 1.e9:
        return 1, list(nn_band1)
    ## b2
    elif min(np.abs(data_freqs - 216008.1953 * 1.e6)) < 1.e9:
        return 2, list(nn_band2)

    ## b3
    elif min(np.abs(data_freqs - 226191.7891 * 1.e6)) < 1.e9:
        return 3, list(nn_band3)

    ## b4
    elif min(np.abs(data_freqs - 230008.1953 * 1.e6)) < 1.e9:
        return 4, list(nn_band4)
    else:
        raise ValueError("data_freqs outside of specified EHT bands, it is: " + str(data_freqs) + " <eht_freq_b1,2,3,4> are=212191.7891, 216008.1953, 226191.7891, 230008.1953")


def get_nnscans(visname, nn_ant, pv_ant): ## WARNING maybe there is an existing function for this?
    tb_tool = casac.table()
    tb_tool.open(visname)
    cal = '(ANTENNA1=={}'.format(nn_ant) + ') && (ANTENNA2=={}'.format(pv_ant) + ')'
    scan_rows = tb_tool.query(cal)
    scans = np.unique(scan_rows.getcol("SCAN_NUMBER"))
    tb_tool.close()
    return list(scans)

def _mpi_helper(nn_scans, _inp_params, _mpi_client, nn_band, scratchdir, band, combine, singleband):
    mpi_jobIDs_import = []
    for server in list(_inp_params.MPI_processIDs.keys()):
        mpi_jobIDs_import.append(_mpi_client.push_command_request(
            'from pipe_modules.manual_calibration import fringefit_nn',
            target_server=server            )[0])
    mpi_jobIDs = []
    ### iterate through scans to get all fringe fits
    for scan in nn_scans:
        cmd = ("""fringefit_nn('{0}',""".format(_inp_params.store_ms_metadata)
               +"""'{0}', """.format(str(repr(nn_band)))
               +"""'{0}',""".format(_inp_params.ms_name)
               +"""'{0}',""".format(scratchdir)
               +"""'{0}',""".format(str(scan))
               +"""'{0}',""".format(str(band))
               +"""combine='{0}',""".format(combine) ## WARNING update once casa bug is fixed
               +"""singleband={0},)""".format(singleband) ## WARNING set to True by default, False untested
        )
        mpi_jobIDs.append(_mpi_client.push_command_request(cmd)[0])

    _mpi_client.get_command_response(mpi_jobIDs, True)

def task_NNphasecal(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                 _spwpartition, _mpi_client):
    """
    Executes fringefit_nn to correct NN-phasejumps.
    """
    try:
        ant1 = _ms_metadata.yield_antID('NN')
    except (AttributeError, ValueError) as _:
        print('    Skipping the NN phase offset calibration because NN is not present in this observation.')
        return False
    if auxiliary.is_set(_inp_params, 'NNphasecal_keep_diagnostics'):
        keepdiags = True
    else:
        keepdiags = False
    visname  = _inp_params.ms_name
    pv_ant   = _ms_metadata.yield_antID('PV') # PV ant
    nn_ant   = _ms_metadata.yield_antID('NN') # NN ant
    nn_scans = get_nnscans(visname, nn_ant, pv_ant)  # gets the NN-PV scans
    idx_model, offset_model_rr, offset_model_ll = {}, {} , {}

    # depending on the number spws choose singleband or not. WARNING will in none-singleband mode if not full 128 spw are choosen.
    if len(_ms_metadata.yield_spwds()[0][0]) ==  128:
        singleband = False
    elif len(_ms_metadata.yield_spwds()[0][0]) == 32:
        singleband = True
    else:
        raise ValueError("Unexpected number of spws, can be either 32 (singleband mode) or 128 (bandcombine mode), but is: " + str(len(_ms_metadata.yield_spwds()[0][0])))

    max_chi2   = _inp_params.maxchi2_NNphasecal
    scratchdir = _inp_params.scratchdir + '/'
    auxiliary.makedir(scratchdir)

    # WARNING hardcoded controls, adapt as seen fit
    # print("New: combine spw for ff")
    combine    = "" # WARNING default to "", change for spw combine
    list_tabnames = []

    if _inp_params.verbose:
        print('NN phase cal creation')
        print('Following scans have the NN-PV baseline:', nn_scans)
        print("Singleband mode is actve: ", singleband)
        print("Applying a red. chi^2 max-cutoff for FF solutions of: ", max_chi2)
    if singleband:
        band, nn_band = get_band(_ms_metadata)
        if _mpi_client:
            _mpi_helper(nn_scans, _inp_params, _mpi_client, nn_band, scratchdir, band, combine, singleband)
        else:
            for scan in nn_scans:
                _ = fringefit_nn(_inp_params.store_ms_metadata, repr(nn_band), visname, scratchdir, str(scan), str(band), combine=combine, singleband=singleband)
                list_tabnames = np.append(list_tabnames, _)
        # iterate through scans to get offset tables for the found solutions
        for scan in nn_scans:
            ## WARNING here we have to adapt the nn_band1to 4 arrays so that they don't contain any stupid startspws
            offsets_nn(_ms_metadata, visname, nn_band, str(scan), band, scratchdir, nn_ant, pv_ant, start_nnspw=0,
                       end_nnspw=len(nn_band)-1, max_chi2=max_chi2, singleband=singleband, test=keepdiags
                      )

        # nearest neighbour interpolated/extrapolated
        # the underlying code can be improved, for now just simple nearest neighbour interpolation
        idx_model_, offset_model_rr_, offset_model_ll_ = get_offsetModel(nn_scans, _ms_metadata, scratchdir, band, start_ehtspw=0, end_ehtspw=32) ##
        idx_model["single"]       = idx_model_
        offset_model_rr["single"] = offset_model_rr_
        offset_model_ll["single"] = offset_model_ll_

        # write caltab solution
        get_caltab(_ms_metadata, _inp_params, nn_scans, caltable, idx_model, offset_model_rr, offset_model_ll, singleband, [band], pv_ant, nn_ant)
    else:
        # version for band combine code.
        # we iterate through bands and get the offsets for each band independently.
        for band, nn_band in zip([1,2,3,4], [nn_band1, nn_band2, nn_band3, nn_band4]):
            if _mpi_client:
                _mpi_helper(nn_scans, _inp_params, _mpi_client, list(nn_band), scratchdir, band, combine, singleband)
            else:
                for scan in nn_scans:
                    _ = fringefit_nn(_inp_params.store_ms_metadata, repr(list(nn_band)), visname, scratchdir, str(scan), band,
                                 combine=combine, singleband=singleband
                                )
                    list_tabnames = np.append(list_tabnames, _)

            # iterate through scans to get offset tables for the found solutions
            for scan in nn_scans:
                offsets_nn(_ms_metadata, visname, list(nn_band), str(scan), band, scratchdir, nn_ant, pv_ant, \
                           start_nnspw=0, end_nnspw=len(nn_band)-1, start_ehtspw=32*(band-1), end_ehtspw=32*band, \
                           max_chi2=max_chi2, singleband=singleband, test=keepdiags
                          )

            # nearest neighbour interpolated/extrapolated
            idx_model_, offset_model_rr_, offset_model_ll_ = get_offsetModel(nn_scans, _ms_metadata, scratchdir, band, start_ehtspw=32*(band-1), end_ehtspw=32*band)

            # since we have more than one dict, we make a dict of dicts for the different bands
            idx_model[str(band)]       = idx_model_
            offset_model_rr[str(band)] = offset_model_rr_
            offset_model_ll[str(band)] = offset_model_ll_

        # write caltab solution
        get_caltab(_ms_metadata, _inp_params, nn_scans, caltable, idx_model, offset_model_rr, offset_model_ll, singleband, [1,2,3,4], pv_ant, nn_ant)
    if keepdiags:
        for scan in nn_scans:
            for ehtspw in range(1, 28): #assuming single-band run
                for cor_idx in [0, 1]:
                    _helper_plotsolutions(_ms_metadata, visname, nn_scans, list(nn_band), str(scan), scratchdir, band, singleband, ehtspw,nn_ant, pv_ant, cor_idx)
    # clean up
    print("Starting to clean up tmp_files")
    try:
        for tab in list_tabnames:
            auxiliary.rm_dir_if_present(tab)

    except Exception as e:
        print("Failed to clean up of tmp_files because: ", e)
    return True


def _helper_plotsolutions(_ms_metadata, visname, nn_scans, nn_bands, scan, path, band, singleband, ehtspw,nn_ant, pv_ant, cor_idx, test=False):
    #if singleband:
    #    idx_model_, offset_model_rr_, offset_model_ll_ = get_offsetModel(nn_scans, scratchdir, band, start_ehtspw=0, end_ehtspw=32)
    #else:
    #    idx_model_, offset_model_rr_, offset_model_ll_ = get_offsetModel(nn_scans, scratchdir, band, start_ehtspw=32*(band-1), end_ehtspw=32*band)

    if singleband:
        visphi, visphi_error = get_visphi(visname, band, scan, nn_ant, pv_ant, start_ehtspw=0, end_ehtspw=32, singleband=singleband)
    else:
        visphi, visphi_error = get_visphi(visname, band, scan, nn_ant, pv_ant, start_ehtspw=int((int(band)-1)*32), end_ehtspw=int(int(band)*32), singleband=singleband)

    model_dict_rr, model_dict_ll = {}, {}

    for cor in [0,3]:  # 0: RR, 3: LL
        for nnspw in range(0, len(nn_bands)-1):
            try:
                freq1_raw, model1_raw, phasejump_spw, phasejump_idx = get_phase_nn(_ms_metadata, visphi, scan, band, cor, path,
                                                                                   nn_bands, nnspw, pv_ant, test=test, singleband=singleband
                                                                                  )
                if cor == 0:
                    model_dict_rr[str(phasejump_spw)] = [freq1_raw, model1_raw, phasejump_spw, phasejump_idx]
                else:
                    model_dict_ll[str(phasejump_spw)] = [freq1_raw, model1_raw, phasejump_spw, phasejump_idx]
            except NNfringefitError as e:
                print(f"Couldn't fringefit band, scan, nnspw: {band}, {scan},{nnspw}")
    model_dict = [model_dict_rr, model_dict_ll]
    offset_rr, offset_ll = {}, {}
    cor = [0,3]
    if singleband:
        spw_off = 0
    else:
        spw_off = int((int(band)-1)*32)


    idx1, idx2 = get_matchingspws(ehtspw, model_dict[cor_idx])

    freq1_raw, model1_raw, phasejump_spw1, phasejump_idx1 = model_dict[cor_idx][str(idx1)]
    freq2_raw, model2_raw, phasejump_spw2, phasejump_idx2 = model_dict[cor_idx][str(idx2)]

    model1 = interp1d(freq1_raw, model1_raw, bounds_error=False, fill_value="extrapolate")
    model2 = interp1d(freq2_raw, model2_raw, bounds_error=False, fill_value="extrapolate")

    try:
        ## get jump idx in ehtspw
        jumpidx = get_jumpindx(_ms_metadata, ehtspw, nn_bands, band)
    except:
        return

    ## get_offset
    offset = get_distance(_ms_metadata, model1, model2, ehtspw, jumpidx)
    print("offset:", offset)
    chi = get_chi2red(_ms_metadata, visphi[cor[cor_idx], ehtspw-spw_off].copy(), visphi_error[cor[cor_idx], ehtspw-spw_off].copy(),
                      band, offset, ehtspw, jumpidx, model1)
    print("chi2:", chi)

    plt.plot(_ms_metadata.yield_freq_of_channels_in_spw(ehtspw), visphi[cor_idx, ehtspw,:], ".k")

    plt.plot(_ms_metadata.yield_freq_of_channels_in_spw(ehtspw-1), visphi[cor_idx, ehtspw-1,:], ".", c="grey")
    plt.plot(_ms_metadata.yield_freq_of_channels_in_spw(ehtspw+1), visphi[cor_idx, ehtspw+1,:], ".", c="grey")
    #plt.plot(freq1_raw, model1(freq1_raw), "-", color="C0")
    #plt.plot(freq2_raw, model2(freq2_raw), "-", color="C0")
    #plt.plot(freq2_raw, np.angle(np.exp(1j*model2(freq2_raw)) * np.exp(1j*np.radians(offset))), "--", color="C1")
    plt.scatter(freq1_raw, model1(freq1_raw), color="C0")
    plt.scatter(freq2_raw, model2(freq2_raw), color="C0")
    plt.scatter(freq2_raw, np.angle(np.exp(1j*model2(freq2_raw)) * np.exp(1j*np.radians(offset))), color="C1")
    #plt.plot(freq2_raw, np.angle(np.exp(1j*model2(freq2_raw)) * np.exp(1j*np.radians(270))), ":")
    plt.savefig('helpplot_scan'+scan+'_ehtspw'+str(ehtspw)+'_corr'+str(cor_idx)+'_offset'+str(offset)+'_chi2'+str(chi)+'.png')
    plt.close('all')
