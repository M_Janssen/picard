import os
import interactive_utils
#Script to image a SOURCE from data in a UVFITS_file with a few non-default options.
#Call the script with:
#mpicasa -n 4 /usr/local/src/casa-feature-CAS-10684-30.el7/bin/casa --nologger -c goimage.py 2>/dev/null
#for a CASA version installed in /usr/local/src/casa-feature-CAS-10684-30.el7

SOURCE      = '3C279'
UVFITS_file = 'example.uvf'


os.system('rm -r {0}.{1}.ms*'.format(UVFITS_file, SOURCE))


interactive_utils.imager(SOURCE, UVFITS_file,
                         cellsize='0.000001arcsec',
                         imsize=512,
                         interactive=True,
                         goautomated=False,
                         solint_denominator=2,
                         usemask='user',
                         robust=-2,
                         niter0=500,
                         startsolint=10,
                         mask='',
                         gain=0.005,
                         cleaniterations='constant'
                        )
