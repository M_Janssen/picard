#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Routines to correct for ionospheric effects beyond what fringefit can do. """
import pipe_modules.auxiliary   as auxiliary
import pipe_modules.calibration as calibration
from pipe_modules.default_casa_imports import *


def task_ionospheric_tec_map(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                             _spwpartition, _mpi_client):
    """
    Standard CASA method to correct for ionospheric delays using a map of the total electron content (tec) above each station.
    The tec map will be downloaded from the internet.
    """
    print('!!As the TEC correction does not seem to be doing the correct astrometry, it will be skipped!!')
    return False
    if not auxiliary.is_set(_inp_params, 'perform_tec_corr'):
        print('    Skipping ionospheric TEC correction because the perform_tec_corr parameter is not set.')
        return False
    GHzfreq_thresh = _inp_params.perform_tec_corr
    GHzfreq_data   = _ms_metadata.smallest_GHzdatafreq
    if GHzfreq_thresh < _ms_metadata.smallest_GHzdatafreq:
        prt_msg = '    Skipping ionospheric TEC correction because the {0} GHz smallest obs. freq.'.format(str(GHzfreq_data))
        prt_msg+= ' is beyond the specified perform_tec_corr limit of {0} GHz.'.format(str(GHzfreq_thresh))
        print(prt_msg)
        return True
    print('\n  This task will need an internet connection to download a TEC map!')
    if _inp_params.verbose:
        print('  Note: On-the-fly calibration is actually ignored for the ionospheric_tec_map task.')
    tec_image, _ , _ = tec_maps.create(vis=_inp_params.ms_name)
    calibration.task_gencal_general(_inp_params, caltable, 'tecim', tec_image)
    return True
