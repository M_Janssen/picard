#
# Copyright (C) 2016
# Joint Institute for VLBI ERIC, Dwingeloo, The Netherlands
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#

#Script from Mark Kettenis with slight modifications
import sys
import pipe_modules.JIVE_scripts.key as key
import datetime


def flag(flagfile, first_date, out_fg_file):

    fp = open(flagfile, 'r')
    flags = key.read_keyfile(fp)
    fp.close()

    flags = [dict(x) for x in flags]

    for flag in flags:
        antenna = flag['ANT_NAME'].upper()
        timerange = flag['TIMERANG']
        if timerange == [0.0, 0.0, 0.0, 0.0, 400.0, 0.0, 0.0, 0.0]:
            timerange = ""
        else:
            #take the year of the first date in the fits-idi files as reference for the day-of-year values in the uvflg file:
            year = datetime.datetime(datetime.datetime.strptime(first_date, '%Y/%m/%d').year, 1, 1)
            year_date1 = str(( year + datetime.timedelta(timerange[0]) - datetime.timedelta(1) ).strftime('%Y/%m/%d'))
            year_date2 = str(( year + datetime.timedelta(timerange[4]) - datetime.timedelta(1) ).strftime('%Y/%m/%d'))
            timerange = "%s/%02d:%02d:%02d~%s/%02d:%02d:%02d" % \
                (year_date1, timerange[1], timerange[2], timerange[3],
                 year_date2, timerange[5], timerange[6], timerange[7])
            pass
        if 'BIF' in flag:
            spw = "%d~%d" % (flag['BIF'] -1, flag['EIF'] - 1)
        else:
            spw = ""
            pass
        if 'BCHAN' in flag:
            spw = spw + ":%d~%d" % (flag['BCHAN']-1, flag['ECHAN']-1)
            pass
        cmd = "antenna='%s'" % antenna
        if timerange:
            cmd = cmd + " timerange='%s'" % timerange
        if spw:
            cmd = cmd + " spw='%s'" % spw
            pass
        out_fg_file.write(cmd+'\n')
