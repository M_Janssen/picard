#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Phase calibration tasks (fringe-fitting, complex bandpass). """
from decimal import Decimal
from ast import literal_eval
from collections import namedtuple, defaultdict, OrderedDict
import os
import copy
import time
import json
import shutil
import datetime
import numpy as np
import pipe_modules.auxiliary                as auxiliary
import pipe_modules.calibration              as calibration
import pipe_modules.diagnostics              as diagnostics
import pipe_modules.flagging_algorithm       as flagging_algorithm
import pipe_modules.exhaustive_fringe_search as exhaustive
from pipe_modules.default_casa_imports import *


wrote_scan_refants_file_sci = False
wrote_scan_refants_file_cal = False


def determine_refants_sc_per_scan(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                  delay_win=[], rate_win=[], corrcomb='none', sourcetype='all', _mpi_client=False):
    """
    Goes through all scans for the specified sourcetype and picks the first station from the prioritized refant list which has
    at least _inp_params.refant_minvaliddata valid data. Considers only the data from the line spw for spectral line experiments.
    Plus, sub clusters from an exhaustive_baseline_search() are added.
    All this information will be written as dictionaries to disk.
    """
    if sourcetype not in ['sci', 'cal', 'all']:
        raise ValueError('sourcetype must be sci, cal, or all. Got ' + str(sourcetype))
    global wrote_scan_refants_file_sci
    global wrote_scan_refants_file_cal
    sci_refants = {}
    cal_refants = {}
    sci_file    = _inp_params.store_scan_refants+'.sci'
    cal_file    = _inp_params.store_scan_refants+'.cal'
    comb_dict   = {}
    do_compute  = True
    if auxiliary.isfile(sci_file) and (_inp_params.fringe_params_load or wrote_scan_refants_file_sci):
        sci_refants = auxiliary.store_object(sci_file, _operation='read')
        if sourcetype=='sci':
            do_compute = False
    if auxiliary.isfile(cal_file) and (_inp_params.fringe_params_load or wrote_scan_refants_file_cal):
        cal_refants = auxiliary.store_object(cal_file, _operation='read')
        if sourcetype=='cal':
            do_compute = False
    if sourcetype=='all' and sci_refants and cal_refants:
        do_compute = False
    comb_dict.update(sci_refants)
    comb_dict.update(cal_refants)
    if do_compute:
        if _inp_params.verbose:
            print('  Determining scan-refants from scratch.')
        refantlist    = _inp_params.refant.split(',')
        try:
            refantlistIDs = [_ms_metadata.yield_antID(ant) for ant in refantlist]
        except ValueError:
            _errmsg = '{0} is not a valid refant (not present in the data).\n'.format(str(ant))
            _errmsg += 'Please remove it from the refant input parameter list.'
            raise ValueError(_errmsg)
        if sourcetype=='sci':
            these_sources = _ms_metadata.yield_science_targets(_inp_params)
        elif sourcetype=='cal':
            these_sources = _ms_metadata.yield_calibrators(_inp_params)
        else:
            all_selected  = _ms_metadata.selected_scans[0] + auxiliary.flatten_list(_ms_metadata.selected_scans[1])
            these_sources = [item[0] for item in all_selected]
        scratchdir = _inp_params.scratchdir + '/'
        auxiliary.makedir(scratchdir)
        these_scans = auxiliary.flatten_list(_ms_metadata.yield_scans(these_sources, do_not_squeeze_single_source=True))
        N_textfiles = []
        mpi_jobIDs  = []
        if _mpi_client:
            mpi_jobIDs_import = []
            for server in list(_inp_params.MPI_processIDs.keys()):
                mpi_jobIDs_import.append(_mpi_client.push_command_request(
                                        'from pipe_modules.phase_calibration import refant_sc_per_scan_compute',
                                         target_server=server            )[0])
            _mpi_client.get_command_response(mpi_jobIDs_import, True)
        for scan in these_scans:
            if scan in comb_dict.keys():
                continue
            these_scanants = _ms_metadata.yield_antennas(str(scan))
            this_textfile  = auxiliary.unique_filename(scratchdir + 'scan' + str(scan) + '.refant')
            if _mpi_client:
                cmd = ("""refant_sc_per_scan_compute('{0}',""".format(_inp_params.ms_name)
                      +"""'{0}',""".format(this_textfile)
                      +"""{0},""".format(str(refantlist))
                      +"""{0},""".format(str(refantlistIDs))
                      +"""{0},""".format(str(list(these_scanants)))
                      +"""{0},""".format(str(scan))
                      +"""{0},""".format(str(_inp_params.refant_minvaliddata))
                      +"""{0},""".format(str(_ms_metadata.line_spw))
                      +"""{0})""".format(str(_inp_params.verbose))
                      )
                mpi_jobIDs.append(_mpi_client.push_command_request(cmd)[0])
            else:
                refant_sc_per_scan_compute(_inp_params.ms_name, this_textfile, refantlist, refantlistIDs, these_scanants, scan,
                                           _inp_params.refant_minvaliddata, _ms_metadata.line_spw, _inp_params.verbose
                                          )
            N_textfiles.append(this_textfile)
        if _mpi_client:
            _mpi_client.get_command_response(mpi_jobIDs, True)
        refantstore = {}
        for sr_file in N_textfiles:
            with open(sr_file, 'r', encoding='utf-8') as f:
                for line in f:
                    sr = line.split(':')
                    refantstore[sr[0]] = sr[1]
            auxiliary.rm_file_if_present(sr_file)
        refantstore_plus = copy.deepcopy(refantstore)
        if sourcetype == 'sci' and auxiliary.is_set(_inp_params, 'fringe_exhaustive_refant_search_islands'):
            allow_isolations = True
        else:
            allow_isolations = False
        fringe_dict, fringe_subclusters = exhaustive_baseline_search(_inp_params, _ms_metadata, these_sources, refantstore,
                                                                     _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                                                     delay_win, rate_win, corrcomb, allow_isolations, _mpi_client
                                                                    )
        if _inp_params.diag_fringe_overview:
            ff_overview = _inp_params.diag_fringe_overview + '.' + sourcetype
        else:
            ff_overview = None
        if fringe_dict:
            diagnostics.print_fringe_overview(_inp_params, _ms_metadata, fringe_dict, ff_overview)
        for scan in refantstore_plus:
            if fringe_subclusters:
                refantstore_plus[scan] = (refantstore_plus[scan], fringe_subclusters[scan])
            else:
                refantstore_plus[scan] = (refantstore_plus[scan], False)
        if sourcetype=='sci':
            auxiliary.store_object(sci_file, refantstore_plus, 'write')
            wrote_scan_refants_file_sci = True
        elif sourcetype=='cal':
            auxiliary.store_object(cal_file, refantstore_plus, 'write')
            wrote_scan_refants_file_cal = True
        elif sourcetype=='all':
            sci_scans = auxiliary.flatten_list(_ms_metadata.yield_scans(_ms_metadata.yield_science_targets(_inp_params)), True)
            cal_scans = auxiliary.flatten_list(_ms_metadata.yield_scans(_ms_metadata.yield_calibrators(_inp_params)), True)
            for scan in refantstore_plus:
                if scan in sci_scans:
                    sci_refants[scan] = refantstore_plus[scan]
                elif scan in cal_scans:
                    cal_refants[scan] = refantstore_plus[scan]
                else:
                    raise ValueError('Unknown scan ' + str(scan))
            auxiliary.store_object(sci_file, sci_refants, 'write')
            auxiliary.store_object(cal_file, cal_refants, 'write')
            wrote_scan_refants_file_sci = True
            wrote_scan_refants_file_cal = True
        comb_dict.update(refantstore_plus)
        if allow_isolations:
            allscan_flags = ''
            for scan in fringe_subclusters:
                if not fringe_subclusters[scan]:
                    continue
                cluster_per_primary_refant = {}
                for path in fringe_subclusters[scan].keys():
                    ref = path.split(',')[0]
                    these_ants = fringe_subclusters[scan][path]
                    these_ants.extend(path.split(','))
                    for ant in these_ants:
                        try:
                            if ant not in cluster_per_primary_refant[ref]:
                                cluster_per_primary_refant[ref].append(ant)
                            else:
                                pass
                        except KeyError:
                            cluster_per_primary_refant[ref] = [ant]
                reflist = list(cluster_per_primary_refant.keys())
                #Handle case of an isolated cluster around a secondary refant with no primary refant conections
                if not refantstore[scan] in reflist:
                    reflist.append(refantstore[scan])
                    cluster_per_primary_refant[refantstore[scan]] = [refantstore[scan]]
                if len(reflist) == 1:
                    continue
                antflags = ''
                for r1 in reflist:
                    for a1 in cluster_per_primary_refant[r1]:
                        for r2 in reflist:
                            if a1 in cluster_per_primary_refant[r2]:
                                continue
                            for a2 in cluster_per_primary_refant[r2]:
                                if "{0}&{1};".format(str(a1), str(a2)) in antflags \
                                or "{0}&{1};".format(str(a2), str(a1)) in antflags:
                                    continue
                                antflags+= "{0}&{1};".format(str(a1), str(a2))
                if antflags:
                    allscan_flags+= "scan='{0}' antenna='{1}'\n".format(str(scan), antflags[:-1])
            if allscan_flags:
                fg_fnam = auxiliary.unique_filename(_inp_params.diagdir + _inp_params.flag_dir + 'EXH_FRINGE_ISOLATIONS.CFG')
                #Create flag_dir folder.
                flagging_algorithm.go_flag(_inp_params, _ms_metadata, 'pass')
                fg_file = open(fg_fnam, 'w', encoding='utf-8')
                fg_file.write(allscan_flags)
                fg_file.close()
                flagging_algorithm.task_flagdata(_inp_params, fg_fnam)
                print('>>Applied baseline flags for isolated subclusters stored in '+fg_fnam)
    return comb_dict


def refant_sc_per_scan_compute(ms_name, txt_outfile, _refantlist, _refantlistIDs, _these_scanants, _scan, _refant_minvaliddata,
                               spw=-1, _verbose=False):
    """
    Single function that can run in MPI mode to compute optimal reference antennas for determine_refants_sc_per_scan().
    To be called per scan to write <scan>:<refant> in txt_outfile.
    """
    gotscore   = False
    ant_scores = []
    this_score = 0.
    max_score  = 0.
    for ant, antID in zip(_refantlist, _refantlistIDs):
        if antID in _these_scanants:
            this_score = float(diagnostics.station_scan_flag_perc(ms_name, antID, _scan, spw))
        else:
            this_score = 0
        ant_scores.append(this_score)
        if this_score > _refant_minvaliddata:
            if max_score and auxiliary.frac_diff(max_score, this_score) < 0.15:
                #No significant improvement over the amount of valid data of a previous preferred refant
                pass
            else:
                refantstore = ant
                refantscore = this_score
                gotscore    = True
                break
        if this_score > max_score:
            max_score = this_score
    if not gotscore:
        best_score = max(ant_scores)
        for this_index, this_score in enumerate(ant_scores):
            if auxiliary.frac_diff(best_score, this_score) < 0.15:
                refantstore = _refantlist[this_index]
                refantscore = ant_scores[this_index]
                break
            else:
                pass
    if _verbose:
        infoprt = '    Refant {0} for scan {1} with {2:.1f}% good data.'.format(refantstore, str(_scan), 100.*refantscore)
        print(infoprt)
    with open(txt_outfile, 'w', encoding='utf-8') as f:
        f.write('{0}:{1}'.format(str(_scan), str(refantstore)))


def exhaustive_baseline_search(_inp_params, _ms_metadata, sources, primary_scan_refants, _fly_calib_tables, _fly_calib_interp,
                               _fly_calib_gainfd, delay_win=[], rate_win=[], corrcomb='none', allow_isolations= False,
                               _mpi_client=False):
    """
    For all scans on sources, determines optimal paths for an exhaustive baseline search via connected sub-clusters of stations
    (see documentation).
    The basic idea is that for a set of refants, each may have connecting fringes to distinct set of stations -- thereby forming
    'sub-clusters' or 'islands' of stations. These islands can be connected in a global fringe-fit if fringes are found between
    the reference stations. The connections are made by re-referencing all fringe solutions within a sub-cluster.
    Returns:
      - fringe_overview, containing all fringes searched with SNR for each [scan][src][refant].
      - subcluster_connections, containing all paths to be taken to connect sub-clusters for each [src][scan].
    """
    _debug = False
    if not sources:
        return False, False
    min_constraints  = 2
    max_search_depth = _inp_params.fringe_exhaustive_refant_search_depth
    if _inp_params.science_target and sources[0] in _inp_params.science_target:
        minsnr = _inp_params.fringe_minSNR_mb_long_sci
    else:
        minsnr = _inp_params.fringe_minSNR_sb_instrumental
    #'Punish' detections from an exhaustive fringe search (they add noise).
    minsnr_exhaustive = 1.01 * minsnr
    if auxiliary.is_set(_inp_params, 'spwpartition_exhaustive_refant_search_selection'):
        spw_exhaustive = _inp_params.spwpartition_exhaustive_refant_search_selection
    else:
        spw_exhaustive = ''
    scratchdir = _inp_params.scratchdir + '/'
    auxiliary.makedir(scratchdir)
    all_refants = _inp_params.refant.split(',')
    #Stores all FFT fringes per src,scan,refant.
    fringe_overview_all = defaultdict(lambda: defaultdict(dict))
    #Stores the shortest paths to connect sub-clusters for the most detections per src and scan.
    subcluster_connections_all = {}
    sc_conn_files_list         = []
    fo_files_list              = []
    testtab_list               = []
    if _inp_params.verbose:
        print('  Determining scan-FFTs, potentially for an exhaustive fringe search.')
    balance_mem = False
    if _mpi_client:
        mpi_jobIDs_import = []
        for server in list(_inp_params.MPI_processIDs.keys()):
            mpi_jobIDs_import.append(_mpi_client.push_command_request(
                                    'from pipe_modules.phase_calibration import exh_basel_compute',
                                     target_server=server            )[0])
        _mpi_client.get_command_response(mpi_jobIDs_import, True)
        mpi_jobIDs = []
        if _inp_params.mpi_memory_safety or _inp_params.long_solint_no_mpi:
            auxiliary.wait_until_memory_settles(_inp_params, for_fringefit=True)
            free_memory0, allowed_mem = auxiliary.get_free_memory()
            balance_mem               = True
            N_PIDs                    = len(_inp_params.MPI_processIDs.values())
            N_active                  = 0
    #For MPI memory management, sort scans by increasing number of visibilities:
    src_scan_tuples = []
    for src in sources:
        scans = _ms_metadata.yield_scans(src)
        for scan in scans:
            Nvis = _ms_metadata.yield_numvisi(scan)
            src_scan_tuples.append((src, scan, Nvis))
    src_scan_tuples = sorted(src_scan_tuples, key=lambda x: x[2])[::-1]
    for sst in src_scan_tuples:
        src  = sst[0]
        scan = sst[1]
        primary_refant = primary_scan_refants[scan]
        if primary_refant not in all_refants:
            raise ValueError('Primary refant {0} not in refant list {1}'.format(str(primary_refant), str(all_refants)))
        sc_conn_file = auxiliary.unique_filename('{0}/sc_conn_scan{1}'.format(scratchdir, str(scan)))
        fo_file      = auxiliary.unique_filename('{0}/fo_src{1}_scan{2}'.format(scratchdir, str(src), str(scan)))
        if  _mpi_client:
            if not _fly_calib_tables:
                _fly_calib_tables = []
            if not _fly_calib_interp:
                _fly_calib_interp = "''"
            cmd = ("""exh_basel_compute("{0}",""".format(repr(dict(_inp_params._asdict())))
                  +"""'{0}',""".format(_inp_params.store_ms_metadata)
                  +"""'{0}',""".format(sc_conn_file)
                  +"""'{0}',""".format(fo_file)
                  +"""{0},""".format(str(all_refants))
                  +"""'{0}',""".format(primary_refant)
                  +"""'{0}',""".format(src)
                  +"""{0},""".format(str(scan))
                  +"""'{0}',""".format(scratchdir)
                  +"""{0},""".format(str(minsnr))
                  +"""{0},""".format(str(minsnr_exhaustive))
                  +"""{0},""".format(str(_fly_calib_tables))
                  +""" "{0}", """.format(str(_fly_calib_gainfd))
                  +"""{0},""".format(str(_fly_calib_interp))
                  +"""{0},""".format(str(list(delay_win)))
                  +"""{0},""".format(str(list(rate_win)))
                  +"""'{0}',""".format(str(corrcomb))
                  +"""'{0}',""".format(spw_exhaustive)
                  +"""{0},""".format(str(min_constraints))
                  +"""{0},""".format(str(max_search_depth))
                  +"""{0},""".format(str(allow_isolations))
                  +"""mpi_compute=True,"""
                  +"""_debug={0})""".format(str(_debug))
                  )
            mpi_jobIDs.append(_mpi_client.push_command_request(cmd)[0])
            if balance_mem:
                if N_active > N_PIDs:
                    pass
                elif N_active:
                    N_active = auxiliary.free_mem_continue(_inp_params, N_active)
                else:
                    # Use a simple fudge factor here, assuming no process will need much more mem than the first.
                    N_active = int(0.8*free_memory0 / auxiliary.get_max_used_mem(_inp_params))
        else:
            exh_basel_compute(_inp_params, _ms_metadata, sc_conn_file, fo_file, all_refants, primary_refant, src,
                              scan, scratchdir, minsnr, minsnr_exhaustive, _fly_calib_tables, _fly_calib_gainfd,
                              _fly_calib_interp, delay_win, rate_win, corrcomb, spw_exhaustive, min_constraints,
                              max_search_depth, allow_isolations=allow_isolations, mpi_compute=False, _debug=_debug
                             )
        sc_conn_files_list.append(sc_conn_file)
        fo_files_list.append(fo_file)
    if _mpi_client:
        _mpi_client.get_command_response(mpi_jobIDs, True)
    missing_scans = []
    for scf, fof in zip(sc_conn_files_list, fo_files_list):
        try:
            with open (scf, 'r', encoding='utf-8') as f:
                for line in f:
                    if not line:
                        continue
                    lline = line.split(';')
                    if lline[1] == 'False':
                        subcluster_connections_all[lline[0]] = False
                    else:
                        subcluster_connections_all[lline[0]] = json.load(open(lline[1], encoding='utf-8'))
                        auxiliary.rm_file_if_present(lline[1])
            auxiliary.rm_file_if_present(scf)
            with open(fof, 'r', encoding='utf-8') as f:
                for line in f:
                    if not line:
                        continue
                    lline = line.split(';')
                    if lline[3] == 'False':
                        fringe_overview_all[lline[0]][lline[1]][lline[2]] = False
                    else:
                        fname = lline[3].rstrip('\n')
                        fringe_overview_all[lline[0]][lline[1]][lline[2]] = json.load(open(fname, encoding='utf-8'))
                        auxiliary.rm_file_if_present(fname)
            auxiliary.rm_file_if_present(fof)
        except FileNotFoundError:
            this_scan = scf.split('sc_conn_scan')[-1].split('.')[0]
            missing_scans.append(this_scan)
            subcluster_connections_all[this_scan] = False
    if any(missing_scans):
        print('  Did not find good fringe parameters for these scans:')
        print(auxiliary.wrap_list(missing_scans, items_per_line=5, indent='    '))
        print('    I recommend to expand your refant list!')
        print('    Most likely no good refant was present for these scans from the current selection.')
    if _debug:
        print ('All subcluster_connections:')
        print (subcluster_connections_all)
    return fringe_overview_all, subcluster_connections_all


def exh_basel_compute(_inp_params, _ms_metadata, sc_conn_file, fo_file, all_refants, primary_refant, src, scan,
                      scratchdir, minsnr, minsnr_exhaustive, _fly_calib_tables, _fly_calib_gainfd, _fly_calib_interp, delay_win,
                      rate_win, corrcomb, spw_exhaustive, min_constraints, max_search_depth, allow_isolations=False, mpi_compute=False,
                      _debug=False):
    """
    Single function that can run in MPI mode to compute per-scan fringe overviews and subcluster connections for
    exhaustive_baseline_search().
    If mpi_compute is set, inp_params must be passed as repr(dict(_inp_params._asdict()))
    and _ms_metadata as _inp_params.store_ms_metadata.
    With allow_isolations=True, isolated sub-arrays with fringe detections between telescopes that cannot be connected to the
    primary refant are allowed to pass; should be set only for science targets after all instrumental phases have been aligned
    across the array.
    """
    def write_results(sc, fo):
        with open(sc_conn_file, 'w', encoding='utf-8') as f:
            if sc==False:
                f.write(str(scan) + ';' + str(sc))
            else:
                json.dump(sc, open(sc_conn_file+'.dict.json', 'w', encoding='utf-8'))
                f.write(str(scan) + ';' + sc_conn_file+'.dict.json')
        with open(fo_file, 'w', encoding='utf-8') as f:
            if fo==False:
                f.write(str(src) + ';' + str(scan) + ';' + str(primary_refant) + ';' + str(fo))
            else:
                for refant in fo:
                    json.dump(fo[refant], open(fo_file+'.refant{0}.dict.json'.format(refant), 'w', encoding='utf-8'))
                    f.write(str(src) + ';' + str(scan) + ';' + refant + ';' + fo_file+'.refant{0}.dict.json\n'.format(refant))
    if mpi_compute:
        _ms_metadata = auxiliary.store_object(_ms_metadata, _operation='read')
        back_to_dict = literal_eval(_inp_params)
        _Inp_params  = namedtuple('_Inp_params', back_to_dict)
        _inp_params  = _Inp_params(**back_to_dict)
    if isinstance(_fly_calib_gainfd, str):
        _fly_calib_gainfd = eval(_fly_calib_gainfd)
    if auxiliary.is_set(_inp_params, 'fringe_exhaustive_refant_search_threshold'):
        weak_threshold = _inp_params.fringe_exhaustive_refant_search_threshold
    else:
        weak_threshold = 1.5
    #Keep track of useful refants that have usable connected fringes in the order of preference for this scan.
    useful_refants = copy.deepcopy(all_refants)
    useful_refants.remove(primary_refant)
    useful_refants = [primary_refant] + useful_refants
    #Make  list of refants with the primary one removed (comes first).
    other_refants = copy.deepcopy(all_refants)
    other_refants.remove(primary_refant)
    testtable = auxiliary.unique_filename(scratchdir+'/tmp_fringedetections.{0}.{1}'.format(str(scan), str(primary_refant)))
    #Look for detections to this scan's primary refant first
    task_fringefit_general(_inp_params, _ms_metadata, testtable, primary_refant, src, scan=scan,
                           combine='spw', minsnr=minsnr, gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                           interp=_fly_calib_interp, globalsolve=False, delaywindow=delay_win, ratewindow=rate_win,
                           corrcomb=corrcomb, _spwpartition=spw_exhaustive
                          )
    if not auxiliary.isdir(testtable):
        #Unusable scan.
        write_results(False, False)
        if _inp_params.verbose:
            print('    Processed scan {0}'.format(scan))
        return
    these_fringes   = exhaustive.get_detections(_inp_params, _ms_metadata, testtable, scan)
    fringe_overview = {}
    #Note that the refant is not included in these_fringes.
    fringe_overview[primary_refant] = these_fringes
    #Non-detections is initialized here and will be adjusted whenever new detections are found in the exhaustive search.
    detections, non_detections, weak_detections = exhaustive.extract_detections_per_station(these_fringes, minsnr*weak_threshold)
    auxiliary.rm_dir_if_present(testtable)
    search_exhaustively = bool(_inp_params.fringe_exhaustive_refant_search_depth)
    if not any(detections) and not allow_isolations:
        #Cannot do anything if there are no detections to the primary refant.
        search_exhaustively = False
    if not any(non_detections) and not weak_detections:
        #Cannot extend detections because they are complete or no path to another sub-cluster can be made.
        search_exhaustively = False
    if not allow_isolations and not any(auxiliary.overlap_lists(detections, other_refants)):
        #Cannot extend detections because no path to another sub-cluster can be made.
        search_exhaustively = False
    if not search_exhaustively:
        subcluster_connections = False
    else:
        #Stores all new detections and connections to other refants for a given secondary refant.
        sub_cluster_paths                            = defaultdict(dict)
        sub_cluster_paths[primary_refant]['detect']  = detections
        sub_cluster_paths[primary_refant]['connect'] = auxiliary.overlap_lists(useful_refants, detections)
        if _debug:
            print ('Scan and a few variables:')
            print (scan)
            print (other_refants)
            print (these_fringes)
            print (detections, non_detections, auxiliary.overlap_lists(detections, other_refants))
            print (sub_cluster_paths)
        #Go through all secondary refants to look for more fringes that were not found to the primary refant:
        for secondary_refant in other_refants:
            testtable = auxiliary.unique_filename(scratchdir+'/tmp_fringedetections.{0}.{1}'.format(str(scan),
                                                                                                    str(secondary_refant)
                                                                                                   ))
            try:
                task_fringefit_general(_inp_params, _ms_metadata, testtable, secondary_refant, src, scan=scan,
                                       combine='spw', minsnr=minsnr_exhaustive, gaintable=_fly_calib_tables,
                                       gainfield=_fly_calib_gainfd, interp=_fly_calib_interp, globalsolve=False,
                                       delaywindow=delay_win, ratewindow=rate_win, corrcomb=corrcomb,
                                       _spwpartition=spw_exhaustive, only_refant = secondary_refant
                                      )
            except:
                useful_refants.remove(secondary_refant)
                continue
            if not auxiliary.isdir(testtable):
                useful_refants.remove(secondary_refant)
                continue
            these_fringes                                = exhaustive.get_detections(_inp_params, _ms_metadata,
                                                                                     testtable, scan
                                                                                    )
            fringe_overview[secondary_refant]            = these_fringes
            these_detections, _, detection_sns           = exhaustive.extract_detections_per_station(these_fringes,
                                                                                                     minsnr*weak_threshold, True
                                                                                                    )
            auxiliary.rm_dir_if_present(testtable)
            new_detections = auxiliary.overlap_lists(non_detections, these_detections)
            for wd in weak_detections:
                try:
                    # Use exhaustive refants (at a max_search_depth of 1) when detections to primary refant are possible but
                    # too weak (with a margin of 10%).
                    if detection_sns[wd] > 1.1*weak_detections[wd]:
                        new_detections.append(wd)
                        non_detections.append(wd)
                        d1 = sub_cluster_paths[primary_refant]['detect']
                        d1.remove(wd)
                        sub_cluster_paths[primary_refant]['detect'] = d1
                        c1 = sub_cluster_paths[primary_refant]['connect']
                        if wd in c1:
                            c1.remove(wd)
                            sub_cluster_paths[primary_refant]['connect'] = c1
                except KeyError:
                    pass
            #If a connection to a useful refant is made, that refant will have at least one detection.
            #Two, if that refant on its own is to be useful further on.
            #This should satisfy the min_constraints detection below automatically.
            other_refants_connect = auxiliary.overlap_lists(useful_refants, these_detections)
            has_usable_detections = True
            if len(these_detections) < min_constraints-int(allow_isolations):
                #Cannot constrain fringe solutions.
                has_usable_detections = False
            elif not any(auxiliary.overlap_lists(useful_refants, these_detections)) and not allow_isolations:
                #Isolated sub-cluster that cannot be connected to another refant.
                has_usable_detections = False
            elif not any(new_detections) and len(other_refants_connect)<2:
                #Cannot get new detections and cannot use this refant to connect other sub-clusters.
                has_usable_detections = False
            if has_usable_detections:
                sub_cluster_paths[secondary_refant]['detect']  = new_detections
                sub_cluster_paths[secondary_refant]['connect'] = other_refants_connect
            else:
                useful_refants.remove(secondary_refant)
        if _debug:
            print ('Sub cluster paths:')
            print (sub_cluster_paths)
            print ('Useful refants:')
            print (useful_refants)
            print ('Fringe overview with sub-clusters:')
            print (fringe_overview)
        for name in useful_refants:
            if name in sub_cluster_paths:
                path_start = name
                break
        subcluster_connections = exhaustive.find_shortest_path(useful_refants, path_start, sub_cluster_paths, max_search_depth)
        if allow_isolations:
            calibratable_ants = []
            for ant in list(subcluster_connections.values()):
                calibratable_ants+= ant
            isolated_rest = [exhaustive.find_shortest_path(useful_refants, a, sub_cluster_paths, max_search_depth) \
                             for a in useful_refants[1:]
                            ]
            for ri in isolated_rest:
                for path in ri.keys():
                    isolated_new_detections = {}
                    for ant in ri[path]:
                        if ant not in calibratable_ants:
                            try:
                                isolated_new_detections[path].append(ant)
                            except KeyError:
                                isolated_new_detections[path] = [ant]
                            calibratable_ants+= [ant]
                        else:
                            pass
                    subcluster_connections.update(isolated_new_detections)
        if _inp_params.verbose and any(non_detections):
            print('    -- Exhaustive fringe search for scan {0}, for {1} --'.format(scan, ','.join(non_detections)))
    if _inp_params.verbose:
        print('    Processed scan {0}'.format(scan))
    write_results(subcluster_connections, fringe_overview)


def communicate_fringe_solint(_inp_params, _ms_metadata, _mode, _fringe_solints={}, _fringe_refants={}, _source='', _scan='',
                              _fly_calib_tables=[], _fly_calib_interp=[], _fly_calib_gainfd=[], delay_win=[], rate_win=[],
                              corrcomb='none', _mpi_client=False, solint_obj=None, _spwpartition=''):
    """
    Messenger to determine or retrive a solint and refant+sub-clusters for the fringe_solints dict, depending on _mode.
    The fringe_solints dict writes solution intervals for every scan.
      And for every valid solint, a mutiband fringefit() will be performed.
    Handles the case of attempted retrieval of a non-existing solint
      and phase referencing, where the science target is skipped (phaseref_ff_science is False: get a <False> solint).
    The rationale behind this function is to avoid duplicate code for phase-referencing experiments (skip science target part).
    """
    scan_list   = auxiliary.force_list(_scan)
    refant_dict = {}
    #fill value for scans which cannot be fringe-fitted because the refant is not present:
    _no_refant = ''
    for this_scan in scan_list:
        if _fringe_refants:
            try:
                _this_refant = _fringe_refants[this_scan][0]
            except KeyError:
                print('Warning: Scan {0} is not present in the fringe-refants object!'.format(str(this_scan)))
                _this_refant = calibration.get_refant(_inp_params, _ms_metadata, this_scan, 'simple')
        else:
            _this_refant = calibration.get_refant(_inp_params, _ms_metadata, this_scan, 'simple')
        if _this_refant not in [_ms_metadata.yield_antname(ant) for ant in _ms_metadata.yield_antennas(str(this_scan))]:
            _this_refant = _no_refant
            if _inp_params.verbose:
                print('  ->Skipping scan '+str(this_scan)+' because none of the specified refants are present')
            else:
                pass
        refant_dict[this_scan] = _this_refant
    if auxiliary.is_set(_inp_params, 'calibrators_phaseref'):
        try:
            if not _inp_params.phaseref_ff_science:
                if isinstance(_source, list):
                    if _source[0] in _inp_params.science_target:
                        return False
                    else:
                        pass
                elif _source in _inp_params.science_target:
                    return False
                else:
                    pass
            else:
                pass
        except TypeError:
            raise ValueError('Cannot do phase referencing when no science target is specified.')
    if _mode == 'retrieve':
        if not _fringe_solints:
            raise ValueError('No valid fringe_solint dict specified. Cannot retrieve a solint value.')
        if not _source:
            raise ValueError('No valid source specified. Cannot retrieve a solint value.')
        if not _scan:
            raise ValueError('No valid scan specified. Cannot retrieve a solint value.')
        try:
            #here, _fringe_solints  is a pre-determined dict with (solint, (refant, sub-clusters)) from readback_fringeparams()
            solint_refantsubclust = _fringe_solints[_source][_scan]
            return solint_refantsubclust[0], solint_refantsubclust[1][0], solint_refantsubclust[1][1]
        except KeyError:
            print('  Warning: Did not find a pre-set solution interval\n'
                  '    for the scan ' + str(_scan) + ' on the source ' + str(_source) +'.\n'
                  '    Skipping this scan for the fringefit() run...'
                 )
            return False
    elif _mode == 'closest':
        if not _fringe_solints:
            raise ValueError('No valid fringe_solint dict specified. Cannot get the closest solint value.')
        if not _source:
            raise ValueError('No valid source specified. Cannot get the closest solint value.')
        if not _scan:
            raise ValueError('No valid scan specified. Cannot get the closest solint value.')
        try:
            #use the closest match from the pre-computed ones:
            closest_match = auxiliary.get_closest_match(_scan, list(_fringe_solints[_source].keys()), ignoreval=_no_refant)
            if closest_match[2]:
                closest_selected_scan = closest_match[0][1]
            else:
                raise IndexError('Cannot find closest match between' + str(_scan) + ' and ' + \
                                 str(_fringe_solints[_source].keys()))
            return _fringe_solints[_source][closest_selected_scan]
        except KeyError:
            print('  Warning: Did not find a pre-set solution interval\n'
                  '    for the scan ' + str(_scan) + ' on the source ' + str(_source) +'.\n'
                  '    Skipping this scan for the fringefit() run...'
                 )
            return False
    elif _mode == 'iterate':
        if not _scan:
            raise ValueError('No valid scan specified. Cannot determine a solint value.')
        if auxiliary.is_set(_inp_params, 'calibrators_phaseref'):
            if not _source:
                raise ValueError('No valid source specified, while doing phase referencing.'
                                 'Will abort as fringefit may be run on the science target otherwise...'
                                )
        return solint_iter_general(_inp_params, _ms_metadata, scan_list, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                   _mpi_client, _inp_params.fringe_minSNR_mb_shortFFT, solint_obj, refant_dict, None,
                                   delay_win, rate_win, corrcomb, _spwpartition, False, 'iter'
                                  )
    elif _mode == 'write':
        if auxiliary.is_set(_inp_params, 'calibrators_phaseref'):
            if not _source:
                raise ValueError('No valid source specified, while doing phase referencing.'
                                 'Will abort as fringefit may be run on the science target otherwise...'
                                )
        return solint_obj
    else:
        raise ValueError(str(_mode) + ' is not a valid mode. Must be retrieve, closest, iterate, or write.')


def task_fringefit_solint_cal(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                              _spwpartition, _mpi_client):
    """
    Wrapper around fringefit_solint_refant_optimize() for calibrators.
    Has functionality of general calibration step.
    """
    _srcs = _ms_metadata.yield_calibrators(_inp_params)
    if not _srcs:
        print ('    Skipping fringefit_solint_cal because no calibrators was set.')
        return False
    delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                           _inp_params.fringe_rate_window_initial
                                                          )
    return fringefit_solint_refant_optimize(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                            _inp_params.store_optimal_fringe_params + '.cal'+_spwpartition, _srcs, 'cal',
                                            delay_window, rate_window, 'all', _spwpartition, _mpi_client
                                           )


def task_fringefit_solint_sci(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                              _spwpartition, _mpi_client):
    """
    Wrapper around fringefit_solint_refant_optimize() for science targets.
    Has functionality of general calibration step.
    """
    _srcs = _ms_metadata.yield_science_targets(_inp_params)
    if not _srcs:
        print('    Skipping fringefit_solint_sci calibration because no science target was set.')
        return False
    musthavetab = False
    if not auxiliary.is_set(_inp_params, 'calibrators_phaseref') or _inp_params.phaseref_ff_science:
        musthavetab = True
    else:
        for source in _srcs:
            if not _ms_metadata.yield_phaseref(_inp_params, source):
                musthavetab = True
                break
    if not musthavetab:
        if _inp_params.verbose:
            print('    Skipping solint estimation for science targets for phase-referencing with phaseref_ff_science=False.')
        return False
    delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_mb_sci_short,
                                                           _inp_params.fringe_rate_window_mb_sci_short
                                                          )
    if auxiliary.is_set(_inp_params, 'atmo_selfcal') and _inp_params.atmo_selfcal=='only':
        print('    Skipping solint estimation for science targets because atmo_selfcal=only has been set.')
        return False
    return fringefit_solint_refant_optimize(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                            _inp_params.store_optimal_fringe_params + '.sci'+_spwpartition, _srcs, 'sci',
                                            delay_window, rate_window, 'all', _spwpartition, _mpi_client
                                           )


def fringefit_solint_refant_optimize(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                     objname, _sources, sourcetype, delay_win=[], rate_win=[], corrcomb='none', _spwpartition='',
                                     _mpi_client=False):
    """
    Wrapper around find_optimal_fringe_params that can be called by the general calibration.go_calibrate() function
      to get the fly_calib parameters properly. Ignores the passed caltable.
    Stores the dict with the optimal_fringe_params on disk as objname and reads it back in instead of determining it again
      if _inp_params.fringe_params_load=True. Does this for separate _sources (science targets or calibrators).
    Returns False as it is just an optimization procedure that will write a _fringe_params object to disk.
    Only real calibration tasks will return True, meaning that their calibration tables will be used for calibration.
    """
    if _inp_params.fringe_params_load and auxiliary.isfile(objname):
        if _inp_params.verbose:
            print('  Loading previously determined optimal fringe parameters from disk.')
        _fringe_params = auxiliary.store_object(objname)
    else:
        if _inp_params.verbose:
            print('  Determining optimal fringe parameters from scratch.')
        auxiliary.rm_file_if_present(objname)
        _fringe_params = find_optimal_fringe_params(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp,
                                                    _fly_calib_gainfd, sourcetype, _sources, delay_win, rate_win,
                                                    corrcomb, _spwpartition, _mpi_client
                                                   )
        if _fringe_params:
            if _inp_params.verbose:
                print('\n  Done determining optimal fringe parameters.')
            auxiliary.store_object(objname, _fringe_params, 'write')
    objname_ld = objname + '.longsolints'
    if auxiliary.is_set(_inp_params, 'fringe_solint_mb_reiterate') and _inp_params.fringe_params_load and \
    auxiliary.isfile(objname_ld):
        if _inp_params.verbose:
            print('  Loading previously determined long solints from disk.')
        _fringe_params_ld = auxiliary.store_object(objname_ld)
    elif auxiliary.is_set(_inp_params, 'fringe_solint_mb_reiterate'):
        if _inp_params.verbose:
            print('  Determining solints for detections with longer integration times.')
        auxiliary.rm_file_if_present(objname_ld)
        _fringe_params_ld = fringefit_obtain_long_solints(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp,
                                                          _fly_calib_gainfd, _mpi_client, _sources, delaywin=delay_win,
                                                          ratewin=rate_win, corrcomb=corrcomb, _spwpartition='')
        if _fringe_params_ld:
            if _inp_params.verbose:
                print('\n  Done determining long solint parameters.')
            auxiliary.store_object(objname_ld, _fringe_params_ld, 'write')
    return False


def find_optimal_fringe_params(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd, sourcetype,
                               sources='', delay_win=[], rate_win=[], corrcomb='none', _spwpartition='', _mpi_client=False):
    """
    Fill a dict 'fringe_params' with solution intervals per scan.
    If _inp_params.fringe_searchscans == 'all' then determine optimal parameters for every scan.
    Elif _inp_params.fringe_searchscans == 'selected', do it only for _inp_params.selcted_scans.
    If _inp_params.fringe_solint_optimize_search_sci/cal is a single number, then use that one for all scans.
    Else, if it is an array then do trial and error, with a comparison metric from calibration.calibration_quality(),
    In the actual fringe-fit production run, the solint from the closest scan (per source) will be used.
    """
    if sourcetype == 'cal':
        solint_obj = _inp_params.fringe_solint_optimize_search_cal
    elif sourcetype == 'sci':
        solint_obj = _inp_params.fringe_solint_optimize_search_sci
    else:
        raise ValueError('sourcetype must be cal or sci, got ' + str(sourcetype))
    if not sources:
        sources = list(_ms_metadata.selected_scans_dict.keys())
    fringe_params = defaultdict(dict)
    fringe_refant = determine_refants_sc_per_scan(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp,
                                                  _fly_calib_gainfd, delay_win, rate_win, corrcomb, sourcetype, _mpi_client
                                                 )
    slnts = True
    if isinstance(solint_obj, list) and _inp_params.fringe_searchscans == 'selected':
        #Pre-compute optimal solution intervals for selected scans:
        selected_solints = defaultdict(dict)
        these_scans      = auxiliary.flatten_list([_ms_metadata.yield_selected_scans(src) for src in sources])
        slnts = communicate_fringe_solint(_inp_params, _ms_metadata, 'iterate', _fringe_refants=fringe_refant,
                                          _source=sources, _scan=these_scans,_fly_calib_tables=_fly_calib_tables,
                                          _fly_calib_interp=_fly_calib_interp, _fly_calib_gainfd=_fly_calib_gainfd,
                                          delay_win=delay_win, rate_win=rate_win, corrcomb=corrcomb, _mpi_client=_mpi_client,
                                          solint_obj=solint_obj, _spwpartition=_spwpartition
                                         )
        for src in sources:
            for scan in _ms_metadata.yield_selected_scans(src):
                selected_solints[src][scan] = slnts[scan]
    these_scans = auxiliary.flatten_list(_ms_metadata.yield_scans(sources, do_not_squeeze_single_source=True))
    if isinstance(solint_obj, list) and _inp_params.fringe_searchscans == 'all':
        slnts = communicate_fringe_solint(_inp_params, _ms_metadata, 'iterate', _fringe_refants=fringe_refant,
                                          _source=sources, _scan=these_scans,_fly_calib_tables=_fly_calib_tables,
                                          _fly_calib_interp=_fly_calib_interp, _fly_calib_gainfd=_fly_calib_gainfd,
                                          delay_win=delay_win, rate_win=rate_win, corrcomb=corrcomb, _mpi_client=_mpi_client,
                                          solint_obj=solint_obj, _spwpartition=_spwpartition
                                         )
    if not slnts:
        return fringe_params
    #Now fill the fringe_params object with solints for every scan:
    for src in sources:
        scans = _ms_metadata.yield_scans(src)
        for scan in scans:
            if not isinstance(solint_obj, list):
                #use the same value for every scan:
                slnt = communicate_fringe_solint(_inp_params, _ms_metadata, 'write', _source=src, _scan=scan,
                                                 _fringe_refants=fringe_refant, solint_obj=solint_obj
                                                )
            elif _inp_params.fringe_searchscans == 'all':
                #computed it for every scan:
                slnt = slnts[scan]
            elif _inp_params.fringe_searchscans == 'selected':
                #use the closest match from the pre-computed ones:
                slnt = communicate_fringe_solint(_inp_params, _ms_metadata, 'closest', _fringe_solints=selected_solints,
                                                 _fringe_refants=fringe_refant, _source=src, _scan=scan,
                                                 _fly_calib_tables=_fly_calib_tables, _fly_calib_interp=_fly_calib_interp,
                                                 _fly_calib_gainfd=_fly_calib_gainfd, solint_obj=solint_obj,
                                                 _spwpartition=_spwpartition
                                                )
            fringe_params[src][scan] = slnt
    return fringe_params


def solint_iter_general(_inp_params, _ms_metadata, scan_list, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                        _mpi_client, min_fft_snr, solint_obj, refant_dict=None, important_antennas=None,
                        delay_win=[], rate_win=[], corrcomb='none', _spwpartition='', force_no_mpi=False, iter_mode='iter'):
    """
    Uses MPI to to the brute-force search for an. optimal fringe-fit solint
    If iter_mode='iter':
      Iterates over a range of values given by _inp_params.fringe_solint_optimize_search_cal/sci (solint_obj)
      and determines the best solint based on a comparison metric from calibration.calibration_quality().
    Elif iter_mode='reiter':
      Similar to 'iter_mode'. However, pre-defined search values are searched and only the important_antennas are
      considered when looking for the best solint.
      Used by task_fringefit_multi_sci when _inp_params.fringe_solint_mb_reiterate is set for example.
    """
    if important_antennas:
        try:
            is_antID = float(important_antennas[list(important_antennas.keys())[0]][0])
        except ValueError:
            for scan in important_antennas:
                important_antennas[scan] = [_ms_metadata.yield_antID(antname) for antname in important_antennas[scan]]
    if not isinstance(solint_obj, list):
        raise ValueError('To find the best solint, solint_obj must be a list.\n' \
                         'But I got solint_obj=' + str(solint_obj)
                        )
    step = float(_ms_metadata.acp)
    #Determine solint ranges and sources for every scan.
    scan_sources = []
    scan_search  = {}
    scan_lengths = {}
    good_scans   = []
    for scan in scan_list:
        if refant_dict and not refant_dict[scan]:
            continue
        good_scans.append(scan)
        scan_sources.append(_ms_metadata.yield_sourcename_from_scan(scan))
        this_scan_len      = float(_ms_metadata.yield_scan_length(scan))
        scan_lengths[scan] = this_scan_len
        if iter_mode == 'reiter':
            scan_search[scan] = solint_obj
            continue
        if len(solint_obj) > 2:
            search = [max(float(forced_solint), 2*step) for forced_solint in solint_obj]
        else:
            start         = max(solint_obj[0], 2*step)
            stop          = min(solint_obj[1], 1.1*this_scan_len)
            prelim_search = np.arange(start, stop, step)
            prelim_search = np.append(prelim_search, stop)
            search        = []
            if _inp_params.floating_fringe_solint:
                for slnt in prelim_search:
                    if Decimal(str(this_scan_len)) % Decimal(str(slnt)) == 0:
                        search.append(slnt)
                if len(search) < 10:
                    #not enough solints that evenly divide the scan
                    for slnt in prelim_search:
                        if divmod(this_scan_len, slnt)[1] >= 2 * float(_ms_metadata.acp):
                            search.append(slnt)
                            if len(search) > 10:
                                break
                #enforce adding some large solint numbers:
                large_solints = np.linspace(0.1,1.0,10) * stop
                for ls in large_solints:
                    if ls>start and ls not in search and divmod(this_scan_len, ls)[1] >= 2 * float(_ms_metadata.acp):
                        search.append(ls)
                if not search:
                    errmsg = 'No valid (floating) solints founds for scan ' + str(scan) + ' with length ' + str(this_scan_len)
                    errmsg += ' for the fringe_solint input parameter being set as ' + str(solint_obj)
                    raise ValueError(errmsg)
            else:
                search = prelim_search
        scan_search[scan] = sorted([round(s,4) for s in search])
    scratchdir = _inp_params.scratchdir + '/'
    auxiliary.makedir(scratchdir)
    scan_sources    = list(auxiliary.unique_unsorted(scan_sources))
    Max_search_len  = max([len(scan_search[scan]) for scan in good_scans])
    remaining_scans = copy.deepcopy(good_scans)
    cnt_scores      = defaultdict(lambda: defaultdict(dict))
    min_scores      = defaultdict(lambda: defaultdict(dict))
    max_scores      = defaultdict(lambda: defaultdict(dict))
    p0_fringes      = defaultdict(lambda: defaultdict(dict))
    p1_fringes      = defaultdict(lambda: defaultdict(dict))
    p0_fringes_max  = defaultdict(dict)
    p1_fringes_max  = defaultdict(dict)
    include_solint  = defaultdict(dict)
    solints         = {}
    gotsol          = {}
    last_solints    = {}
    for scan in good_scans:
        gotsol[scan]       = False
        last_solints[scan] = ''
    #We are going to use an MPI loop over all scans for each solution interval. Then we check which scans we can drop because a
    #good enough solint was found. Then we are doing the next MPI loop for the next solint for all remaining scans. And so on
    #until we have found a solint for every scan.
    for j_iter in range(Max_search_len):
        scans_with_solints = []
        these_scan_solints = {}
        for scan in remaining_scans:
            this_search = scan_search[scan]
            try:
                this_solint = this_search[j_iter]
                scans_with_solints.append(scan)
                these_scan_solints[scan] = this_solint
            except IndexError:
                continue
        if not scans_with_solints:
            continue
        if _inp_params.verbose:
            print('  ->Fringe-fitting scans with solint #'+str(j_iter+1)+'/'+str(Max_search_len))
        this_caltab    = auxiliary.unique_filename(scratchdir + 'tmp_fringetest-solintiter.' + str(j_iter+1))
        actual_solints = fringefit_mpi_exhaustive(_inp_params, _ms_metadata, this_caltab, 0.1, scan_sources, '',
                                                  scans_with_solints, these_scan_solints, 'spw',
                                                  gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                                                  interp=_fly_calib_interp, globalsolve=False, delaywindow=delay_win,
                                                  ratewindow=rate_win, corrcomb=corrcomb, MPICLIENT=_mpi_client,
                                                  force_no_mpi=force_no_mpi, input_refant=refant_dict, pass_missing_table=True,
                                                  silence=True, _spwpartition=_spwpartition, return_solints=True
                                                 )
        #Go through results to store fringes and identify scans that can be dropped:
        for _scan in actual_solints:
            _this_solint = actual_solints[_scan]
            if auxiliary.isdir(this_caltab):
                scores = calibration.calibration_quality(_inp_params, _ms_metadata, this_caltab, 'range_solution_SNR_per_station',
                                                         min_fft_snr, _scan
                                                        )
                if important_antennas:
                    antenna_iterator = important_antennas[_scan]
                else:
                    antenna_iterator = scores.keys()
                for ant in antenna_iterator:
                    _this_avgsnr = scores[ant]
                    if not _this_avgsnr[0]:
                        _this_avgsnr = [0,0,0], 0, 0
                    try:
                        _ = _this_avgsnr[0][2]
                    except (TypeError, IndexError) as _:
                        _this_avgsnr = [0,0,0], 0, 0
                    min_scores[_scan][ant][_this_solint] = _this_avgsnr[0][0]
                    if iter_mode == 'reiter':
                        cnt_scores[_scan][ant][_this_solint] = _this_avgsnr[0][2]
                    else:
                        cnt_scores[_scan][ant][_this_solint] = _this_avgsnr[0][1]
                    max_scores[_scan][ant][_this_solint] = _this_avgsnr[0][2]
                    p0_fringes[_scan][ant][_this_solint] = _this_avgsnr[1]
                    p1_fringes[_scan][ant][_this_solint] = _this_avgsnr[2]
                gotsol[_scan] = True
            else:
                #most likely, the solint was still bad somehow (this should not happen often...)
                if important_antennas:
                    antenna_iterator = important_antennas[_scan]
                else:
                    antenna_iterator = _ms_metadata.yield_antennas(str(_scan))
                for ant in antenna_iterator:
                    min_scores[_scan][ant][_this_solint] = 0
                    cnt_scores[_scan][ant][_this_solint] = 0
                    max_scores[_scan][ant][_this_solint] = 0
                    p0_fringes[_scan][ant][_this_solint] = 0
                    p1_fringes[_scan][ant][_this_solint] = 0
            #Handle case where an antenna was present for an earlier solint but not anymore for the current scores.
            for ant in cnt_scores[_scan].keys():
                try:
                    _ = cnt_scores[_scan][ant][_this_solint]
                except KeyError:
                    min_scores[_scan][ant][_this_solint] = 0
                    cnt_scores[_scan][ant][_this_solint] = 0
                    max_scores[_scan][ant][_this_solint] = 0
                    p0_fringes[_scan][ant][_this_solint] = 0
                    p1_fringes[_scan][ant][_this_solint] = 0
            if _inp_params.fft_solint_estimation != 'sqrt' and _inp_params.fft_solint_quick and gotsol[_scan]:
                N_valid_snrs = 0
                for ant in cnt_scores[_scan].keys():
                    snrs = list(cnt_scores[_scan][ant].values())
                    if not all(snr==0 for snr in snrs):
                        N_valid_snrs += 1
                N_good_snrs = 0
                for ant in cnt_scores[_scan].keys():
                    if not last_solints[_scan]:
                        p0_fringes_max[_scan][ant] = 0
                        p1_fringes_max[_scan][ant] = 0
                    solints[_scan] = [float(slnt) for slnt in cnt_scores[_scan][ant].keys()]
                    snrs           = list(cnt_scores[_scan][ant].values())
                    try:
                        snr_minval = float(_inp_params.fft_solint_estimation) * min_fft_snr
                    except ValueError:
                        raise ValueError("Invalid value for fft_solint_estimation encountered. Must be 'sqrt' or a float. " \
                                         "But I got: " + str(_inp_params.fft_solint_estimation))
                    if not all(snr==0 for snr in snrs):
                        #check if I got more detections for both polarizations with the last solint
                        if max(snrs)>snr_minval:
                            detections_sum_new = p0_fringes[_scan][ant][_this_solint] + p1_fringes[_scan][ant][_this_solint]
                            try:
                                detections_sum_old = p0_fringes_max[_scan][ant] + p1_fringes_max[_scan][ant]
                            except KeyError:
                                detections_sum_old = 0
                            if detections_sum_new > detections_sum_old:
                                include_solint[_scan][ant] = _this_solint
                                p0_fringes_max[_scan][ant] = p0_fringes[_scan][ant][_this_solint]
                                p1_fringes_max[_scan][ant] = p1_fringes[_scan][ant][_this_solint]
                            else:
                                pass
                        #got good enough solint or no more increase of snr with solint
                        if (max(snrs)>snr_minval and last_solints[_scan] and p0_fringes_max[_scan][ant] \
                            and p1_fringes_max[_scan][ant]) \
                        or (len(snrs)>int(_inp_params.fft_solint_quick) and not auxiliary.check_for_positive_trend(snrs)):
                            N_good_snrs += 1
                if N_valid_snrs and N_good_snrs==N_valid_snrs and iter_mode=='iter':
                    #Exclude scans for which a good enough solint has been found in the next MPI loop.
                    if _inp_params.verbose:
                        print('    Found a good enough solint for scan {0}.'.format(_scan))
                    remaining_scans.remove(_scan)
            if gotsol[_scan]:
                last_solints[_scan] = _this_solint
        auxiliary.rm_file_if_present(this_caltab+'.refants')
        if auxiliary.isdir(this_caltab):
            try:
                shutil.rmtree(this_caltab, ignore_errors=True)
            except OSError:
                #this can happen for very slow disks...
                time.sleep(60)
                shutil.rmtree(this_caltab, ignore_errors=True)
    #Pick final solints and create diagnostics:
    final_solints = {}
    add_to_dir = os.path.join(_inp_params.diag_optimize_solint, '')
    if _spwpartition:
        spwextensionname = '.spw' + _spwpartition.replace('~', 'to')
    else:
        spwextensionname = ''
    for _scan in good_scans:
        scan_source  = _ms_metadata.yield_sourcename_from_scan(_scan)
        scan_len     = scan_lengths[_scan]
        if _inp_params.diag_optimize_solint and iter_mode=='iter':
            _this_refant = refant_dict[_scan]
            savedir      = _inp_params.diagdir + add_to_dir + 'scan' + str(_scan) + '_' + scan_source + spwextensionname + '/'
            auxiliary.makedir(savedir)
            svs_file  = savedir + 'snr_vs_solint.dat'
            s_solintf = open(svs_file, 'w')
            s_solintf.write('#Scan length: ' + str(scan_len) + 's\n')
            s_solintf.write('baseline     solint [s]   snr\n')
            try:
                _ = cnt_scores[_scan].keys()
            except KeyError:
                continue
            for ant in cnt_scores[_scan].keys():
                solints = list(cnt_scores[_scan][ant].keys())
                snrs    = list(cnt_scores[_scan][ant].values())
                #exclude stations that are not present:
                if not all(snr==0 for snr in snrs):
                    try:
                        _antname = str(_ms_metadata.yield_antname(ant))
                    except ValueError:
                        _antname = str(ant)+('(unknown name in MS)')
                    _basel = str(_this_refant) + '--' + _antname
                    f_solints = np.asarray(solints).astype('float')
                    s_solints, s_snrs = np.transpose([tmp for tmp in sorted(zip(f_solints,snrs))])
                    for solint,snr in zip(s_solints, s_snrs):
                        s_solintf.write('{0:<12} {1:<12} {2: <12}\n'.format(_basel, str(solint), str(snr)))
            s_solintf.close()
        best_baseline_solints = []
        minimum_solints       = []
        #first collect the minimum solints for detections on all baselines
        for ant in cnt_scores[_scan].keys():
            solints = [float(slnt) for slnt in cnt_scores[_scan][ant].keys()]
            snrs    = list(cnt_scores[_scan][ant].values())
            #exclude stations that are not present:
            if not all(snr==0 for snr in snrs):
                solints = np.array(solints)
                snrs    = np.array(snrs)
                solints = solints[snrs>=min_fft_snr]
                if any(solints):
                    #source detected
                    #if determined, take the solint with the most detections
                    try:
                        this_best_solint = float(include_solint[_scan][ant])
                    except KeyError:
                        this_best_solint = min(solints)
                    minimum_solints.append(this_best_solint)
        if minimum_solints:
            minimum_solint = max(minimum_solints)
        else:
            minimum_solint = 0
        #now find the optimal that is at least as long as minimum_solint
        for ant in cnt_scores[_scan].keys():
            solints  = [float(slnt) for slnt in cnt_scores[_scan][ant].keys()]
            snrs     = list(cnt_scores[_scan][ant].values())
            #exclude stations that are not present:
            if not all(snr==0 for snr in snrs):
                solints       = np.array(solints)
                snrs          = np.array(snrs)
                solints, snrs = np.transpose([tmp for tmp in sorted(zip(solints,snrs))])
                o_solints     = copy.deepcopy(solints)
                o_snrs        = copy.deepcopy(snrs)
                try:
                    min_snrs = np.array([min_scores[_scan][ant][str(slnt)] for slnt in solints])
                    max_snrs = np.array([max_scores[_scan][ant][str(slnt)] for slnt in solints])
                except KeyError:
                    min_snrs = np.array([min_scores[_scan][ant][str(int(slnt))] for slnt in solints])
                    max_snrs = np.array([max_scores[_scan][ant][str(int(slnt))] for slnt in solints])
                solints       = solints[snrs>=min_fft_snr]
                snrs          = snrs[snrs>=min_fft_snr]
                snrs          = snrs[solints>=minimum_solint]
                solints       = solints[solints>=minimum_solint]
                if any(solints):
                    if _inp_params.fft_solint_estimation == 'sqrt' and iter_mode=='iter':
                        best_indx, solint_grid, expected_snrs = auxiliary.find_first_SNR_diff(snrs, solints, 'sqrt', \
                                                                                              room_for_error = 0.05)
                    else:
                        try:
                            snr_minval = float(_inp_params.fft_solint_estimation) * min_fft_snr
                        except ValueError:
                            if iter_mode=='iter':
                                raise ValueError("Invalid value for fft_solint_estimation encountered. " \
                                                 "Must be 'sqrt' or a float. " \
                                                 "But I got: " + str(_inp_params.fft_solint_estimation))
                            else:
                                snr_minval = 1.01 * min_fft_snr
                        best_indx, solint_grid, expected_snrs = auxiliary.find_first_SNR_diff(snrs, solints, 'minsnr', \
                                                                                              snr_mincut = snr_minval)
                    if best_indx is not False:
                        this_best = solints[best_indx]
                    else:
                        #SNR steadily rising up until the longest solint
                        this_best = solints[np.argmax(snrs)]
                    best_baseline_solints.append(this_best)
                else:
                    this_best     = 'None found above threshold'
                    expected_snrs = []
                    solint_grid   = []
                if _inp_params.diag_optimize_solint and iter_mode=='iter':
                    try:
                        _antname = str(_ms_metadata.yield_antname(ant))
                    except ValueError:
                        _antname = str(ant)+('(unknown name in MS)')
                    _basel     = str(_this_refant) + '--' + _antname
                    add_to_dir = os.path.join(_inp_params.diag_optimize_solint, '')
                    if _spwpartition:
                        spwextensionname = '.spw' + _spwpartition.replace('~', 'to')
                    else:
                        spwextensionname = ''
                    savedir    = _inp_params.diagdir+add_to_dir + 'scan'+str(_scan) + '_' + scan_source + spwextensionname + '/'
                    figname    = savedir + 'fringe_snr_vs_solint_' + _basel
                    plt_title  = _basel + '   ||   best solint [s]: ' + str(this_best)
                    auxiliary.makedir(savedir)
                    diagnostics.simple_plotter(o_solints, [o_snrs, min_snrs, max_snrs], 'Fringe solint (s)', 'FFT SNR', plt_title,
                                               figname, solint_grid, expected_snrs, min_fft_snr, minimum_solint, _pltype='errbar',
                                               rescale_axes=True, logy=True
                                              )
                    p0_detectfrac = list(p0_fringes[_scan][ant].values())
                    p1_detectfrac = list(p1_fringes[_scan][ant].values())
                    p0_detectfrac = [float(s)*float(d)/scan_len for s, d in zip(p0_fringes[_scan][ant].keys(),
                                                                                p0_fringes[_scan][ant].values())]
                    p1_detectfrac = [float(s)*float(d)/scan_len for s, d in zip(p0_fringes[_scan][ant].keys(),
                                                                                p1_fringes[_scan][ant].values())]
                    diagnostics.simple_barplot(list(p0_fringes[_scan][ant].keys()), [p0_detectfrac, p1_detectfrac],
                                               'Fringe solint (s)', 'Fraction of possible detections obtained', [0, 1],
                                               savedir + 'detections_' + _basel, _basel, ['RCP', 'LCP'],
                                               0.45*_ms_metadata.acp, [-0.45*_ms_metadata.acp,0]
                                              )
        if best_baseline_solints:
            best_solint = min(best_baseline_solints)
            if _inp_params.diag_optimize_solint and iter_mode=='iter':
                with open(svs_file, 'r+') as s_solintf:
                    old_content = s_solintf.read()
                    s_solintf.seek(0,0)
                    s_solintf.write('#Picked best solint [s]: ' + str(best_solint) + '\n')
                    s_solintf.write(old_content)
            if _inp_params.verbose:
                print('    Best solint for scan ' + str(_scan) +': ' + str(best_solint)+'s')
                if iter_mode=='iter':
                    print('      Used refant: ' + str(_this_refant))
                else:
                    pass
            final_solints[_scan] = best_solint
        elif iter_mode=='reiter':
            if _inp_params.verbose:
                print('      Still no detections above the SNR threshold of '+str(min_fft_snr) + ' found for scan '+str(_scan))
            final_solints[_scan] = ''
        else:
            if auxiliary.is_set(_inp_params, 'fringe_solint_mb_reiterate'):
                try:
                    #Only do this in 2017 (2018+ AP is called AX)
                    AP_ID = _ms_metadata.yield_antID('AP')
                except ValueError:
                    AP_ID = -999
                if not _inp_params.eht_alma_apex_highsnr or not (str(_this_refant)=='AA' and AP_ID in cnt_scores.keys()):
                    #For the EHT: If no high SNR fringes are found between ALMA and APEX something is wrong.
                    if _inp_params.verbose:
                        print('    Passing scan ' + str(_scan) + ' without detections for now.')
                        print('    Fringes may be found on longer timescales in the second fringe-fit.')
                        print('      Used refant: ' + str(_this_refant))
                    final_solints[_scan] = max(search)
            else:
                if _inp_params.verbose:
                    print('    No detections above the SNR threshold of ' + str(min_fft_snr) +' found for scan ' + str(_scan))
                    print('      Used refant: ' + str(_this_refant))
                final_solints[_scan] = ''
    #Filler for failed solutions
    for scan in scan_list:
        try:
            has_entry = final_solints[scan]
        except KeyError:
            final_solints[scan] = ''
    return final_solints


def readback_snrsolints(_inp_params):
    """
    Returns a [scan][antenna] dict of [(solint, snrs)] based on the last solint_iter_general(iter_mode='iter') run.
    """
    all_diag_dir  = auxiliary.glob_all(diagnostics.get_any_diagdir(_inp_params))
    ordered_diags = auxiliary.sort_by_time(all_diag_dir)
    gotcha        = False
    for diag in ordered_diags[::-1]:
        thisdir = diag + '/' + _inp_params.diag_optimize_solint
        if auxiliary.isdir(thisdir):
            gotcha = thisdir
            break
    if not gotcha:
        print('  Warning: Did not find a folder with snr vs solint estimations in any of the diagnostics folders.')
        return False
    snrslnt_files = auxiliary.glob_all(thisdir + '/*/*.dat')
    snrsolints    = defaultdict(dict)
    for ssf in snrslnt_files:
        scan = ssf.split('scan')
        scan = scan[1].split('_')[0]
        with open(ssf, 'r') as f:
            for line in f:
                if '--' in line:
                    line1 = line.split('--')[1]
                    vals  = line1.split()
                    entry = (vals[1], vals[2])
                    try:
                        snrsolints[scan][vals[0]].append(entry)
                    except KeyError:
                        snrsolints[scan][vals[0]] = [entry]
    return snrsolints


def get_lowsnr_scanants(_inp_params):
    """
    Uses readback_snrsolints() to get a dict of all [scans][antennas] where the FFT SNR is below
    _inp_params.fringe_minSNR_mb_shortFFT.
    """
    lowsnrs    = {}
    snrsolints = readback_snrsolints(_inp_params)
    if not snrsolints:
        return False
    for scan in snrsolints.keys():
        for ant in snrsolints[scan]:
            if max([float(snr[1]) for snr in snrsolints[scan][ant]]) < _inp_params.fringe_minSNR_mb_shortFFT:
                try:
                    lowsnrs[scan].append(ant)
                except KeyError:
                    lowsnrs[scan] = [ant]
    return lowsnrs


def readback_fringeparams(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                          delay_win, rate_win, corrcomb='none', sourcetype='all', _spwpartition='', _mpi_client=False):
    """
    Reads back the fringefit parameters (refant+sub-clusters and solint per scan) written by
    task_fringefit_solint_sci() and task_fringefit_solint_cal(), combined in a single dict.
    """
    sci_file        = _inp_params.store_optimal_fringe_params + '.sci' + _spwpartition
    cal_file        = _inp_params.store_optimal_fringe_params + '.cal' + _spwpartition
    sci_params      = {}
    cal_params      = {}
    comb_dict       = {}
    adhoc_comb_dict = defaultdict(dict)
    if auxiliary.isfile(sci_file):
        try:
            sci_params = auxiliary.store_object(sci_file, _operation='read')
        except ValueError:
            #can happen when it was stored for an older CASA version
            pass
    if auxiliary.isfile(cal_file):
        try:
            cal_params = auxiliary.store_object(cal_file, _operation='read')
        except ValueError:
            pass
    refant_params = determine_refants_sc_per_scan(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp,
                                                  _fly_calib_gainfd, delay_win, rate_win, corrcomb, sourcetype, _mpi_client
                                                 )
    comb_dict.update(cal_params)
    comb_dict.update(sci_params)
    #add [source] info to [scans] and combine solints and refants to a single dict for both sci and cal sources
    for scan in refant_params:
        this_src = _ms_metadata.yield_sourcename_from_scan(scan)
        try:
            this_solint = comb_dict[this_src][scan]
        except KeyError:
            this_solint = 'inf'
        adhoc_comb_dict[this_src][scan] = (this_solint, refant_params[scan])
    return adhoc_comb_dict


def task_fringefit_multi_general(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                 _mpi_client, _sources, fringe_snr_cutoff, overwrite_solint='', overwrite_scans=[],
                                 do_rerefant=True, overwrite_mbdelay_smoothtime=0, overwrite_delaywin=[], overwrite_ratewin=[],
                                 corrcomb='none', _spwpartition='', force_no_mpi=False, solvefor='delay,rate', input_antenna='',
                                 quickmem=False):
    """
    Executes CASA's fringefit task to remove residual phase, delay and rate offsets from the multiband (combined spw) data.
    Reads from optimal-fringe-parameters-storage object which must have been written to disk by task_fringefit_optimize.
      Gets all scans on the selected sources from _ms_metadata
      and for each scan the fringe-fitting solution interval is read from the fringe-parameters-storage.
      Runs fringe-fit on each scan and appends to a single caltable.
      Extends the determined solutions to all spwds in the data.
        Delete the caltable first if one was present from a previous run.
    """
    fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, fringe_snr_cutoff,
                             _sources, input_antenna=input_antenna, input_scans=overwrite_scans, input_solint=overwrite_solint,
                             combine='spw', gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                             interp=_fly_calib_interp, delaywindow=overwrite_delaywin, ratewindow=overwrite_ratewin,
                             corrcomb=corrcomb, MPICLIENT=_mpi_client, force_no_mpi=force_no_mpi, flag_uncalib_scans=True,
                             _spwpartition=_spwpartition, solvefor=solvefor, quickmem=quickmem
                            )
    if _inp_params.fringefit_mb_interpolate_over_flags:
        try:
            oob = int(str(_inp_params.fringefit_mb_interpolate_over_flags))
        except ValueError:
            oob = 10
        calibration.interpolate_over_flags(caltable, param='FPARAM', out_of_bounds=oob)
    if _inp_params.average_mb_rates:
        #avg RCP and LCP rates
        calibration.avg_solutions(caltable, [_inp_params.F_0RATE, _inp_params.F_1RATE])
    if overwrite_mbdelay_smoothtime or overwrite_mbdelay_smoothtime==None:
        mbdelay_smoothtime = overwrite_mbdelay_smoothtime
    else:
        mbdelay_smoothtime = _inp_params.fringe_mbdelay_smoothtime
    if mbdelay_smoothtime and overwrite_solint!='inf':
        #smooth delays
        delay_indices = [_inp_params.F_0DELA, _inp_params.F_1DELA]
        if _inp_params.verbose:
            print('  Smoothing '+str(caltable)+' delays with a t='+str(mbdelay_smoothtime)+' [s] sliding window.')
        if any(overwrite_delaywin):
            calibration.medfilt_solutions(_inp_params, caltable, delay_indices, overwrite_delaywin,
                                          _smoothtime=mbdelay_smoothtime, _targetcol='FPARAM')
        else:
            calibration.medfilt_solutions(_inp_params, caltable, delay_indices,
                                          _smoothtime=mbdelay_smoothtime, _targetcol='FPARAM')
    elif _inp_params.verbose and overwrite_solint!='inf':
        print('  mbdelay_smoothtime input not set - skipping the smoothing.')
    #auxiliary.extend_solutions_to_all_spw(_ms_metadata, caltable)
    if do_rerefant:
        if auxiliary.is_set(_inp_params, 'unflag_bad_rerefant'):
            unflag_bad_rerefant = True
        else:
            unflag_bad_rerefant = False
        calibration.rerefant_general(_inp_params, _ms_metadata, caltable, unflag_bad_rerefant)
    calibration.unify_spectral_window_ids(caltable)
    return True


def fringefit_obtain_long_solints(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                  _mpi_client, _srcs, delaywin=[], ratewin=[], corrcomb='none', _spwpartition=''):
    """
    Obtain detections on longer solints with the FFT method for the fringefit_with_new_solints task and write them to disk.
    """
    ldetect = {}
    if not auxiliary.is_set(_inp_params, 'fringe_solint_mb_reiterate'):
        return ldetect
    solint3_search = _inp_params.fringe_solint_mb_reiterate
    if not isinstance(solint3_search, list):
        solint3_search = [solint3_search]
    non_detections = get_lowsnr_scanants(_inp_params)
    if not non_detections:
        if _inp_params.verbose:
            print('  fringe_solint_mb_reiterate specified but no non-detections found. Continuing.')
        return ldetect
    non_detections_srcs = {}
    for source in _srcs:
        thesescans = _ms_metadata.yield_scans(source)
        for scan in thesescans:
            if scan in non_detections.keys():
                non_detections_srcs[str(scan)] = non_detections[scan]
    if not non_detections_srcs:
        if _inp_params.verbose:
            print('  No non-detections found. Continuing.')
        return ldetect
    else:
        for scan in non_detections_srcs.keys():
            ants = non_detections_srcs[scan]
            if _inp_params.verbose:
                print('  Trying to get detections for {0} in scan {1} with longer solints.'.format(str(ants), scan))
            else:
                pass
        new_solints = solint_iter_general(_inp_params, _ms_metadata, list(non_detections_srcs.keys()), _fly_calib_tables,
                                          _fly_calib_interp, _fly_calib_gainfd, _mpi_client,
                                          _inp_params.fringe_minSNR_mb_reiterate, solint3_search, refant_dict=None,
                                          important_antennas=non_detections_srcs, delay_win=delaywin, rate_win=ratewin,
                                          corrcomb=corrcomb, _spwpartition=_spwpartition,
                                          force_no_mpi=_inp_params.long_solint_no_mpi, iter_mode='reiter'
                                         )
        if not new_solints:
            if _inp_params.verbose:
                print('  No detections on longer solints found.')
            return ldetect
        for scan in non_detections_srcs.keys():
            try:
                if new_solints[scan]:
                    ldetect[scan] = (new_solints[scan], non_detections_srcs[scan])
                else:
                    pass
            except KeyError:
                pass
    return ldetect


def fringefit_with_new_solints(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                               _mpi_client, _srcs, fringe_snr_cutoff, overwrite_solint='', overwrite_delaywin=[],
                               overwrite_ratewin=[], corrcomb='none', _spwpartition='', overwrite_mbdelay_smoothtime=0,
                               zero_long_solint_delays=False, solvefor='delay,rate'):
    """
    Interface between the fringefit_multi_sci and fringefit_multi_cal tasks and task_fringefit_multi_general.
    Contains the framework for the fringe-fit reiteration based on the fringe_minSNR_mb_reiterate input parameter.
    """
    got_long_solutions = False
    if auxiliary.is_set(_inp_params, 'fringe_solint_mb_reiterate'):
        sci_file_ld  = _inp_params.store_optimal_fringe_params + '.sci' + _spwpartition + '.longsolints'
        cal_file_ld  = _inp_params.store_optimal_fringe_params + '.cal' + _spwpartition + '.longsolints'
        sci_ldetect  = {}
        cal_ldetect  = {}
        comb_ldetect = {}
        if auxiliary.isfile(sci_file_ld):
            sci_ldetect = auxiliary.store_object(sci_file_ld, _operation='read')
        if auxiliary.isfile(cal_file_ld):
            cal_ldetect = auxiliary.store_object(cal_file_ld, _operation='read')
        comb_ldetect.update(cal_ldetect)
        comb_ldetect.update(sci_ldetect)
        if comb_ldetect:
            scan_newsolints     = {}
            non_detections_srcs = {}
            for source in _srcs:
                thesescans = _ms_metadata.yield_scans(source)
                for scan in thesescans:
                    if scan in comb_ldetect.keys():
                        sscan                      = str(scan)
                        got_long_solutions         = True
                        scan_newsolints[sscan]     = comb_ldetect[sscan][0]
                        non_detections_srcs[sscan] = comb_ldetect[sscan][1]
        if got_long_solutions:
            if _inp_params.verbose:
                print('  Obtaining long solint detections:')
            replace_caltb = auxiliary.unique_filename(caltable + '.new_detections')
            if zero_long_solint_delays:
                this_overwrite_mbdelay_smoothtime = None
                #This should be the mode for fringefit_multi_sci_short where prior delays have been solved over scans.
            else:
                this_overwrite_mbdelay_smoothtime = 'inf'
                #This should be the mode for fringefit_multi_cal_short where no prior delays have been solved.
            task_fringefit_multi_general(_inp_params, _ms_metadata, replace_caltb, _fly_calib_tables, _fly_calib_interp,
                                         _fly_calib_gainfd, _mpi_client, _srcs, _inp_params.fringe_minSNR_mb_reiterate,
                                         scan_newsolints, list(scan_newsolints.keys()),
                                         force_no_mpi=_inp_params.long_solint_no_mpi,
                                         overwrite_mbdelay_smoothtime=this_overwrite_mbdelay_smoothtime,
                                         overwrite_delaywin=overwrite_delaywin, overwrite_ratewin=overwrite_ratewin,
                                         corrcomb=corrcomb, _spwpartition=_spwpartition, do_rerefant=False, solvefor=solvefor
                                        )
            if zero_long_solint_delays:
                calibration.zero_fringe(_inp_params, replace_caltb, 'delay')
        elif _inp_params.verbose:
            print('  No detections on long solints to be obtained. Continuing.')
    if _inp_params.verbose:
        print('  Obtaining the default detections:')
    status = task_fringefit_multi_general(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                          _fly_calib_gainfd, _mpi_client, _srcs, fringe_snr_cutoff,
                                          overwrite_solint=overwrite_solint,
                                          overwrite_mbdelay_smoothtime=overwrite_mbdelay_smoothtime,
                                          overwrite_delaywin=overwrite_delaywin, overwrite_ratewin=overwrite_ratewin,
                                          corrcomb=corrcomb, _spwpartition=_spwpartition, do_rerefant=False, solvefor=solvefor
                                         )
    if got_long_solutions:
        if _inp_params.verbose:
            print('  Putting the long solint detections in ' + str(caltable))
        calibration.overwrite_solutions(_inp_params, _ms_metadata, replace_caltb, caltable, non_detections_srcs,
                                        list(scan_newsolints.keys())
                                       )
        auxiliary.rm_dir_if_present(replace_caltb)
        auxiliary.rm_file_if_present(replace_caltb+'.refants')
    if auxiliary.is_set(_inp_params, 'unflag_bad_rerefant'):
        unflag_bad_rerefant = True
    else:
        unflag_bad_rerefant = False
    calibration.rerefant_general(_inp_params, _ms_metadata, caltable, unflag_bad_rerefant)
    return status

def task_fringefit_multi_sci_long(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                  _spwpartition, _mpi_client):
    """
    First multiband fringefit for science targets on long timescales.
    """
    _srcs = _ms_metadata.yield_science_targets(_inp_params)
    musthavetab = False
    if not auxiliary.is_set(_inp_params, 'calibrators_phaseref') or _inp_params.phaseref_ff_science:
        musthavetab = True
    else:
        for source in _srcs:
            if not _ms_metadata.yield_phaseref(_inp_params, source):
                musthavetab = True
                break
    if not musthavetab:
        if _inp_params.verbose:
            print('    Skipping multi-band calibration for science targets for phase-referencing with phaseref_ff_science=False.')
        return False
    if _srcs:
        delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                               _inp_params.fringe_rate_window_initial
                                                              )
        return task_fringefit_multi_general(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                            _fly_calib_gainfd, _mpi_client, _srcs, _inp_params.fringe_minSNR_mb_long_sci,
                                            overwrite_solint=_inp_params.fringe_solint_mb_long, overwrite_delaywin=delay_window,
                                            overwrite_ratewin=rate_window, corrcomb='all',
                                            force_no_mpi=_inp_params.long_solint_no_mpi, _spwpartition=_spwpartition,
                                            solvefor=get_fringe_paramactive(_inp_params, 'delay,rate')
                                           )
    else:
        print('    Skipping fringefit_multi_sci calibration because no science target was set.')
        return False

def task_fringefit_multi_sci_long2(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                   _spwpartition, _mpi_client):
    """
    First multiband fringefit for science targets on long timescales.
    """
    if not _spwpartition:
        print('    Skipping fringefit_multi_sci_long2 as no spw partitions are specified.')
        return False
    _srcs = _ms_metadata.yield_science_targets(_inp_params)
    musthavetab = False
    if not auxiliary.is_set(_inp_params, 'calibrators_phaseref') or _inp_params.phaseref_ff_science:
        musthavetab = True
    else:
        for source in _srcs:
            if not _ms_metadata.yield_phaseref(_inp_params, source):
                musthavetab = True
                break
    if not musthavetab:
        if _inp_params.verbose:
            print('    Skipping multi-band calibration for science targets for phase-referencing with phaseref_ff_science=False.')
        return False
    if _srcs:
        delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                               _inp_params.fringe_rate_window_initial
                                                              )
        delay_window = [0.25*dw for dw in delay_window]
        rate_window  = [0.25*rw for rw in rate_window]
        return task_fringefit_multi_general(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                            _fly_calib_gainfd, _mpi_client, _srcs, 0.7*_inp_params.fringe_minSNR_mb_long_sci,
                                            overwrite_solint=_inp_params.fringe_solint_mb_long, overwrite_delaywin=delay_window,
                                            overwrite_ratewin=rate_window, corrcomb='all',
                                            force_no_mpi=_inp_params.long_solint_no_mpi, _spwpartition=_spwpartition,
                                            solvefor=get_fringe_paramactive(_inp_params, 'delay,rate')
                                           )
    else:
        print('    Skipping fringefit_multi_sci calibration because no science target was set.')
        return False

def task_fringefit_multi_sci_long_spwant(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                         _fly_calib_gainfd, _spwpartition, _mpi_client):
    """
    Like task_fringefit_multi_sci_long but uses only the baselines to and obtaines calibration solutions for
    _inp_params.spwpartition_ants.
    """
    if not auxiliary.is_set(_inp_params, 'spwpartition_ants'):
        print('    Skipping multi-band calibration with spwpartitions for specific antennas, because no such antennas were set.')
        return False
    else:
        inp_ants = _inp_params.spwpartition_ants
    _srcs = _ms_metadata.yield_science_targets(_inp_params)
    musthavetab = False
    if not auxiliary.is_set(_inp_params, 'calibrators_phaseref') or _inp_params.phaseref_ff_science:
        musthavetab = True
    else:
        for source in _srcs:
            if not _ms_metadata.yield_phaseref(_inp_params, source):
                musthavetab = True
                break
    if not musthavetab:
        if _inp_params.verbose:
            print('    Skipping multi-band calibration for science targets for phase-referencing with phaseref_ff_science=False.')
        return False
    if _srcs:
        delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                               _inp_params.fringe_rate_window_initial
                                                              )
        if auxiliary.is_set(_inp_params, 'spwpartition_per_pol'):
            ccomb = 'None'
        else:
            ccomb = 'all'
        out = task_fringefit_multi_general(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                           _fly_calib_gainfd, _mpi_client, _srcs, _inp_params.fringe_minSNR_mb_long_sci,
                                           overwrite_solint=_inp_params.fringe_solint_mb_long, overwrite_delaywin=delay_window,
                                           overwrite_ratewin=rate_window, corrcomb=ccomb,
                                           force_no_mpi=_inp_params.long_solint_no_mpi, _spwpartition=_spwpartition,
                                           solvefor=get_fringe_paramactive(_inp_params, 'delay,rate'), input_antenna=inp_ants,
                                           quickmem=True
                                          )
        calibration.extend_solutions_to_all_scans(_inp_params, _ms_metadata, caltable, all_params=True)
        calibration.keep_only_specific_antenna_fringes(caltable, [_ms_metadata.yield_antID(s) for s in inp_ants.split(',')])
        return out
    else:
        print('    Skipping fringefit_multi_sci calibration because no science target was set.')
        return False


def task_atmo_selfcal_startmod(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                               _spwpartition, _mpi_client):
    """
    Phase self-cal for atmospheric turbulence stabilization.
    """
    _srcs = _ms_metadata.yield_science_targets(_inp_params)
    musthavetab = False
    if not auxiliary.is_set(_inp_params, 'calibrators_phaseref') or _inp_params.phaseref_ff_science:
        musthavetab = True
    else:
        for source in _srcs:
            if not _ms_metadata.yield_phaseref(_inp_params, source):
                musthavetab = True
                break
    if not musthavetab:
        if _inp_params.verbose:
            print('    Skipping atmospheric self-cal for science targets for phase-referencing with phaseref_ff_science=False.')
        return False
    if _srcs:
        if auxiliary.is_set(_inp_params, 'atmo_selfcal'):
            _fringe_solints = readback_fringeparams(_inp_params, _ms_metadata, _fly_calib_tables, _fly_calib_interp,
                                                    _fly_calib_gainfd, [], [], 'all', 'sci', _spwpartition, _mpi_client
                                                   )
            auxiliary.rm_dir_if_present(caltable)
            scratchdir = _inp_params.scratchdir + '/'
            auxiliary.makedir(scratchdir)
            scan_tables = []
            #mpi_jobIDs  = []
            for src in _srcs:
                all_source_scans = _ms_metadata.yield_scans(src)
                for scan in all_source_scans:
                    _, default_refant, _ = communicate_fringe_solint(_inp_params, _ms_metadata, 'retrieve', _fringe_solints,
                                                                     _source=src, _scan=scan
                                                                    )
                    all_refants = _inp_params.refant
                    all_refants = all_refants.split(',')
                    if len(all_refants)>2:
                        all_refants.remove(default_refant)
                        default_refant += ','
                        default_refant += ','.join(all_refants)
                    scan_tmptab = auxiliary.unique_filename(scratchdir + 'atmo.tab.' + str(scan))
                    mID = calibration.task_gaincal_general(_inp_params, _ms_metadata, scan_tmptab, 'T', src, scan=scan,
                                                           solint='int', combine='spw', minblperant=1, minsnr=0.0,
                                                           smodel=[1,0,0,0], solmode='', calmode='p', gaintable=_fly_calib_tables,
                                                           gainfield=_fly_calib_gainfd, interp=_fly_calib_interp, append=False,
                                                           refant=default_refant, refantmode='flex', spw=_spwpartition
                                                           #MPICLIENT=_mpi_client #MPI not working here
                                                          )
                    #mpi_jobIDs.append(mID)
                    scan_tables.append(scan_tmptab)
            #if _mpi_client:
            #    _mpi_client.get_command_response(mpi_jobIDs, True)
            auxiliary.concat_caltables(scan_tables, caltable)
            calibration.unify_spectral_window_ids(caltable)
            if auxiliary.is_set(_inp_params, 'atmo_selfcal_exclude_ants'):
                rm_as = _inp_params.atmo_selfcal_exclude_ants.split(',')
                rm_as = [_ms_metadata.yield_antID(ant) for ant in rm_as]
                calibration.undo_station_CPARAM_solutions(caltable, rm_as)
            return True
        else:
            print('    Skipping atmospheric self-cal because atmo_selfcal was not set.')
            return False
    else:
        print('    Skipping atmospheric self-cal because no science target was set.')
        return False


def task_fringefit_multi_sci_short(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                   _spwpartition, _mpi_client):
    """
    Second multiband fringefit for science targets on short timescales.
    """
    _srcs = _ms_metadata.yield_science_targets(_inp_params)
    musthavetab = False
    if not auxiliary.is_set(_inp_params, 'calibrators_phaseref') or _inp_params.phaseref_ff_science:
        musthavetab = True
    else:
        for source in _srcs:
            if not _ms_metadata.yield_phaseref(_inp_params, source):
                musthavetab = True
                break
    if not musthavetab:
        if _inp_params.verbose:
            print('    Skipping multi-band calibration for science targets for phase-referencing with phaseref_ff_science=False.')
        return False
    if _srcs:
        if auxiliary.is_set(_inp_params, 'atmo_selfcal') and _inp_params.atmo_selfcal=='only':
            print('    Skipping fringefit_multi_sci_short because atmo_selfcal=only has been set.')
            return False
        delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_mb_sci_short,
                                                               _inp_params.fringe_rate_window_mb_sci_short
                                                              )
        return fringefit_with_new_solints(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                          _fly_calib_gainfd, _mpi_client, _srcs, _inp_params.fringe_minSNR_mb_short_sci,
                                          overwrite_solint=_inp_params.fringe_solint_mb_short,
                                          overwrite_delaywin=delay_window, overwrite_ratewin=rate_window, corrcomb='all',
                                          _spwpartition=_spwpartition, zero_long_solint_delays=True, solvefor='delay,rate'
                                          # Solving for rates also improves coherence.
                                         )
    else:
        print('    Skipping fringefit_multi_sci calibration because no science target was set.')
        return False

def task_fringefit_multi_cal_coher(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                   _spwpartition, _mpi_client):
    """
    Multiband fringefit for calibrators where delays are zeroed.
    Used at high frequencies to get an ad-hoc-like phase correction to increase SNR in a subsequent fringefit with solint='inf'.
    """
    if auxiliary.is_set(_inp_params, 'calibrators_phaseref'):
        raise ValueError('The coherence calibration must be skipped when doing phase referencing.')
    delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                           _inp_params.fringe_rate_window_initial
                                                          )
    #if _inp_params.array_type == 'EHT' and auxiliary.is_set(_inp_params, 'eht_temporary_coher_cal_for_all'):
    #    if _inp_params.eht_temporary_coher_cal_for_all == 'skip':
    #        print ('    Skipping fringefit_multi_cal_coher as specified by eht_temporary_coher_cal_for_all.')
    #        return False
    #    _srcs  = _ms_metadata.all_sources
    #    solint = '10'
    #    ant1   = _ms_metadata.yield_antID(_inp_params.refant.split(',')[0])
    #    AAtab  = getattr(_inp_params, 'calib_phase_offsets_ALMA')[_inp_params.C_NAM]
    #    ant2   = _ms_metadata.yield_antID(AAtab.split('_')[-1])
        #Special calib just to have the best ALMA phase calib possible.
    #    out = fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, _inp_params.fringe_minSNR_mb_coher,
    #                                   _srcs, input_antenna=str(ant1)+'&&'+str(ant2), input_solint=solint,
    #                                   combine='spw', gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
    #                                   interp=_fly_calib_interp, delaywindow=delay_window, ratewindow=rate_window,corrcomb='none'
    #                                   MPICLIENT=_mpi_client, _spwpartition=_spwpartition, skip_fringe_params_framework=True,
    #                                   only_refant=ant1, musthave_ant2=ant2, solvefor=''
    #                                  )
    #    calibration.skip_fully_flagged_fringes(caltable)
    #    calibration.rerefant_general(_inp_params, _ms_metadata, caltable, False)
    #    calibration.unify_spectral_window_ids(caltable)
    #else:
    _srcs = _ms_metadata.yield_calibrators(_inp_params)
    if _srcs:
        solint = _inp_params.fringe_solint_mb_coher
        out = fringefit_with_new_solints(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                         _fly_calib_gainfd, _mpi_client, _srcs, _inp_params.fringe_minSNR_mb_coher,
                                         overwrite_solint=solint,
                                         overwrite_delaywin=delay_window, overwrite_ratewin=rate_window, corrcomb='all',
                                         _spwpartition=_spwpartition, overwrite_mbdelay_smoothtime=None, solvefor='delay,rate'
                                        )
    else:
        print ('    Skipping fringefit_multi_cal_coher because no calibrators was set.')
        return False
    calibration.zero_fringe(_inp_params, caltable, 'delay')
    return out

def task_fringefit_multi_cal_short(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                   _spwpartition, _mpi_client):
    """
    Multiband fringefit for calibrators.
    """
    _srcs = _ms_metadata.yield_calibrators(_inp_params)
    if _srcs:
        delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                               _inp_params.fringe_rate_window_initial
                                                              )
        out = fringefit_with_new_solints(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                         _fly_calib_gainfd, _mpi_client, _srcs, _inp_params.fringe_minSNR_mb_short_cal,
                                         overwrite_solint=_inp_params.fringe_solint_mb_short,
                                         overwrite_delaywin=delay_window, overwrite_ratewin=rate_window, corrcomb='all',
                                         _spwpartition=_spwpartition, overwrite_mbdelay_smoothtime=None,
                                         solvefor='delay,rate'
                                        )
        if auxiliary.is_set(_inp_params, 'calibrators_phaseref'):
            phaseref_tab = caltable + '.phaseref'
            auxiliary.rm_dir_if_present(phaseref_tab)
            print('    Writing ' + phaseref_tab +' for phase-referencing.')
            auxiliary.copydir(caltable, phaseref_tab)
            calibration.remove_flagged_solutions(_inp_params, _ms_metadata, phaseref_tab, False)
            calibration.medfilt_solutions(_inp_params, phaseref_tab, _smoothtime='inf')
        return out
    else:
        print('  Skipping fringefit_multi_cal_short calibration because no calibrators were set.')
        return False


def task_fringefit_single(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                          _spwpartition, _mpi_client, overwrite_zerorates='', overwrite_scans='', overwrite_solint=''):
    """
    Executes CASA's fringefit task to remove instrumental phases and delays.
    Combines all scans of _inp_params.calibrators_instrphase
      and finds a single solution for phase and delay per antenna, spw and polarization.
    """
    if auxiliary.is_set(_inp_params, 'calibrators_instrphase'):
        delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                               _inp_params.fringe_rate_window_initial
                                                              )
        if isinstance(overwrite_zerorates, bool):
            _zerorates = overwrite_zerorates
        else:
            _zerorates = True
        if overwrite_solint:
            this_solint = overwrite_solint
        else:
            this_solint = _inp_params.fringe_solint_sb_instrumental
        auxiliary.rm_dir_if_present(caltable)
        if overwrite_scans:
            _scans = overwrite_scans
        else:
            _scans = auxiliary.flatten_list(_ms_metadata.yield_scans(_inp_params.calibrators_instrphase,
                                                                     do_not_squeeze_single_source=True
                                                                    ))
            if _inp_params.fringe_instrumental_scans:
                if '!' in str(_inp_params.fringe_instrumental_scans):
                    skip_scans = str(_inp_params.fringe_instrumental_scans).replace('!','')
                    skip_scans = skip_scans.split(',')
                    for skip_scan in skip_scans:
                        if skip_scan in _scans:
                            _scans.remove(skip_scan)
                        else:
                            pass
                else:
                    _scans = str(_inp_params.fringe_instrumental_scans).split(',')
        fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, _inp_params.fringe_minSNR_sb_instrumental,
                                 _ms_metadata.yield_calibrators(_inp_params), input_scans=_scans, input_solint=this_solint,
                                 zerorates=_zerorates, gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                                 interp=_fly_calib_interp, delaywindow=delay_window, ratewindow=rate_window, corrcomb='none',
                                 MPICLIENT=_mpi_client, solvefor='delay,rate'
                                )
        if auxiliary.isdir(caltable) and auxiliary.is_set(_inp_params, 'coarse_phbpass_stations'):
            skip_ants = _inp_params.coarse_phbpass_stations
            keep_aIDs = list(_ms_metadata.antennaIDs)
            for ant in skip_ants.split(','):
                keep_aIDs.remove(_ms_metadata.yield_antID(ant))
            calibration.zero_fringe(_inp_params, caltable, 'all', keep_aIDs, unflag=True)
        calibration.postprocess_singleband_solutions(_inp_params, _ms_metadata, caltable)
        return True
    else:
        print('    Skipping instrumental phase calibration as no instrphase calibrators were specified.')
        return False


def fringefit_single_overspws_general(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp,
                                      _fly_calib_gainfd, _spwpartition, _mpi_client, zerorates=True):
    """
    Same as task_fringefit_single() but the solutions are not found over individual spws, but ranges of spws, which form distinct
    frequency bands together.
    Options to deviate from the default solint, and range of scans are disabled. Here, zerorates in an input parameter.
    The postprocess_singleband_solutions(rerefant_also=False) steps are done in calibration.go_calibrate() after the caltables
    from the distinct spw partitions are concatenated.
    Called by several task_*_ffospws tasks.
    """
    if not auxiliary.is_set(_inp_params, 'calibrators_instrphase'):
        print('    Skipping spw alignment as no instrphase calibrators were specified.')
        return False
    if not _spwpartition:
        print('    Skipping spw alignment as no spw partitions are specified.')
        return False
    delay_window, rate_window = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                           _inp_params.fringe_rate_window_initial
                                                          )
    auxiliary.rm_dir_if_present(caltable)
    _scans = auxiliary.flatten_list(_ms_metadata.yield_scans(_inp_params.calibrators_instrphase,
                                                             do_not_squeeze_single_source=True
                                                            ))
    if _inp_params.fringe_instrumental_scans:
        if '!' in str(_inp_params.fringe_instrumental_scans):
            skip_scans = str(_inp_params.fringe_instrumental_scans).replace('!','')
            skip_scans = skip_scans.split(',')
            for skip_scan in skip_scans:
                if skip_scan in _scans:
                    _scans.remove(skip_scan)
                else:
                    pass
        else:
            _scans = str(_inp_params.fringe_instrumental_scans).split(',')
    fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, _inp_params.fringe_minSNR_sb_instrumental,
                             _ms_metadata.yield_calibrators(_inp_params), input_scans=_scans, input_solint='inf',
                             combine='spw', zerorates=zerorates, gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                             interp=_fly_calib_interp, delaywindow=delay_window, ratewindow=rate_window, corrcomb='none',
                             force_no_mpi=_inp_params.long_solint_no_mpi, MPICLIENT=_mpi_client, _spwpartition=_spwpartition,
                             solvefor='delay,rate', quickmem=True
                            )
    calibration.rerefant_general(_inp_params, _ms_metadata, caltable)
    return True

def task_alignbands_ffospws(_inp_params, _ms_metadata, caltable,
                            _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd, _spwpartition, _mpi_client):
    """
    Align the EHT frequency bands; 2017: lo+hi; 2018: b1+b2+b3+b4.
    """
    out = fringefit_single_overspws_general(_inp_params, _ms_metadata, caltable,
                                            _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd, _spwpartition, _mpi_client,
                                            zerorates=True
                                           )
    if out and auxiliary.isdir(caltable) and auxiliary.is_set(_inp_params, 'spwpartition_ants'):
        skip_ants = _inp_params.spwpartition_ants
        keep_aIDs = list(_ms_metadata.antennaIDs)
        for ant in skip_ants.split(','):
            keep_aIDs.remove(_ms_metadata.yield_antID(ant))
        calibration.zero_fringe(_inp_params, caltable, 'all', keep_aIDs, unflag=True)
    return out

def task_coarse_phbpass_ffospws(_inp_params, _ms_metadata, caltable,
                                _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd, _spwpartition, _mpi_client):
    """
    Perform a coarse phase bandpass for a selected list of stations by combining ranges of spws.
    """
    if not auxiliary.is_set(_inp_params, 'coarse_phbpass_stations'):
        print('    Skipping corase phase bandpass as no coarse_phbpass_stations are specified.')
        return False
    st_keep_corrections = [_ms_metadata.yield_antID(st) for st in _inp_params.coarse_phbpass_stations.split(',')]
    out                 =  fringefit_single_overspws_general(_inp_params, _ms_metadata, caltable,
                                                             _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                                             _spwpartition, _mpi_client, zerorates=True
                                                            )
    calibration.zero_fringe(_inp_params, caltable, 'all', st_keep_corrections, unflag=True)
    return out


def task_phase_offsets_ALMA(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                            _spwpartition, _mpi_client):
    """
    Uses a-priori hard-coded information to remove the ALMA sbd-mbd offset.
    """
    try:
        ant1 = _ms_metadata.yield_antID(_inp_params.ALMA_antname)
    except ValueError:
        print('    Skipping the ALMA phase offset calibration because ALMA is not present in this observation.')
        return False
    _scans = _ms_metadata.all_scans
    def hops_to_casa_delays(_delays_from_hops, obsyear):
        casa_times = []
        casa_rdela = []
        casa_ldela = []
        # can skip every other line as the delay jumps occur at the same time for RCP and LCP
        skip_line = False
        for line in _delays_from_hops:
            lline      = line.split()
            this_year  = int(lline[0])
            if this_year != obsyear:
                continue
            this_delay = float(lline[5])
            if lline[4] == 'pc_delay_r':
                casa_rdela.append(this_delay)
            elif lline[4] == 'pc_delay_l':
                casa_ldela.append(this_delay)
            if skip_line:
                skip_line = False
                continue
            endt = (datetime.datetime.strptime(lline[3], "%j-%H%M%S").replace(year=this_year)
                    - datetime.datetime.utcfromtimestamp(- 3506716800)
                   ).total_seconds()
            casa_times.append(endt)
            skip_line = True
        return np.asarray(casa_times), np.asarray(casa_rdela), np.asarray(casa_ldela)
    delays_from_hops = OrderedDict()
    # Set delays for bands classified by the lowest freq (approx) of the band in MHz as key.
    # Entries for delays_from_hops must be sorted in time.
    delays_from_hops[348663] = """2021 001-120000 to 300-120000 pc_delay_l -7.25
    2021 001-120000 to 300-120000 pc_delay_r -7.25""".split('\n')
    delays_from_hops[346663] = """2021 001-120000 to 300-120000 pc_delay_l -7.25
    2021 001-120000 to 300-120000 pc_delay_r -7.3""".split('\n')
    delays_from_hops[336604] = """2021 001-120000 to 300-120000 pc_delay_l -7.3
    2021 001-120000 to 300-120000 pc_delay_r -7.4""".split('\n')
    delays_from_hops[334663] = """2021 001-120000 to 300-120000 pc_delay_l 1.65
    2021 001-120000 to 300-120000 pc_delay_r 1.8""".split('\n')
    delays_from_hops[228163] = """2017 094-205959 to 095-210001 pc_delay_l 7.1703
    2017 094-205959 to 095-210001 pc_delay_r 7.1458
    2017 095-205959 to 096-210001 pc_delay_l 7.2662
    2017 095-205959 to 096-210001 pc_delay_r 7.2425
    2017 096-205959 to 097-210001 pc_delay_l 7.3795
    2017 096-205959 to 097-210001 pc_delay_r 7.3620
    2017 099-205959 to 100-210001 pc_delay_l 7.1826
    2017 099-205959 to 100-210001 pc_delay_r 7.1604
    2017 100-205959 to 101-210001 pc_delay_l 7.1930
    2017 100-205959 to 101-210001 pc_delay_r 7.1728
    2018 001-120000 to 300-120000 pc_delay_l -7.3
    2018 001-120000 to 300-120000 pc_delay_r -7.3
    2021 001-120000 to 300-120000 pc_delay_l -7.3
    2021 001-120000 to 300-120000 pc_delay_r -7.3""".split('\n') #b4
    delays_from_hops[226163] = """2017 094-205959 to 095-210001 pc_delay_l 7.2790
    2017 094-205959 to 095-210001 pc_delay_r 7.2670
    2017 095-205959 to 096-210001 pc_delay_l 7.3710
    2017 095-205959 to 096-210001 pc_delay_r 7.3526
    2017 096-205959 to 097-210001 pc_delay_l 7.3274
    2017 096-205959 to 097-210001 pc_delay_r 7.3138
    2017 099-205959 to 100-210001 pc_delay_l 7.2876
    2017 099-205959 to 100-210001 pc_delay_r 7.2774
    2017 100-205959 to 101-210001 pc_delay_l 7.3329
    2017 100-205959 to 101-210001 pc_delay_r 7.3161
    2018 001-120000 to 300-120000 pc_delay_l -7.3
    2018 001-120000 to 300-120000 pc_delay_r -7.3
    2021 001-120000 to 300-120000 pc_delay_l -7.3
    2021 001-120000 to 300-120000 pc_delay_r -7.3""".split('\n') #b3
    delays_from_hops[214163] = """2017 094-205959 to 095-210001 pc_delay_l 7.1703
    2017 094-205959 to 095-210001 pc_delay_r 7.1458
    2017 095-205959 to 096-210001 pc_delay_l 7.2662
    2017 095-205959 to 096-210001 pc_delay_r 7.2425
    2017 096-205959 to 097-210001 pc_delay_l 7.3795
    2017 096-205959 to 097-210001 pc_delay_r 7.3620
    2017 099-205959 to 100-210001 pc_delay_l 7.1826
    2017 099-205959 to 100-210001 pc_delay_r 7.1604
    2017 100-205959 to 101-210001 pc_delay_l 7.1930
    2017 100-205959 to 101-210001 pc_delay_r 7.1728
    2018 001-120000 to 300-120000 pc_delay_l -7.3
    2018 001-120000 to 300-120000 pc_delay_r -7.3
    2021 001-120000 to 300-120000 pc_delay_l -7.3
    2021 001-120000 to 300-120000 pc_delay_r -7.3""".split('\n') #b2
    delays_from_hops[212163] = """2017 094-205959 to 095-210001 pc_delay_l 7.2790
    2017 094-205959 to 095-210001 pc_delay_r 7.2670
    2017 095-205959 to 096-210001 pc_delay_l 7.3710
    2017 095-205959 to 096-210001 pc_delay_r 7.3526
    2017 096-205959 to 097-210001 pc_delay_l 7.3274
    2017 096-205959 to 097-210001 pc_delay_r 7.3138
    2017 099-205959 to 100-210001 pc_delay_l 7.2876
    2017 099-205959 to 100-210001 pc_delay_r 7.2774
    2017 100-205959 to 101-210001 pc_delay_l 7.3329
    2017 100-205959 to 101-210001 pc_delay_r 7.3161
    2018 001-120000 to 300-120000 pc_delay_l 1.8
    2018 001-120000 to 300-120000 pc_delay_r 1.8
    2021 001-120000 to 300-120000 pc_delay_l 1.8
    2021 001-120000 to 300-120000 pc_delay_r 1.8""".split('\n') #b1
    # The EHT will exist for a hundred years right? Extend offsets for future years.
    for key in delays_from_hops:
        y  = 2022
        ld = delays_from_hops[key][-2].strip()[4:]
        rd = delays_from_hops[key][-1].strip()[4:]
        while y < 2117:
            delays_from_hops[key].append(str(y)+ld)
            delays_from_hops[key].append(str(y)+rd)
            y+=1
    # Delays and time indices. Times assumed to be the same for RCP/LCP but can differ between bands.
    delay_endtimes = {}
    hops_rdelays   = {}
    hops_ldelays   = {}
    for minfreq in delays_from_hops.keys():
        delay_endtimes[minfreq], hops_rdelays[minfreq], hops_ldelays[minfreq] = hops_to_casa_delays(delays_from_hops[minfreq],
                                                                                                    _ms_metadata.obsyear
                                                                                                   )
    # Map the spws in the data to the band frequencies.
    rdelays_per_spw  = {}
    ldelays_per_spw  = {}
    endtimes_per_spw = {}
    for spw in _ms_metadata.all_spwds: #ints
        this_chan0_MHzfreq = np.min(_ms_metadata.yield_freq_of_channels_in_spw(spw)) * 1.e-6
        found_match     = False
        for minfreq in delays_from_hops.keys():
            if this_chan0_MHzfreq > minfreq:
                rdelays_per_spw[spw]  = hops_rdelays[minfreq]
                ldelays_per_spw[spw]  = hops_ldelays[minfreq]
                endtimes_per_spw[spw] = delay_endtimes[minfreq]
                found_match = True
                break
        if not found_match:
            raise ValueError('Cannot find match for {0} frequency of spw {1}'.format(str(this_chan0_MHzfreq), str(spw)))
    fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, 0.1, _ms_metadata.all_sources, input_scans=_scans,
                             input_solint='inf', zerorates=True, gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                             interp=_fly_calib_interp, globalsolve=False, MPICLIENT=_mpi_client, only_refant=ant1,
                             input_refant=_inp_params.ALMA_antname, skip_fringe_params_framework=True, solvefor='delay,rate'
                            )
    calibration.set_only_specific_fringe_solutions(_inp_params, caltable, [ant1], [_inp_params.F_0DELA, _inp_params.F_1DELA],
                                                   [rdelays_per_spw, ldelays_per_spw], endtimes_per_spw
                                                  )
    calibration.extend_solutions_to_all_scans(_inp_params, _ms_metadata, caltable)
    return True


def task_phase_offsets_ALMA_old(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                                _spwpartition, _mpi_client):
    """
    Solves for a phase offset introduced by the refant. This should only be needed for 2017 EHT data, where the ALMA
    system had an incorrect spectral phase tuning.
    Uses baselines from the refant=ant1 to a single other station ant2 that is obtained from the name of the caltable which
    should be <something>_<ant2>.
    """
    auxiliary.rm_dir_if_present(caltable)
    ant1   = _ms_metadata.yield_antID(_inp_params.refant.split(',')[0])
    ant2   = _ms_metadata.yield_antID(caltable.split('_')[-1])
    _scans = _ms_metadata.all_scans
    if _inp_params.fringe_instrumental_scans:
        if '!' in str(_inp_params.fringe_instrumental_scans):
            skip_scans = str(_inp_params.fringe_instrumental_scans).replace('!','')
            skip_scans = skip_scans.split(',')
            for skip_scan in skip_scans:
                if skip_scan in _scans:
                    _scans.remove(skip_scan)
                else:
                    pass
        else:
            _scans = str(_inp_params.fringe_instrumental_scans).split(',')
    fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, 0.1, _ms_metadata.all_sources,
                             input_antenna=str(ant1)+'&&'+str(ant2), input_scans=_scans,
                             input_solint='inf', zerorates=False, gaintable=_fly_calib_tables, gainfield=_fly_calib_gainfd,
                             interp=_fly_calib_interp, MPICLIENT=_mpi_client, only_refant=ant1, musthave_ant2=ant2,
                             skip_fringe_params_framework=True, solvefor='delay,rate'
                            )
    calibration.linear_fringe_interpolation_across_spws(caltable, ant2, 'FPARAM',
                                                        _indices=[_inp_params.F_0RATE, _inp_params.F_1RATE]
                                                       )
    calibration.solve_fringe_offsets(_inp_params, _ms_metadata, caltable, ant1, ant2, None, True)
    calibration.rerefant_general(_inp_params, _ms_metadata, caltable, False)
    calibration.extend_solutions_to_all_scans(_inp_params, _ms_metadata, caltable)
    auxiliary.rm_dir_if_present(_inp_params.calib_fringefit_multi_cal_coher[_inp_params.C_NAM])
    return True


def task_phase_offsets_old(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                           _spwpartition, _mpi_client):
    """
    *not used right now*
    Solves for a phase offset introduced by the refant. This should only be needed for 2017 EHT data, where the ALMA
    system had an incorrect spectral phase tuning.
    Uses baselines from the refant=ant1 to a single other station ant2 that is obtained from the name of the caltable which
    should be <something>_<ant2>.
    Method:
    - Fringefit a scan doing a FFT only to get a skeleton table with one FPARAM solution for RCP and LCP per station (inf solint)
    - Unflag all data and zero all phase,delay,rate corrections.
    - Go through all scans and if ant1 and ant2 are present:
      * Copy dummy set of rows.
      * Fit line through scan averaged phases from ant1-ant2 baseline of each spw.
      * Solve for phase offsets of lines across spw boundaries using the median slope of all lines combined.
      * Put in solved phase offsets for each station but the refant.
    """
    mytb      = casac.table()
    ant1      = _ms_metadata.yield_antID(_inp_params.refant.split(',')[0])
    ant2      = _ms_metadata.yield_antID(caltable.split('_')[-1])
    s_scans   = _ms_metadata.selected_scans_dict[_inp_params.calibrators_instrphase.split(',')[0]].split(',')
    for scan in s_scans:
        dofit = str(calibration.get_refant(_inp_params, _ms_metadata, scan))==str(_ms_metadata.yield_antname(ant1)) and \
                ant2 in _ms_metadata.yield_antennas(scan)
        if dofit:
            scan0 = scan
            break
    all_scans = auxiliary.subtract_list(_ms_metadata.all_scans, [str(scan0)])
    task_fringefit_general(_inp_params, _ms_metadata, caltable, ant1, scan=scan0, solint='inf', minsnr=0.1, globalsolve=False,
                           gaintable=_fly_calib_tables, interp=_fly_calib_interp, gainfield=_fly_calib_gainfd
                          )
    auxiliary.check_if_table_is_written(caltable)
    mytb.open(caltable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        thisflag = mytb.getcell('FLAG', row)
        mytb.putcell('FLAG', row, np.zeros_like(thisflag))
        thisfparam = mytb.getcell('FPARAM', row)
        mytb.putcell('FPARAM', row, np.zeros_like(thisfparam))
    all_spwds = _ms_metadata.all_spwds
    if _ms_metadata.yield_freq_of_channels_in_spw(all_spwds[0])[0] > _ms_metadata.yield_freq_of_channels_in_spw(all_spwds[1])[0]:
        all_spwds = all_spwds[::-1]
    all_scans = [scan0] + all_scans
    S_total   = str(len(all_scans))
    qselc     = 'FLAG, CORRECTED_DATA'
    qcrit0    = 'ANTENNA1==' + str(ant1) + ' && ANTENNA2==' + str(ant2)
    numrow    = 0
    for s_counter, scan in enumerate(all_scans):
        if _inp_params.verbose:
            print('    Processing scan ' + str(scan) + ' (' + str(s_counter+1) + '/' + S_total + ')')
        startrow  = numrow * nrows
        endrow    = (numrow+1) * nrows
        dofit     = str(calibration.get_refant(_inp_params, _ms_metadata, scan))==str(_ms_metadata.yield_antname(ant1)) and \
                    ant2 in _ms_metadata.yield_antennas(scan)
        if dofit:
            qcrit1 = qcrit0 + ' && SCAN_NUMBER==' + str(scan)
            gotsol = False
            ref_pR = 0
            ref_pL = 0
            ref_nu = 0
            phoffR = {}
            phoffL = {}
            for spw in all_spwds:
                qcrit      = qcrit1 + ' && DATA_DESC_ID==' + str(spw)
                flag, data = auxiliary.read_CASA_table(_inp_params, _query_select = qselc, _query_criteria = qcrit)
                flag       = np.take(flag, indices = [0,-1], axis = 0)
                data       = np.take(data, indices = [0,-1], axis = 0)
                data       = np.ma.masked_array(data, flag)
                if data[0].count() > 2 and data[1].count() > 2:
                    gotsol      = True
                    avgdat      = np.ma.average(data, axis=-1)
                    real        = np.real(avgdat)
                    imag        = np.imag(avgdat)
                    phase       = np.arctan2(imag,real)
                    phaseR      = auxiliary.wrap_arrayphase(phase[0])
                    phaseL      = auxiliary.wrap_arrayphase(phase[1])
                    nu          = _ms_metadata.yield_freq_of_channels_in_spw(spw)
                    pR_nu, pR_0 = np.ma.polyfit(nu, phaseR, 1)
                    pL_nu, pL_0 = np.ma.polyfit(nu, phaseL, 1)
                    if not ref_nu:
                        ref_nu = nu[-1]
                        ref_pR = pR_0 + pR_nu * ref_nu
                        ref_pL = pL_0 + pL_nu * ref_nu
                    else:
                        phoffR[spw] = (pR_nu, pR_0)
                        phoffL[spw] = (pL_nu, pL_0)
            if gotsol and phoffR:
                #get median phase slope (single band delay):
                sbdR  = np.median([p[0] for p in phoffR.values()])
                sbdL  = np.median([p[0] for p in phoffL.values()])
                sspwR = phoffR.keys()
                sspwL = phoffL.keys()
                if numrow > 0:
                    mytb.copyrows(caltable, nrow=nrows)
                    mytb.flush()
                for row in np.arange(startrow, endrow):
                    mytb.putcell('SCAN_NUMBER', row, int(scan))
                    thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', row)
                    thisant = mytb.getcell('ANTENNA1', row)
                    if str(thisant)!=str(ant1):
                        solveR = 0
                        solveL = 0
                        if thisspw in sspwR:
                            solveR = auxiliary.wrap_phase(ref_pR - phoffR[thisspw][1] - sbdR * ref_nu, np.pi)
                        if thisspw in sspwL:
                            solveL = auxiliary.wrap_phase(ref_pL - phoffL[thisspw][1] - sbdL * ref_nu, np.pi)
                        if solveR or solveL:
                            thisfparam                       = mytb.getcell('FPARAM', row)
                            solvefparam                      = copy.deepcopy(thisfparam)
                            solvefparam[_inp_params.F_0PHAS] = solveR
                            solvefparam[_inp_params.F_1PHAS] = solveL
                            mytb.putcell('FPARAM', row, solvefparam)
                numrow += 1
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    return True


def fringefit_mpi_exhaustive(_inp_params, _ms_metadata, caltable, fringe_snr_cutoff=7.0, input_sources='',
                             input_antenna='', input_scans='', input_solint='coherence', combine='', zerorates=False,
                             gaintable='', gainfield='', interp='', spwmap=[], globalsolve=True,
                             weightfactor=2, delaywindow=[], ratewindow=[], corrcomb='none', MPICLIENT=False, force_no_mpi=False,
                             only_refant='', input_refant='', musthave_ant2='', flag_uncalib_scans=False,
                             pass_missing_table=False, sci_or_cal='unknown', silence=False, _spwpartition='',
                             return_solints=False, skip_fringe_params_framework=False, solvefor='delay,rate',
                             quickmem=False):
    """
    Wrapper around task_fringefit_general() that handles an exhaustive fringe search and MPI parallelization control.
    If force_no_mpi=True, MPI is always disbled for this fringe-fit run.
    only_refant can be specified to only fringe-fit scans where the primary refant is equal to only_refant.
    musthave_ant2 can be specified to only fringe-fit scans where musthave_ant2 is present.
    input_refant can be used to override the refants per scan from readback_fringeparams(_inp_params, _ms_metadata).
    If flag_uncalib_scans=True, scans for which no solint is found are flagged explicitly. Scan selections based on
    input_sources, input_scans, only_refant, and musthave_ant2 are skipped without being flagged.
    If pass_missing_table=True, no error is raised when no calibration table is written.
    sci_or_cal determines of refants are to be extracted for sci, cal, all sources, or unknown (will be determined based on
    input_sources).
    If return_solints=True, a dictionary of scans successfully fringe-fitted and their final solints are returned (depending on
    the inp_params, the solints may be slightly adjusted).
    """
    if isinstance(input_sources, str):
        these_sources = input_sources.split(',')
    else:
        these_sources = input_sources
    if sci_or_cal == 'sci':
        sourcetype = 'sci'
    elif sci_or_cal == 'cal':
        sourcetype = 'cal'
    elif sci_or_cal == 'unknown' and these_sources:
        sci_sources = _ms_metadata.yield_science_targets(_inp_params)
        cal_sources = _ms_metadata.yield_calibrators(_inp_params)
        is_sci_src  = False
        is_cal_src  = False
        if sci_sources:
            for src in these_sources:
                if src in sci_sources:
                    is_sci_src = True
                else:
                    pass
        else:
            pass
        for src in these_sources:
            if src in cal_sources:
                is_cal_src = True
            else:
                pass
        if is_sci_src and is_cal_src:
            sourcetype = 'all'
        elif is_sci_src:
            sourcetype = 'sci'
        elif is_cal_src:
            sourcetype = 'cal'
        else:
            raise ValueError('Got unknown sourcename(s): ' + str(these_sources))
    else:
        sourcetype = 'all'
    if not skip_fringe_params_framework:
        _fringe_solints = readback_fringeparams(_inp_params, _ms_metadata, gaintable, interp, gainfield, delaywindow, ratewindow,
                                                corrcomb, sourcetype, _spwpartition, MPICLIENT
                                               )
    if not MPICLIENT:
        dompi = False
    elif str(force_no_mpi).lower()=='safety':
        dompi = True
    elif force_no_mpi:
        dompi = False
    else:
        dompi = _inp_params.ff_mpi
    if dompi:
        this_mpiclient = MPICLIENT
    else:
        this_mpiclient = None
    if dompi:
        mpi_jobIDs = []
        if _inp_params.mpi_memory_safety or force_no_mpi=='safety':
            auxiliary.wait_until_memory_settles(_inp_params, for_fringefit=False)
            free_memory0, allowed_mem = auxiliary.get_free_memory()
            num_mpi_workers           = len(_inp_params.MPI_processIDs.values())
        else:
            free_memory0    = 999
            num_mpi_workers = 999
            allowed_mem     = 0
        N_active          = 0
        running_processes = {}
    if not globalsolve:
        quickmem = True
    scratchdir = _inp_params.scratchdir + '/'
    auxiliary.makedir(scratchdir)
    this_fringe        = False
    uncalib_scans      = []
    scan_tables        = []
    scan_main_info     = {}
    all_refants        = {}
    used_solints       = {}
    scan_tables_dict   = {}
    exhaustive_iterate = defaultdict(dict)
    #input_solint can also be equal to 'coherence' (handled in loop below).
    if not input_solint:
        solint = 'inf'
    else:
        solint = input_solint
    if not these_sources:
        sources = list(_ms_metadata.selected_scans_dict.keys())
    else:
        sources = these_sources
    sources         = auxiliary.force_list(sources)
    overwrite_scans = auxiliary.force_list(input_scans)
    auxiliary.rm_dir_if_present(caltable)
    #For MPI memory management, sort scans by increasing number of visibilities:
    src_scan_tuples = []
    for source in sources:
        all_source_scans = _ms_metadata.yield_scans(source)
        if overwrite_scans:
            scans = auxiliary.overlap_lists(all_source_scans, overwrite_scans)
        else:
            scans = all_source_scans
        for scan in scans:
            if solint == 'coherence' or solint == 'inf':
                src_scan_tuples.append((source, scan, _ms_metadata.yield_numvisi(scan)))
            else:
                if isinstance(solint, dict):
                    this_solint = solint[scan]
                else:
                    this_solint = solint
                this_solint = float(str(this_solint).replace('s', ''))
                v_persolint = _ms_metadata.yield_numvisi(scan) / _ms_metadata.yield_scan_length(scan)
                src_scan_tuples.append((source, scan, this_solint * v_persolint))
    src_scan_tuples = sorted(src_scan_tuples, key=lambda x: x[2])[::-1]
    for sst in src_scan_tuples:
        #Normal fringe-fitting for each scan first.
        source = sst[0]
        scan   = sst[1]
        if not skip_fringe_params_framework:
            coherence_solint, default_refant, subclusters = communicate_fringe_solint(_inp_params, _ms_metadata, 'retrieve',
                                                                                      _fringe_solints, _source=source,
                                                                                      _scan=scan
                                                                                     )
        else:
            coherence_solint = 'inf'
            default_refant   = _inp_params.refant.split(',')[0]
            subclusters      = False
        if input_refant or input_refant==0:
            if isinstance(input_refant, dict):
                primary_refant = input_refant[scan]
            else:
                primary_refant = input_refant
        else:
            primary_refant = default_refant
        if input_solint == 'coherence':
            this_solint = coherence_solint
        elif isinstance(input_solint, dict):
            this_solint = input_solint[scan]
        else:
            this_solint = solint
        if not coherence_solint or not this_solint:
            if flag_uncalib_scans:
                uncalib_scans.append(scan)
                if _inp_params.verbose and _inp_params.aply_uncalib_scans and not silence:
                    print('  ->Skipping and flagging scan ' + str(scan) + ' because no valid solint was found.')
                else:
                    pass
            else:
                pass
            continue
        if (only_refant!='' and _ms_metadata.yield_antname(only_refant)!=primary_refant) or \
           (musthave_ant2!='' and _ms_metadata.yield_antID(musthave_ant2) not in _ms_metadata.yield_antennas(scan)):
            continue
        scan_tmptab = auxiliary.unique_filename(scratchdir + 'ff.tab.' + str(scan))
        if dompi and not quickmem:
            #Wait for free memory before allocating jobs.
            running_processes = auxiliary.continue_mpi_ff(_inp_params, _ms_metadata, scan, this_solint, scan_tmptab,
                                                          [free_memory0, allowed_mem], num_mpi_workers, running_processes,
                                                          force_no_mpi, for_fringefit=this_solint=='inf')
        if _inp_params.verbose and not silence:
            print('  ->Fringe-fitting scan ' + str(scan) + ' with a solution interval of ' + str(this_solint) + '[s]')
            print('    Using ' + str(primary_refant) + ' as refant.')
        thisID, tslnt = task_fringefit_general(_inp_params, _ms_metadata, scan_tmptab, primary_refant, field=source,
                                               antenna=input_antenna, scan=str(scan), solint=this_solint, combine=combine,
                                               minsnr=fringe_snr_cutoff, zerorates=zerorates,
                                               gaintable=gaintable, gainfield=gainfield, interp=interp,
                                               spwmap=spwmap, globalsolve=globalsolve, weightfactor=weightfactor,
                                               delaywindow=delaywindow, ratewindow=ratewindow, corrcomb=corrcomb,
                                               MPICLIENT=this_mpiclient, _spwpartition=_spwpartition, only_refant=only_refant,
                                               solvefor=solvefor
                                              )
        if dompi and quickmem:
            if not _inp_params.mpi_memory_safety and str(force_no_mpi).lower()!='safety':
                pass
            elif N_active > num_mpi_workers:
                pass
            elif N_active:
                auxiliary.free_mem_continue(_inp_params, N_active)
            else:
                # Use a simple fudge factor here, assuming no process will need much more mem than the first.
                N_active = int(0.8 * free_memory0 / auxiliary.get_max_used_mem(_inp_params))
        used_solints[scan] = tslnt
        this_fringe        = thisID
        if dompi:
            mpi_jobIDs.append(thisID[0])
        scan_tables.append(scan_tmptab)
        scan_tables_dict[scan_tmptab] = scan
        scan_main_info[scan]          = (scan_tmptab, primary_refant, this_solint, source)
        all_refants[scan]             = _ms_metadata.yield_antID(primary_refant)
        if not only_refant and subclusters and subclusters!='non-exhaustive':
            #Store info for exhaustive search.
            refants_processed  = [_ms_metadata.yield_antID(primary_refant)]
            for cluster_ref in sorted(list(subclusters.keys()), key=len):
                cluster_connections = cluster_ref.split(',')
                this_refant         = cluster_connections[-1]
                this_refantID       = _ms_metadata.yield_antID(this_refant)
                if this_refantID in refants_processed:
                    #Got this one already from another path.
                    continue
                else:
                    reref_these = [int(_ms_metadata.yield_antID(antname)) for antname in subclusters[cluster_ref]]
                    refants_processed.append(this_refantID)
                    exhaustive_iterate[cluster_ref][scan] = (this_refant, reref_these)
    #Now the exhaustive fringe search with an inner loop over scans for faster MPI computation.
    #Have to do this afterwards because of per-scan table locks.
    if dompi:
        #Starting from the longest scans again, must reset memory safeguard.
        MPICLIENT.get_command_response(mpi_jobIDs, True)
        mpi_jobIDs        = []
        running_processes = auxiliary.add_known_mem_usages_to_running_processes(running_processes)
        if _inp_params.verbose and exhaustive_iterate:
            print('  ==>Obtaining exhaustive fringes now..')
        auxiliary.wait_until_memory_settles(_inp_params, for_fringefit=False)
    for cluster_ref in exhaustive_iterate:
        for scan in exhaustive_iterate[cluster_ref]:
            scan_tmptab = scan_main_info[scan][0]
            this_solint = scan_main_info[scan][2]
            source      = scan_main_info[scan][3]
            this_refant = exhaustive_iterate[cluster_ref][scan][0]
            #Exhaustive fringe search for a specific source,scan fringe-fit:
            scan_subclust_tmptab = auxiliary.unique_filename('{0}.exhaustive_search.ref_{1}'.format(scan_tmptab, this_refant))
            if dompi and not quickmem:
                running_processes = auxiliary.continue_mpi_ff(_inp_params, _ms_metadata, scan, this_solint,
                                                              scan_subclust_tmptab, [free_memory0, allowed_mem], num_mpi_workers,
                                                              running_processes, force_no_mpi, for_fringefit=this_solint=='inf'
                                                             )
            #Always pass all antennas to fringefit(), non-detections will be discarded automatically, based on minsnr.
            thisID, _ = task_fringefit_general(_inp_params, _ms_metadata, scan_subclust_tmptab, this_refant, field=source,
                                               antenna=input_antenna, scan=str(scan), solint=this_solint, combine=combine,
                                               minsnr=fringe_snr_cutoff, zerorates=zerorates,
                                               gaintable=gaintable, gainfield=gainfield, interp=interp,
                                               spwmap=spwmap, globalsolve=globalsolve, weightfactor=weightfactor,
                                               delaywindow=delaywindow, ratewindow=ratewindow, corrcomb=corrcomb,
                                               MPICLIENT=this_mpiclient, _spwpartition=_spwpartition, only_refant=only_refant,
                                               solvefor=solvefor
                                              )
            if dompi and quickmem:
                if not _inp_params.mpi_memory_safety and str(force_no_mpi).lower()!='safety':
                    pass
                elif N_active > num_mpi_workers:
                    pass
                else:
                    auxiliary.free_mem_continue(_inp_params, N_active)
            if dompi:
                mpi_jobIDs.append(thisID[0])
            exhaustive_iterate[cluster_ref][scan] = (exhaustive_iterate[cluster_ref][scan], scan_subclust_tmptab)
    #Need inner loop over sub-clusters and outer over scans for re-referencing computation.
    reverse_exhaustive_iterate = defaultdict(dict)
    for cluster_ref in exhaustive_iterate:
        for scan in exhaustive_iterate[cluster_ref]:
            reverse_exhaustive_iterate[scan][cluster_ref] = exhaustive_iterate[cluster_ref][scan]
    #Wait until all mpi jobs are finished, then process all generated fringe-fit tables (subcluster connections and scan concat).
    if dompi:
        MPICLIENT.get_command_response(mpi_jobIDs, True)
    #Write all established re-referenced solutions into the main table:
    for scan in reverse_exhaustive_iterate:
        main_scan_table = scan_main_info[scan][0]
        cleanup_only    = False
        if not auxiliary.isdir(main_scan_table):
            cleanup_only = True
        primary_scan_refant = scan_main_info[scan][1]
        fringes_per_refant  = {_ms_metadata.yield_antID(primary_scan_refant): exhaustive.read_caltb_into_memory(main_scan_table)}
        #Read all sub-cluster connections into memory for this scan.
        for cluster_ref in reverse_exhaustive_iterate[scan]:
            this_subclust_table = reverse_exhaustive_iterate[scan][cluster_ref][1]
            if not cleanup_only:
                this_antID                     = _ms_metadata.yield_antID(reverse_exhaustive_iterate[scan][cluster_ref][0][0])
                fringes_per_refant[this_antID] = exhaustive.read_caltb_into_memory(this_subclust_table)
            auxiliary.rm_dir_if_present(this_subclust_table)
        if cleanup_only:
            continue
        #Do the re-referencing for each complete sub-cluster chain.
        #Must do the longest (outer) paths first to avoid re-referencing the inner parts multiple times.
        exiter_longfirst = sorted(list(reverse_exhaustive_iterate[scan].keys()), key=len)[::-1]
        for cluster_ref in exiter_longfirst:
            cluster_connections  = cluster_ref.split(',')
            cluster_ref_IDs      = [int(_ms_metadata.yield_antID(antname)) for antname in cluster_connections]
            got_full_reref_chain = True
            for crid in cluster_ref_IDs:
                if not fringes_per_refant[crid]:
                    got_full_reref_chain = False
                    break
            if not got_full_reref_chain:
                continue
            reref_these = reverse_exhaustive_iterate[scan][cluster_ref][0][1]
            if cluster_ref_IDs[0] != int(_ms_metadata.yield_antID(primary_scan_refant)):
                different_primary_refant = True
                reref_these.append(cluster_ref_IDs[0])
            else:
                different_primary_refant = False
            if _inp_params.verbose and not silence:
                reref_print   = ','.join([_ms_metadata.yield_antname(station) for station in reref_these])
                cluster_print = '=>'.join([_ms_metadata.yield_antname(station) for station in cluster_connections[::-1]])
                print('    Re-referencing {0} via {1} for scan {2}'.format(reref_print, cluster_print, str(scan)))
            exhaustive.connect_subcluster_reref(_ms_metadata, cluster_ref_IDs, main_scan_table, fringes_per_refant, reref_these,
                                                different_primary_refant
                                               )
    missing_tables = auxiliary.concat_caltables(scan_tables, caltable, pass_missing_tables=pass_missing_table,
                                                return_missing_tables=True
                                               )
    if all_refants:
        auxiliary.store_object(caltable+'.refants', all_refants, 'write')
    if flag_uncalib_scans and any(uncalib_scans):
        fg_fnam = auxiliary.unique_filename(_inp_params.diagdir + _inp_params.flag_dir + _inp_params.file_uncalib_scans)
        #Create flag_dir folder.
        flagging_algorithm.go_flag(_inp_params, _ms_metadata, 'pass')
        fg_file = open(fg_fnam, 'w')
        for uncalib_sc in uncalib_scans:
            _thisflag = "scan='{0}' reason='uncalibrated scan'\n".format(str(uncalib_sc))
            fg_file.write(_thisflag)
        fg_file.close()
        if _inp_params.aply_uncalib_scans and auxiliary.isfile(fg_fnam):
            flagging_algorithm.task_flagdata(_inp_params, fg_fnam)
    if not auxiliary.isdir(caltable) and not pass_missing_table:
        raise IOError('No fringe table was written. Check log, fringe-fitting inputs, and the _fringe_solints object.')
    if return_solints:
        for missing_tab in missing_tables:
            missing_scan = scan_tables_dict[missing_tab]
            del used_solints[missing_scan]
        return used_solints
    else:
        return this_fringe


def parse_spectral_line_location(_inp_params, _ms_metadata, source, scan):
    """
    Find the right channels to fringefit a spectral line source as described for the spectral_line input parameter.
    """
    sl = _inp_params.spectral_line
    if sl == 'search':
        sl_per_scan = {}
        with open('spectral_line_peak_channel.txt', 'r', encoding='utf-8') as f:
            for line in f:
                lline = line.split()
                sl_per_scan[lline[0]] = lline[1]
        try:
            sl = sl_per_scan[scan]
        except KeyError:
            sl = sl_per_scan['avg']
    else:
        pass
    bp = np.array(_ms_metadata.channels_nu)
    if not isinstance(sl, list):
        sl = [sl]
    psl = []
    for sll in sl:
        entries = sll.split('~')
        pes     = []
        for i,e in enumerate(entries):
            if 'GHz' in e:
                e = float(e.replace('GHz',''))*1.e9
                e = np.unravel_index(np.abs(bp-e).argmin(), bp.shape)
                e = [str(v) for v in e]
                if i==0:
                    e = ':'.join(e)
                else:
                    e = e[1]
            pes.append(e)
        psl.append('~'.join(pes))
    if len(psl) == 1:
        return psl[0]
    sci_list = _inp_params.science_target
    sci_list = sci_list.split(',')
    sci_indx = sci_list.index(source)
    try:
        return psl[sci_indx]
    except IndexError:
        raise ValueError('No corresponding spectral_line entry for the {0} science target. Check input!'.format(source))


def get_fringe_paramactive(_inp_params, _solvefor):
    """
    Add higher-order effects from mb_higher_order_fringe_search to the _solvefor parameter.
    """
    if not auxiliary.is_set(_inp_params, 'mb_higher_order_fringe_search'):
        return _solvefor
    return _solvefor + ',' + _inp_params.mb_higher_order_fringe_search


def parse_input_fringe_windows(_ms_metadata, delaywindow, ratewindow):
    """
    Puts fringe search windows in format required by fringefit task.
    """
    delay_window_full, rate_window_full = get_default_fringefit_serach_windows(_ms_metadata)
    if not delaywindow:
        delaywindow_p = []
    else:
        delaywindow_p = delaywindow
        try:
            nyquist_frac  = float(delaywindow_p)
            delaywindow_p = nyquist_frac * delay_window_full
        except TypeError:
            pass
    if ',' in delaywindow_p:
        delaywindow_p = delaywindow_p.split(',')
        delaywindow_p = [float(dw) for dw in delaywindow_p]
    if not ratewindow:
        ratewindow_p = []
    else:
        ratewindow_p = ratewindow
        try:
            nyquist_frac  = float(ratewindow_p)
            ratewindow_p = nyquist_frac * rate_window_full
        except TypeError:
            pass
    if ',' in ratewindow_p:
        ratewindow_p = ratewindow_p.split(',')
        ratewindow_p = [float(rw) for rw in ratewindow_p]
    if any(ratewindow_p):
        #picoseconds/second instead of seconds/second
        ratewindow_p = [rw * 1.e-12 for rw in ratewindow_p]
    return delaywindow_p, ratewindow_p


def get_default_fringefit_serach_windows(_ms_metadata):
    """
    Get the window sizes for the default (full Nyquist) delay and rate windows.
    Delays are in units of nano seconds and rates are in units of picosecond per second.
    """
    dw_full, rw_full = get_Nyquist_fringefit_ranges(_ms_metadata)
    dw_full *= 1.e9
    rw_full *= 1.e12 / _ms_metadata.reference_frequency
    return np.asarray([-0.5*dw_full, 0.5*dw_full]), np.asarray([-0.5*rw_full, 0.5*rw_full])


def get_Nyquist_fringefit_ranges(_ms_metadata):
    """
    Get full FFT search ranges of fringefit based on the frequency and time resolution.
    """
    #get average channel spacing as it is not known which spw is processed
    avg_channel_spacing = 0.
    for spw in _ms_metadata.all_spwds:
        avg_channel_spacing += _ms_metadata.yield_channel_spacing(_ms_metadata.all_spwds[spw])
    avg_channel_spacing /= len(_ms_metadata.all_spwds)
    return 1./avg_channel_spacing, 1./_ms_metadata.acp


def get_fringefit_search_space(_ms_metadata, scan, delaywindow=[], ratewindow=[], solint='inf', combine=''):
    """
    Get the number of delay x rate gridpoints to be searched over by the fringefit FFT.
    In the Fourier transform planes, spacings are given as the inverse of solution intervals and vice-versa.
    The number of gridpoints is used to evaluate the reduction of serach space when using open delay and rate windows
    vs. using contrained search windows. Padding is ignored.
    """
    #Get spacings in Fourier space
    frequency_bandwidth = float(_ms_metadata.full_bandwidth)
    if 'spw' in combine:
        #use average spw bandwidth as it not known which spw is processed
        frequency_bandwidth /= len(_ms_metadata.all_spwds)
    delay_spacing = 1. / frequency_bandwidth
    if solint=='inf':
        time_solint = _ms_metadata.yield_scan_length(scan)
    else:
        time_solint = float(solint)
    rate_spacing = 1. / time_solint
    #Get interval lengths in Fourier space
    delay_interval_full, rate_interval_full = get_Nyquist_fringefit_ranges(_ms_metadata)
    if any(delaywindow):
        delay_interval_window = max(delaywindow) - min(delaywindow)
        #delays are given in units of nano seconds
        delay_interval_window *= 1.e-9
        delay_interval        = min(delay_interval_window, delay_interval_full)
    else:
        delay_interval = delay_interval_full
    if any(ratewindow):
        rate_interval_window = max(ratewindow) - min(ratewindow)
        #rates are cast into units of seconds/second by division with the reference frequency
        rate_interval_window *= _ms_metadata.reference_frequency
        rate_interval        = min(rate_interval_window, rate_interval_full)
    else:
        rate_interval = rate_interval_full
    delay_gridpt = float(delay_interval) / delay_spacing
    rate_gridpt  = float(rate_interval) / rate_spacing
    return delay_gridpt * rate_gridpt


def adjust_minsnr_based_on_Perr(_inp_params, _ms_metadata, scan, minsnr, delaywindow=[], ratewindow=[], solint='inf',
                                combine=''):
    """
    Assumes that the set minsnr is appropriate for the full delay and rate search spaces.
    Then, based on narrower windows (if set), returns an adjusted minsnr value, such that the probability of false
    detection P_err is the same.
    P_err is given as n * exp (-snr^2/2)
    with n the number of gridpoints in the delay x rate search space. So, as n decreases, the snr is adjusted such
    that P_err stays the same.
    """
    if not scan:
        return minsnr
    if not any(delaywindow) and not any(ratewindow):
        return minsnr
    open_serach_range   = get_fringefit_search_space(_ms_metadata, scan, [], [], solint, combine)
    narrow_search_range = get_fringefit_search_space(_ms_metadata, scan, delaywindow, ratewindow, solint, combine)
    snr_adjust          = 2*np.log(open_serach_range/narrow_search_range)
    minsnr2             = minsnr**2
    if minsnr2 > snr_adjust:
        modified_snr    = np.sqrt(minsnr2 - snr_adjust)
        min_allowed_snr = min(minsnr, 3.)
        return max(modified_snr, min_allowed_snr)
    else:
        #Work with default search windows.
        delaywindow, ratewindow = parse_input_fringe_windows(_ms_metadata, _inp_params.fringe_delay_window_initial,
                                                             _inp_params.fringe_rate_window_initial
                                                            )
        narrow_search_range = get_fringefit_search_space(_ms_metadata, scan, delaywindow, ratewindow, solint, combine)
        snr_adjust          = 2*np.log(open_serach_range/narrow_search_range)
        minsnr2             = minsnr**2
        if minsnr2 > snr_adjust:
            modified_snr    = np.sqrt(minsnr2 - snr_adjust)
            min_allowed_snr = min(minsnr, 3.)
            return max(modified_snr, min_allowed_snr)
        else:
            return minsnr


def task_fringefit_general(_inp_params, _ms_metadata, caltable, refant, field='', antenna='', scan='', solint='inf',
                           combine='', minsnr=3.0, zerorates=False, append=False, gaintable='', gainfield='', interp='',
                           spwmap=[], globalsolve=True, weightfactor=2, delaywindow=[], ratewindow=[], corrcomb='none',
                           MPICLIENT = False, _spwpartition='', only_refant='', solvefor='delay,rate'):
    """
    Generic fringefit task. Used for single and multi-band calibration
      (both for trial-and-error optimization and the production run).
    Can try to optimize the solint for you if a specific scan was given as input, unless MPICLIENT=True, where
      the mpi command ID is returned.
    """
    DELA_PARAM_INDEX = 0
    RATE_PARAM_INDEX = 1
    DISP_PARAM_INDEX = 2
    LEN_PARAM_INDEX  = 3
    paramactive      = [False] * LEN_PARAM_INDEX
    if 'delay' in solvefor:
        paramactive[DELA_PARAM_INDEX] = True
    if 'rate' in solvefor:
        paramactive[RATE_PARAM_INDEX] = True
    if 'disp' in solvefor:
        paramactive[DISP_PARAM_INDEX] = True
    if auxiliary.is_set(_inp_params, 'spectral_line') and not field:
        raise ValueError('Cannot fringefit in a spectral line experiment without knowing the name of the source!')
    if auxiliary.is_set(_inp_params, 'spectral_line'):
        if field in _inp_params.science_target:
            paramactive[DELA_PARAM_INDEX] = False
            paramactive[DISP_PARAM_INDEX] = False
            _spwpartition = parse_spectral_line_location(_inp_params, _ms_metadata, field, scan)
        elif _ms_metadata.line_spw>-1 and 'spw' in combine:
            # Skip line spw for multi-band calibration steps.
            _spwpartition = ','.join(np.array(np.delete(_ms_metadata.all_spwds, _ms_metadata.line_spw), dtype=str))
        else:
            pass
    if field:
        gaintable, interp, gainfield, _, _ = calibration.get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve', field)
    else:
        gainfield = calibration.get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve')
    if not spwmap:
        spwmap = auxiliary.get_spwmap(_inp_params, _ms_metadata, gaintable)
    if not isinstance(scan, str):
        scan   = str(scan)
    if not isinstance(solint, str):
        solint = str(solint)
    if not only_refant:
        all_refants = _inp_params.refant
        all_refants = all_refants.split(',')
        if len(all_refants)>2:
            all_refants.remove(refant)
            refant += ','
            refant += ','.join(all_refants)
        else:
            pass
    #if field: ## Unused (using CASA gainfield method instead).
    #    gaintable, interp, _ = calibration.select_src_calib_params(field, gaintable, interp)
    try:
        solint_float = float(solint)
        if _inp_params.floating_fringe_solint and not isinstance(_inp_params.fringe_solint_optimize_search_cal, list) \
        and scan and solint!='inf':
            _this_scanlen                = _ms_metadata.yield_scan_length(scan)
            _new_solint, possible_change = auxiliary.get_nearest_divisor(_this_scanlen, solint_float)
            if possible_change and possible_change != 'rounded':
                if _inp_params.verbose:
                    print ('    Good news: I found a solint that fits better into the length of this scan:')
                    print ('      Using ' + str(_new_solint) + 's instead of ' + str(solint) + 's')
                solint = str(_new_solint)
    except ValueError:
        #leave solints of 'int' alone
        pass
    if _inp_params.fringe_SNR_cutoff_float_Perr:
        minsnr = adjust_minsnr_based_on_Perr(_inp_params, _ms_metadata, scan, minsnr, delaywindow, ratewindow, solint, combine)
    auxiliary.mk_dirname_path_if_not_present(caltable)
    if MPICLIENT:
        #seems like mpicasa does not recognize os.chdir so I have to use full paths
        ms_name_fp    = _inp_params.workdir + _inp_params.ms_name
        caltable_fp   = _inp_params.workdir + caltable
        gaintable_fp  = [_inp_params.workdir + gt for gt in gaintable]
        if not interp:
            interp = []
        if not gainfield:
            gainfield = []
        fringefit_cmd = ("""fringefit(vis='{0}',""".format(ms_name_fp)
                         + """caltable='{0}',""".format(caltable_fp)
                         + """field='{0}',""".format(str(field))
                         + """spw='{0}',""".format(str(_spwpartition))
                         + """selectdata={0},""".format('True')
                         + """timerange='{0}',""".format('')
                         + """antenna='{0}',""".format(str(antenna))
                         + """scan='{0}',""".format(str(scan))
                         + """observation='{0}',""".format('')
                         + """msselect='{0}',""".format('')
                         + """solint='{0}',""".format(str(solint))
                         + """combine='{0}',""".format(str(combine))
                         + """refant='{0}',""".format(str(refant))
                         + """minsnr={0},""".format(str(minsnr))
                         + """zerorates={0},""".format(str(zerorates))
                         + """globalsolve={0},""".format(str(globalsolve))
#                         + """weightfactor={0},""".format(str(weightfactor))
                         + """delaywindow={0},""".format(str(list(delaywindow)))
                         + """ratewindow={0},""".format(str(list(ratewindow)))
                         + """niter={0},""".format(str(_inp_params.fringe_maxiter_lsquares))
                         + """append={0},""".format(str(append))
                         + """docallib={0},""".format('False')
                         + """callib='{0}',""".format('')
                         + """gaintable={0},""".format(str(gaintable_fp))
                         + """gainfield={0},""".format(str(gainfield))
                         + """interp={0},""".format(str(interp))
                         + """spwmap={0},""".format(str(spwmap))
                         + """corrdepflags={0},""".format('True')
                         + """paramactive={0},""".format(str(list(paramactive)))
                         + """concatspws={0},""".format('True')
                         + """corrcomb='{0}',""".format(str(corrcomb))
                         + """parang={0}""".format(str(_inp_params.parang))
                         + """)"""
                        )
        return MPICLIENT.push_command_request(fringefit_cmd), solint
    else:
        tasks.fringefit(vis                =  _inp_params.ms_name,
                        caltable           =  caltable,
                        field              =  field,
                        spw                =  _spwpartition,
                        selectdata         =  True,
                        timerange          =  "",
                        antenna            =  str(antenna),
                        scan               =  str(scan),
                        observation        =  "",
                        msselect           =  "",
                        solint             =  solint,
                        combine            =  combine,
                        refant             =  str(refant),
                        minsnr             =  minsnr,
                        zerorates          =  zerorates,
                        niter              =  _inp_params.fringe_maxiter_lsquares,
                        globalsolve        =  globalsolve,
#                        weightfactor       =  weightfactor,
                        delaywindow        =  delaywindow,
                        ratewindow         =  ratewindow,
                        append             =  append,
                        docallib           =  False,
                        callib             =  "",
                        gaintable          =  gaintable,
                        gainfield          =  [],
                        interp             =  interp,
                        spwmap             =  spwmap,
                        corrdepflags       =  True,
                        paramactive        =  paramactive,
                        concatspws         =  True,
                        corrcomb           =  corrcomb,
                        parang             =  _inp_params.parang
                       )
        return solint, solint
