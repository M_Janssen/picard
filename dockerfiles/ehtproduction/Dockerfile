FROM kernsuite/base:dev

# first: add user for consistent ID assignment
# then:  install vim and required packages
RUN  useradd -ms /bin/bash jeanluc \
  && apt-get -qq update \
  && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y ca-certificates \
     vim git curl less locales build-essential ppgplot openssh-client rsync \
     libfreetype6 libsm6 libxi6 libxrender1 libxrandr2 libxfixes3 \
     libxcursor1 libxinerama1 libfontconfig1 libxslt1.1 xauth xvfb dbus-x11 \
     libfontconfig-dev \
     python python-matplotlib python-tk python-casacore \
  && apt-get remove -y ca-certificates && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
#other libraries not required right now:
#libXrandr-dev* libXcursor-dev* libX11-dev* libXinerama-dev* libXext-dev* libXi-dev* libSM-dev* libXrender-dev* libfontconfig libXft* fontconfig-dev* libfontconfig-dev* \

# put picard and the CASA installation specified in its README file in /usr/local/src
# and add picard to the PATH
RUN  cd /usr/local/src \
  && printf "%b\n" " >> Cloning the CASA VLBI pipeline ..." \
  && git clone --single-branch --depth 1 https://bitbucket.org/M_Janssen/picard \
  && printf "%b\n" " >> Downloading the required CASA version ..." \
  && curl -L "$(cat picard/README.md | grep wget | cut -d' ' -f3)" -o CASA.picard.tar.xz \
  && tar xJf CASA.picard.tar.xz \
  && rm -rf CASA.picard.tar.xz \
  && python picard/setup.py -a -p /usr/local/src \
  && git clone --single-branch --depth 1 https://bitbucket.org/M_Janssen/casaeht \
  && chmod +x casaeht/bin/process_eht \
  && git clone --single-branch --depth 1 https://github.com/haavee/jiveplot \
  && chmod +x jiveplot/jplotter \
  && chown -R jeanluc /usr/local/src/* \
  && chmod 777 /usr/local/src \
  && chmod -R 777 /usr/local/src/casa*

# set locale for CASA perl,
# prevent a strange CASA bug,
# and set paths
RUN  sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
  && dpkg-reconfigure --frontend=noninteractive locales \
  && update-locale LANG=en_US.UTF-8 \
  && mkdir /home/jeanluc/.matplotlib \
  && touch /home/jeanluc/.matplotlib/tex.cache \
  && chown -R jeanluc /home/jeanluc/.matplotlib \
  && chmod 777 /home/jeanluc/.matplotlib
ENV LANG=en_US.UTF-8 \
    PATH=/usr/local/src/picard/picard:/usr/local/src/casaeht/bin:/usr/local/src/jiveplot:$PATH \
    PYTHONPATH=/usr/local/src/picard/picard:$PYTHONPATH

# go home
USER jeanluc
WORKDIR /home/jeanluc
CMD ["/bin/bash"]
