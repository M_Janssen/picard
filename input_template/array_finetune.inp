##### THIS FILE CONTAINS INPUTS THAT CAN BE FINE-  #####
##### TUNED WHEN A DIFFERENT ARRAY, DIFFERENT      #####
##### FREQUENCY, OR DIFFERENT CALIBRATION STRATEGY #####
##### IS USED -- THIS IS RARELY NECESSARY          #####


#Minimum fraction of valid (unflagged) data that must be present in a scan for an antenna,
#for that antenna to be chosen as refant for that particuar scan.
#See rpicard_documentation on 'Picking a Refant' for more info.
refant_minvaliddata = 0.5

#Interval on which to solve for gains to correct for the digital sampler bias.
#'int' means use the data (correlator) integration time.
accor_solint = int


#Can give the index of the spectral window that contains the line for spectral line
#observations.
#Only to be used when you have multiple spectral windows from a continuum and a line
#correlation pass loaded into a single MS.
#If <0, the code will pick the spw with the most channels as the line spw.
line_spw = -1


### Fringe fitting parameters. ###

#Experimental option to simply do a phase self-calibration to a point source on the
#data accumulation period. This uses the data from all spws and correlations combined
#and is similar to a `Difmap startmod'. The corresponding rPCIARD task is
#task_atmo_selfcal_startmod. The calibration is done only for the specified science targets.
#It should be used only when every antenna has high S/N data on at least one baseline.
#The following options are available:
# - False: To disable this step.
# - 'both': To run this after the segmented fringe-fitting from the fringefit_multi_sci_short step.
# - 'only': To run this instead of the segmented fringe-fitting. Not recommended if there are
#           delay variations within VLBI scans present in the data.
atmo_selfcal = False

#Can give a list of comma separated antenna names for which the atmo_selfcal solutions are removed.
atmo_selfcal_exclude_ants = None

#Solution intervals for the various fringe-fiting stages.
# - sb_instrumental for task_fringefit_single (instrumental phase and delay calibration).
# - mb_long for task_fringefit_multi_sci_long (long-integration multi-band fringe-fit).
# - mb_coher for task_fringefit_multi_cal_coher (initial intra-scan atmospheric phase and rate
#   calibration for high frequency observations).
# - mb_short for task_fringefit_multi_*_short (short-integration multi-band fringe-fit, to be done
#   after task_fringefit_multi_*_long to solve for residual intra-scan atmospheric effects).
#If a number [in seconds] is given, then that solint will be used for all scans.
#Or the solint can be set to 'inf' to integrate over scan durations.
#Or 'coherence' can be used to fringe-fit on solution intervals determined by
#task_fringefit_solint_* to solve for intra-scan atmopsheric effects on optimal solution intervals
#(see fringe_solint_optimize_search parameter in array.inp).
#See rpicard_documentation on 'Fringe-fitting' for more info.
fringe_solint_sb_instrumental = inf
fringe_solint_mb_long = inf
fringe_solint_mb_coher = coherence
fringe_solint_mb_short = coherence

#SNR cutoffs for the various fringe-fiting stages.
# - sb_instrumental for task_fringefit_single.
# - mb_coher for task_fringefit_multi_cal_coher.
# - mb_short_cal for task_fringefit_multi_cal_short.
# - mb_long_sci for task_fringefit_multi_sci_long.
# - mb_short_sci for task_fringefit_multi_sci_short.
# - mb_shortFFT for task_fringefit_solint_* (min FFT SNR required for solution interval
#   optimization).
# - mb_reiterate for a re-iterations for detections, i.e. for solutions on
#   fringe_solint_mb_reiterate integration times.
#See rpicard_documentation on 'Fringe-fitting' for more info.
fringe_minSNR_sb_instrumental = 3.7
fringe_minSNR_mb_coher = 3.6
fringe_minSNR_mb_short_cal = 3.5
fringe_minSNR_mb_long_sci = 3.3
fringe_minSNR_mb_short_sci = 3.1
fringe_minSNR_mb_shortFFT = 3.2
fringe_minSNR_mb_reiterate = 3.5

#FFT SNR vs solint based estimation of optimal fringefit() solution interval.
#If 'sqrt', the optimal solint is taken as the last solint before the SNR does no longer increase
#with sqrt(solint).
#Else, a number can be given and the optimal solint is taken as the last solint before the SNR
#drops below that number multiplied by fringe_minSNR_mb_shortFFT.
fft_solint_estimation = 1.01

#Switch for averaging RCP and LCP rates for the multiband fringefit.
#Set to True to average the rates (recommended based on physical grounds).
#Set to False for no averaging.
average_mb_rates = True

#Switch for smoothing the multiband delay values in time for the science targets.
#No smoothing is done for the high SNR calibrator sources.
#Will use a sliding median window filter per scan and antenna.
#Set to None for no smoothing.
#Set to a number to smooth over that interval [in s].
#Set to inf to replace all delays by their median (per scan, per antenna).
fringe_mbdelay_smoothtime = 60.

#Specific scan numbers that are to be used for the instrumental phase and delay calibration.
#Can be given as a single number or multiple scans as comma separated numbers (e.g., 3,9,18).
#If None, then all scans on calibrators_instrphase defined in observation.inp will be used.
#If you add a ! (e.g., !4,5,6), all scans on calibrators_instrphase expect the ones given in the
#list with the exclamation mark will be used.
fringe_instrumental_scans = None

#Method of IF alignment (instrumental phase and delay).
#If set to 'constant', then only a single solution (with highest SNR) will be used for each station
#and spw out of all solutions obtained from fringe_instrumental_scans.
#If set to 'interpolate', then all solutions from fringe_instrumental_scans which have an SNR
#higher than fringe_minSNR_instrumental (set array.inp) will be used and interpolated across scans.
fringe_instrumental_method = constant

#Maximum allowed depth of exhaustive refant search (number of connections for sub-clusters,
#see documentation).
#Set to 0 to disable the exhaustive refant search.
#Set to -1 for an unrestricted depth (equivalent to the number of refants specified).
fringe_exhaustive_refant_search_depth = -1

#Multiplicative factor of fringe_minSNR thresholds, which determines "weak" detections that are
#replaced by exhaustive fringes when those have a at least 10% higher S/N.
fringe_exhaustive_refant_search_threshold = 1.5

#Enable calibration of antennas even if their fringe solutions after successful instrumental
#calibration cannot be re-referenced to the main refant in a scan when doing the exhaustive
#fringe search.
#This allows isolated islands or sub-arrays of antennas to be present in the data.
#Will slow down processing.
#Useful for weak sources and global VLBI experiments.
fringe_exhaustive_refant_search_islands = False

#Set to True to re-reference all fringe solutions to a common reference station at the end.
#Should ensure R-L phase stability across scans. Does a simple re-referencing by default.
#Set to 'casatask' to use the CASA rerefant() task for this, which is not recommended as this
#task wil likely flag data that cannot be re-referenced to neighboring scans.
rerefant = True

#Set to True to allow data from stations for which no solutions during the instrumental phase and
#delay calibration step are found to be passed along unaffected (no solutions applied and no data
#flagged). This feature is useful for low SNR data from a digital backend system which is split
#into many spectral windows. As such, the signal within one spectral window may not be strong
#enough to solve for instrumental phases and delays and it is okay to have these left uncorrected,
#since they should be very small (or even non-existent) on digital backends.
sbd_no_stationflag = False

#Can provide a list of comma separated parameters here that are to be solved for in the
#fringefit_multi_sci_long step in addition to delays and rates. The allowed parameters are:
# - 'disp' to solve for dispersive delays at very low frequencies.
mb_higher_order_fringe_search = None

### End of fringe parameters.  ###


### Bandpass parameters.       ###

#How to solve for the scalar bandpass. If set to 'const', take the median of the autocorrelation
#data across all scans to obtain a constant bandpass solution for every station. If 'perscan',
#get a solution for every scan. The 'perscan' option will leave the science target uncalibrated
#for spectral line experiments. If 'skip', skip the scalar bandpass task completely.
solvemode_scalar_bandpass = 'const'

#Minimum number of baselines per antenna for the solution of a complex bandpass.
#0 (= 4) is the default.
minblperant_cmplx_bandpass = 1

#Solve only for phases with the complex bandpass.
phaseonly_cmplx_bandpass = False

#Normalize solutions of complex bandpass.
solnorm_cmplx_bandpass = True

#Type of solution for complex bandpass ('BPOLY' or 'B').
#BPOLY works better if the bandpass calibrator does not have a very high SNR.
#For BPOLY, only a single bandpass calibrator source can be specified (solutions cannot be
#averaged).
#For 'B', the amplitude corrections are scaled to unity if the calib_scalar_bandpass parameter
#is defined below.
bandtype_cmplx_bandpass = 'B'

#Minimum SNR for solutions of complex bandpass - does not apply for bandtype = 'BPOLY'.
minsnr_cmplx_bandpass = 3.0

#Number of channels over which to interpolate, when no solutons were found.
#Only applies to bandtype = 'B'.
fillgaps_cmplx_bandpass = 5

#For bandtype 'BPOLY', the degree of the polynomial for the phase (per spw).
degphase_cmplx_bandpass = 5

#For bandtype 'BPOLY', the degree of the polynomial for the amplitude (per spw).
degamp_cmplx_bandpass = 5

#For bandtype 'BPOLY', if the data is to be normalized prior to the solution.
visnorm_cmplx_bandpass = True

### End of bandpass parameters. ###


### Polarization calibration parameters        ###

#Which stations or baselines to use when computing single global phase and delay offsets.
rldly_stations = ''

#Which steps to perform for the RL delay and phase calibration. If a non-empty string is given that
#contains 'phase', RLphase is performed and if it contains 'delay', RLdelay is performed.
rldly_calibsteps = 'phase,delay'

#Minimum SNR for RL delay and RL phase solutions.
rldly_minsnr = 3.0

#Minimum SNR for D-terms solutions.
dterms_minsnr = 3.0

#Which data axis (scan, spw, ...) to combine when solving for RL-delays.
#If 'spw!' is specified instead of 'spw', the code will solve for RL-delays for each spectral
#window separately but apply the average value to all spws. This can be useful for irregular or
#disjunct spws.
rldelay_axis_combine = 'scan'

#How to calibrate instrumental RL phase offsets.
#Can be per 'spw' or per 'channel' if a calibrator source with high enough SNR is available, or a
#number of channels (x) can be given (e.g., rldly_instrphase_solve=10). In this last mode, groups
#of x(=10) channels are combined in each spw to obtain solutions. This is useful if
#frequency-dependent instrumental polarization structure is present in the data, while the SNR is
#not good enough to obtain per-channel solutions.
#(Note that RL-delay solutions are always per spw).
rldly_instrphase_solve = spw

#How to calibrate the leakage (D-terms).
#The same three options as for the rldly_instrphase_solve are available.
#Additionally, a 'combined' solution for all spw can be obtained.
#The SNR is probably almost never good enough to obtain per 'channel' D-terms.
dterms_solve = spw

### End of polarization calibration parameters ###


### Opacity correction parameters                              ###
### Only used for some arrays.                                 ###
### The path for diagnostic plots is defined in constants.inp. ###

#Which atmospheric model to use for the computation of the average sky temperature:
#'atm' for the accurate Pardo et al. atm model which is built into CASA.
#'simp' for T_atm = 1.12 * T_amb - 50K (Altshuler et al. 1968).
atmo_model = atm

#List of stations (by their stationcode) separated by ; for which no opacity correction is to be
#done.
#Set to None to do the opacity correction for all stations.
#Set to all to skip the opacity correction in case that no wx data is available.
no_opac_corr_stations = PV; PB; ON; YS; LM; GB; NN; AA; GL

#List of stations (by their stationcode) separated by ; which can switch polarizations during
#observations (e.g., observe some scans in RCP and others in LCP).
#Relevant for calibration.py->flag_below_SNR(). For the stations set here, solutions with snr==0
#will not be discarded if it only happens for one pol and that pol does have a snr>0 solution
#somewhere.
switching_pol_stations = JC

#Averaging time (in seconds) for the binning of Tsys measurements for the T_rx fits.
opac_corr_avgtime = 10

#Minimum number of Tsys points in an airmass bin for the T_rx fits.
opac_corr_airm_nbin = 2

#Maximum range of airmass values allowed to be in the same bin for the T_rx fits.
opac_corr_d_airm = 0.3

#Do not use any Tsys measurements for the Trx fit where the airmass exceeds this value.
#Unless, there are not enough measurements in total.
opac_corr_airm_max = 3.5

### End of opacity correction parameters                       ###


#Option to download maps of the total electron content in the ionosphere and to use those
#to correct for a priori ionospheric delays. Relevant for low frequency observations and
#astrometry.
#For observing frequencies below a specified number [in GHz] the correction will be applied.
#Set to False to disable this correction entirely.
perform_tec_corr = 6

#Bad Tsys values that are to be removed from the MS.
invalid_tsys_identifiers = 0;999

#Parallactic angle correction.
#Set to True to do this correction (default).
#It should only be necessary to set this to False if you are dealing with synthetic data,
#where a parallactic angle 'corruption' has not been taken into account by the simulation.
parang = True

#Switch for EHT data when allowing low SNR data to pass through after first fringe-fit
#(to allow for fringes to be found on longer solution intervals via fringe_solint_reiterate).
#Set to True to prohibit this behavior when ALMA and APEX are present in a scan
#(if no strong fringes are found on that baseline, the data really should be flagged).
eht_alma_apex_highsnr = False

#Switch for EHT data to do the coherence calibration for all sources for the calibration of the
#ALMA phase offsets for every scan and to delete the coherence calibration table after the
#sbd calibration.
eht_temporary_coher_cal_for_all = False

#Threshold for writing blank, unflagged calibration solutions when too many solutions failed.
#Set to None to disable this feature.
#Example: If for a specific spw and antenna, the fraction of flagged solutions exceeds
#flagged_calib_thresh after taking into account a-priori flags, the solutions are unflagged
#and replaced by (1,0). I.e., the unsuccessful calibration is skipped instead of flagging data.
flagged_calib_thresh = 0.1

#List of stations (a,b,c,...) for which the fringefit_multi_sci_long_spwant task is to be executed.
#This task is to be used when the fringe-fitting over the entire bandwidth can only be done for
#some stations (same delay and rate for each spw after instrumental effects have been calibrated).
#The other stations, for which there are scan-to-scan differences in delays and rates can be listed
#here. The corresponding spwpartition has to be set in a custom
#task_fringefit_multi_sci_long_spwant entry below.
#The only known case where this issue occurs is the phased SMA in the EHT.
spwpartition_ants = ''

#If set, perform the fringefit_multi_sci_long_spwant with corrcomb='None' instead of ='all'.
#Used in the case that the stations which are unstable over spws are also unstable over pols.
spwpartition_per_pol = False

#List of stations (a,b,c,...) for which the coarse_phbpass_ffospws calibration is to be performed.
coarse_phbpass_stations = None

#Export UVFITS files per set of spws. If set to '', single UVFITS files are produced for all spws
#in the data. If set to 'a~b,c~d,e~f,...', separate UVFITS files are written for spws a to b,
#c to d, e to f, ...
spwpartition_uvf = ''

#'a~b' selection for a range of spws to do the exhaustive fringe search. Set to '', to use all
#available spws. Should be specified only when doing spwpartitioning when sets of spws are disjoint
#and naively integrating over all of them can cause a loss of signal.
spwpartition_exhaustive_refant_search_selection = ''

#chi2 cutoff for the EHT manual_calibration.task_NNphasecal code. Values that work well:
# 30 at 230 GHz, 300 at 345 GHz.
maxchi2_NNphasecal = 30.

#Produce diagnostic plots for the EHT manual_calibration.task_NNphasecal code.
#For now set up only for single-band runs.
NNphasecal_keep_diagnostics = False


### Block of calibration parameters for each calibration step.                                                         ###
### The naming convention as assumed by the generic_calibration.py->go_calibrate() function is:                        ###
###   <calib_taskname> belongs to a function called <task_taskname()>.                                                 ###
### For each step a list of parameters is provided (values separated by semicolons).                                   ###
### 0.  Names of calibration tables. If <just_a_dummy>, no warning will be printed if it does not exist.               ###
### 1.  Interpolation methods in time[,freq] when applying the solutions.                                              ###
### 2.  Whether or not to calibrate data weights according to the calibration solutions.                               ###
### 3.  If <True>, this table is set to contain solutions from multiple sources which are to be applied only to        ###
###     themselves (using the CASA gainfield parameter).                                                               ###
###     Else, set to ''. Note that this parameter is overwritten if phase-referencing is enabled (see below).          ###
###     You will have to make sure that only the right sources are selected in the calibration function.               ###
### 4.  Only used for phase-referencing experiments when applying the solutions:                                       ###
###     If <True>, the solutions from phase-referencing sources in these tables are applied to the corresponding       ###
###     science targets.                                                                                               ###
### 5.  Name of directory for diagnostic output from the calibration step                                              ###
###     (like plots of solution tables). If <None> then no diagnostics will be provided.                               ###
### 6.  For plots of solution tables: xaxis [time or freq] for the plotcal task.                                       ###
###     None means use the default of plotcal and for a csv list plotcal will loop over the values.                    ###
### 7.  For plots of solution tables: yaxis for the CASA plotcal task.                                                 ###
###     None means use the default and for a csv list plotcal will loop over the values.                               ###
### 8.  For plots of solution tables: poln [R and/or L] for the plotcal task.                                          ###
###     NOTE: This parameter will be ignored as all available polarization products will be plotted for rPICARDv4+     ###
### 9.  If <True>, the solutions will be plotted per source. IF <False>, all sources are plotted together.             ###
###     NOTE: This parameter will be ignored as all sources are always plotted together for rPICARDv4+                 ###
### 10. Method to smooth the solutions ('median' or 'mean' filter).                                                    ###
###     If <None> then the solution table will not be smoothed.                                                        ###
### 11. Smoothing time in seconds for filter specified above.                                                          ###
###     Note: Flagged solutions in the input table will not participate in the smoothing calculation, but will be      ###
###           replaced with smoothed values if the smoothing window covers one or more unflagged solutions when        ###
###           centered on the flagged point.                                                                           ###
###     And: The smoothing is always done independently for each field, but scan boundaries are not observed.          ###
###          Thus, if the smoothtime is large enough, smoothing may occur over many boundaries.                        ###
### 12. If <True>, overwrite all values of the calibration table by the median (per scan, antenna, spw).               ###
###     Used to enforce proper smoothing over a whole scan after smoothing with a sliding filter defined above fist.   ###
###     Else, set to ''.                                                                                               ###
### 13. Option to use this table only to redo the calibration solutions for certain antennas of a different caltable.  ###
###     Syntax: '' to disable this feature for normal calibration tables.                                              ###
###             '<calib_step>:<list of antennas>' to redo the calibration of calib_step for the list of antennas.      ###
###               Example: Using the row for calib_fringefit_single2_cal and setting this column to                    ###
###                        'calib_fringefit_single:BR,LA' to replace all solutions for the BR and LA antennas in the   ###
###                        calibration table of calib_fringefit_single with solutions obtained from the                ###
###                        calib_fringefit_single2_cal step. Note that no solution table for                           ###
###                        calib_fringefit_single2_cal will be kept in the end.                                        ###
### 14. Option to perform calibration separately for different spws. Only relevant for calibration steps where         ###
###     multiple spws are to be combined (e.g., multi-band fringe-fitting). For calib_fringefit_multi_cal_*, the       ###
###     calib_fringefit_solint_cal parameter is taken and for calib_fringefit_multi_sci_short,                         ###
###     calib_fringefit_solint_sci is taken.                                                                           ###
###     This is referred to as spwpartitioning in the rPICARD source code.                                             ###
###     Ignored for fringe-fit tasks on spectral line sources.                                                         ###
###     Syntax: '' to disable this feature.                                                                            ###
###             'a~b,c~d,e~f,...' to split calibration for spws a to b, c to d, e to f, ... with a,b,c,d,e,f, ...      ###
###             integers.                                                                                              ###
###             Example: '0~4,5~9,10~16' to perform separate calibration for spws 0 to 4, 5 to 9, and 10 to 16.        ###
###                                                                                                                    ###
###     For each calibration step parameters will be read in as                                                        ###
###     parameter0 ; parameter1 ; parameter2, ... -> [parameter0, parameter1, parameter2, ...].                        ###

calib_accor                     = calibration_tables/accor.t        ; linearperscan  ; True  ; ''   ; ''   ; SAMPLER_CORR ; time      ; amp                  ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_acscl                     = calibration_tables/acscl.t        ; linearperscan  ; True  ; ''   ; ''   ; AC_ADJUST    ; time      ; amp                  ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_tsys                      = calibration_tables/tsys.t         ; linear         ; True  ; True ; ''   ; TSYS         ; time      ; tsys                  ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_tsys_add_exptau           = calibration_tables/tsys_tauc.t    ; linear         ; True  ; True ; ''   ; TSYS         ; time      ; tsys                  ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_gaincurve                 = calibration_tables/gc.t           ; nearest        ; True  ; ''   ; ''   ; GAIN         ; elev      ; gc                  ; None ; False ; None   ; 0 ; ''   ; '' ; ''
calib_ionospheric_tec_map       = calibration_tables/tec_map.t      ; linear         ; True  ; ''   ; ''   ; TEC          ; time      ; tec                  ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_scalar_bandpass           = calibration_tables/bpass_scalar.t ; nearest,linear ; True  ; ''   ; ''   ; SCALAR_BPASS ; freq      ; amp                  ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_complex_bandpass          = calibration_tables/bpass_complx.t ; nearest,linear ; True  ; ''   ; ''   ; COMPLX_BPASS ; freq      ; amp,phase            ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_solint_cal      = tmp_fringe_testing/just_a_dummy   ; linearperscan  ; True  ; ''   ; ''   ; None         ; time      ; phase,delay,rate,snr ; None ; False ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_solint_sci      = tmp_fringe_testing/just_a_dummy   ; linearperscan  ; True  ; ''   ; ''   ; None         ; time      ; phase,delay,rate,snr ; None ; False ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_single          = calibration_tables/ff_sb.t        ; nearest        ; True  ; ''   ; ''   ; FF_SB        ; freq      ; phase,delay,snr      ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_multi_cal_coher = calibration_tables/ff_mb_cal_c.t  ; linearperscan  ; True  ; True ; True ; FF_MB_CAL_C  ; time      ; phase,delay,rate,snr ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_multi_cal_short = calibration_tables/ff_mb_cal_s.t  ; linearperscan  ; True  ; True ; True ; FF_MB_CAL_S  ; time      ; phase,delay,rate,snr ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_multi_sci_long  = calibration_tables/ff_mb_sci_l.t  ; linearperscan  ; True  ; True ; ''   ; FF_MB_SCI_L  ; time      ; phase,delay,rate,snr ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_fringefit_multi_sci_short = calibration_tables/ff_mb_sci_s.t  ; linearperscan  ; True  ; True ; ''   ; FF_MB_SCI_S  ; time      ; phase,delay,rate,snr ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_atmo_selfcal_startmod     = calibration_tables/atmo_sc.t      ; linearperscan  ; True  ; True ; ''   ; ATMO_SC      ; time      ; phase                ; R,L  ; True  ; None   ; 0 ; ''   ; '' ; ''
calib_rldelay                   = calibration_tables/rldelay.t      ; nearest        ; True  ; ''   ; ''   ; RL_DELAY     ; freq      ; delay                ; RL   ; False ; None   ; 0 ; ''   ; '' ; ''
calib_rlphase                   = calibration_tables/rlphase.t      ; nearest        ; True  ; ''   ; ''   ; RL_PHASE     ; freq      ; phase                ; RL   ; False ; None   ; 0 ; ''   ; '' ; ''
calib_dterms                    = calibration_tables/dterms.t       ; nearest        ; True  ; ''   ; ''   ; LEAKAGE      ; freq      ; amp,phase            ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_phase_offsets_ALMA        = calibration_tables/AA_phoff.t_AP  ; linearperscan  ; True  ; ''   ; ''   ; AA_PHASE     ; freq,time ; phase,delay          ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_alignbands_ffospws        = calibration_tables/ff_spw_align.t ; nearest        ; True  ; ''   ; ''   ; FF_SPW_ALIGN ; freq      ; phase,delay,snr      ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
calib_coarse_phbpass_ffospws    = calibration_tables/ff_sb_coarse.t ; nearest        ; True  ; ''   ; ''   ; FF_SB_COARSE ; freq      ; phase,delay,snr      ; R,L  ; False ; None   ; 0 ; ''   ; '' ; ''
#calib_intermediate_applycal = calibration_tables/just_a_dummy ; linear ; True ; '' ; '' ; None ; freq ; amp ; None ; False ; None; 0 ; ''   ; ''

#Indexing scheme for the numbering above.
C_NAM = 0
C_INT = 1
C_CWT = 2
C_GFD = 3
C_FLD = 4
C_DIG = 5
C_XAX = 6
C_YAX = 7
C_PLN = 8
C_SRC = 9
C_SMO = 10
C_STI = 11
C_MED = 12
C_ANT = 13
C_SPW = 14

#length of the array of calibration parameters for each step (highest index number+1).
C_LEN = 15

### End of calibration step parameters                                                                                        ###
