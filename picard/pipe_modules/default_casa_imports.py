#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Standardized set of imports from CASA. """
try:
    try:
        import tasks
        from casac import casac
        from recipes import tec_maps
        from mpi4casa.MPIEnvironment import MPIEnvironment
        from mpi4casa.MPICommandClient import MPICommandClient
    except ModuleNotFoundError:
        #CASA 6+
        import casaplotms
        import casatasks as tasks
        import casatools as casac
        from casatasks.private import tec_maps
        from casampi import MPIEnvironment
        from casampi.MPICommandClient import MPICommandClient
except (ImportError, NameError) as _:
    #Python without CASA
    pass
