#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Statistical methods to detect and remove outliers from data sets. """
import numpy as np


def remove_outliers_from_median(data, N_sig=3):
    np_data = np.asarray(data)
    if len(np_data) < 3:
        return np_data
    median      = np.median(np_data)
    std         = np.std(np_data)
    out_data    = []
    for dat in data:
        if not dat:
            continue
        if np.isnan(dat):
            continue
        difftomed = abs(dat-median)
        if difftomed < 1.e-6 or difftomed < N_sig*std:
            out_data.append(dat)
    if not out_data:
        out_data.append(0)
    return np.asarray(out_data)
