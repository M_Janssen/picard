#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Methods for an opacity correction of mm VLBI data. Following method from Marti-Vidal et al. 2012 """
from collections import defaultdict
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import pipe_modules.auxiliary as auxiliary
from pipe_modules.default_casa_imports import *


bad_weather_counter = 0


def opacity_correction(_inp_params, _ms_metadata, tsys_intable, tsys_outtable_tauc):
    """
    Takes a system temperature calibration table tsys_intable generated for the MS msname
    and generates a new table tsys_outtable_tauc
    which is a copy of tsys_caltable except that every Tsys entry is replaced with
    Tsys -> Tsys* = Tsys * exp(tau).
    Bins Tsys values vs airmass to fit a lower envelope to determine T_rx:
      - _inp_params.opac_corr_avgtime [in s] is used to bin Tsys measurements in time first.
      - _inp_params.opac_corr_airm_nbin is the minimum value of measurements taken at different times that have to be in a bin.
      - _inp_params.opac_corr_d_airm is the maximum difference between airmass values which are allowed to be in the same bin.

    - _inp_params.no_opac_corr_stations defines a list of stations for which no opacity correction is to be performed.
    """
    if _inp_params.verbose:
        print('  Generating a tsys table with an exp(tau) correction.')
    SEC_TO_DAYS = 86400.

    mytb = casac.table()
    myms = casac.ms()
    mymsmd = casac.msmetadata()

    #add slash to msname if not already present
    mspath = os.path.join(_inp_params.ms_name, '')

    #create a dict that contains for every antennaID a function to give the ambient temperature vs time
    station_tamb     = {}
    station_pressure = {}
    present_stations = auxiliary.read_CASA_table(_inp_params, _tablename=mspath+'WEATHER', _query_select = 'unique ANTENNA_ID')
    for station in present_stations:
        _qselc = 'TIME,TEMPERATURE,PRESSURE'
        time,temp,pres = auxiliary.read_CASA_table(_inp_params, _tablename=mspath+'WEATHER', _query_select = _qselc,
                                                   _query_criteria = 'ANTENNA_ID=='+str(station)
                                                  )
        station_tamb[station]     = grid_weather(time, temp)
        station_pressure[station] = grid_weather(time, pres)

    #only use Tsys entries from the ANTAB(SYSTEM_TEMPERATURE) table that were taken during scans
    #get dict: for each scan [begin_time, end_time, [antennas present]] (can take some time...)
    scan_info = {}
    myms.open(mspath)
    scansummary = myms.getscansummary()
    myms.close()
    mymsmd.open(mspath)
    for scan in scansummary:
        t_start   = float(scansummary[scan]['0']['BeginTime']) * SEC_TO_DAYS
        t_end     = float(scansummary[scan]['0']['EndTime'])   * SEC_TO_DAYS
        theseants = mymsmd.antennasforscan(int(scan))
        scan_info[str(scan)] = [t_start, t_end, theseants]
    mymsmd.close()

    #ants will be an array of the [x,y,z] positions in the order of antenna IDs
    mytb.open(mspath + 'ANTENNA')
    ants = np.transpose(mytb.getcol('POSITION'))
    mytb.close()

    #station_alt will be an array of altitudes in the order of antenna IDs
    station_alt = []
    for ant_xyz in ants:
        station_alt.append(auxiliary.xyz_to_height(ant_xyz[0], ant_xyz[1], ant_xyz[2]))

    #fields will be an array of [ra,dec] in the order of observed sources
    mytb.open(mspath + 'FIELD')
    fields = np.transpose(mytb.getcol('REFERENCE_DIR'))
    fields = fields.reshape(len(fields),2)
    mytb.close()

    mytb.open(tsys_intable)
    cptb = mytb.copy(tsys_outtable_tauc, deep=True, valuecopy=True, returnobject=True)
    mytb.flush()
    mytb.done()
    cptb.flush()
    cptb.done()

    add_exptau(_inp_params,_ms_metadata, tsys_outtable_tauc,station_alt,station_tamb,station_pressure,scan_info,ants,fields)


def read_trec_file(_inp_params, ms_metadata, _stations):
    """
    Reads RCP and LCP T_rx values for _stations=[list of station IDs] from all available files.
    Returns dicts with [station_ID] = T_rx for RCP and LCP.
    Fills non-present T_rx values with nan.
    """
    rcp_trec  = {}
    lcp_trec  = {}
    trx_files = auxiliary.get_trecfiles(_inp_params)
    for trx_file in trx_files:
        with open(trx_file, 'r') as tf:
            for line in auxiliary.proper_line(tf):
                for station in _stations:
                    st_name = ms_metadata.yield_antname(station)
                    if st_name in line:
                        if 'RCP' in line or 'rcp' in line:
                            rcp_trec[station] = float(line.split()[2])
                        elif 'LCP' in line or 'lcp' in line:
                            lcp_trec[station] = float(line.split()[2])
    return rcp_trec, lcp_trec


def compute_elev(ra_source, dec_source, xyz_antenna, time):
    """
    - ra_source is given is rad (as in the first entry of the REFERENCE_DIR column in the FIELD subtable of a CASA MS).
    - dec_source is given is rad (as in the second entry of the REFERENCE_DIR column in the FIELD subtable of a CASA MS).
    - xyz_antenna are [x,y,z] positions (as in the POSITION column in the ANTENNA subtable of a CASA MS).
    - time is the time in seconds since 1858/11/17/00:00:00 (MJD) as it is commonly stored in CASA calibration tables.
    Returns elevation of telescope if it looks at the source for the time specified.
    """
    RAD_TO_DEG       = 180./np.pi
    DEG_TO_HOUR      = 1./15.
    SEC_TO_DAYS      = 86400.
    MJD_TO_JD        = 2400000.5
    JD_TO_J2000      = -2451545.0
    DAY_TO_CENTURIES = 1./36525.0

    #latitude in rad and logitude in hours
    lat = np.arcsin( xyz_antenna[2] / np.sqrt(xyz_antenna[0]**2. + xyz_antenna[1]**2. + xyz_antenna[2]**2.) )
    lon = np.arctan2(xyz_antenna[1],xyz_antenna[0]) * RAD_TO_DEG * DEG_TO_HOUR

    #get [hh,mm,ss] of the day
    t = auxiliary.time_convert(time).split('/')[3].split(':')

    #julian day
    jd  = float(time) / SEC_TO_DAYS + MJD_TO_JD
    #UT hour of day
    uth = float(t[0]) + float(t[1])/60. + float(t[2])/3600.

    #GST in hours
    cent = (jd + JD_TO_J2000) * DAY_TO_CENTURIES
    gst  = (6.697374558 + 2400.051336*cent + 0.000025862*cent*cent + 1.0027379093*uth)%24

    #LST in hours
    lst = (gst+lon)%24

    #hour angle in rad
    ha = ( (lst - ra_source*RAD_TO_DEG*DEG_TO_HOUR) / DEG_TO_HOUR )%360 / RAD_TO_DEG

    #return elevation in rad
    sinelev = np.sin(lat)*np.sin(dec_source) + np.cos(lat)*np.cos(dec_source)*np.cos(ha)
    return np.arcsin(sinelev)


def compute_tau(rcp_tsys, lcp_tsys, rcp_trx, lcp_trx, t_atm, ant, time, previous_tau):
    """
    Computes tau from the average of rcp and lcp tsys.
    Simply solves the approximate Tsys equation for tau.
    Resistance is futile.
    """
    _verbose = False
    lproblem = False
    rproblem = False
    if rcp_tsys:
        if rcp_tsys > rcp_trx:
            arg1 = (rcp_tsys - rcp_trx)/t_atm
            if arg1 >= 1.0:
                arg1     = 0.63212
                rproblem = True
                if _verbose:
                    print('    Warning: No solution for tau for\n      ' \
                          + 'station ' + str(ant) + ' at time t=' + str(auxiliary.time_convert(time)) + ' for RCP.\n      ' \
                          + 'Reason: T_rx and/or T_atm are too small.\n      ' \
                          + 'Assuming tau=1.0 Check if here were problems with the weather data or the T_rx fit.'
                         )
            tau1 = max( -np.log( 1 - arg1 ), 0.0 )
        else:
            tau1     = 0.
            rproblem = True
            if _verbose:
                print('    Warning: Skipping computation of tau for\n      ' \
                      + 'station ' + str(ant) + ' at time t=' + str(auxiliary.time_convert(time)) + ' for RCP.\n      ' \
                      + 'Reason: Potentially faulty Tsys measurement:\n        ' \
                      + 'T_sys=' + str(rcp_tsys) + ' < T_rx=' + str(rcp_trx)
                     )
    else:
        tau1 = 0.
    if lcp_tsys:
        if lcp_tsys > lcp_trx:
            arg2 = (lcp_tsys - lcp_trx)/t_atm
            if arg2 >= 1.0:
                arg2     = 0.63212
                lproblem = True
                if _verbose:
                    print('    Warning: No solution for tau for\n      ' \
                          + 'station ' + str(ant) + ' at time t=' + str(auxiliary.time_convert(time)) + ' for LCP.\n      ' \
                          + 'Reason: T_rx and/or T_atm are too small.\n      ' \
                          + 'Assuming tau=1.0 Check if here were problems with the weather data or the T_rx fit.'
                         )
            tau2 = max( -np.log( 1 - arg2 ), 0.0 )
        else:
            tau2     = 0.
            lproblem = True
            if _verbose:
                print('    Warning: Skipping computation of tau for\n      ' \
                      + 'station ' + str(ant) + ' at time t=' + str(auxiliary.time_convert(time)) + ' for LCP.\n      ' \
                      + 'Reason: Potentially faulty Tsys measurement:\n        ' \
                      + 'T_sys=' + str(lcp_tsys) + ' < T_rx=' + str(lcp_trx)
                     )
    else:
        tau2 = 0.
    avg_tau = 0.5 * (tau1 + tau2)
    if avg_tau > 1.0:
        avg_tau  = previous_tau
        rproblem = True
        lproblem = True
    return avg_tau, rproblem, lproblem


def atm_temp(ground_temp, inunit='K', model='simp', altitude_m=5000, pressure_mBar=560):
    """
    Simple model to compute the average atmospheric temperature from the ground temperature.
    """
    global bad_weather_counter
    if inunit=='K':
        thistemp = ground_temp
    elif inunit=='C':
        thistemp = ground_temp + 273.15
    else:
        raise ValueError('inunit must be K or C')
    if model=='simp':
        atemp = 1.12*thistemp - 50.
    elif model=='atm':
        myqa   = casac.quanta()
        myatm  = casac.atmosphere()
        altiM  = myqa.quantity(float(altitude_m), 'm')
        presmB = myqa.quantity(float(pressure_mBar), 'mbar')
        tempK  = myqa.quantity(float(thistemp), 'K')
        myqa.done()
        if any(np.asarray([altitude_m, thistemp, pressure_mBar])<0):
            if thistemp < 2.73:
                thistemp = 273.15
            else:
                pass
            atemp = 1.12*thistemp - 50.
            bad_weather_counter += 1
        else:
            thisatm = myatm.initAtmProfile(altiM, tempK, presmB)
            atmprof = myatm.getProfile()
            avgskyT = 0.
            sum_wt  = 0.
            for layer in range(myatm.getNumLayers()):
                thiswt  = atmprof[1]['value'][layer] * atmprof[3]['value'][layer]
                avgskyT += atmprof[2]['value'][layer] * thiswt
                sum_wt  += thiswt
            myatm.done()
            atemp = avgskyT / sum_wt
    else:
        raise ValueError('model must be simp or atm')
    return max(atemp, 2.73)


def grid_weather(time, w_data):
    """
    Takes array of weather data (pressure or temperature)
      with corresponding timestamps given by the time array
    and returns a function that can yield interpolated values for a finer time grid
      and extrapolated values which will correspond to the nearest neighbors.
    Used because Tsys values are on a different time grid than weather data measurements.
    """
    return interp1d(time, w_data, kind='linear', bounds_error=False, fill_value=(w_data[0],w_data[-1]))


def add_exptau(_inp_params, _ms_metadata, tsys_caltable, alt, tamb, pres, scan_params, ant_positions, field_directions):
    """
    Takes tsys_caltable and replaces every Tsys entry with Tsys* = Tsys * exp(tau).
    ant_positions is a 2D array of xyz positions of all antennas on Earth
      (ordered according to the antenna ID numbering in the tsys_caltable).
    field_directions is a 2D array of ra and dec positions of all observed sources
      (ordered according to the field ID numbering in the tsys_caltable).
    Uses method from Marti-Vidal et al. 2012 to compute T_rx and tau from tsys itself and weather data.
    Will use T_rx values from a trec file if available.
    Computes tau for every spw separately as the opacity is in principle frequency-dependent.
    Important parameters are explained in the opacity_correction() function.
    """
    global bad_weather_counter
    #exclude invalid Tsys measurements
    MIN_TSYS = 2.73

    all_stations = auxiliary.read_CASA_table(_inp_params, _tablename=tsys_caltable, _query_select = 'unique ANTENNA1')
    if _inp_params.no_opac_corr_stations:
        no_opac_corr_stations_present = []
        no_opac_corr_stations         = _inp_params.no_opac_corr_stations
        if not isinstance(no_opac_corr_stations, list):
            no_opac_corr_stations = [no_opac_corr_stations]
        for nocs in no_opac_corr_stations:
            try:
                nocs_present = _ms_metadata.yield_antID(nocs)
                no_opac_corr_stations_present.append(nocs_present)
            except ValueError:
                pass
    else:
        no_opac_corr_stations_present = []
    stations = list(set(all_stations) - set(no_opac_corr_stations_present))
    #per station: average Tsys in timebins (and thereby average over spw) and use the min(average values) for the T_rx fit
    trx_rcp_file, trx_lcp_file = read_trec_file(_inp_params, _ms_metadata, stations)
    trx_rcp                    = {}
    trx_lcp                    = {}
    lost_stations              = []
    for i_plot, station in enumerate(stations):
        xyz_st                 = ant_positions[int(station)]
        time,scan,ant,tsys,src = auxiliary.read_CASA_table(_inp_params, _tablename=tsys_caltable,
                                                           _query_select = 'TIME,SCAN_NUMBER,ANTENNA1,FPARAM,FIELD_ID',
                                                           _query_criteria = 'ANTENNA1=='+str(station),
                                                           _query_sort = 'TIME, SPECTRAL_WINDOW_ID'
                                                          )
        tsys                   = np.transpose(np.squeeze(tsys))
        avg_airm               = []
        avg_rty                = []
        avg_lty                = []
        avgbin_time            = []
        avgbin_rty             = []
        avgbin_lty             = []
        #bin in time intervals
        for i,t in enumerate(time):
            thisant    = int(ant[i])
            thisscan   = str(scan[i])
            thisparams = scan_params[thisscan]
            scan_start = thisparams[0]
            scan_end   = thisparams[1]
            scan_ants  = thisparams[2]
            #check for correct association with scan
            if t > scan_start-1 and t < scan_end+1 and thisant in scan_ants:
                rcp_tsys = tsys[i][0]
                try:
                    lcp_tsys = tsys[i][1]
                except IndexError:
                    lcp_tsys = 0.
                #exclude invalid values in averaging
                if rcp_tsys > MIN_TSYS:
                    this_rcpty = rcp_tsys
                else:
                    this_rcpty = 0.
                if lcp_tsys > MIN_TSYS:
                    this_lcpty = lcp_tsys
                else:
                    this_lcpty = 0.
                avgbin_time.append(t)
                avgbin_rty.append(this_rcpty)
                avgbin_lty.append(this_lcpty)
                dt = max(avgbin_time) - min(avgbin_time)
                if dt > _inp_params.opac_corr_avgtime:
                    avg_time = np.mean(avgbin_time)
                    ra_src   = field_directions[int(src[i])][0]
                    dec_src  = field_directions[int(src[i])][1]
                    avg_elev = compute_elev(ra_src, dec_src, xyz_st, avg_time)
                    #exclude points below 10deg elevation
                    if avg_elev > 0.175:
                        avgbin_rty = np.asarray(avgbin_rty)
                        avgbin_lty = np.asarray(avgbin_lty)
                        avgbin_rty = np.ma.masked_where(avgbin_rty<MIN_TSYS,avgbin_rty)
                        avgbin_lty = np.ma.masked_where(avgbin_lty<MIN_TSYS,avgbin_lty)
                        avg_airm.append(min(1./np.sin(avg_elev),100.))
                        avg_rty.append(np.ma.mean(avgbin_rty))
                        avg_lty.append(np.ma.mean(avgbin_lty))
                    avgbin_time = []
                    avgbin_rty  = []
                    avgbin_lty  = []
        #sort by increasing airmass
        avg_airm, avg_rty, avg_lty = np.transpose([tmp for tmp in sorted(zip(avg_airm,avg_rty,avg_lty))])
        binned_airm = []
        binned_rty  = []
        binned_lty  = []
        bin_airm    = []
        bin_rty     = []
        bin_lty     = []
        avg_airm    = np.ma.masked_where(avg_airm<1,avg_airm)
        avg_rty     = np.ma.masked_where(avg_airm<1,avg_rty)
        avg_lty     = np.ma.masked_where(avg_airm<1,avg_lty)
        avg_rty     = np.ma.masked_where(avg_rty<MIN_TSYS,avg_rty)
        avg_lty     = np.ma.masked_where(avg_rty<MIN_TSYS,avg_lty)
        #bin in airmass intervals - minimum tsys values per bin for lower envelope fit
        for i, (a_airm, a_rty, a_lty) in enumerate(zip(avg_airm, avg_rty, avg_lty)):
            bin_airm.append(a_airm)
            bin_rty.append(a_rty)
            bin_lty.append(a_lty)
            da = max(bin_airm) - min(bin_airm)
            if da > _inp_params.opac_corr_d_airm and len(bin_airm) >= _inp_params.opac_corr_airm_nbin:
                binned_airm.append(np.ma.mean(bin_airm))
                binned_rty.append(np.ma.min(bin_rty))
                binned_lty.append(np.ma.min(bin_lty))
                bin_airm = []
                bin_rty  = []
                bin_lty  = []
        if len(binned_airm)>2:
            binned_airm_c = []
            binned_rty_c  = []
            binned_lty_c  = []
            for i, bia in enumerate(binned_airm):
                if bia<_inp_params.opac_corr_airm_max:
                    binned_airm_c.append(bia)
                    binned_rty_c.append(binned_rty[i])
                    binned_lty_c.append(binned_lty[i])
        else:
            binned_airm_c = binned_airm
            binned_rty_c  = binned_rty
            binned_lty_c  = binned_lty
        try:
            rcp_fit = np.ma.polyfit(binned_airm_c, binned_rty_c, 1)
        except TypeError:
            rcp_fit = np.asarray([0,0])
        try:
            lcp_fit = np.ma.polyfit(binned_airm_c, binned_lty_c, 1)
        except TypeError:
            lcp_fit = np.asarray([0,0])
        #receiver temperature is approx. given by Tsys extrapolated to zero airmass
        rcp_y0  = rcp_fit[-1]
        lcp_y0  = lcp_fit[-1]
        try:
            min_rty = np.ma.min(binned_rty_c)
            min_lty = np.ma.min(binned_lty_c)
        except ValueError:
            #not enough data
            lost_stations.append(station)
            continue
        rcp_lab = 'RCP: T_rx=%.2f K' % rcp_y0
        lcp_lab = 'LCP: T_rx=%.2f K' % lcp_y0
        try:
            rcp_y0  = trx_rcp_file[station]
            rcp_lab = 'RCP: T_rx=%.2f K (from file)' % rcp_y0
            r_from_file = True
        except KeyError:
            r_from_file = False
        if not r_from_file and rcp_y0 > min_rty or not r_from_file and rcp_y0 < MIN_TSYS:
            if _inp_params.verbose:
                print('    Warning: Did not obtain a good RCP fit for antenna ' + str(station))
                print('    Most likely, the weather was bad at high elevation observations.')
                print('    Will use the smallest Tsys value while correcting for the airmass,\n'
                      '    with the assumption of tau=0.05 and T_amb=273.15K locally to guess T_rx.\n'
                      '    Please check the results carefully.'
                     )
            airm_min_rty = binned_airm_c[binned_rty_c.index(min_rty)]
            trx_guess    = min_rty - 0.05 * airm_min_rty * atm_temp(273.15)
            trx_guess    = max(trx_guess, min_rty/2)
            if _inp_params.verbose:
                print('      -->Replacing T_rx=' + str(rcp_y0) + ' with T_rx=' + str(trx_guess))
            rcp_y0  = trx_guess
            rcp_lab = 'RCP: T_rx=%.2f K (guess)' % rcp_y0
        try:
            lcp_y0      = trx_lcp_file[station]
            lcp_lab     = 'LCP: T_rx=%.2f K (from file)' % lcp_y0
            l_from_file = True
        except KeyError:
            l_from_file = False
        if not l_from_file and lcp_y0 > min_lty or not l_from_file and lcp_y0 < MIN_TSYS:
            if _inp_params.verbose:
                print('    Warning: Did not obtain a good LCP fit for antenna ' + str(station))
                print('    Most likely, the weather was bad at high elevation observations.')
                print('    Will use the smallest Tsys value while correcting for the airmass,\n'
                      '    with the assumption of tau=0.05 and T_amb=273.15K locally to guess T_rx.\n'
                      '    Please check the results carefully.'
                     )
            airm_min_lty = binned_airm_c[binned_lty_c.index(min_lty)]
            trx_guess    = min_lty - 0.05 * airm_min_lty * atm_temp(273.15)
            trx_guess    = max(trx_guess, min_lty/2)
            if _inp_params.verbose:
                print('      -->Replacing T_rx=' + str(lcp_y0) + ' with T_rx=' + str(trx_guess))
            lcp_y0  = trx_guess
            lcp_lab = 'LCP: T_rx=%.2f K (guess)' % lcp_y0
        trx_rcp[station] = rcp_y0
        trx_lcp[station] = lcp_y0
        if _inp_params.diag_opac_corr:
            xp_plt  = np.linspace(0, 5, 10)
            rcp_pol = np.poly1d(rcp_fit)
            lcp_pol = np.poly1d(lcp_fit)
            plt.figure(i_plot)
            plt.clf()
            plt.ioff()
            plt.plot(xp_plt, rcp_pol(xp_plt), color='r')
            plt.plot(xp_plt, lcp_pol(xp_plt), color='b')
            plt.scatter(avg_airm, avg_rty, color='r', label=rcp_lab, facecolors='none', s=56)
            plt.scatter(avg_airm, avg_lty, color='b', label=lcp_lab, facecolors='none', s=56)
            plt.scatter(binned_airm_c, binned_rty_c, color='r', marker='v', s=556)
            plt.scatter(binned_airm_c, binned_lty_c, color='b', marker='v', s=556)
            plt.title('Station ' + str(_ms_metadata.yield_antname(station)) + \
                      '.\nTriangle symbols are the binned values used for the lower envelope fit.'
                     )
            plt.legend(loc='best')
            plt.xlim([0,5])
            plpath = os.path.join(_inp_params.diagdir + _inp_params.diag_opac_corr, '')
            auxiliary.makedir(plpath)
            plname = plpath + 'trx_fit_' + str(_ms_metadata.yield_antname(station))
            plt.ylabel(r'Tsys [K]', fontsize=16)
            plt.xlabel(r'airmass', fontsize=14)
            plt.tight_layout()
            plt.savefig(plname)
            plt.close()
    #now the Tsys entries (rcp and lcp averaged) can be used directly to compute tau and replace Tsys by Tsys* in place
    rproblems  = {}
    lproblems  = {}
    anyproblem = False
    for station in stations:
        rproblems[station] = 0
        lproblems[station] = 0
    last_tau = defaultdict(dict)
    mytb     = casac.table()
    mytb.open(tsys_caltable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        antcell   = mytb.getcell('ANTENNA1', row)
        if antcell in stations and antcell not in lost_stations:
            thisant    = int(antcell)
            thisscan   = str(mytb.getcell('SCAN_NUMBER', row))
            thissource = _ms_metadata.yield_sourcename_from_scan(thisscan)
            #initialize tau values to the cutoff of 1.0:
            last_tau[thisant][thissource] = 1.0
            thisparams                    = scan_params[thisscan]
            scan_start                    = thisparams[0]
            scan_end                      = thisparams[1]
            scan_ants                     = thisparams[2]
            timecell                      = mytb.getcell('TIME', row)
            if thisant in scan_ants:
                tsyscell = mytb.getcell('FPARAM', row)
                rcp_tsys = tsyscell[0][0]
                try:
                    lcp_tsys = tsyscell[1][0]
                except IndexError:
                    lcp_tsys = 0.
                if rcp_tsys > MIN_TSYS:
                    this_rcpty = rcp_tsys
                else:
                    this_rcpty = 0.
                if lcp_tsys > MIN_TSYS:
                    this_lcpty = lcp_tsys
                else:
                    this_lcpty = 0.
                try:
                    tatm = atm_temp(tamb[antcell](timecell), 'K', _inp_params.atmo_model, alt[antcell], pres[antcell](timecell))
                except KeyError:
                    an = _ms_metadata.yield_antname(antcell)
                    raise ValueError('No WEATHER data found for {0}. Check MS and WX metadata'.format(an))
                opacity, rp, lp               = compute_tau(this_rcpty, this_lcpty, trx_rcp[antcell], trx_lcp[antcell],
                                                            tatm, antcell, timecell, last_tau[thisant][thissource]
                                                           )
                last_tau[thisant][thissource] = opacity
                tsys_star                     = np.asarray([ty*np.exp(opacity) for ty in tsyscell])
                if rp:
                    rproblems[antcell] += 1
                    anyproblem         = True
                if lp:
                    lproblems[antcell] += 1
                    anyproblem         = True
                mytb.putcell('FPARAM', row, tsys_star)
    mytb.flush()
    mytb.done()
    if bad_weather_counter:
        print('    Please check the WEATHER data attached to the MS. Instances were found with bad (negative) weather values.')
    if anyproblem:
        print('    There were problems with the opacity solutions for some stations:')
        for st in stations:
            if rproblems[st] or lproblems[st]:
                stname = str(_ms_metadata.yield_antname(st))
                rprob  = str(rproblems[st])
                lprob  = str(lproblems[st])
                print('      station {0}: {1} bad RCP fits and {2} bad LCP fits'.format(stname, rprob, lprob))
        print('    Please check the T_rx fits carefully for these stations.')
        print('    Most likely, bad fits are the problem.')
        if _inp_params.diag_opac_corr:
            print('    The fits can be found at\n      ' + plpath)
        else:
            print('    No plots were created because the diag_opac_corr parameter was not set.')
        print('    For bad fits it is advisable to re-run this step\n    with a trec file using manually entered values.')
        print('    Also: Check the Tsys plots themselves.\n    Problems arise when the capped tau value of 1.0 is reached.')
    if lost_stations:
        print('\n    And no opacity correction could be done for the following stations due to missing data:')
        lost_formatted = [str(_ms_metadata.yield_antname(st)) for st in lost_stations]
        print(auxiliary.wrap_list(lost_formatted, items_per_line=5, indent='      '))
