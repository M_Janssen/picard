#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Automated tuning of parameters. """
import copy
import numpy                  as np
import pipe_modules.auxiliary as auxiliary


def estimate_coherencetime(freq, decoherence_amploss_faction=0.99, Allen_standard_deviation=1.e-13, _round=0):
    """
    See TMS.
    """
    # Add a positive round bias for python 2-3 consistency.
    return round(np.sqrt(-6*np.log(decoherence_amploss_faction)) / (2 * np.pi * Allen_standard_deviation * freq)+1.e-13, _round)


def update_fringe_solint_inp_params(_inp_params, _ms_metadata):
    """
    Can set fringefit solution intervals based on estimated coherence times.
    """
    _inp_params_copy = copy.deepcopy(_inp_params)
    try:
        max_freq = np.max(_ms_metadata.channels_nu)
    except ValueError:
        # Spectral windows with different numbers of channels.
        max_freq = np.max(auxiliary.flatten_list(_ms_metadata.channels_nu))
    solint_coher_est = [estimate_coherencetime(max_freq, 0.99, 1.e-13), estimate_coherencetime(max_freq, 0.9, 1.e-14)]
    if _inp_params_copy.fringe_solint_optimize_search_cal == 'estimate':
        _inp_params_copy = _inp_params_copy._replace(fringe_solint_optimize_search_cal=solint_coher_est)
    if _inp_params_copy.fringe_solint_optimize_search_sci == 'estimate':
        _inp_params_copy = _inp_params_copy._replace(fringe_solint_optimize_search_sci=solint_coher_est)
    if _inp_params_copy.fringe_solint_mb_reiterate == 'estimate':
        longest_scan2   = max([_ms_metadata.yield_scan_length(scan) for scan in _ms_metadata.all_scans]) / 2
        shortest_solint = 1.5 * solint_coher_est[1]
        if longest_scan2 > shortest_solint:
            this_solint_re = shortest_solint
            reiter_solints = [this_solint_re]
            safety         = 0
            while this_solint_re < longest_scan2:
                this_solint_re *= 1.5
                reiter_solints.append(this_solint_re)
                safety += 1
                if safety == 1337:
                    raise OverflowError('I am stuck at {0} < {1} for solint reiterate'.format(str(this_solint_re),
                                                                                              str(longest_scan2)
                                                                                             ))
            reiter_solints.append(longest_scan2)
            _inp_params_copy = _inp_params_copy._replace(fringe_solint_mb_reiterate=reiter_solints)
        else:
            _inp_params_copy = _inp_params_copy._replace(fringe_solint_mb_reiterate=None)
    return _inp_params_copy
