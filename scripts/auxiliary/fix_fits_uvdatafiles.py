"""
From a group of UVFITS or FITS-IDI files, remove GAIN_CURVE, WEATHER, and SYSTEM_TEMPERATURE extensions
for all but the first file in the list.

Note:
For old UVFITS output from the VLBA correlator, you will need to add .uvfits to fitsidi_extensions in input/constants.inp
and use this special CASA version in picard/your_casapath.txt:
https://open-bamboo.nrao.edu/browse/CASA-CMDT47-1/artifact/shared/ML2014-Python-3.8-Tar-distribution/casa-CAS-14169-1-py3.8.el7.tar.xz
"""
from glob import glob
try:
    import astropy.io.fits as pyfits
except:
    import pyfits


class IdiHDU(pyfits.PrimaryHDU):
    @classmethod
    def match_header(cls, header):
        try:
            keyword = header.cards[0].keyword
        except:
            keyword = header.ascard[0].key
            pass
        return (keyword == 'SIMPLE' and 'GROUPS' in header and
                header['GROUPS'] == True and 'NAXIS' in header and
                header['NAXIS'] == 0)

inlist  = glob('*.uvfits')
gotgc   = 0
gotwx   = 0
gottsys = 0
for f in inlist:
    editfile = False
    uvd = pyfits.open(f, mode='update', ignore_missing_end=True)
    try:
        _ = uvd['GAIN_CURVE'].data[0]
        gotgc+= 1
    except KeyError:
        pass
    try:
        _ = uvd['WEATHER'].data[0]
        gotwx+= 1
    except KeyError:
        pass
    try:
        _ = uvd['SYSTEM_TEMPERATURE'].data[0]
        gottsys+= 1
    except KeyError:
        pass
    if gotgc>1:
        try:
            del uvd['GAIN_CURVE']
        except KeyError:
            pass
    if gotwx>1:
        try:
            del uvd['WEATHER']
        except KeyError:
            pass
    if gottsys>1:
        try:
            del uvd['SYSTEM_TEMPERATURE']
        except KeyError:
            pass
    pyfits.register_hdu(IdiHDU)
    uvd.close()
