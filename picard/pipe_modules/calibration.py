#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Generic functionalities from and around the CASA calibration framework. """
from collections import defaultdict
import os
import copy
import shutil
import numpy as np
import pipe_modules.auxiliary                as auxiliary
import pipe_modules.diagnostics              as diagnostics
import pipe_modules.outlier_detection        as outlier_detection
import pipe_modules.flagging_algorithm       as flagging_algorithm
import pipe_modules.manual_calibration       as manual_calibration
import pipe_modules.exhaustive_fringe_search as exhaustive
from pipe_modules.default_casa_imports import *


def rerefant_general(_inp_params, _ms_metadata, _phase_caltb, restore_flags=False):
    """
    Executes CASA's rerefant task to reference all phase,delay,rate to common reference antennas.
    If restore_flags is True, all data that rerefant flags will be unflagged.
    """
    if not _inp_params.rerefant:
        return
    if _inp_params.rerefant != 'casatask':
        manual_calibration.my_rerefant(_inp_params, _ms_metadata, _phase_caltb)
        return
    if restore_flags:
        mytb = casac.table()
        mytb.open(_phase_caltb)
        orig_flags = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
        row_iter   = range(mytb.nrows())
        for row in row_iter:
            scan = mytb.getcell('SCAN_NUMBER', row)
            ant1 = mytb.getcell('ANTENNA1', row)
            spw  = mytb.getcell('SPECTRAL_WINDOW_ID', row)
            t    = mytb.getcell('TIME', row)
            flag = mytb.getcell('FLAG', row)
            #store flags
            orig_flags[scan][ant1][spw][t] = flag
        mytb.done()
        mytb.clearlocks()
    tasks.rerefant(vis                =  _inp_params.ms_name,
                   tablein            =  _phase_caltb,
                   caltable           =  "",
                   refantmode         =  "flex",
                   refant             =  _inp_params.refant.split(',')[0]
                  )
    if restore_flags:
        #Restore original flags to ensure that rerefant did not flag data that could not be re-referenced
        reref_isses = 0
        adjusted    = ''
        mytb.open(_phase_caltb, nomodify=False)
        row_iter = range(mytb.nrows())
        for row in row_iter:
            scan = mytb.getcell('SCAN_NUMBER', row)
            ant1 = mytb.getcell('ANTENNA1', row)
            spw  = mytb.getcell('SPECTRAL_WINDOW_ID', row)
            t    = mytb.getcell('TIME', row)
            try:
                these_flags = orig_flags[scan][ant1][spw][t]
                flag        = mytb.getcell('FLAG', row)
                for i,f in enumerate(flag):
                    if f and not these_flags[i]:
                        flag[i] = these_flags[i]
                        if str(scan) not in adjusted:
                            adjusted += str(scan) + ','
                    else:
                        pass
                mytb.putcell('FLAG', row, flag)
            except KeyError:
                reref_isses += 1
        mytb.flush()
        mytb.done()
        mytb.clearlocks()
        adjusted = adjusted.rstrip(',')
        if reref_isses:
            print('Warning: Could not restore rerefant flags in {0} cases.'.format(str(reref_isses)))
        if adjusted:
            print('Restored rerefant flags in these scans: {0}'. format(adjusted))


def get_refant(_inp_params, _ms_metadata, _scan, simple=False):
    """
    If simple is True:
    From the ordered list of preferred refants, returns the first one present in _scan or False if there is no match.
    If simple is False:
    Sophisticated method to determine refant from prioritized list based on the amount of valid data.
    """
    if simple:
        present_ants = [_ms_metadata.yield_antname(ant) for ant in _ms_metadata.yield_antennas(str(_scan))]
        return auxiliary.check_array_for_match(present_ants, _inp_params.refant.split(','))
    else:
        sci_file    = _inp_params.store_scan_refants + '.sci'
        cal_file    = _inp_params.store_scan_refants + '.cal'
        sci_refants = {}
        cal_refants = {}
        comb_dict   = {}
        if auxiliary.isfile(sci_file):
            try:
                sci_refants = auxiliary.store_object(sci_file, _operation='read')
            except ValueError:
                #can happen when it was stored for an older CASA version
                pass
        if auxiliary.isfile(cal_file):
            try:
                cal_refants = auxiliary.store_object(cal_file, _operation='read')
            except ValueError:
                pass
        comb_dict.update(cal_refants)
        comb_dict.update(sci_refants)
        return comb_dict[str(_scan)][0]


def smooth_atmosc(_inp_params, caltable):
    """
    Smooth to remove overfit to noise.
    """
    mytb     = casac.table()
    stations = np.unique(auxiliary.read_CASA_table(_inp_params, 'ANTENNA1', _tablename=caltable))
    t0       = np.min(auxiliary.read_CASA_table(_inp_params, 'TIME', _tablename=caltable))
    fitvals  = defaultdict(dict)
    for st in stations:
        qcrit0 = 'ANTENNA1=={0}'.format(str(st))
        scans  = np.unique(auxiliary.read_CASA_table(_inp_params, 'SCAN_NUMBER', qcrit0, _tablename=caltable))
        for sc in scans:
            qcrit1 = qcrit0 + ' && SCAN_NUMBER=={0}'.format(str(sc))
            ant2   = auxiliary.read_CASA_table(_inp_params, 'ANTENNA2', qcrit1, _tablename=caltable)
            refant = np.argmax(np.bincount(ant2))
            if str(st) == str(refant):
                continue
            qcrit2 = qcrit1 + ' && ANTENNA2=={0}'.format(str(refant))
            t,ph,f = auxiliary.read_CASA_table(_inp_params, 'TIME,CPARAM,FLAG', qcrit2, _tablename=caltable)
            f      = np.invert(np.squeeze(f))
            t      = t[f]
            if not t.any():
                continue
            t-= t0
            ph= np.squeeze(ph)
            ph= ph[f]
            # Phase wraps should have been taken out by previous segmented fringe-fit calibration.
            ph = np.angle(ph)
            N  = len(ph)
            # Find best order for polyfit
            old_metric = np.float('Inf')
            p          = np.array([0])
            for m in range(1, N):
                try:
                    #FIXME
                    z  = np.polynomial.Chebyshev.fit(t, ph, m, full=True)
                    p  = z[0].coef
                    Sr = z[1][0][0]
                except (ValueError, np.linalg.LinAlgError) as _:
                    continue
                if not Sr:
                    continue
                metric = Sr / (N - m - 1)
                if metric < old_metric:
                    old_metric = metric
                else:
                    break
            if p.any():
                fitvals[str(st)][str(sc)] = np.poly1d(p)
    mytb.open(caltable, nomodify=False)
    _nrows = mytb.nrows()
    for _row in range(_nrows):
        t = mytb.getcell('TIME', _row) - t0
        a = mytb.getcell('ANTENNA1', _row)
        s = mytb.getcell('SCAN_NUMBER', _row)
        try:
            angle = fitvals[str(a)][str(s)](t)
            c     = np.cos(angle) + 1.j * np.sin(angle)
            mytb.putcell('CPARAM', _row, np.array([[c]]))
        except KeyError:
            pass
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def postprocess_singleband_solutions(_inp_params, _ms_metadata, caltable, reference_delay_tospw0=False, rerefant_also=True):
    """
    Run after phase_calibration.task_fringefit_single() and phase_calibration.task_fringefit_single_overspws()
    to only select the wanted solutions and re-reference them.
    """
    if not auxiliary.isdir(caltable):
        return
    if _inp_params.fringe_instrumental_method == 'constant':
        if auxiliary.is_set(_inp_params, 'sbd_no_stationflag'):
            this_minSNR = 1.0
        else:
            this_minSNR = _inp_params.fringe_minSNR_sb_instrumental
        if rerefant_also:
            rerefant_general(_inp_params, _ms_metadata, caltable, False)
        pick_highest_average_scansnr(_inp_params, _ms_metadata, caltable, this_minSNR)
    elif _inp_params.fringe_instrumental_method == 'interpolate':
        if rerefant_also:
            rerefant_general(_inp_params, _ms_metadata, caltable, False)
        flag_below_SNR(_inp_params, _ms_metadata, caltable, _inp_params.fringe_minSNR_sb_instrumental)
    else:
        raise ValueError('fringe_instrumental_method input parameter must be either <constant> or <interpolate>.')
    if auxiliary.is_set(_inp_params, 'sbd_no_stationflag'):
        skip_fully_flagged_fringes(caltable)
    #The line must be drawn here - this far, no farther.
    if _inp_params.fringe_instrumental_method == 'interpolate':
        valid_solutions_to_all_scans(_inp_params, _ms_metadata, caltable)
    else:
        remove_flagged_solutions(_inp_params, _ms_metadata, caltable)
    if reference_delay_tospw0:
        reference_fringe_to_spw0(_inp_params, caltable, [_inp_params.F_0DELA, _inp_params.F_1DELA])


def reference_fringe_to_spw0(_inp_params, caltable, FPARAM_INDX):
    """
    Per SCAN and antenna: reference fringe solutions from the FPARAM_INDX to the one from the smallest spw.
    """
    mytb     = casac.table()
    fref     = defaultdict(lambda: defaultdict(dict))
    minspws  = {}
    stations = np.unique(auxiliary.read_CASA_table(_inp_params, 'ANTENNA1', _tablename=caltable))
    for st in stations:
        qcrit       = 'ANTENNA1=={0}'.format(str(st))
        spws        = auxiliary.read_CASA_table(_inp_params, 'SPECTRAL_WINDOW_ID', qcrit, _tablename=caltable)
        minspws[st] = min(spws)
    allscans = np.unique(auxiliary.read_CASA_table(_inp_params, 'SCAN_NUMBER', _tablename=caltable))
    for scan in allscans:
        qcrit0     = 'SCAN_NUMBER=={0}'.format(str(scan))
        these_ants = np.unique(auxiliary.read_CASA_table(_inp_params, 'ANTENNA1', qcrit0, _tablename=caltable))
        for st in these_ants:
            qcrit  = qcrit0 + ' && ANTENNA1=={0} && SPECTRAL_WINDOW_ID=={1}'.format(str(st), str(minspws[st]))
            fparam = auxiliary.read_CASA_table(_inp_params, 'FPARAM', qcrit, _tablename=caltable)
            for indx in FPARAM_INDX:
                try:
                    fref[scan][st][indx] = np.median(fparam[indx][0])
                except IndexError:
                    pass
    mytb.open(caltable, nomodify=False)
    for row in range(mytb.nrows()):
        st     = mytb.getcell('ANTENNA1', row)
        scan   = mytb.getcell('SCAN_NUMBER', row)
        fparam = mytb.getcell('FPARAM', row)
        for indx in FPARAM_INDX:
            try:
                fparam[indx] -= np.asarray([fref[scan][st][indx]])
            except (KeyError, IndexError) as _:
                pass
        mytb.putcell('FPARAM', row, fparam)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def extract_median_fringe(_inp_params, caltable, ant2):
    """
    Takes a fringefit caltable as input and returns a dict of median FPARAMm of all solutions where ANTENNA2==ant2m
    per ANTENNA1 and per SPECTRAL_WINDOW_ID.
    """
    mytb     = casac.table()
    ant2     = str(ant2)
    medf     = defaultdict(dict)
    stations = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', 'ANTENNA2=='+ant2, _tablename=caltable)
    spws     = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', 'ANTENNA2=='+ant2, _tablename=caltable)
    for s in spws:
        for st in stations:
            qcrit             = 'ANTENNA2=={0} && ANTENNA1=={1} && SPECTRAL_WINDOW_ID=={2}'.format(ant2, str(st), str(s))
            this_fparam, flag = auxiliary.read_CASA_table(_inp_params, 'FPARAM, FLAG', qcrit, _tablename=caltable)
            this_fparam       = np.ma.masked_array(this_fparam, flag)
            medf[s][st]       = np.array(np.ma.median(this_fparam, axis=-1))
    return medf


def reduce_fringetable_to_median(_inp_params, caltable_new, caltable_orig, ant2):
    """
    Copies caltable_orig to caltable_new and fills caltable_new only with the solutions extracted from extract_median_fringe().
    """
    auxiliary.rm_dir_if_present(caltable_new)
    auxiliary.copydir(caltable_orig, caltable_new)
    medf = extract_median_fringe(_inp_params, caltable_new, ant2)
    krow = 0
    mytb = casac.table()
    mytb.open(caltable_new, nomodify=False)
    noflag = mytb.getcell('FLAG', 0)
    goodsn = mytb.getcell('SNR', 0)
    noflag.fill(False)
    goodsn.fill(1337)
    for s in sorted(list(medf.keys())):
        for st in sorted(list(medf[s].keys())):
            mytb.putcell('FPARAM', krow, medf[s][st])
            mytb.putcell('SPECTRAL_WINDOW_ID', krow, s)
            mytb.putcell('ANTENNA1', krow, st)
            mytb.putcell('ANTENNA2', krow, int(ant2))
            mytb.putcell('FLAG', krow, noflag)
            mytb.putcell('SNR', krow, goodsn)
            krow+= 1
    del_rows = list( range(krow, mytb.nrows()) )
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()


def median_fringe_across_spws(_inp_params, caltable, sols='phase,delay,rate'):
    """
    Sets FPARAM sols of caltable to the median across spectral windows per scan, antenna, pol.
    Works only for a solint 'inf' fringefit calibration table (otherwise fringes will also be averaged in time).
    """
    mytb = casac.table()
    indices = []
    if 'phase' in sols:
        indices.append(_inp_params.F_0PHAS)
        indices.append(_inp_params.F_1PHAS)
    if 'delay' in sols:
        indices.append(_inp_params.F_0DELA)
        indices.append(_inp_params.F_1DELA)
    if 'rate' in sols:
        indices.append(_inp_params.F_0RATE)
        indices.append(_inp_params.F_1RATE)
    if not indices:
        if _inp_params.verbose:
            print('No solutions selected for fringe averaging across spws.')
        return
    fparam = defaultdict(lambda: defaultdict(dict))
    ants   = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename=caltable)
    scans  = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename=caltable)
    for s in scans:
        for a in ants:
            tfring, tflag = auxiliary.read_CASA_table(_inp_params, 'FPARAM,FLAG', 'ANTENNA1=={0} && SCAN_NUMBER=={1}'.format(a,s),
                                                      _tablename=caltable
                                                     )
            gfring = np.ma.masked_array(tfring, tflag)
            for indx in indices:
                fparam[int(s)][int(a)][indx] = np.asarray([np.ma.median(np.ma.filled(gfring[indx],0))])
    mytb.open(caltable, nomodify=False)
    nrows = mytb.nrows()
    for _row in range(nrows):
        ts = int(mytb.getcell('SCAN_NUMBER', _row))
        ta = int(mytb.getcell('ANTENNA1', _row))
        tf = mytb.getcell('FPARAM', _row)
        for indx in indices:
            try:
                tf[indx] = fparam[ts][ta][indx]
            except KeyError:
                pass
        mytb.putcell('FPARAM', _row, tf)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def split_cal_into_ants(_inp_params, _ms_metadata, _antennas, _caltables, _interp, _gainfields, _weights):
    """
    Unused (replacing solutions for certain antennas directly in the caltables instead):
    [Checks if any caltables has _antennas (from C_ANT) defined, which states that the solutions from that table
    are to be applied to certain set of antennas only.
    Returns a list of [[antennas], [caltables], [interp], [gainfields], [weights]] that can be iterated
    over with applycal.]
    """
    lengths = []
    lengths.append(len(_antennas))
    lengths.append(len(_caltables))
    lengths.append(len(_interp))
    lengths.append(len(_gainfields))
    lengths.append(len(_weights))
    if len(np.unique(lengths)) != 1:
        raise ValueError('Not all input calib params have the same length. Got ' + str(lengths))
    all_ants     = _ms_metadata.antenna_set
    perant_steps = [[],[],[],[],[]]
    apply_to_all = [[],[],[],[],[]]
    ant_apl_sep  = []
    for i,ants in enumerate(_antennas):
        if not ants:
            apply_to_all[1].append(_caltables[i])
            apply_to_all[2].append(_interp[i])
            apply_to_all[3].append(_gainfields[i])
            apply_to_all[4].append(_weights[i])
    for i, ants in enumerate(_antennas):
        if ants:
            apply_to_ant = [[],[],[],[],[]]
            these_ants   = ants.split(',')
            if '!' in ants:
                these_ants = [_ms_metadata.yield_antID(a.strip('!')) for a in these_ants]
                for a in all_ants:
                    if a not in these_ants:
                        apply_to_ant[0].append(a)
                        if a not in ant_apl_sep:
                            ant_apl_sep.append(a)
            else:
                these_ants = [_ms_metadata.yield_antID(a) for a in these_ants]
                for a in these_ants:
                    apply_to_ant[0].append(a)
                    if a not in ant_apl_sep:
                        ant_apl_sep.append(a)
            apply_to_ant[0] = ','.join(str(a) for a in apply_to_ant[0])
            apply_to_ant[1] = [_caltables[i]]
            apply_to_ant[1].extend(apply_to_all[1])
            apply_to_ant[2] = [_interp[i]]
            apply_to_ant[2].extend(apply_to_all[2])
            apply_to_ant[3] = [_gainfields[i]]
            apply_to_ant[3].extend(apply_to_all[3])
            apply_to_ant[4] = [_weights[i]]
            apply_to_ant[4].extend(apply_to_all[4])
            for j, ata in enumerate(apply_to_ant):
                perant_steps[j].append(ata)
    remaining_ants  = auxiliary.subtract_list(all_ants, ant_apl_sep)
    remaining_ants  = ','.join(str(a) for a in remaining_ants)
    if remaining_ants:
        apply_to_all[0] = remaining_ants
        for j, ata in enumerate(apply_to_all):
            perant_steps[j].append(ata)
    return perant_steps


def True_fraction(inarray):
    """
    For a 2D numpy boolean ndarray, returns the fraction of True/False (flag/good).
    """
    flatarr = auxiliary.flatten_list(inarray)
    return float(sum(flatarr)) / float(len(flatarr))


def calibration_quality(_inp_params, _ms_metadata, _calibrationtable, metric, minsnr=3, scan_number=''):
    """
    Access point for calls to routines which access the goodness of the calibration solutions written to a _calibrationtable.
    Used for checking the goodness of fringe solutions vs solution intervals.
    For now: scan_number not used for fraction_good_solutions metric.
    """
    auxiliary.check_if_table_is_written(_calibrationtable)
    if metric == 'fraction_good_solutions':
        return fraction_good_solutions(_inp_params, _calibrationtable)
    elif metric == 'average_solution_SNR':
        return typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, 'average', scan_number=scan_number)
    elif metric == 'median_solution_SNR':
        return typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, 'median', scan_number=scan_number)
    elif metric == 'average_solution_SNR_per_station':
        return typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, 'average', True, scan_number=scan_number)
    elif metric == 'median_solution_SNR_per_station':
        return typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, 'median', True, scan_number=scan_number)
    elif metric == 'min_solution_SNR_per_station':
        return typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, 'min', True, scan_number=scan_number)
    elif metric == 'range_solution_SNR_per_station':
        return typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, 'range', True, minsnr, scan_number=scan_number)
    else:
        raise ValueError(str(metric) + ' is not a known quality comparison metric.')


def fraction_good_solutions(_inp_params, _calibrationtable):
    """
    -- calibration quality metric --
    Takes a calibration table and returns the fraction of good (non-flagged) solutions of that table.
    """
    flags = auxiliary.read_CASA_table(_inp_params, 'FLAG', _tablename = _calibrationtable)
    flags = flags.ravel()
    return 1. - float(sum(flags)) / float(len(flags))


def typical_solution_SNR(_inp_params, _ms_metadata, _calibrationtable, _measure='average', per_station=False, minsnr=3,
                         scan_number=''):
    """
    -- calibration quality metric --
    Takes a calibration table and returns the average SNR of all solutions.
    Only works if a single refant was used for _calibrationtable (no rereferencing).
    Returns a single average if per_station=False and else a dict of snr values per station.
    """
    if _measure == 'average':
        this_typical = auxiliary.average_unique_values
    elif _measure == 'median':
        this_typical = auxiliary.median_unique_values
    elif _measure == 'min':
        this_typical = auxiliary.min_unique_values
    elif _measure == 'range':
        this_typical = auxiliary.range_unique_values_min
    else:
        raise ValueError(str(_measure) + ' is not a known statistical measure. Must be average, median, or min.')
    qcrit  = 'ANTENNA1!=ANTENNA2'
    if scan_number:
        qcrit += ' && SCAN_NUMBER==' + str(scan_number)
    if per_station:
        stations   = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', qcrit, _tablename = _calibrationtable)
        st_snr     = {}
        for st in stations:
            this_qcrit = 'ANTENNA1=='+str(st)
            if scan_number:
                this_qcrit += ' && SCAN_NUMBER==' + str(scan_number)
            snr = auxiliary.read_CASA_table(_inp_params, 'SNR', this_qcrit, _tablename = _calibrationtable)
            snr = auxiliary.transpose_list(snr, True, True)
            st_snr[st] = this_typical(snr, minsnr)
        return st_snr
    else:
        snr = auxiliary.read_CASA_table(_inp_params, 'SNR', qcrit, _tablename = _calibrationtable)
        snr = auxiliary.transpose_list(snr, True, True)
        return this_typical(snr, minsnr)


def average_solution_SNR_detailed(_inp_params, _ms_metadata, _calibrationtable):
    """
    Does the same as average_solution_SNR(), but per scan, station, spw, and pol.
    Returns a dict[station][spw][scan] with snr values (averaged in time per scan).
    """
    snr_vals  = defaultdict(lambda: defaultdict(dict))
    scans     = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename = _calibrationtable)
    spws      = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', _tablename = _calibrationtable)
    stations  = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename = _calibrationtable)
    for scan in scans:
        for station in stations:
            for spw in spws:
                this_qcrit = 'SCAN_NUMBER=='+str(scan) + ' && SPECTRAL_WINDOW_ID=='+str(spw) + ' && ANTENNA1=='+str(station)
                this_snr   = auxiliary.read_CASA_table(_inp_params, 'SNR', this_qcrit, _tablename = _calibrationtable)
                this_flag  = auxiliary.read_CASA_table(_inp_params, 'FLAG', this_qcrit, _tablename = _calibrationtable)
                # Set flagged solutions to S/N of 1
                this_snr[this_flag]          = 1.
                snr                          = auxiliary.transpose_list(this_snr, True, True)
                snr_vals[station][scan][spw] = auxiliary.unique_unsorted(np.nan_to_num(snr))
    return snr_vals


def pick_highest_snr(_calibrationtable):
    """
    Takes a calibration table and per antenna and spw, deletes all rows except the ones with the highest SNR.
    Ignores flagged entries.
    Treats the two polarizations separately.
    Modifies _calibrationtable directly.
    Useful for the instrumental phase calibration.
    """
    mytb = casac.table()
    #2D dict: keys are antenna and spw, storing (SNR, rownumber)
    highest_snr = defaultdict(lambda: defaultdict(dict))
    mytb.open(_calibrationtable, nomodify=False)
    _nrows   = mytb.nrows()
    all_rows = range(_nrows)
    #Pick the rows with the highest SNR:
    for _row in all_rows:
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        thisant = mytb.getcell('ANTENNA1', _row)
        thisflg = auxiliary.unique_unsorted(mytb.getcell('FLAG', _row))
        thissnr = auxiliary.unique_unsorted(mytb.getcell('SNR', _row))
        for tflg, tsnr in zip(thisflg, thissnr):
            if not tflg and not np.isnan(tsnr):
                try:
                    current_high = highest_snr[thisant][thisspw][0]
                except KeyError:
                    #First one encountered
                    current_high = -float('Inf')
                if tsnr > current_high:
                    highest_snr[thisant][thisspw] = (tsnr, _row)
    #Get a list of all rows to be kept based on highest SNR:
    keep_rows = []
    for values in highest_snr.values():
        for value in values.values():
            keep_rows.append(value[1])
    del_rows = list( set(all_rows) - set(keep_rows) )
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()


def pick_highest_average_scansnr(_inp_params, _ms_metadata, _calibrationtable, _minspwSNR):
    """
    Does the same as pick_highest_snr but on a per-scan basis:
    Per antenna, only keeps the solutions from the scan which has the highest average SNR of the solutions across all spw, with
    a _minspwSNR SNR cut required that must be minimally present for each spw.
    This means that, other than pick_highest_snr(), this function will keep all solutions from a single scan.
    For stations which switch polarizations in different scans, one scan for each pol is kept.
    Should be run after rerefant() as it removes rows of potential secondary refants.
    """
    _debug         = False
    mytb           = casac.table()
    scan_snrs      = average_solution_SNR_detailed(_inp_params, _ms_metadata, _calibrationtable)
    all_stations   = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename = _calibrationtable)
    highest_snr    = defaultdict(dict)
    pol2sols       = defaultdict(dict)
    primary_refant = int(_ms_metadata.yield_antID(_inp_params.refant.split(',')[0]))
    if _debug:
        print(scan_snrs)
    #strive to have solutions to primary refant first
    for station in scan_snrs.keys():
        current_high = [0, 0]
        try:
            antname = str(_ms_metadata.yield_antname(station))
        except ValueError:
            #MS or metadata incomplete
            antname = 'unknown'
        for scan in scan_snrs[station].keys():
            this_refant = int(_ms_metadata.yield_antID(get_refant(_inp_params, _ms_metadata, scan)))
            if this_refant != primary_refant:
                continue
            good_stationscan = True
            num_vals_counter = [0, 0]
            SNR_sum          = [0, 0]
            for spw in scan_snrs[station][scan].keys():
                try:
                    if antname in _inp_params.switching_pol_stations:
                        this_scan_snr = scan_snrs[station][scan][spw]
                        if len(this_scan_snr) == 1:
                            this_scan_snr = np.append(this_scan_snr, 0.1)
                    else:
                        #Could change the line below if I wanted to have the option of separate sbd scans per pol.
                        this_scan_snr = [np.mean(scan_snrs[station][scan][spw]), 0.1]
                    if any(val > _minspwSNR for val in this_scan_snr):
                        if primary_refant != int(station):
                            if this_scan_snr[0] > _minspwSNR:
                                num_vals_counter[0] += 1
                                SNR_sum[0]          += this_scan_snr[0]
                            if this_scan_snr[1] > _minspwSNR:
                                num_vals_counter[1] += 1
                                SNR_sum[1]          += this_scan_snr[1]
                        else:
                            good_stationscan = False
                            break
                    else:
                        #Punish scans where no solutions for specific spws are found
                        num_vals_counter[0] += 1337
                        num_vals_counter[1] += 1337
                except (KeyError, ValueError) as no_entry:
                    good_stationscan = False
                    break
            if good_stationscan and num_vals_counter[0]:
                SNR_sum[0] /= float(num_vals_counter[0])
                SNR_sum[1] /= float(max(1, num_vals_counter[1]))
                if SNR_sum[0] > current_high[0]:
                    current_high[0]         = SNR_sum[0]
                    highest_snr[station][0] = scan
                if SNR_sum[1] > current_high[1]:
                    current_high[1]         = SNR_sum[1]
                    highest_snr[station][1] = scan
    if _debug:
        print(highest_snr)
    #search all refants next
    for station in all_stations:
        if station not in highest_snr.keys() and station!=primary_refant:
            current_high = [0, 0]
            try:
                antname = str(_ms_metadata.yield_antname(station))
            except ValueError:
                #MS or metadata incomplete
                antname = 'unknown'
            for scan in scan_snrs[station].keys():
                this_refant = int(_ms_metadata.yield_antID(get_refant(_inp_params, _ms_metadata, scan)))
                good_stationscan = True
                num_vals_counter = [0, 0]
                SNR_sum          = [0, 0]
                for spw in scan_snrs[station][scan].keys():
                    try:
                        if antname in _inp_params.switching_pol_stations:
                            this_scan_snr = scan_snrs[station][scan][spw]
                            if len(this_scan_snr) == 1:
                                this_scan_snr = np.append(this_scan_snr, 0.1)
                        else:
                            #Could change the line below if I wanted to have the option of separate sbd scans per pol.
                            this_scan_snr = [np.mean(scan_snrs[station][scan][spw]), 0.1]
                        if any(val > _minspwSNR for val in this_scan_snr):
                            if this_refant != int(station) and primary_refant != int(station):
                                if this_scan_snr[0] > _minspwSNR:
                                    num_vals_counter[0] += 1
                                    SNR_sum[0]          += this_scan_snr[0]
                                if this_scan_snr[1] > _minspwSNR:
                                    num_vals_counter[1] += 1
                                    SNR_sum[1]          += this_scan_snr[1]
                            else:
                                good_stationscan = False
                                break
                        else:
                            good_stationscan = False
                            break
                    except (KeyError, ValueError) as no_entry:
                        good_stationscan = False
                        break
                if good_stationscan and num_vals_counter[0]:
                    SNR_sum[0] /= float(num_vals_counter[0])
                    SNR_sum[1] /= float(max(1, num_vals_counter[1]))
                    if SNR_sum[0] > current_high[0]:
                        current_high[0]         = SNR_sum[0]
                        highest_snr[station][0] = scan
                    if SNR_sum[1] > current_high[1]:
                        current_high[1]         = SNR_sum[1]
                        highest_snr[station][1] = scan
    if _debug:
        print(highest_snr)
    #Do a simple search for the highest SNR in any spw for the case that a station does not have anything for some spw
    for station in all_stations:
        if station not in highest_snr.keys() and station!=primary_refant:
            current_high = [-float('Inf'), 0]
            try:
                antname = str(_ms_metadata.yield_antname(station))
            except ValueError:
                #MS or metadata incomplete
                antname = 'unknown'
            for scan in scan_snrs[station].keys():
                this_refant = int(_ms_metadata.yield_antID(get_refant(_inp_params, _ms_metadata, scan)))
                if this_refant != int(station) and primary_refant != int(station):
                    for spw in scan_snrs[station][scan].keys():
                        try:
                            if antname in _inp_params.switching_pol_stations:
                                this_scan_snr = scan_snrs[station][scan][spw]
                                if len(this_scan_snr) == 1:
                                    this_scan_snr = np.append(this_scan_snr, 0)
                            else:
                                #Could change the line below if I wanted to have the option of separate sbd scans per pol.
                                this_scan_snr = [np.mean(scan_snrs[station][scan][spw]), 0]
                            if any(val > _minspwSNR for val in this_scan_snr):
                                if this_scan_snr[0] > _minspwSNR:
                                    current_high[0]         = this_scan_snr
                                    highest_snr[station][0] = scan
                                if this_scan_snr[1] > _minspwSNR:
                                    current_high[1]         = this_scan_snr
                                    highest_snr[station][1] = scan
                        except (KeyError, ValueError) as no_entry:
                            pass
    if _debug:
        print(highest_snr)
    del_rows = []
    mytb.open(_calibrationtable, nomodify=False)
    _nrows    = mytb.nrows()
    all_rows  = range(_nrows)
    all_scans = auxiliary.flatten_list([k.values() for k in highest_snr.values()])
    #Pick the rows with the highest SNR:
    for _row in all_rows:
        #thisref  = mytb.getcell('ANTENNA2', _row)
        thisant  = mytb.getcell('ANTENNA1', _row)
        thisscan = mytb.getcell('SCAN_NUMBER', _row)
        try:
            goodscan0 = highest_snr[thisant][0]
        except KeyError:
            goodscan0 = -1337
        try:
            goodscan1 = highest_snr[thisant][1]
        except KeyError:
            goodscan1 = -1337
        if goodscan0<0:
            # Station with only LCP.
            goodscan0 = goodscan1
        #keep refants
        if int(thisscan)!=int(goodscan0):
            this_refant = int(_ms_metadata.yield_antID(get_refant(_inp_params, _ms_metadata, thisscan)))
            if this_refant != int(thisant) or thisscan not in all_scans:
                del_rows.append(_row)
                #Move fringe solutions from goodscan1 to the corresponding goodscan0 to still have a single scan
                #for switching pol stations.
                if int(thisscan)==int(goodscan1) \
                and str(_ms_metadata.yield_antname(thisant)) in _inp_params.switching_pol_stations:
                    thisspw                    = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
                    thissnr                    = mytb.getcell('SNR', _row)
                    thisflg                    = mytb.getcell('FLAG', _row)
                    thispar                    = mytb.getcell('FPARAM', _row)
                    pol2sols[thisant][thisspw] = (thispar, thisflg, thissnr)
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    if not pol2sols:
        return
    mytb.open(_calibrationtable, nomodify=False)
    _nrows   = mytb.nrows()
    all_rows = range(_nrows)
    for _row in all_rows:
        thisant = mytb.getcell('ANTENNA1', _row)
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        try:
            thesesols = pol2sols[thisant][thisspw]
            solsindx  = []
            for i, sn in enumerate(thesesols[2]):
                if sn:
                    solsindx.append(i)
                else:
                    pass
            thissnr = mytb.getcell('SNR', _row)
            thisflg = mytb.getcell('FLAG', _row)
            thispar = mytb.getcell('FPARAM', _row)
            for indx in solsindx:
                thispar[indx] = thesesols[0][indx]
                thisflg[indx] = thesesols[1][indx]
                thissnr[indx] = thesesols[2][indx]
            mytb.putcell('FPARAM', _row, thispar)
            mytb.putcell('FLAG', _row, thisflg)
            mytb.putcell('SNR', _row, thissnr)
        except KeyError:
            pass
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def flag_below_SNR(_inp_params, _ms_metadata, _calibrationtable, _minSNR):
    """
    Takes a calibration table and marks solutions as flagged if they are below _minSNR.
      Looks at every row (combined solution for RCP and LCP per spw and time) separately.
      If the RCP and LCP SNRs are both bigger than zero (excluding the non-existing feed for single pol stations),
        then both solutions will be flagged unless the SNR of both is larger than _minSNR.
    Modifies _calibrationtable directly.
    Useful to weed out bad solutions for the instrumental phase and delay calibration for example.
    """
    _p1  = [_inp_params.F_0PHAS, _inp_params.F_0DELA, _inp_params.F_0RATE, _inp_params.F_0DISP]
    _p2  = [_inp_params.F_1PHAS, _inp_params.F_1DELA, _inp_params.F_1RATE, _inp_params.F_1DISP]
    mytb = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    _nrows = mytb.nrows()
    #fist check if a station is fully single-pol (then ignore snr=0 on other pol):
    unique_stations = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename = _calibrationtable)
    unique_spws     = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', _tablename = _calibrationtable)
    station_pols    = defaultdict(dict)
    for st in unique_stations:
        for spw in unique_spws:
            station_pols[st][spw] = []
    for _row in range(_nrows):
        thisant = mytb.getcell('ANTENNA1', _row)
        thissnr = mytb.getcell('SNR', _row)
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        #if there is any solution where the snr is not zero for a pol then this pol exists
        for i,snr in enumerate(thissnr):
            if snr:
                if i in _p1:
                    if 'p1' not in station_pols[thisant][thisspw]:
                        station_pols[thisant][thisspw].append('p1')
                elif i in _p2:
                    if 'p2' not in station_pols[thisant][thisspw]:
                        station_pols[thisant][thisspw].append('p2')
    unknown_ids      = []
    default_nperrset = np.seterr(invalid='ignore')
    for _row in range(_nrows):
        thisflg = mytb.getcell('FLAG', _row)
        thissnr = mytb.getcell('SNR', _row)
        thisant = mytb.getcell('ANTENNA1', _row)
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        try:
            antname = str(_ms_metadata.yield_antname(thisant))
        except ValueError:
            #MS or metadata incomplete
            if str(thisant) not in unknown_ids:
                unknown_ids.append(str(thisant))
            antname = 'unknown'
        allsols = len(thissnr)
        if 'p1' in station_pols[thisant][thisspw] and 'p2' in station_pols[thisant][thisspw] and \
        antname not in _inp_params.switching_pol_stations:
            badsols = len(np.where(np.logical_and(thissnr>=0, thissnr<_minSNR))[0])
        else:
            #single pol station, should not disrcard when snr==0 somewhere
            badsols = len(np.where(np.logical_and(thissnr>0, thissnr<_minSNR))[0])
        not_present = len(np.where(thissnr==0)[0])
        #handle dual pol stations or single pol stations:
        if badsols == allsols or (2*badsols == allsols) or not_present==allsols:
            flgthis = copy.deepcopy(thisflg)
            for i,flg in enumerate(flgthis):
                flgthis[i] = True
            mytb.putcell('FLAG', _row, flgthis)
    np.seterr(**default_nperrset)
    if unknown_ids and _inp_params.verbose:
        print('    Warning: Unknown station IDs: ' + ','.join(unknown_ids) +'.')
        print('      This can be caused when you are not working with the complete MS (used split for example).')
        print('      If this is not the case, you should re-determine your metadata by passing -m to picard')
    mytb.flush()
    mytb.done()


def remove_rows_based_on_ant(_calibrationtable, antIDs_tokeep):
    """
    Removes all rows in _calibrationtable except for those ANTENNA1 IDs that are in a antIDs_tokeep list (integers).
    """
    antIDs_tokeep = [int(ant) for ant in antIDs_tokeep]
    mytb          = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    _nrows   = mytb.nrows()
    del_rows = []
    for _row in range(_nrows):
        thisant = mytb.getcell('ANTENNA1', _row)
        if int(thisant) not in antIDs_tokeep:
            del_rows.append(_row)
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def keep_only_specific_antenna_fringes(_calibrationtable, antIDs_tokeep):
    """
    Set FPARAM=FLAG=0 for all ANTENNA1 IDs that are not in antIDs_tokeep.
    """
    antIDs_tokeep = [int(ant) for ant in antIDs_tokeep]
    mytb          = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        thisant = mytb.getcell('ANTENNA1', row)
        if thisant in antIDs_tokeep:
            continue
        thisflag   = mytb.getcell('FLAG', row)
        thisfparam = mytb.getcell('FPARAM', row)
        mytb.putcell('FLAG', row, np.zeros_like(thisflag))
        mytb.putcell('FPARAM', row, np.zeros_like(thisfparam))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def undo_station_CPARAM_solutions(_calibrationtable, antIDs_toremove):
    """
    Set CPARAM=1.+0j for all ANTENNA1 IDs that are in antIDs_toremove.
    """
    antIDs_toremove = [int(ant) for ant in antIDs_toremove]
    mytb            = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        thisant = mytb.getcell('ANTENNA1', row)
        if thisant not in antIDs_toremove:
            continue
        thiscparam = mytb.getcell('CPARAM', row)
        mytb.putcell('CPARAM', row, np.ones_like(thiscparam))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def skip_fully_flagged_fringes(_calibrationtable):
    """
    Takes a fringe-fit _calibrationtable and writes zero phase, delay, and rate and un-flagged solutions for every entry of a
    station where all solutions are flagged, i.e., nothing will be done to stations which just have spectral windows or
    polarization feeds partially flagged.
    The rationale for a particular usecase is given in the description of the sbd_no_stationflag parameter in the
    array_finetune.inp input file.
    """
    mytb                  = casac.table()
    station_fully_flagged = {}
    mytb.open(_calibrationtable, nomodify=False)
    row_iter = range(mytb.nrows())
    for row in row_iter:
        thisflag = mytb.getcell('FLAG', row)
        thisant  = mytb.getcell('ANTENNA1', row)
        uflag    = auxiliary.unique_unsorted(thisflag)
        if thisant in station_fully_flagged and not station_fully_flagged[thisant]:
            #Some other data was un-flagged for this station.
            continue
        elif len(uflag)!=1 or uflag[0]!=True:
            #This data is not fully flagged for this station.
            station_fully_flagged[thisant] = False
        else:
            station_fully_flagged[thisant] = True
    if not any(station_fully_flagged.values())==True:
        return
    for row in row_iter:
        thisant = mytb.getcell('ANTENNA1', row)
        if thisant not in station_fully_flagged or not station_fully_flagged[thisant]:
            continue
        thissnr    = mytb.getcell('SNR', row)
        thisflag   = mytb.getcell('FLAG', row)
        thisfparam = mytb.getcell('FPARAM', row)
        mytb.putcell('SNR', row, np.ones_like(thissnr))
        mytb.putcell('FLAG', row, np.zeros_like(thisflag))
        mytb.putcell('FPARAM', row, np.zeros_like(thisfparam))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def remove_flagged_solutions(_inp_params, _ms_metadata, _calibrationtable, strict_pol=False):
    """
    Takes a calibration table and deletes all rows (a single row contains an RCP+LCP solution) where all solutions
      (i.e., both RCP and LCP) are flagged, for all solutions where good solutions exist in other rows
      (so only the good solutions will be applied and interpolated in the end). If for a combination of antenna,spw only
      flagged solutions exist, then the rows will be kept as flagged (since the data cannot be calibrated).
      A warning will be printed if that happens.
    If strict_pol=True, will also remove row when only a single polarization is flagged
      (not necessary if used after flag_below_SNR()).
    Modifies _calibrationtable directly.
    Useful when interpolating solutions across scans while some scans are missing stations (leading to flagged solutions).
      E.g., when doing a time-dependent instrumental phase and delay calibration.
    """
    unique_stations = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename = _calibrationtable)
    unique_spws     = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', _tablename = _calibrationtable)
    left_flagged    = defaultdict(lambda: defaultdict(dict))
    for st in unique_stations:
        for spw in unique_spws:
            left_flagged[st][spw] = True
    mytb = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    _nrows   = mytb.nrows()
    del_rows = []
    for _row in range(_nrows):
        thisflg = auxiliary.unique_unsorted(mytb.getcell('FLAG', _row))
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        thisant = mytb.getcell('ANTENNA1', _row)
        if not all(thisflg):
            #a good solution does exist
            left_flagged[thisant][thisspw] = False
    for _row in range(_nrows):
        thisflg = auxiliary.unique_unsorted(mytb.getcell('FLAG', _row))
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        thisant = mytb.getcell('ANTENNA1', _row)
        if strict_pol and any(thisflg) and not left_flagged[thisant][thisspw]:
            del_rows.append(_row)
        elif len(thisflg)==1 and thisflg[0]==True and not left_flagged[thisant][thisspw]:
            #completely flagged entries for which a good solution does exist somewhere else in the table
            del_rows.append(_row)
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()
    existing_fully_flagged = False
    for st in unique_stations:
        if existing_fully_flagged:
            break
        for spw in unique_spws:
            if left_flagged[st][spw]:
                existing_fully_flagged = True
                break
    if _inp_params.verbose and existing_fully_flagged:
        print ('    For the calibration table ' + str(_calibrationtable))
        print ('    the following (antenna,spw) do not have solutions and will be flagged:')
        for st in unique_stations:
            for spw in unique_spws:
                if left_flagged[st][spw]:
                    try:
                        antname = str(_ms_metadata.yield_antname(st))
                    except ValueError:
                        antname = str(st) + ' (antenna name unknown in this MS)'
                    _uncalib_msg = 'antenna: {0}, spw: {1}'.format(antname, str(spw))
                    print ('      ' + _uncalib_msg)
        print ('    Possible reasons: bad/missing data, too high SNR cutoffs, missing stations in the calibration scans.')


def valid_solutions_to_all_scans(_inp_params, _ms_metadata, _calibrationtable, param='FPARAM'):
    """
    - Mark all param solutions for a (ant1, scan) where less than the max number of unflagged solutions are present across spws.
    - Replace all marked solutions by nearest-neighbor interpolation and interpolates+extrapolates to all scans in the MS.
    Only works when we have only one solution per (ant1, scan, spw).
    Needed, because the default nearest interpolation has problems with flagged solutions --> for a fingefit_single table, extend
    good solutions to all scans and then use linearperscan interpolation.
    Modifies _calibrationtable directly.
    """
    mytb = casac.table()
    #retrieve valid values that can be interpolated
    flagvals2 = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    flagvals  = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    paramvals = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    snrvals   = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    mytb.open(_calibrationtable)
    row_iter = range(mytb.nrows())
    for _row in row_iter:
        thisant2  = mytb.getcell('ANTENNA2', _row)
        thisant   = mytb.getcell('ANTENNA1', _row)
        thisscan  = mytb.getcell('SCAN_NUMBER', _row)
        thissnr   = mytb.getcell('SNR', _row)
        thisflag  = mytb.getcell('FLAG', _row)
        thisparam = mytb.getcell(param, _row)
        thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        flagvals2[thisant2][thisant][thisscan][thisspwid] = thisflag
        flagvals[thisant][thisscan][thisspwid]            = thisflag
        paramvals[thisant][thisscan][thisspwid]           = thisparam
        snrvals[thisant][thisscan][thisspwid]             = thissnr
    mytb.close()
    antscanflag = defaultdict(dict)
    antmaxflag  = defaultdict(dict)
    maxantflag  = defaultdict(dict)
    goodscanspr = defaultdict(dict)
    goodscans   = {}
    for refant in list(flagvals2.keys()):
        for ant in list(flagvals2[refant].keys()):
            max_unflag = 0
            for scan in list(flagvals2[refant][ant].keys()):
                this_unflag = 0
                for spw in list(flagvals2[refant][ant][scan].keys()):
                    this_unflag+= np.count_nonzero(np.invert(flagvals2[refant][ant][scan][spw]))
                max_unflag = max(max_unflag, this_unflag)
                antscanflag[ant][scan] = this_unflag
            antmaxflag[refant][ant] = max_unflag
            maxantflag[ant][refant] = max_unflag
        for ant in list(flagvals2[refant].keys()):
            goodscanspr[refant][ant] = []
            for scan in list(flagvals2[refant][ant].keys()):
                if antscanflag[ant][scan] == antmaxflag[refant][ant] and antmaxflag[refant][ant]:
                    goodscanspr[refant][ant].append(scan)
                    try:
                        goodscans[ant].append(scan)
                    except KeyError:
                        goodscans[ant] = [scan]
                else:
                    pass

    if _inp_params.verbose:
        print('  Interpolating between these scans for the calibration:')
        for ant in goodscans:
            if int(ant) == int(_ms_metadata.yield_antID(_inp_params.refant.split(',')[0])):
                continue
            antname = _ms_metadata.yield_antname(ant)
            print(f'    {antname}: {goodscans[ant]}')
    #interpolate
    mytb.open(_calibrationtable, nomodify=False)
    row_iter = range(mytb.nrows())
    for _row in row_iter:
        thisant  = mytb.getcell('ANTENNA1', _row)
        thisant2 = mytb.getcell('ANTENNA2', _row)
        thisscan = mytb.getcell('SCAN_NUMBER', _row)
        try:
            if thisscan in goodscans[thisant]:
                continue
            else:
                pass
        except KeyError:
            #this is a refant and it is the only refant
            continue
        thisspwid   = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        interpscan  = None
        these_scans = goodscans[thisant]
        try:
            if any(goodscanspr[thisant2][thisant]):
                #if possible, do interpolation within scans that use the same refant
                these_scans = goodscanspr[thisant2][thisant]
            else:
                pass
        except KeyError:
            pass
        #interpolate as would be done by 'nearest' across scans properly
        try:
            interpscan = auxiliary.closest_match(these_scans, thisscan)
        except ValueError:
            print(f'    Cannot interpolate ant {thisant}, scan {thisscan}.')
            continue
        try:
            mytb.putcell(param, _row, paramvals[thisant][interpscan][thisspwid])
            mytb.putcell('FLAG', _row, flagvals[thisant][interpscan][thisspwid])
            mytb.putcell('SNR', _row, snrvals[thisant][interpscan][thisspwid])
        except RuntimeError:
            # Catch issue where some antennas have additional spws without valid data
            mytb.putcell(param, _row, paramvals[thisant][thisscan][thisspwid])
            mytb.putcell('FLAG', _row, flagvals[thisant][thisscan][thisspwid])
            mytb.putcell('SNR', _row, snrvals[thisant][thisscan][thisspwid])
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    extend_solutions_to_all_scans(_inp_params, _ms_metadata, _calibrationtable, all_params=True)


def interpolate_over_flags(_calibrationtable, param='FPARAM', out_of_bounds='extrapolate'):
    """
    Similar to remove_flagged_solutions(), but this function will overwrite flagged solutions by interpolating over the good ones
    instead of just removing the flagged rows.
    The advantages are that the number of rows and their association to a scan and antenna are conserved and
    that the timestamps are kept consistent for different antennas in each scan.
    If all solutions for a (scan, antenna, spw) are flagged, they will be kept flagged.
    The out_of_bounds parameter controls what happens when extrapolation is required:
      - 'extrapolate' to fill all out of bounds values with the constant first/last values.
      - A number x to do extrapolation when less than x unflagged solutions are present in an interpolation interval.
        Otherwise, leave data outside of bounds flagged.
    """
    _debug = False
    mytb = casac.table()
    #retrieve valid values that can be interpolated
    timevals  = defaultdict(lambda: defaultdict(dict))
    paramvals = defaultdict(lambda: defaultdict(dict))
    snrvals   = defaultdict(lambda: defaultdict(dict))
    flagvals  = defaultdict(lambda: defaultdict(dict))
    mytb.open(_calibrationtable)
    row_iter = range(mytb.nrows())
    for _row in row_iter:
        thisant   = mytb.getcell('ANTENNA1', _row)
        thisscan  = mytb.getcell('SCAN_NUMBER', _row)
        thistime  = mytb.getcell('TIME', _row)
        thisflag  = mytb.getcell('FLAG', _row)
        thisparam = mytb.getcell(param, _row)
        thissnr   = mytb.getcell('SNR', _row)
        thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        try:
            timevals[thisscan][thisant][thisspwid]  = np.append(timevals[thisscan][thisant][thisspwid], thistime)
            paramvals[thisscan][thisant][thisspwid] = np.hstack([paramvals[thisscan][thisant][thisspwid], thisparam])
            snrvals[thisscan][thisant][thisspwid]   = np.hstack([snrvals[thisscan][thisant][thisspwid], thissnr])
            flagvals[thisscan][thisant][thisspwid]  = np.hstack([flagvals[thisscan][thisant][thisspwid], thisflag])
        except KeyError:
            timevals[thisscan][thisant][thisspwid]  = np.asarray([thistime])
            paramvals[thisscan][thisant][thisspwid] = thisparam
            snrvals[thisscan][thisant][thisspwid]   = thissnr
            flagvals[thisscan][thisant][thisspwid]  = thisflag
    mytb.close()

    #interpolate values
    interp_paramvals = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    interp_snrvals   = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    interp_count     = defaultdict(lambda: defaultdict(dict))
    flag_count       = defaultdict(lambda: defaultdict(dict))
    for scan in timevals.keys():
        for ant in timevals[scan].keys():
            for spwid in timevals[scan][ant].keys():
                interp_count[scan][ant][spwid] = 0
                flag_count[scan][ant][spwid]   = 0
                thesetimes                     = timevals[scan][ant][spwid]
                for i,vals in enumerate(paramvals[scan][ant][spwid]):
                    theseflags                            = np.invert(flagvals[scan][ant][spwid][i])
                    interp_paramvals[scan][ant][spwid][i] = auxiliary.interpolate1d_over_flags(thesetimes, vals, theseflags,
                                                                                               input_bounds_error=out_of_bounds,
                                                                                               pass_single=True
                                                                                              )
                    interp_snrvals[scan][ant][spwid][i]   = auxiliary.interpolate1d_over_flags(thesetimes,
                                                                                               snrvals[scan][ant][spwid][i],
                                                                                               theseflags,
                                                                                               input_bounds_error=out_of_bounds,
                                                                                               pass_single=True
                                                                                              )

    #replace flagged values by interpolation
    mytb.open(_calibrationtable, nomodify=False)
    for _row in row_iter:
        thisflag = copy.deepcopy(mytb.getcell('FLAG', _row))
        if any(thisflag):
            thisant  = mytb.getcell('ANTENNA1', _row)
            thisscan = mytb.getcell('SCAN_NUMBER', _row)
            thistime = mytb.getcell('TIME', _row)
            thisvals = copy.deepcopy(mytb.getcell(param, _row))
            thissnr  = copy.deepcopy(mytb.getcell('SNR', _row))
            thisspw  = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
            for i,flag in enumerate(thisflag):
                interpval = interp_paramvals[thisscan][thisant][thisspw][i]
                if flag and interpval:
                    interp_count[thisscan][thisant][thisspw] += 1
                    try:
                        thisvals[i] = np.asarray([interpval(thistime)])
                        interpsnr   = interp_snrvals[thisscan][thisant][thisspw][i]
                        if interpsnr:
                            snr_filler = interpsnr(thistime)
                        else:
                            snr_filler = 15
                        thissnr[i] = np.asarray([snr_filler])
                        thisflag[i] = np.asarray([False])
                    except ValueError:
                        pass
                elif flag:
                    flag_count[thisscan][thisant][thisspw] += 1
            mytb.putcell(param, _row, thisvals)
            mytb.putcell('SNR', _row, thissnr)
            mytb.putcell('FLAG', _row, thisflag)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    if _debug:
        pltant  = 3
        pltscan = 14
        pltspw  = 0
        pltparm = 1
        qselc   = 'ANTENNA1=={0} && SCAN_NUMBER=={1}'.format(str(pltant), str(pltscan))
        times   = auxiliary.read_CASA_table(False, 'unique TIME', qselc, _tablename=_calibrationtable)
        yvals   = [interp_paramvals[pltscan][pltant][pltspw][pltparm](t) for t in times]
        diagnostics.simple_plotter(times, yvals, 'time', str(pltparm), ' ', 'interpolate_over_flags.png')
        for scan in interp_count.keys():
            for ant in interp_count[scan].keys():
                for spwid in interp_count[scan][ant].keys():
                    thisinterp = interp_count[scan][ant][spwid]
                    thisflag   = flag_count[scan][ant][spwid]
                    infostr1 = 'interp scan {0} ant {1} spw {2}: {3}'.format(str(scan), str(ant), str(spwid),
                                                                             str(thisinterp)
                                                                            )
                    infostr2 = 'flag scan {0} ant {1} spw {2}: {3}'.format(str(scan), str(ant), str(spwid),
                                                                           str(thisflag)
                                                                          )
                    if thisinterp:
                        print(infostr1)
                    if thisflag:
                        print(infostr2)


def linear_fringe_interpolation_across_spws(_calibrationtable, antindx, param='FPARAM', _indices=[2, 6]):
    """
    Simple liner interpolation across spw index numbers, replacing the antname calibration solutions of param
    selected by _indices.
    Assumes that there are only single solutions per scan for the antindx antenna for each index in _indices (solint 'inf').
    Ignores flags and SNR.
    Used to replace bad rate outliers occurring in the APEX solutions for the ALMA mbd-sbd offset.
    """
    mytb = casac.table()
    # Retrieve values to be interpolated.
    paramvals = defaultdict(lambda: defaultdict(dict))
    mytb.open(_calibrationtable)
    row_iter = range(mytb.nrows())
    for _row in row_iter:
        thisant = mytb.getcell('ANTENNA1', _row)
        if int(thisant) != int(antindx):
            continue
        thisscan  = mytb.getcell('SCAN_NUMBER', _row)
        thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        thisparam = mytb.getcell(param, _row)
        for indx in _indices:
            paramvals[thisscan][indx][thisspwid] = thisparam[indx][0]
    mytb.close()
    # Interpolate values.
    for scan in paramvals.keys():
        for indx in paramvals[scan].keys():
            spwindices = list(paramvals[scan][indx].keys())
            thesevals  = list(paramvals[scan][indx].values())
            polycoeffs = np.polyfit(spwindices, thesevals, 1)
            poly1d_fn  = np.poly1d(polycoeffs)
            interpvals = poly1d_fn(spwindices)
            # Replace values and format for CASA.
            for spw, ival in zip(spwindices, interpvals):
                paramvals[scan][indx][spw] = np.asarray([ival])
    # Fill _calibrationtable with the interpolated values.
    mytb.open(_calibrationtable, nomodify=False)
    for _row in row_iter:
        thisant = mytb.getcell('ANTENNA1', _row)
        if int(thisant) != int(antindx):
            continue
        thisscan  = mytb.getcell('SCAN_NUMBER', _row)
        thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        thisparam = mytb.getcell(param, _row)
        for indx in _indices:
            try:
                thisparam[indx] = paramvals[thisscan][indx][thisspwid]
            except KeyError:
                continue
        mytb.putcell(param, _row, thisparam)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def remove_amp_corr(_inp_params, B_caltable):
    """
    Takes a complex B-Jones (bandpass) calibration table and sets all amplitude corrections to unity.
    """
    mytb = casac.table()
    try:
        data = auxiliary.read_CASA_table(_inp_params, 'CPARAM', _tablename=B_caltable)
        for indx, val in np.ndenumerate(data):
            data[indx] = auxiliary.unity_ampltiude_solution(val)
        mytb.open(B_caltable, nomodify=False)
        mytb.putcol('CPARAM', data)
        mytb.flush()
        mytb.done()
        mytb.clearlocks()
    except RuntimeError:
        # Different number of channels in different spws.
        mytb.open(B_caltable, nomodify=False)
        _nrows = mytb.nrows()
        for _row in range(_nrows):
            data = mytb.getcell('CPARAM', _row)
            for i,x in enumerate(data):
                for j,y in enumerate(x):
                    data[i][j] = auxiliary.unity_ampltiude_solution(y)
            mytb.putcell('CPARAM', _row, data)
        mytb.flush()
        mytb.done()
        mytb.clearlocks()


def normalize_amp_corr(B_caltable):
    """
    Takes a complex B-Jones (bandpass) calibration table and normalizes the amplitude solutions per polarization
    by the mean for every row.
    """
    mytb = casac.table()
    mytb.open(B_caltable, nomodify=False)
    _nrows = mytb.nrows()
    for _row in range(_nrows):
        data = copy.deepcopy(mytb.getcell('CPARAM', _row))
        flag = mytb.getcell('FLAG', _row)
        dfg  = np.ma.masked_array(data, flag)
        ravg = np.ma.mean(dfg[0])
        lavg = np.ma.mean(dfg[1])
        data[0] /= ravg
        data[1] /= lavg
        mytb.putcell('CPARAM', _row, data)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def unity_amplitude_based_on_ant(selfcaltable, antIDs_unchanged, unflag_also=False):
    """
    For all ANTENNA1 IDs that are NOT IN a antIDs_unchanged list (integers),
    changes the rows of a selfcaltable to leave amplitudes unchanged.
    """
    antIDs_unchanged = [int(ant) for ant in antIDs_unchanged]
    mytb             = casac.table()
    mytb.open(selfcaltable, nomodify=False)
    _nrows = mytb.nrows()
    for _row in range(_nrows):
        thisant = mytb.getcell('ANTENNA1', _row)
        if int(thisant) not in antIDs_unchanged:
            calval = copy.deepcopy(mytb.getcell('CPARAM', _row))
            for i,val in enumerate(calval):
                calval[i] = np.asarray([auxiliary.unity_ampltiude_solution(val[0])])
            mytb.putcell('CPARAM', _row, calval)
            if unflag_also:
                flagval = copy.deepcopy(mytb.getcell('FLAG', _row))
                mytb.putcell('FLAG', _row, np.zeros_like(flagval))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def constrain_gains(selfcaltable, station_constraints, station_namelist, data_inttime, cumulative_tables):
    """
    Takes a amplitude (mode 'a', type 'G') selfcaltable from a gaincal run and removes amplitude corrections that are too
    extreme. Assumes that flagged solutions have been removed.
    Bounds for amplitude correction factors are set by the station_constraints input dict, with station names as keys. It can
    also be a dict of dicts to specify distinct bounds for individual scans. The bounds are imposed on selfcaltable, relative to
    all gains from cumulative_tables.
    station_namelist is used as a list of station names as strings in the order of antenna IDs.
    data_inttime should give the basic data integration time (accumulation period), which is used to put the amplitude
    correction on a fine grid when computing the cumulative corrections.
    """
    mytb            = casac.table()
    cumulative_corr = []
    regrid_time     = data_inttime/2
    for ctab in cumulative_tables:
        cumulative_corr.append(auxiliary.extract_amp_selfcalsols(ctab))
    this_ampsc        = auxiliary.extract_amp_selfcalsols(selfcaltable)
    constrained_gains = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    for scan in this_ampsc.keys():
        for ant in this_ampsc[scan].keys():
            for spwid in this_ampsc[scan][ant].keys():
                for polid in this_ampsc[scan][ant][spwid].keys():
                    this_timevals  = [t[0] for t in this_ampsc[scan][ant][spwid][polid]]
                    time_bounds    = [min(this_timevals), max(this_timevals)]
                    this_ampvals   = [a[1] for a in this_ampsc[scan][ant][spwid][polid]]
                    t_grid, a_grid = auxiliary.regrid_1D_fine(this_timevals, this_ampvals, regrid_time, time_bounds, True)
                    #get grid with all cumulative corrections taken into account
                    other_grid = None
                    for other_ampsc in cumulative_corr:
                        try:
                            other_timevals = [t[0] for t in other_ampsc[scan][ant][spwid][polid]]
                            other_ampvals  = [a[1] for a in other_ampsc[scan][ant][spwid][polid]]
                            try:
                                other_grid *= auxiliary.regrid_1D_fine(other_timevals, other_ampvals, regrid_time, time_bounds)
                            except TypeError:
                                other_grid = auxiliary.regrid_1D_fine(other_timevals, other_ampvals, regrid_time, time_bounds)
                        except KeyError:
                            pass
                    #smooth gains to lie within station_constraints
                    st_name = station_namelist[int(ant)]
                    if st_name in station_constraints.keys():
                        try:
                            perc_gain_var = station_constraints[st_name][int(scan)]
                        except (TypeError, KeyError) as _:
                            perc_gain_var = station_constraints[st_name]
                        if isinstance(perc_gain_var, dict):
                            raise ValueError('No gain constraints given for scan {0} for the {1} station'.format(str(scan),
                                                                                                                 st_name)
                                            )
                        max_allowed_gain = 1. + perc_gain_var/100.
                        min_allowed_gain = 1./max_allowed_gain
                        if other_grid is not None:
                            a_grid        *= other_grid
                            max_other_gain = max(other_grid)
                            min_other_gain = min(other_grid)
                            if max_other_gain > 1:
                                max_allowed_gain = min(max_allowed_gain, max_allowed_gain / max_other_gain)
                            if min_other_gain < 1:
                                min_allowed_gain = max(min_allowed_gain, min_allowed_gain / min_other_gain)
                        if max_allowed_gain < 1:
                            max_allowed_gain = 1.001
                        if min_allowed_gain > 1:
                            min_allowed_gain = 0.999
                        max_present_gain = max(a_grid)
                        min_present_gain = min(a_grid)
                        if max_present_gain > max_allowed_gain:
                            correction_factor = max_present_gain / max_allowed_gain
                            for i, gain_val in enumerate(a_grid):
                                if gain_val > max_allowed_gain:
                                    a_grid[i] /= correction_factor
                        if min_present_gain < min_allowed_gain:
                            correction_factor = min_present_gain / min_allowed_gain
                            for i, gain_val in enumerate(a_grid):
                                if gain_val < min_allowed_gain:
                                    a_grid[i] /= correction_factor
                        #fix points that got pushed out of bounds
                        for i, gain_val in enumerate(a_grid):
                            if gain_val > max_allowed_gain or gain_val < min_allowed_gain:
                                a_grid[i] = 1.
                    constrained_gains[scan][ant][spwid][polid] = auxiliary.interpolate1d_single(t_grid, a_grid)
    mytb.open(selfcaltable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        thisscan  = mytb.getcell('SCAN_NUMBER', row)
        thisant   = mytb.getcell('ANTENNA1', row)
        thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', row)
        thistime  = mytb.getcell('TIME', row)
        calval    = copy.deepcopy(mytb.getcell('CPARAM', row))
        for i,val in enumerate(calval):
            calval[i] = np.asarray([(complex(constrained_gains[thisscan][thisant][thisspwid][i](thistime), 0))])
        mytb.putcell('CPARAM', row, calval)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def mod_MS_antwt(ms_name, antwt_dict):
    """
    Multiplies weights on baselines according to station names and values given in antwt_dict.
    Used by the interactive imager via the station_weights input parameter.
    """
    antwt_dict_ID = auxiliary.station_NamesToIds(ms_name, antwt_dict)
    mod_stations  = list(antwt_dict_ID.keys())
    mytb          = casac.table()
    mytb.open(ms_name, nomodify=False)
    for row in range(mytb.nrows()):
        ant1 = int(mytb.getcell('ANTENNA1', row))
        ant2 = int(mytb.getcell('ANTENNA2', row))
        if ant1 in mod_stations:
            thisant = ant1
        elif ant2 in mod_stations:
            thisant = ant2
        else:
            continue
        thiswt = copy.deepcopy(mytb.getcell('WEIGHT', row))
        thiswt *= antwt_dict_ID[thisant]
        mytb.putcell('WEIGHT', row, thiswt)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def set_uvzero_modelflux(ms_name, uvmax, flux0):
    """
    For all rows in the MS where the u-v distance is less than uvmax, set the RR and LL fluxes to (flux0,0) in the
    MODEL_DATA column.
    """
    mytb = casac.table()
    mytb.open(ms_name, nomodify=False)
    nrows = mytb.nrows()
    cmod  = (flux0 + 0j)
    for row in range(nrows):
        uvw = mytb.getcell('UVW', row)
        if np.sqrt(uvw[0]**2 + uvw[1]**2) < uvmax:
            model_data        = copy.deepcopy(mytb.getcell('MODEL_DATA', row))
            model_data[0][0]  = cmod
            model_data[-1][0] = cmod
            mytb.putcell('MODEL_DATA', row, model_data)


def avg_solutions(_calibrationtable, _indices, _targetcol='FPARAM', _weightcol='SNR'):
    """
    Takes a _calibrationtable and averages entries in _targetcol by _weightcol.
    The entries that will be averaged are taken from all values in the _indices input array which are valid indices in _targetcol.
    Modifies _calibrationtable directly.
    Example usage: Replace the RCP and LCP rates by their combined median (necessary for polarization calibration).
    """
    mytb = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    _nrows = mytb.nrows()
    for _row in range(_nrows):
        valcell       = mytb.getcell(_targetcol, _row)
        weightcell    = mytb.getcell(_weightcol, _row)
        weighted_mean = 0.
        snr_sum       = 0.
        for _index in _indices:
            try:
                thisval    = valcell[_index]
                thisweight = weightcell[_index]
                if not np.isnan(thisval):
                    if np.isnan(abs(thisweight)):
                        thisweight = 1.
                    weighted_mean += thisval * thisweight
                    snr_sum       += thisweight
            #Exception may be for single pol stations:
            except IndexError:
                pass
        #flagged solutions:
        if not snr_sum:
            snr_sum = 1.
        weighted_mean /= snr_sum
        #do nothing for completely invalid (nan) solutions:
        if weighted_mean:
            new_valcell = copy.deepcopy(valcell)
            for _index in _indices:
                try:
                    new_valcell[_index] = weighted_mean
                except IndexError:
                    pass
            mytb.putcell(_targetcol, _row, new_valcell)
    mytb.flush()
    mytb.done()


def avg_solutions_over_scans(_ms_metadata, _calibrationtable, _indices, stationname, _targetcol='FPARAM', _weightcol='SNR'):
    """
    Per spw, average values in _targetcol for the given _indices, weighted by _weightcol for all scans for a provided
    stationname.
    """
    mytb = casac.table()
    mytb.open(_calibrationtable)
    station_id    = int(_ms_metadata.yield_antID(stationname))
    _nrows        = mytb.nrows()
    weighted_mean = defaultdict(dict)
    snr_sum       = defaultdict(dict)
    for _row in range(_nrows):
        this_station = mytb.getcell('ANTENNA1', _row)
        if int(this_station) != station_id:
            continue
        valcell    = mytb.getcell(_targetcol, _row)
        weightcell = mytb.getcell(_weightcol, _row)
        spwcell    = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        for _index in _indices:
            try:
                thisval    = valcell[_index]
                thisweight = weightcell[_index]
                if not np.isnan(thisval):
                    if np.isnan(abs(thisweight)):
                        thisweight = 1.
                    try:
                        weighted_mean[_index][spwcell] += thisval * thisweight
                        snr_sum[_index][spwcell]       += thisweight
                    except KeyError:
                        weighted_mean[_index][spwcell] = thisval * thisweight
                        snr_sum[_index][spwcell]       = thisweight
            #Exception may be for single pol stations:
            except IndexError:
                pass
    mytb.done()
    mytb.clearlocks()
    for _index in weighted_mean.keys():
        for spwcell in weighted_mean[_index].keys():
            this_sum  = snr_sum[_index][spwcell]
            this_mean = weighted_mean[_index][spwcell]
            #fully flagged solutions:
            if not this_sum:
                this_sum = 1.
            this_mean /= this_sum
            #do nothing for completely invalid (nan) solutions:
            if this_mean:
                weighted_mean[_index][spwcell] = this_mean
    mytb.open(_calibrationtable, nomodify=False)
    for _row in range(_nrows):
        this_station = mytb.getcell('ANTENNA1', _row)
        if int(this_station) != station_id:
            continue
        spwcell     = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        valcell     = mytb.getcell(_targetcol, _row)
        weightcell  = mytb.getcell(_weightcol, _row)
        new_valcell = copy.deepcopy(valcell)
        for _index in _indices:
            try:
                new_valcell[_index] = weighted_mean[_index][spwcell]
            except IndexError:
                pass
            mytb.putcell(_targetcol, _row, new_valcell)
            mytb.putcell(_weightcol, _row, np.full_like(weightcell,100))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def median_all_scans(_inp_params, _ms_metadata, _calibrationtable, _targetcol='CPARAM', ignoreval=0+0j):
    """
    Per antenna and spw: fills _targetcol solutions with median over all scans.
    """
    mytb = casac.table()
    all_ants = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename=_calibrationtable)
    all_vals = defaultdict(dict)
    for ant in all_ants:
        for spw in _ms_metadata.all_spwds:
            these_vals = auxiliary.read_CASA_table(_inp_params, _targetcol,
                                                   'ANTENNA1=={0} && SPECTRAL_WINDOW_ID=={1}'.format(str(ant), str(spw)),
                                                   _tablename=_calibrationtable)
            these_vals = np.ma.masked_equal(these_vals, ignoreval)
            these_vals = np.ma.median(these_vals, -1)
            #avg median vals over all scans:
            all_vals[str(ant)][str(spw)] = np.ma.filled(these_vals, ignoreval)
    mytb.open(_calibrationtable, nomodify=False)
    _nrows = mytb.nrows()
    for _row in range(_nrows):
        this_station = mytb.getcell('ANTENNA1', _row)
        this_spw     = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        try:
            these_vals = all_vals[str(this_station)][str(this_spw)]
            mytb.putcell(_targetcol, _row, these_vals)
        except KeyError:
            pass
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def avg_caltables(_inp_params, caltables, outtable, _targetcol='pick', mode='', pass_missing=False):
    """
    Takes caltables as input and averages all solutions into a single output table.
    Averages either the CPARAM or FPARAM columns (depending which is present) unless _targetcol is explicitly specified.
    Averages weighted by SNR by default, unless mode='med_bp', where the median from solutions of bandpass tables will be picked.
    All caltables must have the same length (solved by same task using different sources with combine='scan').
    Overwrites outtable if it already exists and deletes the original caltables.
    """
    if not isinstance(caltables, list):
        caltables = [caltables]
    existing_tables = []
    for caltb in caltables:
        if auxiliary.isdir(caltb):
            auxiliary.check_if_table_is_written(caltb)
            existing_tables.append(caltb)
    if not existing_tables:
        if pass_missing:
            return
        else:
            _err = 'Cannot average non-existing tables: ' + str(caltables)
            _err+= '. Check the input parameters of your last calibration step.'
            raise ValueError(_err)
    auxiliary.rm_dir_if_present(outtable)
    if len(existing_tables)<2:
        shutil.move(existing_tables[0], outtable)
        return
    tcols = []
    mytb  = casac.table()
    if _targetcol == 'pick':
        for caltb in existing_tables:
            mytb.open(caltb)
            _colnames = mytb.colnames()
            if 'CPARAM' in _colnames:
                tcol = 'CPARAM'
            elif 'FPARAM' in _colnames:
                tcol = 'FPARAM'
            else:
                raise RuntimeError('Cannot smooth ' + caltb + ' because neither CPARAM nor FPARAM columns are present.')
            tcols.append(tcol)
        mytb.done()
        if len(np.unique(tcols)) != 1:
            raise ValueError('Not all caltables have the same column structure so averaging cannot be done. Got ' + str(tcols))
        _targetcol = tcols[0]
    nrows = []
    for caltb in existing_tables:
        mytb.open(caltb)
        nrows.append(mytb.nrows())
        mytb.close()
    if len(np.unique(nrows)) != 1:
        raise ValueError('Not all caltables have the same number of rows. Got ' + str(nrows))
    mytb.open(existing_tables[0])
    mytb.copy(outtable, deep=True)
    mytb.flush()
    mytb.done()
    if mode=='med_bp':
        avg_bptbs_by_median(_inp_params, existing_tables, outtable)
    else:
        avg_caltbs_by_snr(_inp_params, existing_tables, outtable, _targetcol)
    for caltb in existing_tables:
        auxiliary.rm_dir_if_present(caltb)


def avg_bptbs_by_median(_inp_params, existing_tables, outtable):
    """
    *Unused for now: The normal bandpass task with combine='scan' is good enough if there are enough scans.*
    Used by avg_caltables(). existing_tables must be single-scan bandpass tables.
    """
    mytb = casac.table()
    #handle flags as in avg_caltbs_by_snr()
    flag = auxiliary.read_CASA_table(_inp_params, 'FLAG', _tablename=existing_tables[0])
    flag = np.invert(flag)
    for caltb in existing_tables[1:]:
        thisflag = auxiliary.read_CASA_table(_inp_params, 'FLAG', _tablename=caltb)
        flag     += np.invert(thisflag)
    flag = np.invert(flag)
    mytb.open(outtable, nomodify=False)
    mytb.putcol('FLAG', flag)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    _targetcol      = 'CPARAM'
    unique_spws     = sorted(auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', _tablename=existing_tables[0]))
    unique_antennas = sorted(auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename=existing_tables[0]))
    cparam          = defaultdict(lambda: defaultdict(dict))
    for spw in unique_spws:
        for ant in unique_antennas:
            cparam[str(spw)][str(ant)][0] = []
            cparam[str(spw)][str(ant)][1] = []
    for caltb in existing_tables:
        for spw in unique_spws:
            for ant in unique_antennas:
                qcrit    = 'SPECTRAL_WINDOW_ID=={0} && ANTENNA1=={1}'.format(str(spw), str(ant))
                thisdata = auxiliary.read_CASA_table(_inp_params, _targetcol, qcrit, _tablename=caltb)
                thisflag = auxiliary.read_CASA_table(_inp_params, 'FLAG', qcrit, _tablename=caltb)
                gooddata = np.ma.masked_array(thisdata, thisflag)
                cparam[str(spw)][str(ant)][0].append(gooddata[0])
                cparam[str(spw)][str(ant)][1].append(gooddata[1])
    for spw in unique_spws:
        for ant in unique_antennas:
            cparam[str(spw)][str(ant)][0] = np.ma.median(cparam[str(spw)][str(ant)][0], axis=0)
            cparam[str(spw)][str(ant)][1] = np.ma.median(cparam[str(spw)][str(ant)][1], axis=0)
    mytb.open(outtable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        thisflag = mytb.getcell('FLAG', row)
        mytb.putcell('FLAG', row, np.zeros_like(thisflag))
        thisspw = mytb.getcell('SPECTRAL_WINDOW_ID', row)
        thisant = mytb.getcell('ANTENNA1', row)
        vals    = [cparam[str(thisspw)][str(thisant)][0], cparam[str(thisspw)][str(thisant)][1]]
        mytb.putcell('CPARAM', row, np.squeeze(np.asarray(vals)))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def avg_caltbs_by_snr(_inp_params, existing_tables, outtable, _targetcol):
    """ Used by avg_caltables(). """
    mytb    = casac.table()
    flag    = auxiliary.read_CASA_table(_inp_params, 'FLAG', _tablename=existing_tables[0])
    avgdata = auxiliary.read_CASA_table(_inp_params, _targetcol, _tablename=existing_tables[0])
    avgdata = np.ma.masked_array(avgdata, flag)
    #invert flag logic: summing up must amount to False (no flag) if any individual entry was False
    flag    = np.invert(flag)
    snr     = auxiliary.read_CASA_table(_inp_params, 'SNR', _tablename=existing_tables[0])
    hassnr  = snr.any()
    if hassnr:
        avgdata *= snr
    for caltb in existing_tables[1:]:
        thisflag = auxiliary.read_CASA_table(_inp_params, 'FLAG', _tablename=caltb)
        thisdata = auxiliary.read_CASA_table(_inp_params, _targetcol, _tablename=caltb)
        thisdata = np.ma.masked_array(thisdata, thisflag)
        flag     += np.invert(thisflag)
        if hassnr:
            thissnr  = auxiliary.read_CASA_table(_inp_params, 'SNR', _tablename=caltb)
            thisdata *= thissnr
            snr      += thissnr
        avgdata += thisdata
    if hassnr:
        avgdata /= snr
    else:
        avgdata /= float(len(existing_tables))
    flag = np.invert(flag)
    mytb.open(outtable, nomodify=False)
    mytb.putcol('FLAG', flag)
    mytb.putcol(_targetcol, avgdata)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def concat_spwpartition_tables(_inp_params, caltablist, outtablename):
    """
    Takes a list of calibration tables and concatenates them into a single output table with a given outtablename.
    Used when a calibration step is perferomed multiple times for different sets of spectral windows (see spwpartition parameter)
    and a sinlge talbe is to be applied to all data in the MS.
    """
    mytb = casac.table()
    if isinstance(caltablist, str):
        caltablist = [caltablist]
    caltablist_orig = list(caltablist)
    caltablist      = []
    for caltb in caltablist_orig:
        if auxiliary.isdir(caltb):
            caltablist.append(caltb)
    if not caltablist:
        return
    tables_sorted_by_spw = {}
    chan_freqs           = {}
    for caltb in caltablist:
        these_freqs = auxiliary.read_CASA_table(_inp_params, 'CHAN_FREQ', _tablename=caltb+'/SPECTRAL_WINDOW/')[0]
        these_spws  = auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', _tablename=caltb)
        minspw      = these_spws[0]
        for spw in these_spws:
            chan_freqs[spw] = these_freqs[spw]
        tables_sorted_by_spw[int(minspw)] = caltb
    spws_sorted = sorted(list(tables_sorted_by_spw.keys()))
    first_table = tables_sorted_by_spw[spws_sorted[0]]
    #Find row insertion points after each SCAN_NUMBER:
    mytb.open(first_table)
    first_nrows         = mytb.nrows()
    insertion_points    = {}
    previous_scannumber = mytb.getcell('SCAN_NUMBER', 0)
    for _row in range(first_nrows):
        this_scannumber = mytb.getcell('SCAN_NUMBER', _row)
        if this_scannumber != previous_scannumber:
            insertion_points[previous_scannumber] = _row - 1
            previous_scannumber                   = this_scannumber
    insertion_points[this_scannumber] = first_nrows
    mytb.done()
    mytb.clearlocks()
    #Table rows must be sorted by SCAN_NUMBER
    reverse_scan_numbers = sorted(list(insertion_points.keys()))[::-1]
    #Insert rows from all other tables for each SCAN_NUMBER:
    #Unfortunately, copyrows to a specific location is not working so everything needs to be appended at the last row.
    #for scan_number in reverse_scan_numbers:
    #    for ctb_spw in spws_sorted[1:]:
    #        this_caltb = tables_sorted_by_spw[ctb_spw]
    #        these_scans = auxiliary.read_CASA_table(_inp_params, 'SCAN_NUMBER', _tablename=this_caltb)
    #        if  scan_number not in these_scans:
    #            continue
    #        these_scanrows = np.argwhere(these_scans==scan_number)
    #        first_scanrow  = these_scanrows[0][0]
    #        num_scanrow    = these_scanrows[-1][0] - first_scanrow
    #        mytb.open(this_caltb, nomodify=False)
    #        mytb.copyrows(first_table, startrowin=first_scanrow, startrowout=insertion_points[scan_number], nrow=num_scanrow)
    #        mytb.flush()
    #        mytb.done()
    #        mytb.clearlocks()
    #        #Take into account the number of rows already copied for this SCAN_NUMBER for the next calibration table.
    #        insertion_points[scan_number] = insertion_points[scan_number] + num_scanrow
    for ctb_spw in spws_sorted[1:]:
        this_caltb = tables_sorted_by_spw[ctb_spw]
        mytb.open(this_caltb, nomodify=False)
        mytb.copyrows(first_table)
        mytb.flush()
        mytb.done()
        mytb.clearlocks()
    #Set the correct channel (reference) frequencies:
    mytb.open(first_table+'/SPECTRAL_WINDOW/', nomodify=False)
    these_chan_freqs = copy.deepcopy(mytb.getcol('CHAN_FREQ'))
    for spw_id in chan_freqs:
        these_chan_freqs[0][spw_id] = chan_freqs[spw_id]
    mytb.putcol('CHAN_FREQ', these_chan_freqs)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    auxiliary.rm_dir_if_present(outtablename)
    shutil.move(first_table, outtablename)
    for caltb in caltablist:
        auxiliary.rm_dir_if_present(caltb)


def separate_fringe_solutions_by_refant(_inp_params, _ms_metadata, caltable):
    """
    *unused*
    Will separate single-band cal solutions per reference station by filling each empty scan by interpolation between values
    from scans with the same refant. Writes zeroes when no solutions for that scan's refant are present in caltable.
    Assumes that single flagged solutions correspond to fully flagged solutions of (antenna, spw) and will keep them flagged.
    Adjusts caltable directly.
    """
    debug        = False
    mytb         = casac.table()
    unique_scans = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename=caltable)
    unique_ant2  = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA2', _tablename=caltable)
    unique_spw_per_ant2 = {}
    for ant2 in unique_ant2:
        selector                  = 'ANTENNA2=='+str(ant2)
        unique_spw_per_ant2[ant2] = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA2', selector, _tablename=caltable)

    #get values
    refvals     = {}
    replacevals = {}
    timevals    = defaultdict(lambda: defaultdict(dict))
    paramvals   = defaultdict(lambda: defaultdict(dict))
    snrvals     = defaultdict(lambda: defaultdict(dict))
    flagvals    = defaultdict(lambda: defaultdict(dict))
    mytb.open(caltable)
    refvals['INTERVAL']       = mytb.getcell('INTERVAL',0)
    refvals['OBSERVATION_ID'] = mytb.getcell('OBSERVATION_ID', 0)
    refvals['PARAMERR']       = mytb.getcell('PARAMERR', 0)
    unflagged_solution        = np.zeros_like(mytb.getcell('FLAG', 0))
    replacevals['FPARAM']     = mytb.getcell('FPARAM', 0)
    replacevals['SNR']        = mytb.getcell('SNR', 0)
    nrows_orig                = mytb.nrows()
    for _row in range(nrows_orig):
        thisant2  = mytb.getcell('ANTENNA2', _row)
        thisant   = mytb.getcell('ANTENNA1', _row)
        thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        thistime  = mytb.getcell('TIME', _row)
        thisparam = mytb.getcell('FPARAM', _row)
        thissnr   = mytb.getcell('SNR', _row)
        thisflag  = mytb.getcell('FLAG', _row)
        try:
            timevals[thisant2][thisant][thisspwid]  = np.append(timevals[thisant2][thisant][thisspwid], thistime)
            paramvals[thisant2][thisant][thisspwid] = np.hstack([paramvals[thisant2][thisant][thisspwid], thisparam])
            snrvals[thisant2][thisant][thisspwid]   = np.hstack([snrvals[thisant2][thisant][thisspwid], thissnr])
        except KeyError:
            timevals[thisant2][thisant][thisspwid]  = np.asarray([thistime])
            paramvals[thisant2][thisant][thisspwid] = thisparam
            snrvals[thisant2][thisant][thisspwid]   = thissnr
        if thisflag.all():
            flagvals[thisant2][thisant][thisspwid]  = thisflag
    mytb.close()
    if debug:
        print('\n\n -- got these values:')
        print(timevals)
        print(paramvals)
        print(snrvals)
        print(flagvals)

    #interpolate
    all_ants         = []
    all_spwids       = []
    interp_paramvals = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    interp_snrvals   = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    for ant2 in timevals.keys():
        ants_present = list(timevals[ant2].keys())
        if len(ants_present) > len(all_ants):
            all_ants = ants_present
        for ant in ants_present:
            spwids_present = list(timevals[ant2][ant].keys())
            if len(spwids_present) > len(all_spwids):
                all_spwids = spwids_present
            for spwid in spwids_present:
                thesetimes = timevals[ant2][ant][spwid]
                for i,theseparams in enumerate(paramvals[ant2][ant][spwid]):
                    thesesnrs                             = snrvals[ant2][ant][spwid][i]
                    interp_snrvals[ant2][ant][spwid][i]   = auxiliary.interpolate1d_single(thesetimes, thesesnrs)
                    interp_paramvals[ant2][ant][spwid][i] = auxiliary.interpolate1d_single(thesetimes, theseparams)

    missing_scans = auxiliary.subtract_list(_ms_metadata.all_scans, [str(scan) for scan in unique_scans])
    add_Nrows     = int(len(missing_scans) * len(all_ants) * len(all_spwids))
    mytb.open(caltable, nomodify=False)
    mytb.addrows(add_Nrows)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    if debug:
        print('\n\n -- interpolated these values:')
        print(missing_scans)
        print(all_ants)
        print(all_spwids)
        print(nrows_orig, add_Nrows)
        print(interp_paramvals)
        print(interp_snrvals)

    #fill all missing scans
    rownum = nrows_orig
    mytb.open(caltable, nomodify=False)
    for scan_iter,scan in enumerate(missing_scans):
        thisscan = int(scan)
        for ant_iter,thisant in enumerate(all_ants):
            for spwid_iter, thisspwid in enumerate(all_spwids):
                thisparam   = copy.deepcopy(replacevals['FPARAM'])
                thissnr     = copy.deepcopy(replacevals['SNR'])
                thisfieldID = int(_ms_metadata.yield_sourceID(_ms_metadata.yield_sourcename_from_scan(scan)))
                thisrefant  = int(_ms_metadata.yield_antID(get_refant(_inp_params, _ms_metadata, scan)))
                thistime    = _ms_metadata.yield_scantime(scan)
                for param_iter, _ in enumerate(thisparam):
                    try:
                        fill_param = interp_paramvals[thisrefant][thisant][thisspwid][param_iter](thistime)
                        fill_snr   = interp_snrvals[thisrefant][thisant][thisspwid][param_iter](thistime)
                    except KeyError:
                        #no calibration solutions present
                        if debug:
                            print('\n -- no calib solutions for:')
                            print(thisrefant, thisant, thisspwid, thisscan)
                        fill_param = 0.
                        fill_snr   = 10.
                    thisparam[param_iter] = np.asarray([fill_param])
                    thissnr[param_iter]   = np.asarray([fill_snr])
                mytb.putcell('SCAN_NUMBER', rownum, thisscan)
                mytb.putcell('ANTENNA1', rownum, thisant)
                mytb.putcell('ANTENNA2', rownum, thisrefant)
                mytb.putcell('SPECTRAL_WINDOW_ID', rownum, thisspwid)
                mytb.putcell('FPARAM', rownum, thisparam)
                mytb.putcell('SNR', rownum, thissnr)
                mytb.putcell('FIELD_ID', rownum, thisfieldID)
                mytb.putcell('TIME', rownum, thistime)
                for column in refvals.keys():
                    mytb.putcell(column, rownum, refvals[column])
                try:
                    theseflags = flagvals[thisrefant][thisant][thisspwid]
                    if debug:
                        print('\n -- flagging:')
                        print(thisrefant, thisant, thisspwid, thisscan)
                except KeyError:
                    theseflags = unflagged_solution
                mytb.putcell('FLAG', rownum, theseflags)
                rownum += 1
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def skip_failed_solutions(_inp_params, _ms_metadata, caltable, param='CPARAM'):
    """
    Replaces rows of a caltable where too many solutions have failed with unit gain solutions without flags (benign).
    Works only with bandpass tables at the moment.
    """
    if not auxiliary.is_set(_inp_params, 'flagged_calib_thresh'):
        return
    mytb  = casac.table()
    mytb.open(caltable, nomodify=False)
    nrows = mytb.nrows()
    for _row in range(nrows):
        this_spw     = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        these_flags  = mytb.getcell('FLAG', _row)
        flagamount   = True_fraction(these_flags)
        flag_allowed = flagging_algorithm.nominal_spw_flagperc(_inp_params, _ms_metadata, this_spw)
        if flagamount - flag_allowed > _inp_params.flagged_calib_thresh:
            these_flags.fill(False)
            calib_sols = mytb.getcell(param, _row)
            calib_sols.fill(1+0j)
            mytb.putcell('FLAG', _row, these_flags)
            mytb.putcell(param, _row, calib_sols)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def overwrite_solutions(_inp_params, _ms_metadata, table_donor, table_acceptor, replace_ants, replace_scans=[], param='FPARAM'):
    """
    Similar to replace_solutions(), but this function will overwrite the solutions from table_acceptor by
    table_donor by interpolation (also over flagged values).
    The advantages are that the number of rows and their association to a scan and antenna are conserved and
    that the timestamps are kept consistent for different antennas in each scan.
    """
    mytb         = casac.table()
    all_ants     = _ms_metadata.antenna_set
    replace_ants = copy.deepcopy(replace_ants)
    if isinstance(replace_ants, dict):
        pass
    elif isinstance(replace_ants, list):
        replace_ants = [_ms_metadata.yield_antID(rant) for rant in replace_ants]
        ant_array    = copy.deepcopy(replace_ants)
        replace_ants = {}
        all_scans    = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename=table_donor)
        for scan in all_scans:
            replace_ants[str(scan)] = [str(ant) for ant in ant_array]
    else:
        raise ValueError('replace_ants must be a list or dict. Got ' + str(replace_ants))
    for scan in replace_ants.keys():
        replace_ants[scan] = [str(_ms_metadata.yield_antID(rant)) for rant in replace_ants[scan]]
        for ant in replace_ants[scan]:
            if str(ant) not in [str(ant) for ant in all_ants]:
                raise ValueError(str(ant) + ' in replace_ants is not a valid antenna. Must be one of ' + str(all_ants))

    #retrieve values from table_donor that can be interpolated into table_acceptor
    timevals      = defaultdict(lambda: defaultdict(dict))
    ant2vals      = defaultdict(lambda: defaultdict(dict))
    paramvals     = defaultdict(lambda: defaultdict(dict))
    snrvals       = defaultdict(lambda: defaultdict(dict))
    flagvals      = defaultdict(lambda: defaultdict(dict))
    selected_ant  = []
    selected_scan = []
    selected_spw  = []
    mytb.open(table_donor)
    for _row in range(mytb.nrows()):
        thisant   = mytb.getcell('ANTENNA1', _row)
        thisscan  = mytb.getcell('SCAN_NUMBER', _row)
        donor_row = False
        if any(replace_scans):
            if str(thisscan) in replace_scans and str(thisant) in replace_ants[str(thisscan)]:
                donor_row = True
        elif str(thisant) in replace_ants[str(thisscan)]:
            donor_row = True
        if donor_row:
            thistime  = mytb.getcell('TIME', _row)
            thisflag  = mytb.getcell('FLAG', _row)
            thisparam = mytb.getcell(param, _row)
            thissnr   = mytb.getcell('SNR', _row)
            thisspwid = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
            thisant2  = mytb.getcell('ANTENNA2', _row)
            try:
                timevals[thisscan][thisant][thisspwid]  = np.append(timevals[thisscan][thisant][thisspwid], thistime)
                ant2vals[thisscan][thisant][thisspwid]  = np.append(ant2vals[thisscan][thisant][thisspwid], thisant2)
                paramvals[thisscan][thisant][thisspwid] = np.hstack([paramvals[thisscan][thisant][thisspwid], thisparam])
                snrvals[thisscan][thisant][thisspwid]   = np.hstack([snrvals[thisscan][thisant][thisspwid], thissnr])
                flagvals[thisscan][thisant][thisspwid]  = np.hstack([flagvals[thisscan][thisant][thisspwid], thisflag])
            except KeyError:
                timevals[thisscan][thisant][thisspwid]  = np.asarray([thistime])
                ant2vals[thisscan][thisant][thisspwid]  = np.asarray([thisant2])
                paramvals[thisscan][thisant][thisspwid] = thisparam
                snrvals[thisscan][thisant][thisspwid]   = thissnr
                flagvals[thisscan][thisant][thisspwid]  = thisflag
            if thisant not in selected_ant:
                selected_ant.append(thisant)
            if thisscan not in selected_scan:
                selected_scan.append(thisscan)
            if thisspwid not in selected_spw:
                selected_spw.append(thisspwid)
    mytb.close()

    #interpolate values
    interp_paramvals = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    interp_snrvals   = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    interp_flagvals  = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    for scan in selected_scan:
        for ant in selected_ant:
            for spwid in selected_spw:
                try:
                    thesetimes = timevals[scan][ant][spwid]
                except KeyError:
                    continue
                singleval  = False
                novalp1    = False
                novalp2    = False
                noflagsum  = 0
                for vals in np.invert(flagvals[scan][ant][spwid]):
                    thisflagsum = sum(vals)
                    if thisflagsum > noflagsum:
                        bestflags = vals
                        noflagsum = thisflagsum
                for i,vals in enumerate(paramvals[scan][ant][spwid]):
                    theseflags                            = np.invert(flagvals[scan][ant][spwid][i])
                    interp_paramvals[scan][ant][spwid][i] = auxiliary.interpolate1d_over_flags(thesetimes, vals, theseflags)
                    if isinstance(interp_paramvals[scan][ant][spwid][i], bool):
                        if any(theseflags):
                            goodvals = vals[theseflags]
                            if len(goodvals)>1:
                                raise ValueError('This should not have happened. Please contact the developer.')
                            interp_paramvals[scan][ant][spwid][i] = goodvals
                            interp_snrvals[scan][ant][spwid][i]   = snrvals[scan][ant][spwid][i][theseflags]
                            invert_flagvals                       = np.invert(flagvals[scan][ant][spwid][i])[theseflags]
                            interp_flagvals[scan][ant][spwid][i]  = np.invert(invert_flagvals)
                            singleval                             = True
                        else:
                            if i<_inp_params.F_01LEN/2:
                                novalp1 = True
                            else:
                                novalp2 = True
                    else:
                        interp_snrvals[scan][ant][spwid][i]  = auxiliary.interpolate1d_over_flags(thesetimes,
                                                                                                  snrvals[scan][ant][spwid][i],
                                                                                                  theseflags
                                                                                                 )
                        interp_flagvals[scan][ant][spwid][i] = auxiliary.interpolate1d_over_flags(thesetimes,
                                                                                                  flagvals[scan][ant][spwid][i],
                                                                                                  theseflags
                                                                                                 )
                if singleval:
                    ant2vals[scan][ant][spwid] = ant2vals[scan][ant][spwid][bestflags][0]
                    if _inp_params.verbose:
                        print('    Only single value for scan {0}, ant {1}, spw {2}'.format(str(scan), str(ant), str(spwid)))
                        print('    -> No interpolation for this calibration data.')
                elif not (novalp1 and novalp2):
                    ant2vals[scan][ant][spwid] = auxiliary.interpolate1d_over_flags(thesetimes,
                                                                                    ant2vals[scan][ant][spwid],
                                                                                    bestflags, kind='nearest'
                                                                                   )
                if _inp_params.verbose:
                    if novalp1:
                        print('    All values are flagged for scan {0}, ant {1}, spw {2}, pol 1'.format(str(scan), str(ant),
                                                                                                        str(spwid))
                             )
                        print('    -> Calibration data left flagged.')
                    if novalp2:
                        print('    All values are flagged for scan {0}, ant {1}, spw {2}, pol 2'.format(str(scan), str(ant),
                                                                                                        str(spwid))
                             )
                        print('    -> Calibration data left flagged.')

    #replace values within rows of table_acceptor with interpolation of values obtained above from table_donor
    mytb.open(table_acceptor, nomodify=False)
    for _row in range(mytb.nrows()):
        thisant      = mytb.getcell('ANTENNA1', _row)
        thisscan     = mytb.getcell('SCAN_NUMBER', _row)
        acceptor_row = False
        if any(replace_scans):
            if str(thisscan) in replace_scans and str(thisant) in replace_ants[str(thisscan)]:
                acceptor_row = True
        elif str(thisant) in replace_ants[str(thisscan)]:
            acceptor_row = True
        if acceptor_row:
            thistime = mytb.getcell('TIME', _row)
            thisspw  = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
            thisvals = copy.deepcopy(mytb.getcell(param, _row))
            thissnr  = copy.deepcopy(mytb.getcell('SNR', _row))
            thisflag = copy.deepcopy(mytb.getcell('FLAG', _row))
            thisant2 = copy.deepcopy(mytb.getcell('ANTENNA2', _row))
            for i,val in enumerate(thisvals):
                try:
                    thisparam = interp_paramvals[thisscan][thisant][thisspw][i]
                except KeyError:
                    thisparam = None
                if thisparam:
                    try:
                        thisant2 = ant2vals[thisscan][thisant][thisspw](thistime)
                    except TypeError:
                        thisant2 = ant2vals[thisscan][thisant][thisspw]
                    try:
                        thisvals[i] = np.asarray([thisparam(thistime)])
                    except TypeError:
                        thisvals[i] = np.asarray(thisparam)[0]
                    try:
                        thissnr[i]  = np.asarray([interp_snrvals[thisscan][thisant][thisspw][i](thistime)])
                    except TypeError:
                        candidate_snr = np.asarray(interp_snrvals[thisscan][thisant][thisspw][i])
                        if isinstance(candidate_snr, bool):
                            #fringefit can write NaNs for the SNR but leave the rest OK
                            candidate_snr = [0]
                        thissnr[i]  = candidate_snr
                    try:
                        thisflag[i] = np.asarray([interp_flagvals[thisscan][thisant][thisspw][i](thistime)])
                    except TypeError:
                        thisflag[i] = np.asarray(interp_flagvals[thisscan][thisant][thisspw][i])[0]
            mytb.putcell(param, _row, thisvals)
            mytb.putcell('SNR', _row, thissnr)
            mytb.putcell('FLAG', _row, thisflag)
            mytb.putcell('ANTENNA2', _row, int(thisant2))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def replace_solutions(_inp_params, _ms_metadata, table_donor, table_acceptor, replace_ants, replace_scans=[]):
    """
    Takes table_acceptor and removes all rows for all antennas in replace_ants.
    Then, copies over all rows for all antennas in replace_ants from table_donor to table_acceptor.
    If replace_scans is given (list of strings of scan numbers), this replacement will be done only for the specified scans.
    replace_ants can be either a list of antenans or a dict of antennas per scan.
    """
    mytb     = casac.table()
    all_ants = _ms_metadata.antenna_set
    if isinstance(replace_ants, dict):
        pass
    elif isinstance(replace_ants, list):
        replace_ants = [_ms_metadata.yield_antID(rant) for rant in replace_ants]
        ant_array    = copy.deepcopy(replace_ants)
        replace_ants = {}
        all_scans    = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename=table_donor)
        for scan in all_scans:
            replace_ants[str(scan)] = [str(ant) for ant in ant_array]
    else:
        raise ValueError('replace_ants must be a list or dict. Got ' + str(replace_ants))
    for scan in replace_ants.keys():
        replace_ants[scan] = [str(_ms_metadata.yield_antID(rant)) for rant in replace_ants[scan]]
        for ant in replace_ants[scan]:
            if str(ant) not in [str(ant) for ant in all_ants]:
                raise ValueError(str(ant) + ' in replace_ants is not a valid antenna. Must be one of ' + str(all_ants))

    mytb.open(table_acceptor, nomodify=False)
    _nrows   = mytb.nrows()
    del_rows = []
    for _row in range(_nrows):
        thisant  = mytb.getcell('ANTENNA1', _row)
        thisscan = mytb.getcell('SCAN_NUMBER', _row)
        if any(replace_scans):
            if str(thisscan) in replace_scans and str(thisant) in replace_ants[str(thisscan)]:
                del_rows.append(_row)
        elif str(thisant) in replace_ants[str(thisscan)]:
            del_rows.append(_row)
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    #create a dummy table where all rows but the ones for replace_ants are removed so that the remaining ones can be copied later
    dummytab = auxiliary.unique_filename(table_donor + '.tmp')
    mytb.open(table_donor)
    mytb.copy(dummytab, deep=True)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    flagcells = defaultdict(lambda: defaultdict(dict))
    mytb.open(dummytab, nomodify=False)
    _nrows   = mytb.nrows()
    del_rows = []
    for _row in range(_nrows):
        thisant  = mytb.getcell('ANTENNA1', _row)
        thisscan = mytb.getcell('SCAN_NUMBER', _row)
        replace  = True
        if any(replace_scans):
            if str(thisscan) not in replace_scans or str(thisant) not in replace_ants[str(thisscan)]:
                del_rows.append(_row)
                replace = False
        elif str(thisant) not in replace_ants[str(thisscan)]:
            del_rows.append(_row)
            replace = False
        if replace:
            thistime = mytb.getcell('TIME', _row)
            thisspw  = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
            flagcells[thisant][thistime][thisspw] = mytb.getcell('FLAG', _row)
    mytb.removerows(del_rows)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    mytb.open(dummytab)
    mytb.copyrows(table_acceptor)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    #fix flags...
    mytb.open(table_acceptor, nomodify=False)
    _nrows   = mytb.nrows()
    for _row in range(_nrows):
        thisant  = mytb.getcell('ANTENNA1', _row)
        thistime = mytb.getcell('TIME', _row)
        thisspw  = mytb.getcell('SPECTRAL_WINDOW_ID', _row)
        try:
            orig_flag = flagcells[thisant][thistime][thisspw]
            mytb.putcell('FLAG', _row, orig_flag)
        except KeyError:
            pass
    mytb.flush()
    mytb.done()
    mytb.clearlocks()

    auxiliary.rm_dir_if_present(dummytab)


def extend_solutions_to_all_scans(_inp_params, _ms_metadata, caltable, all_params=False):
    """
    Extends rows in caltable to all scans present in the MS via nearest-neighbor interpolation from the scans for which
    solutions are already present in caltable.
    Does not work for time-dependent solutions or when rows for specific antennas or spws are deleted or not present.
    Requires an FPARAM caltable.
    """
    mytb       = casac.table()
    timedata   = {}
    paramdata  = {}
    fielddata  = {}
    scanfinder = {}
    # Get table data.
    table_scans = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename=caltable)
    blocklen    = {}
    other       = 'SPECTRAL_WINDOW_ID, ANTENNA1, ANTENNA2, FLAG'
    spwd        = {}
    ant1d       = {}
    ant2d       = {}
    flagd       = {}
    for scanint in table_scans:
        scan                                              = str(scanint)
        timedata[scan], paramdata[scan], fielddata[scan]  = auxiliary.read_CASA_table(_inp_params, 'TIME, FPARAM, FIELD_ID',
                                                                                      'SCAN_NUMBER=='+scan, _tablename=caltable
                                                                                     )
        spwd[scan], ant1d[scan], ant2d[scan], flagd[scan] = auxiliary.read_CASA_table(_inp_params, other,
                                                                                      'SCAN_NUMBER=='+scan, _tablename=caltable
                                                                                      )
        scanfinder[timedata[scan][0]]                     = scan
        paramdata[scan]                                   = np.swapaxes(paramdata[scan], 0, -1)
        paramdata[scan]                                   = np.swapaxes(paramdata[scan], 1, -1)
        flagd[scan]                                       = np.swapaxes(flagd[scan], 0, -1)
        flagd[scan]                                       = np.swapaxes(flagd[scan], 1, -1)
        blocklen[scan]                                    = len(timedata[scan])
    # Interpolate.
    add_Nrows = 0
    for scan in _ms_metadata.all_scans:
        if int(scan) in table_scans:
            continue
        this_time  = _ms_metadata.yield_scantime(scan, scan_part='middle')
        this_field = _ms_metadata.yield_sourceID(_ms_metadata.yield_sourcename_from_scan(scan))
        clst_time  = auxiliary.closest_match(list(scanfinder.keys()), this_time)
        clst_scan  = scanfinder[clst_time]
        # Add values for new scans.
        new_timedata = copy.deepcopy(timedata[clst_scan])
        new_timedata.fill(this_time)
        timedata[scan] = new_timedata
        new_fielddata  = copy.deepcopy(fielddata[clst_scan])
        new_fielddata.fill(this_field)
        fielddata[scan] = new_fielddata
        paramdata[scan] = paramdata[clst_scan]
        this_blocklen   = len(timedata[scan])
        blocklen[scan]  = this_blocklen
        add_Nrows      += this_blocklen
        spwd[scan]      = spwd[clst_scan]
        ant1d[scan]     = ant1d[clst_scan]
        ant2d[scan]     = ant2d[clst_scan]
        flagd[scan]     = flagd[clst_scan]
    # Add rows for all missing scans.
    while add_Nrows:
        mytb.open(caltable, nomodify=False)
        if add_Nrows > mytb.nrows():
            rowstoadd = this_blocklen
        else:
            rowstoadd = add_Nrows
        add_Nrows-= rowstoadd
        mytb.copyrows(caltable, nrow=rowstoadd)
        mytb.flush()
        mytb.done()
        mytb.clearlocks()
    # Fill rows in the order of scans.
    scansorted = copy.deepcopy(_ms_metadata.all_scans)
    auxiliary.natural_sort_Ned_Batchelder(scansorted)
    mytb.open(caltable, nomodify=False)
    row = 0
    for scan in scansorted:
        for b in range(blocklen[scan]):
            mytb.putcell('SCAN_NUMBER', row, int(scan))
            mytb.putcell('TIME', row, timedata[scan][b])
            mytb.putcell('FPARAM', row, paramdata[scan][b])
            mytb.putcell('FIELD_ID', row, fielddata[scan][b])
            if all_params:
                mytb.putcell('SPECTRAL_WINDOW_ID', row, spwd[scan][b])
                mytb.putcell('ANTENNA1', row, ant1d[scan][b])
                mytb.putcell('ANTENNA2', row, ant2d[scan][b])
                mytb.putcell('FLAG', row, flagd[scan][b])
            row += 1
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def overwrite_solutions_by_spwavg(table_acceptor, table_donor, param='FPARAM', indx_val=[0], cleanup=True):
    """
    table_acceptor should have one solution per spw and table_donor should have solutions for multiple spws.
    This function then computes the average calibration solution over all unflagged solutions per spw from table_donor
    and overwrites the entries in table_acceptor with those. This is done only for the index given by the indx_val input.
    If cleanup is True, table_donor will be deleted afterwards.
    This is used for the 'spw!' option of the rldelay_axis_combine input parameter for example.
    """
    mytb     = casac.table()
    spw_sols = {}
    mytb.open(table_donor)
    nrowiter = range(mytb.nrows())
    for row in nrowiter:
        thisflag = mytb.getcell('FLAG', row)
        if thisflag[indx_val][0]:
            continue
        else:
            pass
        thisant = mytb.getcell('ANTENNA1', row)
        thisval = mytb.getcell(param, row)[indx_val][0]
        try:
            spw_sols[thisant].append(thisval)
        except KeyError:
            spw_sols[thisant] = [thisval]
    mytb.done()
    mytb.clearlocks()
    avg_vals = {}
    for ant in spw_sols:
        avg_vals[ant] = np.average(outlier_detection.remove_outliers_from_median(spw_sols[ant], 3))
    mytb.open(table_acceptor, nomodify=False)
    nrowiter = range(mytb.nrows())
    for row in nrowiter:
        thisant           = mytb.getcell('ANTENNA1', row)
        thisval           = mytb.getcell(param, row)
        thisval[indx_val] = np.asarray([avg_vals[thisant]])
        mytb.putcell(param, row, thisval)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
    if cleanup:
        auxiliary.rm_dir_if_present(table_donor)


def extract_p1_minus_p2_fringes_from_caltb_per_scan(_inp_params, caltablelist, mode, to_specific_refantID=False):
    mytb = casac.table()
    if mode=='phase':
        #get phases
        _indices = [_inp_params.F_0PHAS, _inp_params.F_1PHAS]
    elif mode=='delay':
        #get delays
        _indices = [_inp_params.F_0DELA, _inp_params.F_1DELA]
    elif mode=='rate':
        #get rates
        _indices = [_inp_params.F_0RATE, _inp_params.F_1RATE]
    else:
        raise ValueError('mode must be either phase, delay, or rate.')
    fringes = defaultdict(lambda: defaultdict(dict))
    for caltable in caltablelist:
        mytb.open(caltable)
        _nrows   = mytb.nrows()
        all_rows = range(_nrows)
        for _row in all_rows:
            thisant1 = mytb.getcell('ANTENNA1', _row)
            thisant2 = mytb.getcell('ANTENNA2', _row)
            if not isinstance(to_specific_refantID, bool) and thisant2!=to_specific_refantID:
                continue
            if thisant1==thisant2:
                continue
            thisflag = mytb.getcell('FLAG', _row)
            if any(np.asarray(thisflag)):
                continue
            thisfparam = mytb.getcell('FPARAM', _row)
            thisscan   = int(mytb.getcell('SCAN_NUMBER', _row))
            fdiff      = thisfparam[_indices[0]][0] - thisfparam[_indices[1]][0]
            try:
                fringes[caltable][thisant1][thisscan].append(fdiff)
            except KeyError:
                fringes[caltable][thisant1][thisscan] = [fdiff]
        mytb.done()
        mytb.clearlocks()
    for caltb in fringes:
        for ant in fringes[caltb]:
            for scan in fringes[caltb][ant]:
                this_avg = np.average(outlier_detection.remove_outliers_from_median(fringes[caltb][ant][scan], 3))
                fringes[caltb][ant][scan] = this_avg
    return fringes


def extract_p1_minus_p2_fringes_from_fringeoverviews(fringeoverviewf, mode, refant):
    antenna_index   = [4, 5]
    pol_index       = 6
    detection_index = 7
    if mode=='delay':
        #get delays
        fparam_index = 9
    elif mode=='rate':
        #get rates
        fparam_index = 10
    else:
        raise ValueError('mode must be delay or rate.')
    fringes = {}
    with open(fringeoverviewf, 'r') as f:
        for line in auxiliary.proper_line(f):
            lline = line.split(',')
            ant1  = lline[antenna_index[0]]
            ant2  = lline[antenna_index[1]]
            if ant1 == refant:
                thisant = ant2
                sign    = 1
            elif ant2==refant:
                thisant = ant1
                sign    = -1
            else:
                continue
            thispol    = lline[pol_index]
            thisdetect = lline[detection_index]
            if thisdetect == 'Y':
                detect_counter = 1
            else:
                detect_counter = 0
            #Fringes alternate between pol 1 and pol 2. First entry is pol 1 and they both have the same ant1-ant2 ordering.
            if thispol=='1':
                thisfringe   = float(lline[fparam_index])
                fringedetect = detect_counter
            elif thispol=='2':
                fdiff        = sign * ( thisfringe - float(lline[fparam_index]) )
                fringedetect += detect_counter
                if fringedetect == 2:
                    try:
                        fringes[thisant].append(fdiff)
                    except KeyError:
                        fringes[thisant] = [fdiff]
                else:
                    pass
    return fringes



def solve_fringe_offsets(_inp_params, _ms_metadata, caltable, ant1, ant2, mode, copy_to_all=False):
    """
    Takes solutions on a high SNR baseline ant1-ant2, where ant1 is the refant and applies 'phase' or 'delay' offsets solutions
    per spw to all rows depending on the mode input parameter. Must have only one solution per scan (fringefit with inf solint).
    """
    if mode=='phase':
        #remove delay
        _indices = [_inp_params.F_0DELA, _inp_params.F_1DELA]
    elif mode=='delay':
        #remove phase
        _indices = [_inp_params.F_0PHAS, _inp_params.F_1PHAS]
    elif not mode:
        #do nothing (keep both phase and delay)
        _indices = None
    else:
        raise ValueError('mode must be either phase, delay, or None.')
    mytb = casac.table()
    qselc       = 'FPARAM'
    qcrit0      = 'ANTENNA1==' + str(ant2)
    all_fparams = defaultdict(dict)
    for scan in _ms_metadata.all_scans:
        for spw in _ms_metadata.all_spwds:
            qcrit  = qcrit0 + ' && SCAN_NUMBER==' + str(scan) + ' && SPECTRAL_WINDOW_ID==' + str(spw)
            fparam = auxiliary.read_CASA_table(_inp_params, _query_select = qselc, _query_criteria = qcrit, _tablename = caltable)
            if any(fparam) and not any(np.isnan(fparam)):
                thisfparam = copy.deepcopy(np.swapaxes(fparam,0,1)[0])
                if _indices:
                    thisfparam[_indices[0]] = [0]
                    thisfparam[_indices[1]] = [0]
                all_fparams[int(scan)][int(spw)] = thisfparam
    mytb.open(caltable, nomodify=False)
    nrows = mytb.nrows()
    #handle unitialized fparam
    thisfparam = mytb.getcell('FPARAM', 0)
    for row in range(nrows):
        thisant  = mytb.getcell('ANTENNA1', row)
        thisspw  = mytb.getcell('SPECTRAL_WINDOW_ID', row)
        thisscan = mytb.getcell('SCAN_NUMBER', row)
        thisflag = mytb.getcell('FLAG', row)
        mytb.putcell('FLAG', row, np.zeros_like(thisflag))
        if str(thisant)==str(ant1):
            pass
        elif str(thisant)==str(ant2) or copy_to_all:
            try:
                thisfparam = all_fparams[int(thisscan)][int(thisspw)]
                mytb.putcell('FPARAM', row, thisfparam)
            except KeyError:
                mytb.putcell('FPARAM', row, np.zeros_like(thisfparam))
        else:
            mytb.putcell('FPARAM', row, np.zeros_like(thisfparam))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def zero_fringe(_inp_params, caltable, mode, for_all_stationIDs_but=None, unflag=False):
    """
    Replaces all phases or delays in a fringe caltable with zero.
    """
    if mode=='phase':
        #zero phases
        _indices = [_inp_params.F_0PHAS, _inp_params.F_1PHAS]
    elif mode=='delay':
        #zero delays
        _indices = [_inp_params.F_0DELA, _inp_params.F_1DELA]
    elif mode=='all':
        #zero all
        _indices = range(_inp_params.F_01LEN)
    else:
        raise ValueError('mode must be either phase, delay, or all.')
    auxiliary.check_if_table_is_written(caltable)
    mytb = casac.table()
    mytb.open(caltable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        if for_all_stationIDs_but:
            if mytb.getcell('ANTENNA1', row) in for_all_stationIDs_but:
                continue
            else:
                pass
        thisfparam = copy.deepcopy(mytb.getcell('FPARAM', row))
        for indx in _indices:
            thisfparam[indx] = [0]
        mytb.putcell('FPARAM', row, thisfparam)
        if unflag:
            thisflag = mytb.getcell('FLAG', row)
            mytb.putcell('FLAG', row, np.zeros_like(thisflag))
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def set_only_specific_fringe_solutions(_inp_params, caltable, ant1, fringe_indices, fringe_solutions_per_spw,
                                       endtimes_sol_index_matching, unflag_all=True):
    """
    Modify a fringe-fit caltable by zeroing all solutions except for those set by the indices of fringe_indices for ant1.
    The solutions are set by fringe_solutions_per_spw dictionaries with spws as keys, one for each index of fringe_indices.
    The fringe_solutions_per_spw dictionary items are numpy arrays. The correct indices are determined from
    endtimes_sol_index_matching, which is a spws key dict containing the TIME entries where new fringe_indices are set.
    Sets all flags to 0 if unflag_all is set.
    """
    iitr  = range(_inp_params.F_01LEN)
    i_ant = [int(a) for a in ant1]
    mytb  = casac.table()
    mytb.open(caltable, nomodify=False)
    nrows = mytb.nrows()
    for row in range(nrows):
        thisfparam = copy.deepcopy(mytb.getcell('FPARAM', row))
        this_ant1  = mytb.getcell('ANTENNA1', row)
        if unflag_all:
            thisflag = mytb.getcell('FLAG', row)
            mytb.putcell('FLAG', row, np.zeros_like(thisflag))
        for indx in iitr:
            thisfparam[indx] = np.array([0])
        if int(this_ant1) in i_ant:
            this_spw  = mytb.getcell('SPECTRAL_WINDOW_ID', row)
            this_time = mytb.getcell('TIME', row)
            time_indx = np.argmax(this_time<=endtimes_sol_index_matching[this_spw])
            for indx, fsols in zip(fringe_indices, fringe_solutions_per_spw):
                thisfparam[indx] = fsols[this_spw][time_indx]
        mytb.putcell('FPARAM', row, thisfparam)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def unify_spectral_window_ids(caltable):
    """
    Replaces all entries in the SPECTRAL_WINDOW_ID column in caltable with the minimum value in that column.
    Can be used to replace erroneous entries written by multi-band (combine='spw') fringe-fit tables.
    """
    mytb = casac.table()
    mytb.open(caltable, nomodify=False)
    spws = mytb.getcol('SPECTRAL_WINDOW_ID')
    spws.fill(min(spws))
    mytb.putcol('SPECTRAL_WINDOW_ID', spws)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def sort_caltb_by_scannum(caltable):
    """
    For spwpartitions, the caltables that were combined from multiple spws are sorted by SPECTRAL_WINDOW_ID,SCAN_NUMBER.
    This task fixes the sorting to SCAN_NUMBER,SPECTRAL_WINDOW_ID.
    Benign because casac.table.open already sorts by SCAN_NUMBER,SPECTRAL_WINDOW_ID.
    """
    values = defaultdict(dict)
    orig_scans = []
    mytb = casac.table()
    mytb.open(caltable)
    colnames  = mytb.colnames()
    used_cols = []
    orig_rows = range(mytb.nrows())
    for row in orig_rows:
        orig_scans.append(int(mytb.getcell('SCAN_NUMBER', row)))
        for col in colnames:
            if col == 'WEIGHT':
                continue
            try:
                values[row][col] = mytb.getcell(col, row)
                used_cols.append(col)
            except RuntimeError:
                pass
    orig_scans  = np.array(orig_scans)
    orig_rows   = np.array(orig_rows)
    scansort    = orig_scans.argsort(kind='mergesort')
    sorted_rows = orig_rows[scansort]
    mytb.done()
    mytb.clearlocks()
    mytb.open(caltable, nomodify=False)
    for orig_row,new_row in enumerate(sorted_rows):
        for col in used_cols:
            mytb.putcell(col, orig_row, values[new_row][col])
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def medfilt_solutions(_inp_params, _calibrationtable, _indices='all', _allowed_range=[-float('Inf'), float('Inf')],
                      _bad_vals=[0], _smoothtime=60., _targetcol='pick'):
    """
    Takes a _calibrationtable and applies a sliding median window filter using a kernel with _smoothtime [seconds] length.
    The filter is applied to the _indices slices of the _targetcol on a per-scan, per-antenna basis, per spw.
    Any values equal to _bad_vals (can be an array) or outside of the _allowed_range
      are first replaced by the median value of the entire scan (avoid bias from invalid values for sliding filter).
    If _smoothtime='inf', all values will be replaced by the single median per scan, antenna, and spw.
      Note that - normally - _smoothtime should be a float and not a string!
    If _targetcol='pick', then either 'CPARAM' or 'FPARAM' will be used depending on which is present in the table.
    If _indices='all', then all slices of _targetcol are smoothed.
      Note that - normally - _indices should be a list of indices.
    Modifies _calibrationtable directly.
    Example usage: Smooth the delays of a fringe table while leaving rates and phases untouched.

    Method:
      - Load all rows of the _targetcol and form arrays from subsets, by selecting one scan and one antenna at a time.
      - Keep track of the original indices/positions (which selected array element belogs to which row originally).
      - Smooth the selected subset arrays and put the smoothed values back in place in the original selection.
      - Write the update values.
    """
    mytb = casac.table()
    if _targetcol == 'pick':
        mytb.open(_calibrationtable)
        _colnames = mytb.colnames()
        if 'CPARAM' in _colnames:
            _targetcol = 'CPARAM'
        elif 'FPARAM' in _colnames:
            _targetcol = 'FPARAM'
        else:
            raise RuntimeError('Cannot smooth ' + _calibrationtable + ' because neither CPARAM nor FPARAM columns are present.')
        mytb.done()
    if _indices == 'all':
        mytb.open(_calibrationtable)
        _indices = range(np.shape(mytb.getcell(_targetcol,0))[0])
        mytb.done()
    if str(_smoothtime).lower() == 'inf':
        _smoothtime    = 1.
        _allowed_range = [0,0]
    _kernel_sizes   = {}
    unique_spws     = sorted(auxiliary.read_CASA_table(_inp_params, 'unique SPECTRAL_WINDOW_ID', _tablename=_calibrationtable))
    unique_antennas = sorted(auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', _tablename=_calibrationtable))
    unique_scans    = auxiliary.read_CASA_table(_inp_params, 'unique SCAN_NUMBER', _tablename=_calibrationtable)
    for scan in unique_scans:
        #translate _smoothtime into an odd integer for the kernel size as number of array elements
        qcrit = 'SCAN_NUMBER=='+str(scan)
        time  = auxiliary.read_CASA_table(_inp_params, 'unique TIME', qcrit, _tablename=_calibrationtable)
        try:
            dt = np.abs(time[1] - time[0])
            ks = max(int(_smoothtime / dt),1)
        except IndexError:
            ks = 1
        if ks%2 == 0:
            ks += 1
        _kernel_sizes[str(scan)] = ks
    #4D dict: keys are [scan][antenna][spw][_index]. It stores [[subset array][correpsonding original positions]]
    smoothed_subarrays                  = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))
    select                              = _targetcol+',FLAG, ANTENNA1, SCAN_NUMBER, SPECTRAL_WINDOW_ID'
    values, flag, antennas, scans, spws = auxiliary.read_CASA_table(_inp_params, select, _tablename=_calibrationtable)
    all_values                          = []
    used_indices                        = []
    for _index in _indices:
        try:
            all_values.append(np.ma.masked_array(copy.deepcopy(values[_index][0]), flag[_index][0]))
            used_indices.append(_index)
        #Exception may be for single pol stations:
        except IndexError:
            pass
    for i, (thisant, thisscan, thisspw) in enumerate(zip(antennas, scans, spws)):
        for thisindex, thesevalues in zip(used_indices, all_values):
            thisvalue = thesevalues[i]
            try:
                smoothed_subarrays[thisscan][thisant][thisspw][thisindex][0].append(thisvalue)
                smoothed_subarrays[thisscan][thisant][thisspw][thisindex][1].append(i)
            except KeyError:
                smoothed_subarrays[thisscan][thisant][thisspw][thisindex] = [[],[]]
                smoothed_subarrays[thisscan][thisant][thisspw][thisindex][0].append(thisvalue)
                smoothed_subarrays[thisscan][thisant][thisspw][thisindex][1].append(i)
    for unique_scan in unique_scans:
        for unique_antenna in unique_antennas:
            for unique_spw in unique_spws:
                for index in used_indices:
                    try:
                        original_positions = smoothed_subarrays[unique_scan][unique_antenna][unique_spw][index][1]
                        corresponding_vals = smoothed_subarrays[unique_scan][unique_antenna][unique_spw][index][0]
                        smoothed_values    = auxiliary.medfilt_nobias(corresponding_vals,
                                                                      _kernel_sizes[str(unique_scan)], _bad_vals, _allowed_range)
                        smoothed_values    = np.ma.filled(smoothed_values,0)
                        smoothed_values    = np.nan_to_num(smoothed_values)
                        for smoothed_value, original_position in zip(smoothed_values, original_positions):
                            values[index][0][original_position] = smoothed_value
                    except KeyError:
                        #spw/antenna combination not present in this scan
                        pass
    mytb.open(_calibrationtable, nomodify=False)
    mytb.putcol(_targetcol, values)
    mytb.flush()
    mytb.done()


def get_gainfields(_inp_params, _ms_metadata, calib_input, _mode, this_field=None):
    """
    Processes and formats calib_gainfd taking phase-referencing into account
      (the gainfields for every calibration table as set in array.inp, obtained from the _inp_params.C_GFD index).
      Only needed in some cases for advanced calibration parameter selections.
    If _mode='prepare':
      - calib_input must come from get_calib_steps().
      - A dictionary of arrays for C_NAM, C_INT, C_CWT, C_GFD, C_ANT, C_FLD table parameters as set in array.inp are returned.
      - This mode is run outside of calibration functions; in applycal() and the go_calibrate() wrapper.
    If _mode='retrieve':
      - calib_input must be a dictionary from the 'prepare' mode; it should be passed to all calibration tasks by default.
      - This mode is run inside of a calibration function, where the calibration source (this_field) is known.
      - If this_field is not set, emtpy gainfields are returned (all solutions from all sources are applied from all gaintables).
      - If this_field is set:
        > If it is a science source with a phaseref source, replace True C_GFD entries with the phaseref source if C_FLD entries
          are also True.
        > Else, replace True C_GFD entries with this_field.
        > Checks if the sources substituted in the C_GFD entries are actually present in the corresponding tables.
          If not, the entries are removed from C_NAM, C_INT, C_GFD, C_CWT, and C_ANT.
        > Returns processed arrays for C_NAM, C_INT, C_GFD, C_CWT, and C_ANT.
    """
    if _mode == 'prepare':
        calib_params               = return_calib_params(_inp_params, calib_input)
        calib_params_dict          = {}
        calib_params_dict['C_NAM'] = calib_params[_inp_params.C_NAM]
        calib_params_dict['C_INT'] = calib_params[_inp_params.C_INT]
        calib_params_dict['C_CWT'] = calib_params[_inp_params.C_CWT]
        calib_params_dict['C_GFD'] = calib_params[_inp_params.C_GFD]
        calib_params_dict['C_FLD'] = calib_params[_inp_params.C_FLD]
        calib_params_dict['C_ANT'] = calib_params[_inp_params.C_ANT]
        return calib_params_dict
    elif _mode == 'retrieve':
        if not calib_input:
            return []
        calib_params_cgfd = calib_input['C_GFD']
        if not this_field:
            return [''] * len(calib_params_cgfd)
        else:
            if ',' in this_field or isinstance(this_field, list):
                raise ValueError('Got src=' + str(this_field) + ' but I cannot unravel gainfields for more than one source.')
            default_field  = this_field
            maybe_phaseref = False
            if _inp_params.science_target:
                if this_field in _inp_params.science_target:
                    maybe_phaseref = _ms_metadata.yield_phaseref(_inp_params, this_field)
                    if maybe_phaseref:
                        default_field = maybe_phaseref
            calib_params_cgfd = auxiliary.replace_valids_by_input(calib_params_cgfd, default_field)
            c_nam = []
            c_int = []
            c_gfd = []
            c_cwt = []
            c_ant = []
            for i_nam, i_int, i_gfd, i_fld, i_cwt, i_ant in zip(calib_input['C_NAM'], calib_input['C_INT'], \
                                                         calib_params_cgfd, calib_input['C_FLD'], calib_input['C_CWT'], \
                                                         calib_input['C_ANT']):
                this_gfd = i_gfd
                if this_gfd:
                    if not auxiliary.check_field_in_table(_inp_params, _ms_metadata, i_nam, i_gfd):
                        if auxiliary.is_set(_inp_params, 'phaseref_ff_science') and \
                           auxiliary.check_field_in_table(_inp_params, _ms_metadata, i_nam, this_field):
                            this_gfd = this_field
                        else:
                            #skip this table
                            continue
                    elif not i_fld:
                        this_gfd = this_field
                this_nam = i_nam
                if maybe_phaseref:
                    phaseref_tab = i_nam + '.phaseref'
                    if auxiliary.isdir(phaseref_tab):
                        this_nam = phaseref_tab
                        i_int    = i_int.replace('perscan', '')
                c_nam.append(this_nam)
                c_int.append(i_int)
                c_gfd.append(this_gfd)
                c_cwt.append(i_cwt)
                c_ant.append(i_ant)
            return c_nam, c_int, c_gfd, c_cwt, c_ant
    else:
        raise ValueError(str(_mode) + ' is not an allowed _mode. Must be <prepare> or <retrieve>.')


def make_calib_folder(_inp_params, _calibration_step):
    """
    Create directories for calibration table.
    """
    calib_params = return_calib_params(_inp_params, _calibration_step)
    foldernames  = calib_params[_inp_params.C_NAM]
    if not isinstance(foldernames, list):
        foldernames = [foldernames]
    for tabdir in foldernames:
        auxiliary.makedir(os.path.dirname(tabdir))


def return_calib_params(_inp_params, calibration_kwargs):
    """
    Takes inp_params key words for calibration steps as input: calibration_kwargs.
    Returns a list of all calibration parameters for the kwargs provided, sorted by parameter.
    Assumes that the calibration_kwargs come as arrays according to the calib_taskname<->task_taskname() scheme within array.inp.
    """
    if not calibration_kwargs:
        return [''] * _inp_params.C_LEN
    if isinstance(calibration_kwargs, list):
        islist = True
    else:
        islist = False
        calibration_kwargs = [calibration_kwargs]
    calib_params = []
    for ck in calibration_kwargs:
        c_param = getattr(_inp_params, ck)
        if len(c_param) != _inp_params.C_LEN:
            raise IndexError('Calibration parameters for\n' + ck + '=' + str(c_param) + \
                             '\ndoes not have ' + str(_inp_params.C_LEN) + ' values!' + \
                             ' Check input/array.inp'
                            )
        else:
            calib_params.append(c_param)
    if islist:
        calib_params = auxiliary.transpose_list(calib_params)
    else:
        calib_params = calib_params[0]
    return calib_params


def get_calib_steps(_inp_params, _steps, _allother_functions = [], _calibration_function = '', _quiet=False):
    """
    Task to get the calib_steps (as defined in array.inp) for the on-the-fly calibration of go_calibrate
      and the final calibration with task_applycal.
    These calibration_steps will then be processed by return_calib_params to get the array elements of these input parameters.
    Will just pass on the _steps unless _steps is equal to certain keywords:
      - if 'all_previous':
          Returns all steps listed in inp_params.diag_steps_completed.
          Useful for go_calibrate in the default pipeline mode.
      - if 'solved_functions':
          If _calibration_function is given:
            Returns a list of calib_tasknames corresponding to all task_tasknames in _allother_functions
              that come before _calibration_function.
            Useful for on-the-fly calibration of go_calibrate.
          Else:
            Returns a list of calib_tasknames corresponding to all task_tasknames specified in _allother_functions.
            Useful for task_applycal.
          This default mode should always work for both go_calibrate and task_applycal for both the default mode and quickmode.
    Does a last check for every mode: Removes calib_steps if the corresponding tables do not exists and print a warning.
    """
    calib_steps = []
    if _steps == 'all_previous':
        logfile     = _inp_params.diagdir + _inp_params.diag_steps_completed
        try:
            with open(logfile, 'r') as file_steps:
                for line in auxiliary.proper_line(file_steps):
                    attr = str(line.split()[0])
                    if hasattr(_inp_params, attr):
                        calib_steps.append(attr)
        #create file if it does not exist yet:
        except IOError:
            _steps = open(logfile, 'a')
            _steps.close()
    elif _steps == 'solved_functions':
        for func in _allother_functions:
            taskname    = func.__name__
            taskname    = taskname.split('task_', 1)[1]
            calib_param = 'calib_' + taskname
            calib_steps.append(calib_param)
        if _calibration_function:
            cfunc_name  = _calibration_function.__name__
            cfunc_name  = cfunc_name.split('task_', 1)[1]
            cparam_name = 'calib_' + cfunc_name
            calib_steps = calib_steps[:calib_steps.index(cparam_name)]
    else:
        if isinstance(_steps, list):
            calib_steps = _steps
        else:
            calib_steps = [_steps]
    filtered_calib_steps = []
    tables_selected      = []
    for cs in calib_steps:
        try:
            c_param = getattr(_inp_params, cs)
            c_table = c_param[_inp_params.C_NAM]
            if c_table not in tables_selected:
                if auxiliary.isdir(c_table):
                    _pass = True
#                    if _sci_or_cal and not auxiliary.is_set(_inp_params, 'calibrators_phaseref'):
#                        _this_table_mark = c_param[_inp_params.C_SVC]
#                        if _sci_or_cal == 's':
#                            if _this_table_mark == 'c':
#                                _pass = False
#                        elif _sci_or_cal == 'c':
#                            if _this_table_mark == 's':
#                                _pass = False
#                        else:
#                            raise ValueError("Got invalid argument for _sci_or_cal: <"+str(_sci_or_cal)+">. Must be 'c' or 's'.")
                    if _pass:
                        filtered_calib_steps.append(cs)
                elif 'just_a_dummy' not in c_table and not _quiet:
                    print('  Warning: The requested calibration table\n    ' + c_table + '\n  does not exist.')
                #Avoid using the same table more than once if it is used multiple times by different calibration tasks:
                tables_selected.append(c_table)
        except AttributeError:
            # _inp_params not up-to-date with latest calib steps in picard/main_picard.py
            pass
    return filtered_calib_steps


def go_calibrate(_inp_params, _ms_metadata, function, on_the_fly_calib='', _all_calibration_steps='', _mpi_client=None,
                 *args, **kwargs):
    """
    Wrapper around a CASA calibration task which is passed via the function argument.
    I.e. all of the task_taskname functions from the pipeline that are direct calibration tasks.
    Handles the on-the-fly calibration, smoothing, and generation of diagnostic plots.
    Concerning input parameters, the ones for on_the_fly_calib are given directly as input.
    These are specified via the general calibration strategy defined in main file of the pipeline.
    The input parameters for the specified task are derived from the name of the passed function.
      This assumes the naming convention that the calibration parameters for
      'calib_taskname' belong to a function called 'task_taskname()'.
    If the task_taskname() function returns True, then it will be added to the _inp_params.diag_steps_completed file,
      meaning that this calibration will be applied on-the-fly for all following calibration steps.
    I believe that this wrapper is more elegant than having to write duplicate code for every calibration task.
    Engage!
    """
    if not _inp_params.diag_steps_completed:
        raise IOError('Error: Should set inp_params.store_steps. ' \
                 'Without this on-the-fly calibration will not work in the default mode.\n')
    logfile     = _inp_params.diagdir + _inp_params.diag_steps_completed
    taskname    = function.__name__
    taskname    = taskname.split('task_', 1)[1]
    calib_param = 'calib_' + taskname
    try:
        _ = getattr(_inp_params, calib_param)[_inp_params.C_NAM]
    except AttributeError:
        raise ValueError('I did not find an input parameter array for ' + str(taskname)+'. ' \
                         'There should have been a ' + calib_param + ' parameter defined in array_finetune.inp.')
    make_calib_folder(_inp_params, calib_param)
    print ('\nPerforming ' + taskname + ' calibration...')
    on_the_fly_calib = get_calib_steps(_inp_params, on_the_fly_calib, _all_calibration_steps, function)
    fly_calib_params = return_calib_params(_inp_params, on_the_fly_calib)
    fly_calib_tables = fly_calib_params[_inp_params.C_NAM]
    fly_calib_interp = fly_calib_params[_inp_params.C_INT]
    fly_calib_gainfd = get_gainfields(_inp_params, _ms_metadata, on_the_fly_calib, 'prepare')
    _dosmo           = False
    _name_caltable   = getattr(_inp_params, calib_param)[_inp_params.C_NAM]
    _overwrite_sols  = getattr(_inp_params, calib_param)[_inp_params.C_ANT]
    if _overwrite_sols:
        if ':' not in _overwrite_sols:
            raise ValueError('C_ANT parameter must be <calib_param>:<antenna list>. Got ' + str(_overwrite_sols))
        _overwrite_calpar, _overwrite_ants = _overwrite_sols.split(':')
        _overwrite_ants                    = _overwrite_ants.split(',')
        _overwrite_caltable                = getattr(_inp_params, _overwrite_calpar)[_inp_params.C_NAM]
        if _overwrite_caltable not in fly_calib_tables:
            errmsg = 'Tried to overwrite solutions of {0} with {1} solutions.'.format(str(_overwrite_calpar), str(calib_param))
            errmsg += 'But the former one does not seem to be solved yet.'
            errmsg += 'Please make sure that it comes before the latter one in array_specific_steps in main_picard.py.'
            raise IOError(errmsg)
        other_fly_list                                     = [fly_calib_interp]
        fly_calib_tables, other_fly_list, fly_calib_gainfd = auxiliary.remove_selected_from_lists(_overwrite_caltable, \
                                                                                                  fly_calib_tables, \
                                                                                                  other_fly_list, \
                                                                                                  fly_calib_gainfd
                                                                                                 )
        fly_calib_interp                                   = other_fly_list[0]
    try:
        if getattr(_inp_params, calib_param)[_inp_params.C_SMO] or getattr(_inp_params, calib_param)[_inp_params.C_MED]:
            _dosmo = True
            _name_caltable += '_pre_smoothing'
    except IndexError:
        pass
    if _inp_params.verbose:
        if not fly_calib_tables:
            pr_fly_calib_tables = 'None'
            pr_fly_calib_interp = 'None'
        else:
            pr_fly_calib_tables = fly_calib_tables
            pr_fly_calib_interp = fly_calib_interp
        if not isinstance(fly_calib_tables, list):
            pr_fly_calib_tables = [pr_fly_calib_tables]
            pr_fly_calib_interp = [pr_fly_calib_interp]
        print ('  Using on-the-fly calibration tables:\n' + auxiliary.wrap_list(pr_fly_calib_tables, indent='    '))
        print ('  With interpolation methods:\n' + auxiliary.wrap_list(pr_fly_calib_interp, indent='    '))

    #Making sure only sensible values are used for spw partitioning.
    perspw_tasks = ['accor', 'acscl', 'tsys', 'tsys_add_exptau', 'gaincurve', 'scalar_bandpass', 'complex_bandpass',
                    'fringefit_single', 'phase_offsets_ALMA'
                   ]
    if taskname in perspw_tasks:
        these_spwpartitions = ''
    elif taskname == 'fringefit_multi_cal_coher' or taskname == 'fringefit_multi_cal_short':
        these_spwpartitions = getattr(_inp_params, 'calib_fringefit_solint_cal')[_inp_params.C_SPW]
    elif taskname == 'fringefit_multi_sci_short':
        these_spwpartitions = getattr(_inp_params, 'calib_fringefit_solint_sci')[_inp_params.C_SPW]
    else:
        these_spwpartitions = getattr(_inp_params, calib_param)[_inp_params.C_SPW]
    these_spwpartitions = these_spwpartitions.split(',')
    spwpartition_caltbs = []
    len_spwpartitions   = len(these_spwpartitions)
    last_spwpartition   = range(len_spwpartitions)[-1]
    if 'spwpartition_part' in kwargs and len_spwpartitions>1:
        do_single_spwpartition = int(kwargs['spwpartition_part'])
    else:
        do_single_spwpartition = -999
    for spwpartition_iter, spwpartition in enumerate(these_spwpartitions):
        this_caltbname = _name_caltable
        if spwpartition:
            this_caltbname += '.spw' + spwpartition.replace('~', 'to')
            spwpartition_caltbs.append(this_caltbname)
        if do_single_spwpartition>-100 and spwpartition_iter != do_single_spwpartition:
            if spwpartition_iter==last_spwpartition and do_single_spwpartition != last_spwpartition:
                print('Done\n')
                return False
            else:
                pass
            continue
        if 'spwpartition_part' in kwargs and do_single_spwpartition==-999 and int(kwargs['spwpartition_part'])!=0:
            #Prevent redoing the same step when the pipeline is called with -q step.x while no spwpartitions are defined.
            print('Done\n')
            return False
        response = function(_inp_params, _ms_metadata, this_caltbname, fly_calib_tables, fly_calib_interp, fly_calib_gainfd,
                            spwpartition, _mpi_client
                           )
    if len(spwpartition_caltbs)>1:
        concat_spwpartition_tables(_inp_params, spwpartition_caltbs, _name_caltable)
        if 'ffospws' in taskname:
            postprocess_singleband_solutions(_inp_params, _ms_metadata, _name_caltable, rerefant_also=False)

    if _dosmo and auxiliary.isdir(_name_caltable) and response:
        if _inp_params.verbose:
            print ('  Smoothing the determined solution table.')
        cp_pre_smoothing = True
        try:
            if getattr(_inp_params, calib_param)[_inp_params.C_SMO]:
                task_smoothcal(_inp_params, _name_caltable, getattr(_inp_params, calib_param)[_inp_params.C_NAM],
                               getattr(_inp_params, calib_param)[_inp_params.C_SMO],
                               getattr(_inp_params, calib_param)[_inp_params.C_STI]
                              )
                cp_pre_smoothing = False
        except IndexError:
            pass
        try:
            if getattr(_inp_params, calib_param)[_inp_params.C_MED]:
                if cp_pre_smoothing:
                    auxiliary.copydir(_name_caltable, getattr(_inp_params, calib_param)[_inp_params.C_NAM])
                medfilt_solutions(_inp_params, getattr(_inp_params, calib_param)[_inp_params.C_NAM], _smoothtime='inf')
        except IndexError:
            pass
        if _inp_params.verbose:
            print ('  Finished smoothing the determined solution table.')
    if _overwrite_sols:
        replace_solutions(_inp_params, _ms_metadata, getattr(_inp_params, calib_param)[_inp_params.C_NAM], \
                          _overwrite_caltable, _overwrite_ants
                         )
        auxiliary.rm_dir_if_present(getattr(_inp_params, calib_param)[_inp_params.C_NAM])
        response = False
    fix_snr_nans(getattr(_inp_params, calib_param)[_inp_params.C_NAM])
    if getattr(_inp_params, calib_param)[_inp_params.C_DIG] and auxiliary.isdir(_name_caltable) and response:
        if _inp_params.verbose:
            print ('  Generating diagnostic plots for the determined solutions.')
        diagnostics.my_plotcal(_inp_params, _ms_metadata, getattr(_inp_params, calib_param)[_inp_params.C_NAM], taskname,
                               getattr(_inp_params, calib_param)[_inp_params.C_DIG],
                               getattr(_inp_params, calib_param)[_inp_params.C_XAX],
                               getattr(_inp_params, calib_param)[_inp_params.C_YAX]
                              )
        if _inp_params.verbose:
            print ('  Finished generating solution plots.')
    if response:
        _steps = open(logfile, 'a')
        _steps.write(calib_param+'\n')
        _steps.close()
    print ('Done\n')
    return response


def fix_snr_nans(_calibrationtable):
    """
    Replaces nans in SNR column with unity.
    """
    if not auxiliary.isdir(_calibrationtable):
        return
    mytb = casac.table()
    mytb.open(_calibrationtable, nomodify=False)
    sn = mytb.getcol('SNR')
    sn[np.isnan(sn)] = 1
    mytb.putcol('SNR', sn)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def scrap_old_caltables(_inp_params, _all_calibration_steps):
    """
    Removes all calibration tables from the array_specific_steps specified in the main script which were solved for earlier.
    """
    all_calib_steps  = get_calib_steps(_inp_params, 'solved_functions', _all_calibration_steps, _quiet=True)
    all_calib_tables = return_calib_params(_inp_params, all_calib_steps)[_inp_params.C_NAM]
    for calib_table in all_calib_tables:
        auxiliary.rm_dir_if_present(calib_table + '*')


def task_clearcal(_inp_params):
    """
    Clears all existing calibrated data from previous applycal runs.
    """
    print ('\nClearing previous calibration...')
    tasks.clearcal(vis      = _inp_params.ms_name,
                   field    = "",
                   spw      = "",
                   intent   = "",
                   addmodel = False
                  )
    print ('Done\n')


def task_intermediate_applycal(_inp_params, _ms_metadata, caltable, _fly_calib_tables, _fly_calib_interp, _fly_calib_gainfd,
                               _mpi_client):
    """
    Wrapper around task_applycal(), masked as a normal calibration task.
    Allows to run applycal in the middle of calibration steps.
    Use cases: Intermediate plotting or for user-written tasks that do not make use of on-the-fly calibration.
    """
    task_applycal(_inp_params, _ms_metadata, 'solved_functions', overwrite_calib_params_gf=_fly_calib_gainfd)


def task_applycal(_inp_params, _ms_metadata, calib_steps, functions=[], overwrite_calib_params_gf = ''):
    """
    Executes CASA's applycal task for the calibration steps specified by calib_steps, by calling task_applycal_general().
    Calls applycal twice: For calibrators and the science_target(s).
    Limits the number of scans processed via MPI in parallel to _inp_params.mpi_memory_safety.
    """
    print ('\nApplying calibration tables...')
    if not _inp_params.mpi_memory_safety:
        num_chopping = 1000
    else:
        num_chopping = _inp_params.mpi_memory_safety
    if overwrite_calib_params_gf:
        calib_params = overwrite_calib_params_gf
    else:
        calib_steps  = get_calib_steps(_inp_params, calib_steps, functions)
        calib_params = get_gainfields(_inp_params, _ms_metadata, calib_steps, 'prepare')
    all_sources  = _ms_metadata.yield_calibrators(_inp_params)
    sci_sources  = _ms_metadata.yield_science_targets(_inp_params)
    if sci_sources:
        all_sources.extend(sci_sources)
    for source in all_sources:
        caltables, interp, gainfields, weights, _ = get_gainfields(_inp_params, _ms_metadata, \
                                                                   calib_params, 'retrieve', source
                                                                  )
        if _inp_params.verbose:
            print ('\n  Will apply:\n' + auxiliary.wrap_list(caltables, indent='    '))
            print ('  With interpolation methods:\n' + auxiliary.wrap_list(interp, indent='    '))
            print ('  To: ' + source)
            #print ('\n  You may see messages like')
            #print ('    "*** Error *** Error in data selection specification: MSSelectionNullSelection : The selected table has zero rows."')
            #print ('  These messages can safely be ignored.\n')
        for scans in auxiliary.chop_scans(_ms_metadata, source, num_chopping):
            task_applycal_general(_inp_params, _ms_metadata, caltables, interp, weights, gainfields, source, scans)
    auxiliary.only_keep_latest_flagver(_inp_params, 'applycal')
    print ('Done\n')
    #if auxiliary.is_set(_inp_params, 'rldelay_per_station'):
    #    print ('< Performing manual RL-delay calibration per station >')
    #    st_delays = read_per_station_RLdelays_from_caltables(_inp_params, _ms_metadata)
    #    manual_calibration.rldelay_per_station_constant(_inp_params, _ms_metadata, st_delays)
    #    print ('Done\n')
    #elif auxiliary.is_set(_inp_params, 'Use_ALMA_rldly') and 'AA' in _ms_metadata.antennanames:
    #    print ('< Performing manual RL-delay calibration using phased ALMA >')
    #    sci_rld, cal_rld = ALMA_rldelay_calib(_inp_params, _ms_metadata)
    #    manual_calibration.rldelay_per_station_per_scan(_inp_params, _ms_metadata, sci_rld, cal_rld)
    #    print ('Done\n')


def task_applycal3(_inp_params, _ms_metadata, calib_steps, functions=[]):
    """
    *deprecated*
    Executes CASA's applycal task for the calibration steps specified by calib_steps, by calling task_applycal_general().
    Calls applycal twice: For calibrators and the science_target(s).
    Limits the number of scans processed via MPI in parallel to _inp_params.applycal_scanlim to avoid memory overflow.
    """
    print ('\nApplying calibration tables...')
    calib_steps  = get_calib_steps(_inp_params, calib_steps, functions)
    calib_params = get_gainfields(_inp_params, _ms_metadata, calib_steps, 'prepare')
    all_sources  = _ms_metadata.yield_calibrators(_inp_params)
    sci_sources  = _ms_metadata.yield_science_targets(_inp_params)
    if sci_sources:
        all_sources.extend(sci_sources)
    for source in all_sources:
        caltables, interp, gainfields, weights, ants = get_gainfields(_inp_params, _ms_metadata, \
                                                                      calib_params, 'retrieve', source
                                                                     )
        ants_a, caltables_a, interp_a, gainfields_a, weights_a = split_cal_into_ants(_inp_params, _ms_metadata, ants, \
                                                                                     caltables, interp, gainfields, weights
                                                                                    )
        for ants, caltables, interp, gainfields, weights in zip(ants_a, caltables_a, interp_a, gainfields_a, weights_a):
            if _inp_params.verbose:
                these_antnames = [_ms_metadata.yield_antname(int(antid)) for antid in ants.split(',')]
                print ('\n  Will apply:\n' + auxiliary.wrap_list(caltables, indent='    '))
                print ('  With interpolation methods:\n' + auxiliary.wrap_list(interp, indent='    '))
                print ('  To: ' + source)
                print ('  To calibrate:\n' + auxiliary.wrap_list(these_antnames, items_per_line=5, indent='    ') + '\n')
            for scans in auxiliary.chop_scans(_ms_metadata, source, _inp_params.applycal_scanlim):
                task_applycal_general(_inp_params, _ms_metadata, caltables, interp, weights, gainfields, source, scans, ants)
    auxiliary.only_keep_latest_flagver(_inp_params, 'applycal')
    print ('Done\n')


def task_applycal2(_inp_params, _ms_metadata, calib_steps, functions=[]):
    """
    *deprecated*
    Executes CASA's applycal task for the calibration steps specified by calib_steps, by calling task_applycal_general().
    Calls applycal twice: For calibrators and the science_target(s).
    Limits the number of scans processed via MPI in parallel to _inp_params.applycal_scanlim to avoid memory overflow.
    """
    print ('\nApplying calibration tables...')
    science_vs_calib = ['s', 'c']
    svc_sources      = [[],[]]
    try:
        svc_sources[0] = ','.join(_ms_metadata.yield_science_targets(_inp_params))
    except TypeError:
        svc_sources[0] = None
    try:
        svc_sources[1] = ','.join(_ms_metadata.yield_calibrators(_inp_params))
    except TypeError:
        svc_sources[1] = None
    calib_steps_svc  = []
    if svc_sources[0]:
        print (' Collecting calibration parameters for the science sources:')
        calib_steps_svc.append(get_calib_steps(_inp_params, calib_steps, functions, _sci_or_cal=science_vs_calib[0]))
    else:
        calib_steps_svc.append(None)
        print (' Nothing to do for the science targets since none were specified.')
    if svc_sources[1]:
        print (' Collecting calibration parameters for the calibrator sources:')
        calib_steps_svc.append(get_calib_steps(_inp_params, calib_steps, functions, _sci_or_cal=science_vs_calib[1]))
    else:
        calib_steps_svc.append(None)
    for calib_steps, calib_src, svc in zip(calib_steps_svc, svc_sources, science_vs_calib):
        if calib_src:
            apply_calib_params = return_calib_params(_inp_params, calib_steps)
            apply_calib_tables = apply_calib_params[_inp_params.C_NAM]
            apply_calib_interp = apply_calib_params[_inp_params.C_INT]
            apply_calib_weight = apply_calib_params[_inp_params.C_CWT]
            if _inp_params.verbose:
                print ('\n  Will apply:\n' + auxiliary.wrap_list(apply_calib_tables, indent='    '))
                print ('  With interpolation methods:\n' + auxiliary.wrap_list(apply_calib_interp, indent='    '))
                print ('  To: ' + calib_src + '\n')
            #Phase-referencing: Enforce to apply 'c' calibration to 's' targets:
            if auxiliary.is_set(_inp_params, 'calibrators_phaseref') and svc=='s':
                phaseref_field = _ms_metadata.yield_phaseref(_inp_params)
                science_field  = _ms_metadata.yield_science_targets(_inp_params)
                if _inp_params.verbose:
                    print('  Running applycal for phase-referencing!')
                if len(science_field) != len(phaseref_field):
                    raise ValueError('Science targets=\n' + str(science_field) + \
                                     '\nand phase-referencing sources=\n' + str(phaseref_field) + '\ndo not match one to one.')
                for phref, science in zip(phaseref_field, science_field):
                    apply_calib_gaintb = auxiliary.replace_valids_by_input(apply_calib_params[_inp_params.C_FLD], phref)
                    for scans in auxiliary.chop_scans(_ms_metadata, science, _inp_params.applycal_scanlim):
                        task_applycal_general(_inp_params, _ms_metadata, apply_calib_tables, apply_calib_interp,
                                              apply_calib_weight, science, scans, apply_calib_gaintb
                                             )
            else:
                for scans in auxiliary.chop_scans(_ms_metadata, calib_src, _inp_params.applycal_scanlim):
                    task_applycal_general(_inp_params, _ms_metadata, apply_calib_tables, apply_calib_interp, apply_calib_weight,
                                          calib_src, scans)
    print ('Done\n')


def task_smoothcal(_inp_params, _pre_smoothed_table, _out_table, _smoothtype, _smoothtime):
    """
    Generic smoothcal task. Possibly called by different calibration tasks.
    Parameters are from the calib_* input parameters, unpacked by return_calib_params() in the go_calibrate() function.
    """
    if _inp_params.verbose:
        print ('    Smoothing: ' + _pre_smoothed_table + '\n    using a ' + _smoothtype + ' filter\n    ' + \
               'with a ' + _smoothtime + 's interval'
              )
    tasks.smoothcal(vis        = _inp_params.ms_name,
                    tablein    = _pre_smoothed_table,
                    caltable   = _out_table,
                    field      = '',
                    smoothtype = _smoothtype,
                    smoothtime = float(_smoothtime)
                   )


def task_gencal_general(_inp_params, caltable, caltype, infile, spw=""):
    """
    Generic gencal task. Used in different modes, to generate Tsys and gain curve calibration tables.
    """
    tasks.gencal(vis                =  _inp_params.ms_name,
                 caltable           =  caltable,
                 caltype            =  caltype,
                 infile             =  infile,
                 spw                =  spw,
                 antenna            =  "",
                 pol                =  "",
                 parameter          =  [],
                 uniform            =  False
                )


def task_gaincal_general(_inp_params, _ms_metadata, caltable, gaintype, field='', spw='', antenna='', scan='', solint='inf',
                         combine='', minblperant=4, minsnr=3.0, solnorm=False, smodel=[], solmode='L1', rmsthresh=[],
                         calmode='ap', gaintable=[], gainfield=[], interp=[], spwmap=[], append=False, refant=None,
                         refantmode='strict', MPICLIENT=False):
    """
    Generic gaincal task. Used for KCROSS calibration for the rldelay.
    """
    if field:
        gaintable, interp, gainfield, _, _ = get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve', field)
    else:
        gainfield = get_gainfields(_inp_params, _ms_metadata, gainfield, 'retrieve')
    if not spwmap:
        spwmap = auxiliary.get_spwmap(_inp_params, _ms_metadata, gaintable)
    if not refant:
        refant = str(_inp_params.refant)
    if MPICLIENT:
        #seems like mpicasa does not recognize os.chdir so I have to use full paths
        ms_name_fp    = _inp_params.workdir + _inp_params.ms_name
        caltable_fp   = _inp_params.workdir + caltable
        gaintable_fp  = [_inp_params.workdir + gt for gt in gaintable]
        if not interp:
            interp = []
        if not gainfield:
            gainfield = []
        gaincal_cmd =   ("""gaincal(vis='{0}',""".format(ms_name_fp)
                         + """caltable='{0}',""".format(caltable_fp)
                         + """field='{0}',""".format(str(field))
                         + """spw='{0}',""".format(str(spw))
                         + """intent='{0}',""".format('')
                         + """selectdata={0},""".format('True')
                         + """timerange='{0}',""".format('')
                         + """antenna='{0}',""".format(str(antenna))
                         + """scan='{0}',""".format(str(scan))
                         + """observation='{0}',""".format('')
                         + """msselect='{0}',""".format('')
                         + """solint='{0}',""".format(str(solint))
                         + """combine='{0}',""".format(str(combine))
                         + """preavg={0},""".format('-1.0')
                         + """refant='{0}',""".format(str(refant))
                         + """refantmode='{0}',""".format(str(refantmode))
                         + """minblperant={0},""".format(str(minblperant))
                         + """minsnr={0},""".format(str(minsnr))
                         + """solnorm={0},""".format(str(solnorm))
                         + """gaintype='{0}',""".format(str(gaintype))
                         + """smodel={0},""".format(str(smodel))
                         + """solmode='{0}',""".format(str(solmode))
                         + """rmsthresh={0},""".format(str(rmsthresh))
                         + """calmode='{0}',""".format(str(calmode))
                         + """append={0},""".format(str(append))
                         + """splinetime={0},""".format('3600.0')
                         + """npointaver={0},""".format('3')
                         + """phasewrap={0},""".format('180.0')
                         + """docallib={0},""".format('False')
                         + """callib='{0}',""".format('')
                         + """gaintable={0},""".format(str(gaintable_fp))
                         + """gainfield={0},""".format(str(gainfield))
                         + """interp={0},""".format(str(interp))
                         + """spwmap={0},""".format(str(spwmap))
                         + """corrdepflags={0},""".format('True')
                         + """parang={0}""".format(str(_inp_params.parang))
                         + """)"""
                        )
        return MPICLIENT.push_command_request(gaincal_cmd)
    else:
        tasks.gaincal(vis                = _inp_params.ms_name,
                      caltable           =  caltable,
                      field              =  field,
                      spw                =  spw,
                      intent             =  "",
                      selectdata         =  True,
                      timerange          =  "",
                      uvrange            =  "",
                      antenna            =  antenna,
                      scan               =  scan,
                      observation        =  "",
                      msselect           =  "",
                      solint             =  solint,
                      combine            =  combine,
                      preavg             =  -1.0,
                      refant             =  refant,
                      refantmode         =  refantmode,
                      minblperant        =  minblperant,
                      minsnr             =  minsnr,
                      solnorm            =  solnorm,
                      gaintype           =  gaintype,
                      smodel             =  smodel,
                      solmode            =  solmode,
                      rmsthresh          =  rmsthresh,
                      calmode            =  calmode,
                      append             =  append,
                      splinetime         =  3600.0,
                      npointaver         =  3,
                      phasewrap          =  180.0,
                      docallib           =  False,
                      callib             =  "",
                      gaintable          =  gaintable,
                      gainfield          =  gainfield,
                      interp             =  interp,
                      spwmap             =  spwmap,
                      corrdepflags       =  True,
                      parang             =  _inp_params.parang
                     )
        return True


def task_applycal_general(_inp_params, _ms_metadata, apply_calib_tables, apply_calib_interp, apply_calib_weight,
                          apply_calib_gainfd =[], field='', scan='', antenna=''):
    """
    Genereic applycal task. Called once or twice by task_applycal() depending if phase-referencing is being done.
    """
    spwmap = auxiliary.get_spwmap(_inp_params, _ms_metadata, apply_calib_tables)
    apply_calib_weight_boolf = [bool(acw) for acw in apply_calib_weight]
    tasks.applycal(vis         = _inp_params.ms_name,
                   field       = field,
                   spw         = '',
                   intent      = '',
                   selectdata  = True,
                   timerange   = '',
                   uvrange     = '',
                   antenna     = antenna,
                   scan        = scan,
                   observation = '',
                   msselect    = '',
                   docallib    = False,
                   callib      = '',
                   gaintable   = apply_calib_tables,
                   gainfield   = apply_calib_gainfd,
                   interp      = apply_calib_interp,
                   spwmap      = spwmap,
                   calwt       = apply_calib_weight_boolf,
                   parang      = _inp_params.parang,
                   applymode   = _inp_params.applycal_applymode,
                   flagbackup  = True
                  )


def load_models(_inp_params, _ms_metadata):
    """
    Tries to find model-files that match to the observed sources and populates the MODEL_DATA column of the MS with these models.
    Calibration tasks will divide the data by the model when solving for solutions. Can be useful for fringefit() for example.
    """
    print ('\nLoading model data for the observed sources...')
    models = auxiliary.get_modelfiles(_inp_params)
    if models:
        sources       = list(_ms_metadata.selected_scans_dict.keys())
        already_used  = []
        applied_model = False
        for model in models:
            model_source = auxiliary.check_array_for_match(model, sources)
            if not model_source:
                print ('  Ignoring model data from unknown source:\n    ' + model)
            elif not auxiliary.isdir(model):
                print ('  Got a model that does not seem to be in the CASA model table format.\n' \
                       '  Will ignore it. You may need to run importfits in CASA with this file first:\n    ' \
                       + str(model)
                      )
            elif model_source not in already_used:
                if _inp_params.verbose:
                    print ('  Using\n    ' + model + '\n  as model for ' + model_source)
                tasks.delmod(vis            = _inp_params.ms_name,
                             otf            = True,
                             field          = str(model_source),
                             scr            = True
                            )
                tasks.ft(vis                =  _inp_params.ms_name,
                         field              =  str(model_source),
                         spw                =  "",
                         model              =  model,
                         nterms             =  1,
                         reffreq            =  "",
                         complist           =  "",
                         incremental        =  False,
                         usescratch         =  True
                        )
                already_used.append(model_source)
                applied_model = True
            else:
                print ('  Ignoring the model\n    ' + model + '\n  as a model was already loaded for this source!\n' + \
                       '  I recommend to delete all redundant source models in the workdir folder.'
                      )
        if auxiliary.is_set(_inp_params, 'normalize_inputmodel') and applied_model:
            print('  Normalizing the model data...')
            auxiliary.set_const_modelamp(_inp_params.ms_name, 1.0)
    else:
        print ('  No models found. Continuing.')
    print ('Done\n')


def select_src_calib_params(src, calib_tables, calib_interp=[], calib_weight=[]):
    """
    Unused (using CASA gainfield method instead):
    [Multiband fringefit tables are written per source according to auxiliary.src_caltb(caltable, sourcename).
    When general calibration parameters are retrieved, they get the default calibration table names set in array.inp.
    This function searches through calib_tables and checks for each table:
      Does any auxiliary.src_caltb(caltable, sourcename) extensions exist?
        If yes, see if one of the source extensions matches with the input src.
          If that is also True, replace the corresponding generic calib_table with the auxiliary.src_caltb.
          If none of the source extensions matches with src, the corresponding generic calib_table is removed from the
          calib_tables list and also the corresponding list elements in calib_interp and maybe calib_weight.
    Returns the adjusted tables, interp, and weight lists.]
    """
    src_calib_tables = []
    src_calib_interp = []
    src_calib_weight = []
    if not calib_interp:
        calib_interp = [0] * len(calib_tables)
    if not calib_weight:
        calib_weight = [0] * len(calib_tables)
    for cb, ci, cw in zip(calib_tables, calib_interp, calib_weight):
        source_extension_tb = auxiliary.findall_src_caltb(cb)
        if source_extension_tb:
            this_src_tb = auxiliary.src_caltb(cb, src)
            #only keep list values if there is a match
            if this_src_tb in source_extension_tb:
                src_calib_tables.append(this_src_tb)
                src_calib_interp.append(ci)
                src_calib_weight.append(cw)
        else:
            #seems to be an ordinary table that is not source-specific
            src_calib_tables.append(cb)
            src_calib_interp.append(ci)
            src_calib_weight.append(cw)
    return src_calib_tables, src_calib_interp, src_calib_weight


def read_per_station_RLdelays_from_caltables(_inp_params, _ms_metadata):
    """
    Reads RL-delays from calibration tables written by polarization_calibration.task_rldelay() for the rldelay_per_station mode.
    """
    rlfringes = {}
    mytb      = casac.table()
    caltable0 = _inp_params.calib_rldelay[_inp_params.C_NAM]
    for ant in _ms_metadata.antenna_set:
        this_caltable = caltable0 + '.__{0}__'.format(ant)
        if auxiliary.isdir(this_caltable):
            mytb.open(this_caltable)
            rlfringes[ant] = - mytb.getcell('FPARAM', 0)[0][0]
            mytb.done()
            mytb.clearlocks()
    return rlfringes


def ALMA_rldelay_calib(_inp_params, _ms_metadata):
    """
    Unused (using RL fringes from many combined fringes to ALMA instead with a more general method).
    Use RR-LL delays to ALMA to compute RL delays to be used by the manual_caliration.rldelay_per_station function.
    """
    potential_tables_all = ['calib_fringefit_single', 'calib_phase_offsets_ALMA']
    potential_tables_sci = ['calib_fringefit_multi_sci_long'] + potential_tables_all
    potential_tables_cal = ['calib_fringefit_multi_cal_short'] + potential_tables_all
    potential_tables_sci = [getattr(_inp_params, calib_param)[_inp_params.C_NAM] for calib_param in potential_tables_sci]
    potential_tables_cal = [getattr(_inp_params, calib_param)[_inp_params.C_NAM] for calib_param in potential_tables_cal]
    useful_tables_sci    = []
    useful_tables_cal    = []
    for pt in potential_tables_sci:
        if auxiliary.isdir(pt):
            useful_tables_sci.append(pt)
    if not useful_tables_sci:
        raise IOError('Cannot find any fringe-fit calibration tables for the AA RLdelay calibration for science targets.')
    for pt in potential_tables_cal:
        if auxiliary.isdir(pt):
            useful_tables_cal.append(pt)
    if not useful_tables_cal:
        raise IOError('Cannot find any fringe-fit calibration tables for the AA RLdelay calibration for calibrator sources.')
    ALMAid           = int(_ms_metadata.yield_antID('AA'))
    rminl_delays_sci = extract_p1_minus_p2_fringes_from_caltb_per_scan(_inp_params, useful_tables_sci, 'delay', ALMAid)
    rminl_delays_cal = extract_p1_minus_p2_fringes_from_caltb_per_scan(_inp_params, useful_tables_cal, 'delay', ALMAid)
    if _inp_params.verbose:
        print('The following R-L delays have been found for sci and cal respectively:')
        print(rminl_delays_sci)
        print(rminl_delays_cal)
    return rminl_delays_sci, rminl_delays_cal
