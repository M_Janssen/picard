#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
""" Exhaustive refant search for fringefit. """
import copy
import numpy as np
from collections import defaultdict
import pipe_modules.auxiliary as auxiliary
from pipe_modules.default_casa_imports import *


def extract_subcluster(subclist_dict, source, scan):
    """
    Handle case of subclist_dict=False.
    """
    if not subclist_dict:
        return False
    else:
        return subclist_dict[source][scan]


def get_detections(_inp_params, _ms_metadata, _calibrationtable, _scan):
    """
    Takes a _calibrationtable and reports all SNRs and detections for all baselines to the reference station
    for a specific _scan in a dictionary.
    Only one solution per antenna and _scan may be present ('inf' solint).
    """
    auxiliary.check_if_table_is_written(_calibrationtable)
    qcrit      = 'ANTENNA1!=ANTENNA2 && SCAN_NUMBER=='+str(_scan)
    stations   = auxiliary.read_CASA_table(_inp_params, 'unique ANTENNA1', qcrit, _tablename = _calibrationtable)
    detections = defaultdict(dict)
    for st in stations:
        snr, flag, fringes = auxiliary.read_CASA_table(_inp_params, 'SNR,FLAG,FPARAM', 'ANTENNA1=='+str(st),
                                                       _tablename = _calibrationtable
                                                      )
        #Fill:
        st_name                       = _ms_metadata.yield_antname(st)
        detections[st_name]['snr']    = [snr[_inp_params.F_0PHAS][0][0], snr[_inp_params.F_1PHAS][0][0]]
        detections[st_name]['flag']   = [int(flag[_inp_params.F_0PHAS][0][0]), int(flag[_inp_params.F_1PHAS][0][0])]
        detections[st_name]['fringe'] = [(fringes[_inp_params.F_0DELA][0][0],fringes[_inp_params.F_0RATE][0][0]),
                                         (fringes[_inp_params.F_1DELA][0][0],fringes[_inp_params.F_1RATE][0][0])
                                        ]
    return detections


def extract_detections_per_station(detections, weak_detect_sn_thresh, only_strong_detections=False):
    """
    Takes a detections dict determined by the get_detections() function and extracts two lists with:
      - All stations that were detected for either polarization.
      - All stations that were not detected (flagged) for both polarizations, while still having
        SNR>0 (station is at least present).
      - List S/N of weak fringes   in special_detect if only_strong_detections==False.
        List S/N of strong fringes in special_detect if only_strong_detections==True.
    """
    detect         = []
    not_detect     = []
    special_detect = {}
    for stname in detections:
        if all(detections[stname]['flag']):
            if any(detections[stname]['snr']):
                not_detect.append(stname)
            else:
                pass
        else:
            this_sn = np.mean((detections[stname]['snr']))
            if only_strong_detections and this_sn > weak_detect_sn_thresh:
                detect.append(stname)
                special_detect[stname] = this_sn
            elif not only_strong_detections:
                detect.append(stname)
                if this_sn < weak_detect_sn_thresh:
                    special_detect[stname] = this_sn
    return detect, not_detect, special_detect


def connect_subcluster_reref(_ms_metadata, cluster_path, main_phase_caltb, fringe_table_info_raw, reref_these,
                             different_primary_refant=False):
    """
    Input:
      - cluster_path: List of re-referencing stations IDs.
      - main_phase_caltb: Fringe table with cluster_path[0] as reference station.
      - fringe_table_info: Dictionary with caltb info from read_caltb_into_memory() for stations in cluster_path.
      - reref_these: Stations in fringe_table_info that are to be re-referenced to main_phase_caltb.
    Process:
      Write re-referenced solutions for the reref_these stations into main_phase_caltb. Use phase,delay,rate relations from the
      cluster_path; from cluster_path[-1] to cluster_path[0].
    """
    _debug = False
    if len(cluster_path)<2-int(different_primary_refant):
        return
    fringe_table_info = copy.deepcopy(fringe_table_info_raw)
    mytb              = casac.table()
    reref_chain       = defaultdict(lambda: defaultdict(dict))
    #Build the re-referencing chain of fringes.
    if different_primary_refant:
        cp_iter = cluster_path
    else:
        cp_iter = cluster_path[:-1]
    for i,ref in enumerate(cp_iter):
        #Get relation from current refant to next.
        ii = i + 1 - int(different_primary_refant)
        this_relation = fringe_relation_to_refant(_ms_metadata, fringe_table_info[ref], cluster_path[ii])
        for time in this_relation:
            for spectral_window_id in this_relation[time]:
                rtime = round(float(time), 2)
                try:
                    #All True flags are added to the chain.
                    reref_chain[rtime][spectral_window_id]['FLAG']   += this_relation[time][spectral_window_id]['FLAG']
                    reref_chain[rtime][spectral_window_id]['FPARAM'] += this_relation[time][spectral_window_id]['FPARAM']
                except KeyError:
                    reref_chain[rtime][spectral_window_id]['FLAG']   = this_relation[time][spectral_window_id]['FLAG']
                    reref_chain[rtime][spectral_window_id]['FPARAM'] = this_relation[time][spectral_window_id]['FPARAM']
    if _debug:
        print ('re-referencing chain:')
        print (reref_chain)
    #Write re-referenced solutions into the main table.
    end_of_chain = fringe_table_info[cluster_path[-1]]
    mytb.open(main_phase_caltb, nomodify=False)
    for row in end_of_chain:
        thisant = end_of_chain[row]['ANTENNA1']
        if thisant in reref_these:
            try:
                thisspw      = end_of_chain[row]['SPECTRAL_WINDOW_ID']
                thissnr      = end_of_chain[row]['SNR']
                thistime     = end_of_chain[row]['TIME']
                thisrtime    = round(float(thistime), 2)
                flag_chain   = end_of_chain[row]['FLAG'] + reref_chain[thisrtime][thisspw]['FLAG']
                fringe_chain = end_of_chain[row]['FPARAM'] + reref_chain[thisrtime][thisspw]['FPARAM']
                mytb.putcell('SNR', row, thissnr)
                mytb.putcell('FLAG', row, flag_chain)
                mytb.putcell('FPARAM', row, fringe_chain)
            except KeyError:
                #Full chain could not be established.
                pass
    mytb.flush()
    mytb.done()
    mytb.clearlocks()


def read_caltb_into_memory(_calibrationtable, params='TIME,SPECTRAL_WINDOW_ID,ANTENNA1,FPARAM,FLAG,SNR'):
    """
    Returns a dictionary with values for each [row][value in params].
    """
    if not auxiliary.isdir(_calibrationtable):
        return None
    mytb       = casac.table()
    mem_caltb  = defaultdict(dict)
    listparams = params.replace(' ','').split(',')
    mytb.open(_calibrationtable)
    nrows = range(mytb.nrows())
    for _row in nrows:
        for param in listparams:
            thiscell = mytb.getcell(param, _row)
            if param=='TIME':
                thiscell = str(thiscell)
            mem_caltb[_row][param] = thiscell
    mytb.done()
    mytb.clearlocks()
    return mem_caltb


def fringe_relation_to_refant(_ms_metadata, _phase_caltb_mem, rel_ant):
    """
    Returns a dict with the phase,delay,rate relations for pol1,pol2 for the refant of
    _phase_caltb_mem to rel_ant in _phase_caltb_mem.
    _phase_caltb_mem contains the content of a fringe table as dict from read_caltb_into_memory().
    The dict will have the FPARAM and FLAG cells as values for [TIME][SPECTRAL_WINDOW_ID] keys.
    """
    try:
        antID = int(rel_ant)
    except ValueError:
        antID = _ms_metadata.yield_antID(rel_ant)
    fringe_rel = defaultdict(lambda: defaultdict(dict))
    for row in _phase_caltb_mem:
        if _phase_caltb_mem[row]['ANTENNA1'] == rel_ant:
            thistime                                = _phase_caltb_mem[row]['TIME']
            thisspw                                 = _phase_caltb_mem[row]['SPECTRAL_WINDOW_ID']
            thisflag                                = _phase_caltb_mem[row]['FLAG']
            thisfparam                              = _phase_caltb_mem[row]['FPARAM']
            fringe_rel[thistime][thisspw]['FLAG']   = thisflag
            fringe_rel[thistime][thisspw]['FPARAM'] = thisfparam
    return fringe_rel


def find_shortest_path(prioritized_list, path_start, paths, max_len_allowed):
    """
    Generalized function to connect sub-clusters via reference stations for a global fringe-fit (see documentation).
    Input:
      - prioritized_list: List of names in order of priority for paths.
      - path_start: Which antenna in the prioritized_list to take as the reference.
      - paths: dict with [name][<'detect' or 'connect'>].
      - max_len_allowed: No paths longer than this are included. Ignored if <0.
    Output:
      - A single dictionary with keys: [shortest paths]=['station1', 'station2', ...]
                           and values: [detections in paths]=['station1', 'station2', ...].
    """
    all_paths      = explore_all_paths(path_start, paths)
    shortest_paths = {}
    len_paths      = {}
    weight_paths   = {}
    #Get shortest paths to each station.
    for path in all_paths:
        for name in path[1]:
            these_connections = path[0]
            thislen           = len(these_connections)
            try:
                oldlen    = len_paths[name]
                oldweight = weight_paths[name]
            except KeyError:
                #First entry for name.
                oldlen    = float("inf")
                oldweight = float("inf")
            if (thislen<max_len_allowed or max_len_allowed<0):
                #Weight based on priority.
                thisweight = auxiliary.list_position_weights(these_connections, prioritized_list)
                if (thislen < oldlen) or (thislen==oldlen and thisweight<oldweight):
                    len_paths[name]      = thislen
                    weight_paths[name]   = thisweight
                    shortest_paths[name] = path[0]
                else:
                    pass
    common_paths = {}
    #Organize names into common paths.
    for name in shortest_paths:
        thispath = ','.join(shortest_paths[name])
        try:
            common_paths[thispath].append(name)
        except KeyError:
            common_paths[thispath] = [name]
    return common_paths


def explore_all_paths(starting_point, all_paths, max_depth=-1):
    """
    Generalized function that finds all paths to 'detections' via 'connections'.
    This is done via recursion.
    Input:
      - starting_point: Where to start the search (primary reference station).
      - all_paths: [station][<'detect' or 'connect'>] dictionary that contains all possible sub-clusters and connections.
      - max_depth: Maximum allowed recursion depth (-1 to use the large default python limit).
    """
    def recursive_path_explore(current_path, depth):
        """
        Go through all paths via recursion, while keeping track of the previous steps.
        Updates the complete_paths list defined in the parent function.
        """
        detections = all_paths[current_path[-1]]['detect']
        complete_paths.append((current_path, detections))
        if depth != 0:
            for connection in all_paths[current_path[-1]]['connect']:
                if connection not in current_path and connection in all_paths.keys():
                    extended_path = copy.deepcopy(current_path)
                    extended_path.append(connection)
                    recursive_path_explore(extended_path, depth-1)
    if not isinstance(starting_point, list) and not isinstance(starting_point, np.ndarray):
        starting_point = [starting_point]
    complete_paths = []
    recursive_path_explore(starting_point, max_depth)
    return complete_paths
