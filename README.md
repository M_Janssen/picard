﻿README file for the  
Radboud PIpeline for the Calibration of high Angular Resolution Data (rPICARD).  
By Michael Janssen.  
Questions, comments, feature requests, and bug reports should be sent to M.Janssen@astro.ru.nl.  

Please cite https://ui.adsabs.harvard.edu/abs/2019A%26A...626A..75J (https://doi.org/10.1051/0004-6361/201935181) if you are using the code.  
  
Rationale:  
==========  
This pipeline makes use of CASA tasks to calibrate VLBI datasets.  
Presently, data in the form of fits-idi files or MS are supported from the following arrays:  
EHT, GMVA, VLBA, EVN. Requests to add more arrays are always welcome.  
And, the following observation types are currently supported:  
Continuum, polarization, phase-referencing.  
An overview of the pipeline workflow is given in documentation/picard_documentation.pdf.  
The documentation for CASA can be found at https://casa.nrao.edu/casadocs .  
  
Requirements:  
=============  
When using Docker:  
None. It will also run under macOS and Windows.  
  
For a manual installation:  
A Linux distribution which can run CASA is required (CASA is shipped as a binary tarball, compiled  
under Red Hat Linux - it should run under all standard Linux distributions).  
Unfortunately, the pipeline does not work together with CASA on macOS yet, because the mpicasa  
functionality from the Linux CASA distribution is required. But Docker can be used (see below).  
  
Installation:  
=============  
If you want to be system independent and have Docker installed, you can get the latest rPICARD  
image with:  
$ docker pull mjanssen2308/casavlbi:latest  
  
To do a manual installation:  
1)Obtain the correct CASA version for the pipeline:  
$ wget ftp://ftp.mpifr-bonn.mpg.de/outgoing/mjanssen/casa-CAS-13781-5-py3.6.tar.xz  
$ tar xvJf casa-CAS-13781-5-py3.6.tar.xz  
2)Git clone the repository (do not download as tarball because the .git folder is needed):  
$ git clone https://bitbucket.org/M_Janssen/picard  
3)Link the pipeline to the CASA installation:  
$ ./picard/setup.py -p casa-CAS-13781-5-py3.6  
The script will walk you through the automatic installation.  
4)Add picard to your path:  
$ printf '\nexport PATH=$PATH:'"$(pwd)"'/picard/picard\n' >> ~/.bashrc  
5)Add picard to your PYTHONPATH:  
$ printf '\nexport PYTHONPATH=$PYTHONPATH:'"$(pwd)"'/picard/picard\n' >> ~/.bashrc  
$ source ~/.bashrc  
6)Install https://github.com/haavee/jiveplot and/or [Singularity](https://docs.sylabs.io/guides/3.0/user-guide/installation.html)  
and add it to your path for much better and quicker plotting (optional but strongly recommended).  
  
Note that it is not necessary for the pipeline to have CASA in your PATH and that you can have  
multiple CASA versions on your system at the same time.  
  
How to run the pipeline:  
========================  
To run the pipeline, type:  
$ picard [-p,--input <dir>] [-f] [-m] [-l (e)] [-d] [-r (a)] [-s] [-q (<list>)] [-n <#cpu>] [-i] [-h]  
The command line arguments are explained in documentation/picard_documentation.pdf,  
'Command Line Arguments' section.  
The calibration type/mode, the array to calibrate, and the path to the raw data are all handled  
by the input files (see documentation/picard_documentation.pdf, 'Input Parameters' section).  
  
With Docker you can do something like  
$ docker run --name picard.cont -it --init --network=host -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY=$DISPLAY -v /scratch/dataset:/data mjanssen2308/casavlbi:latest  
Or (if there are problems with the user ID, forbidding write access to /data in the container)  
$ docker run --name picard.cont -it --init --env HOME=/data --user $(id -u) -v /etc/passwd:/etc/passwd --network=host -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY=$DISPLAY -v /scratch/dataset:/data mjanssen2308/casavlbi:latest  
You are now within a container from which you can run picard. In the above example, the local  
/scratch/dataset folder has been shared with the /data folder of the container - this should be  
your working directory for input files, the data, and diagnostic output from the pipeline.  
Note: If you are using links to the shared workdir, then the original directories must be shared too.  
  
Note that Docker can be run in root-less mode since version 19.03. Also, Singularity can be used  
instead of Docker with  
$singularity build casavlbi.pipe docker://mjanssen2308/casavlbi:latest  
$singularity run ./casavlbi.pipe  
Here, the present working directory will be shared. Other directories can be shared with -B.  
The --cleanenv argument should be used if rPICARD is already installed on the host machine.  
  
Instructions for a test run are outlined in documentation/picard_documentation.pdf,  
'Quick Start Guide' section.  
  
Versioning:  
===========  
Use $ git pull  
to get code updates.  
Version numbers (git tags) will follow the scheme MAJOR.MINOR.PATCH.  
Major versions go along with updates that are not backwards compatible:  
 - If the code requires a new CASA version  
   (you will pull changes of README.md).  
 - Or if the set of required input (*.inp) files changes  
   (you will pull changes of the input_template/*.inp files).  
Minor versions go along with added functionalities which are backwards compatible:  
 - Added new modules.  
 - Code improvements and updates.  
Patches are backwards compatible bug fixes.  
  
Acknowledgements:  
=================  
This work is supported by the ERC Synergy Grant  
"BlackHoleCam: Imaging the Event Horizon of Black Holes" (Grant 610058).  
